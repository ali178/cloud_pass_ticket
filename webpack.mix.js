let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/admin/theme/assets/plugins/pace/pace-theme-flash.css',
    'resources/assets/admin/theme/assets/plugins/bootstrapv3/css/bootstrap.min.css',
    'resources/assets/admin/theme/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css',
    // 'resources/assets/admin/theme/assets/plugins/font-awesome/css/font-awesome.css',
    'node_modules/font-awesome/css/font-awesome.css',
    'resources/assets/admin/theme/material_icons.css',
    'resources/assets/admin/theme/webarch/css/webarch.css',
    'resources/assets/admin/data-tables/datatables.css',
    'resources/assets/admin/theme/assets/plugins/jquery-notifications/css/messenger.css',
    'resources/assets/admin/theme/assets/plugins/jquery-notifications/css/messenger-theme-flat.css',
   // 'node_modules/select2/dist/css/select2.css',
  // 'node_modules/blueimp-gallery/css/blueimp-gallery.min.css',
    'resources/assets/admin/custom/bootstrap-fileupload.css',
    'resources/assets/admin/custom/imageareaselect.css',
    'resources/assets/admin/clockpicker/dist/bootstrap-clockpicker.min.css',
    'resources/assets/admin/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    'bower_components/pick-a-color/build/1.2.3/css/pick-a-color-1.2.3.min.css',
    'resources/assets/admin/custom/custom.css'
], 'public/css/admin.css').version();

mix.styles([
    'resources/assets/admin/theme/assets/plugins/bootstrapv3/css/bootstrap.min.css',
    'resources/assets/admin/theme/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css',
    'node_modules/font-awesome/css/font-awesome.css',
    'resources/assets/admin/select2/dist/css/select2.css',
    'resources/assets/admin/select2/dist/css/select2-bootstrap.css',
    'resources/assets/admin/custom/site_custom.css',
], 'public/css/site.css').version();

mix.copy([
    'node_modules/font-awesome/fonts/',
], 'public/fonts');

mix.scripts([
    'resources/assets/admin/theme/assets/plugins/jquery/jquery-1.11.3.min.js',
    'resources/assets/admin/theme/assets/plugins/bootstrapv3/js/bootstrap.min.js',
    'resources/assets/admin/theme/assets/plugins/pace/pace.min.js',
    'resources/assets/admin/theme/assets/plugins/jquery-block-ui/jqueryblockui.min.js',
    'resources/assets/admin/theme/assets/plugins/jquery-validation/js/jquery.validate.min.js',
    'resources/assets/admin/theme/webarch/js/webarch.js',
    'resources/assets/admin/data-tables/jquery-datatable/js/jquery.dataTables1.10.16.min.js',
    'resources/assets/admin/theme/assets/plugins/jquery-notifications/js/messenger.min.js',
    'resources/assets/admin/theme/assets/plugins/jquery-notifications/js/messenger-theme-future.js',
   // 'node_modules/select2/dist/js/select2.full.js',
   //'node_modules/blueimp-gallery/js/jquery.blueimp-gallery.min.js',

    'resources/assets/admin/custom/bootstrap-fileupload.js',
    'resources/assets/admin/custom/jquery.imgareaselect.pack.js',

    'resources/assets/admin/clockpicker/dist/bootstrap-clockpicker.min.js',
    'resources/assets/admin/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    
    'bower_components/pick-a-color/build/dependencies/tinycolor-0.9.15.min.js',
    'bower_components/pick-a-color/build/1.2.3/js/pick-a-color-1.2.3.min.js',
    'resources/assets/admin/select2/dist/js/select2.js',
    'resources/assets/admin/custom/custom.js'
], 'public/js/admin.js').version();