<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');

            $table->string('logo_image')->nullable();
            $table->string('cover_image')->nullable();

            $table->string('title_english')->nullable();
            $table->string('title_arabic')->nullable();
            $table->integer('tag')->default(0);

            $table->string('venue_english')->nullable();
            $table->string('venue_arabic')->nullable();

            $table->string('from_date_english')->nullable();
            $table->string('to_date_english')->nullable();
            $table->string('from_time_english')->nullable();
            $table->string('to_time_english')->nullable();
            $table->string('from_date_arabic')->nullable();
            $table->string('to_date_arabic')->nullable();
            $table->string('from_time_arabic')->nullable();
            $table->string('to_time_arabic')->nullable();
            $table->integer('is_another_dt')->default(0);
            $table->string('another_date')->nullable();
            $table->string('another_time')->nullable();

            $table->longText('description_english')->nullable();
            $table->longText('description_arabic')->nullable();

            $table->longText('tc_english')->nullable();
            $table->longText('tc_arabic')->nullable();

            $table->string('twitter_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('url_link')->nullable();
            $table->string('snapchat_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('url_link2')->nullable();

            // map related things
            $table->string('map_address')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('map_lataddress')->nullable();
            
            $table->integer('status')->default(0);
            $table->integer('is_draft')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
