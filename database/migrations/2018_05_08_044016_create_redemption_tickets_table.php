<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedemptionTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redemption_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->integer('issued_ticket_id')->unsigned();
            $table->integer('issued_ticket_detail_id')->unsigned();
            $table->integer('tier_id')->unsigned();
            $table->string('ticket_no');
            $table->enum('redeem_status',array(0, 1))->default(1); // 1 for open, 0 for close
            $table->integer('redemption_from')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redemption_tickets');
    }
}
