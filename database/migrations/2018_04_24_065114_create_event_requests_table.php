<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->integer('total_qty');
            $table->float('total_price',8,2);
            $table->string('pre_request_no');
            $table->string('post_request_no');
            $table->string('complete_request_no');
            $table->integer('event_request_status_id')->unsigned();
            $table->integer('request_from')->default(1);
            $table->integer('setting_id');
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_requests');
    }
}
