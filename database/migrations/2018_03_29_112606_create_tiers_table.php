<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->default(0);
            $table->string('title_english')->nullable();
            $table->string('title_arabic')->nullable();
            $table->longText('description_english')->nullable();
            $table->longText('description_arabic')->nullable();
            $table->string('header_color')->nullable();
            $table->integer('price_type')->default(0);
            $table->integer('total_quantity_type')->default(0);
            $table->float('price', 8, 2);
            $table->bigInteger('total_quantity')->default(0);

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiers');
    }
}
