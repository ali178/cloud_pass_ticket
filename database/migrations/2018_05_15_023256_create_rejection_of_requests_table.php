<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectionOfRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rejection_of_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reason_id')->unsigned();
            $table->integer('event_request_id')->unsigned();
            $table->integer('event_request_status_id')->unsigned();
            $table->string('description_arabic')->nullable();
            $table->string('description_english')->nullable();
            $table->enum('from',array(1,2))->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rejection_of_requests');
    }
}
