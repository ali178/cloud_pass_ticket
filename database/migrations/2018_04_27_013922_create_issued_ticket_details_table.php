<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedTicketDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_ticket_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('issued_ticket_id')->unsigned();
            $table->integer('tier_id')->unsigned();
            $table->integer('total_qty');
            $table->double('price',8,2);
            $table->integer('setting_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_ticket_details');
    }
}
