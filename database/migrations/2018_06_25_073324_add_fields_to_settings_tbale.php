<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSettingsTbale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('phone')->nullable();
            $table->string('email')->nullable();

            $table->string('copyright_english')->nullable();
            $table->string('copyright_arabic')->nullable();

            $table->dropColumn('title_english');
            $table->dropColumn('title_arabic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('email');

            $table->dropColumn('copyright_english');
            $table->dropColumn('copyright_arabic');

            $table->string('title_english');
            $table->string('title_arabic');
        });
    }
}
