<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_request_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->enum('price_type',array(1, 2))->default(1);
            $table->enum('tag_as_vip',array(0, 1))->default(0);
            $table->integer('total_qty');
            $table->float('total_price',8,2);
            $table->string('pre_ticket_no');
            $table->string('post_ticket_no');
            $table->string('complete_ticket_no');
            $table->integer('event_request_status_id')->unsigned();
            $table->integer('ticket_from')->default(1);
            $table->integer('setting_id');
           // $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_tickets');
    }
}
