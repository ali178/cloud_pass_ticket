<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVatInTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issued_tickets', function (Blueprint $table) {
            $table->integer('vat')->default(0);
        });

        Schema::table('issued_ticket_details', function (Blueprint $table) {
            $table->integer('vat')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issued_tickets', function (Blueprint $table) {
            $table->dropColumn('vat');
        });

        Schema::table('issued_ticket_details', function (Blueprint $table) {
            $table->dropColumn('vat');
        });
    }
}
