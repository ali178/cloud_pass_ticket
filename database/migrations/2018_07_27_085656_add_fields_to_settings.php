<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('beneficiary_name_english')->nullable();
            $table->string('beneficiary_name_arabic')->nullable();

            $table->string('bank_name_english')->nullable();
            $table->string('bank_name_arabic')->nullable();

            $table->string('account')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('beneficiary_name_english');
            $table->dropColumn('beneficiary_name_arabic');

            $table->dropColumn('bank_name_english');
            $table->dropColumn('bank_name_arabic');

            $table->dropColumn('account');
        });
    }
}
