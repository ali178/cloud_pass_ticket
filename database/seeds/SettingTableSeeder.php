<?php

use Illuminate\Database\Seeder;
use  Ticketing\Models\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = [
            [
                'title_english'=>'Vat',
                'title_arabic'=>'برميل',
                'vat_value'=>'5'
            ]
        ];

        foreach ($setting as $key => $value) {
            Setting::create($value);
        }
    }
}
