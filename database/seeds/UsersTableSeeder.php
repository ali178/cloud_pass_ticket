<?php

use Illuminate\Database\Seeder;
use Ticketing\User;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Models\Role;
use Ticketing\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = User::where('email','superadmin@gmail.com')->first();
        if (!$super_admin) {
            $super_admin = User::updateOrCreate([
            // 'first_name' => 'Super',
            // 'last_name' => 'Admin',
            'name' => 'superadmin',
            'username' => 'superadmin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('secret'),
            'type'=>0,
            'status'=>1
            // 'profile_picture' => 'no-picture',
            // 'status' => 1,
            // 'user_type' => 'User',
            ]);
            $super_admin->save();
        }
        
        $role = Role::where('name','superadmin')->first();
        if (!$role) {
            $role = Role::updateOrCreate([
                'name' => 'superadmin',
                'display_name' => 'Super Admin',
            ]);
            $role->save();
        }
        $super_admin->roles()->sync([$role->id]);

        $permissions=Controller::permissionsArray();
        $pids = [];
        foreach ($permissions as $module => $actions) {
            foreach ($actions as $key => $value) {
                $permission = Permission::firstOrNew(['name'=>$key]);
                $permission->name = $key;
                $permission->display_name = $value;
                $permission->save();
                $pids[] = $permission->id;
            }
        }
        $role->perms()->sync($pids);
    }
}
