<?php

use Illuminate\Database\Seeder;
use Ticketing\Models\Event_request_status;

class EventRequestStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventRequestSatus = [
            [
                'title_english'=>'Ordered',
                'title_arabic'=>'طلب مسبق'
            ],
            [
                'title_english'=>'Pending',
                'title_arabic'=>'قيد الانتظار'
            ],
            [
                'title_english'=>'Issued',
                'title_arabic'=>'القضية'
            ],
            [
                'title_english'=>'Cancelled',
                'title_arabic'=>'ألغيت'
            ],
            [
                'title_english'=>'Refunded',
                'title_arabic'=>'ألغيت'
            ]

        ];

        foreach ($eventRequestSatus as $key => $value) {
            Event_request_status::create($value);
        }
    }
}
