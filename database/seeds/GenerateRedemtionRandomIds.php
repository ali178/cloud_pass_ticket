<?php

use Illuminate\Database\Seeder;
use Ticketing\Models\Redemption_ticket;

class GenerateRedemtionRandomIds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $redemption_tickets= Redemption_ticket::whereNull('random_id_string')->get();
	    
        $count = $redemption_tickets->count();
        echo $count.' Records are ready to processed'. PHP_EOL;
        $processed = 0;

	    foreach ($redemption_tickets as $key => $redemption_ticket) {
	        $random_id_string = Redemption_ticket::genRandomId();
	        $redemption_ticket->update(['random_id_string'=>$random_id_string]);

            $count = $count - 1;
            $processed = $processed + 1;
            echo $processed.' Record processed and '.$count. ' remaining' . PHP_EOL;
	    }
    }
}
