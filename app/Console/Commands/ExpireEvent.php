<?php

namespace Ticketing\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Ticketing\Models\Event;

class ExpireEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExpireEvent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire events which has past end/another date and time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $current_dt = Carbon::now();
        // $events = Event::where('is_draft',0)->where('selling_off',0)->get();
        // foreach ($events as $key => $event) {
        //     $end_dt_type = '';
        //     if($event->is_another_dt)
        //     {
        //         $end_dt = isset($event->another_date) ? $event->another_date.' '.$event->another_time : '';
        //         $end_dt = Carbon::parse($end_dt);
        //         $end_dt_type = 'another_dt';
        //     }else{
        //         $end_dt = $event->to_date_english.' '.$event->to_time_english;
        //         $end_dt = Carbon::parse($end_dt);
        //         $end_dt_type = 'end_dt';
        //     }
        //
        //     if($end_dt->lte($current_dt))
        //     {
        //         $event->selling_off = 1;
        //         $event->save();
        //     }
        // }

        $current_dt = Carbon::now();
        $events = Event::where('is_draft',0)->where('selling_off',0)->get();
        $pass = true;
        foreach ($events as $key => $event) {
            $end_dt_type = '';
            if($event->is_another_dt)
            {
                $end_dt = isset($event->another_date) ? $event->another_date.' '.$event->another_time : '';
                $end_dt_type = 'another_dt';
            }else{
                $end_dt = $event->to_date_english.' '.$event->to_time_english;
                $end_dt_type = 'end_dt';
            }

            try {
              $end_dt = Carbon::parse($end_dt);
            } catch (\Exception $e) {
              $pass = false;
            }

            if($pass && $end_dt->lte($current_dt))
            {
                $event->selling_off = 1;
                $event->save();
            }
        }
    }
}
