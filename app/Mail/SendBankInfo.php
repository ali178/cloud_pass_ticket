<?php

namespace Ticketing\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBankInfo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content=$content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->markdown('emails.admin.sendbankinfo');
        // return $this->markdown('emails.frontend.eventticketconfirmation');
        // return $this->from('iqbalchannar796@gmail.com')->markdown('emails.frontend.eventticketconfirmation');
        return $this->markdown('emails.admin.sendbankinfo')->from('iqbalchannar796@gmail.com','Channar')->with('content',$this->content);
    }
}
