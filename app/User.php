<?php

namespace Ticketing;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    use SoftDeletes { SoftDeletes::restore insteadof EntrustUserTrait; }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status','profile_picture','deleted_at','username','type','assign_all_events'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function get_role(){
        return !empty($this->roles()) ? $this->roles()->first() : $this->roles();
    }

    public static $TYPE = ['Admin'=>0, 'Volunteer'=>1];

    public function volunteer_events()
    {
        return $this->hasMany('Ticketing\Models\VolunteerEvent');
    }
}
