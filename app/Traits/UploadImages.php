<?php

namespace Ticketing\Traits;
use Input,Image,File;
use Illuminate\Support\Facades\Storage;
trait UploadImages
{
    public static function uploadImage($data)
    {
        $image_name = $data['image_name'];
        $image_path = $data['path'];
        $number = $data['number'];
        $mode = $data['mode'];
        $object = $data['object'];
        $input = $data['input'];
        $image_url = 'ignore';

        $storagePath  = env('AWS_URL');
        
        if (Input::file($image_name)) {
            $image = Input::file($image_name);
            $filename = time() .$image_name.'.' . $image->getClientOriginalExtension();

            $path = public_path($image_path . $filename);

            $s3 = Storage::disk('s3');

            if (!file_exists(public_path('uploads/')))
                File::makeDirectory(public_path('uploads/'));
            if (!file_exists(public_path($image_path)))
                File::makeDirectory(public_path($image_path));

            if ($input['logo_w'.$number] > 0 && $input['logo_h'.$number] > 0) {
                $width = $input['logo_w'.$number] == '' ? nil : $input['logo_w'.$number];
                $height = $input['logo_h'.$number] == '' ? nil : $input['logo_h'.$number];
                $x = $input['logo_x'.$number] == '' ? nil : $input['logo_x'.$number];
                $y = $input['logo_y'.$number] == '' ? nil : $input['logo_y'.$number];
                $file = Image::make($image->getRealPath());
                if ($width) {
                    $mode == 'wh' ? $file->crop($width, $height, $x, $y) : $file->crop($height, $width, $x, $y);
                }

                $filePath = $image_path.''.rand(1, 4).'/'. $filename;
                $file->save($path);
                $s3->put($filePath, $file->encoded, 'public');

                
            } else {
                $file = Image::make($image->getRealPath());

                $filePath = $image_path.''.rand(1, 4).'/'. $filename;
                $file->save($path);
                $s3->put($filePath, $file->encoded, 'public');

            }
            $image_url = $storagePath.$filePath;
            File::delete($path);
        }else {
            if(isset($object))
            {
                if(array_key_exists($image_name, $input) && $input[$image_name] == '' && file_exists($object->$image_name)){
                    File::delete($object->$image_name);
                    $image_url = '';
                }    
            }
            
        }
        return $image_url;
    }
}