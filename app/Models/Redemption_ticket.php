<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Redemption_ticket extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    public function tier(){
        return $this->belongsTo('Ticketing\Models\Tier');
    }

    public static function boot()
    {
        parent::boot();

//        self::creating(function($object){
//            $object->event_id_str = Event::genRandomId();
//        });


        self::saving(function($object){

            if($object)
            {
                $changed = $object->isDirty() ? $object->getDirty() : false;
                $changed = empty($changed) ? false : $changed;

                if ($changed) {
                    if(Auth::user())
                    {
                      $object->last_edit_by = Auth::user()->id;
                    }
                }
            }

        });

        self::creating(function($object){
          $object->random_id_string = Redemption_ticket::genRandomId();
        });

    }

    public static function genRandomId()
    {
      $existed_random_ids = Redemption_ticket::select('random_id_string')->where('random_id_string','<>','')->pluck('random_id_string')->toArray();
      return Redemption_ticket::genRandomString($existed_random_ids);
    }

    public static function genRandomString($existed_random_ids)
    {
      $myStr = str_random(8);
      if(in_array($myStr, $existed_random_ids))
      {
        return Redemption_ticket::genRandomString($existed_random_ids);
      }else{
        return $myStr;
      }
    }

}
