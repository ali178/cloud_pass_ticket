<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Event_request extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    public static $status_YN = [1 =>'Public',2 =>'Admin'];

//    protected $fillable = [
//        'customer_id',
//        'event_id',
//        'total_qty',
//        'total_price',
//        'pre_request_no',
//        'post_request_no',
//        'complete_request_no',
//        'event_request_status_id',
//        'request_from',
//        'setting_id',
//        'last_edit_by',
//        'status',
//        'deleted_at',
//
//    ];








    public function event_request_details()
    {
        return $this->hasMany('Ticketing\Models\Event_request_detail');
    }


    public function event_request_status(){
        return $this->belongsTo('Ticketing\Models\Event_request_status');
    }

    public function customer(){
        return $this->belongsTo('Ticketing\Models\Customer');
    }

    public static function getTierColorByTierId($id){
        $tier=Tier::find($id);
        return $tier;
    }

    public static function boot()
    {
        parent::boot();

//        self::creating(function($object){
//            $object->event_id_str = Event::genRandomId();
//        });


        self::updating(function($object){

            if($object)
            {
                $changed = $object->isDirty() ? $object->getDirty() : false;
                $changed = empty($changed) ? false : $changed;

                if ($changed) {
                    $object->last_edit_by = Auth::user()->id;
                }
            }

        });





        Event_request::deleting(function($model)
        {
            foreach ($model->event_request_details as $event_request_detail)
            {
                $event_request_detail->delete();
            }

        });
    }


}
