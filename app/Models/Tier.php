<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ticketing\Models\Event_request_detail;

class Tier extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'title_english',
    	'title_arabic',
    	'description_english',
    	'description_arabic',
    	'header_color',
    	'price',
    	'total_quantity',
        'event_id',
        'sequence',
        'price_type',
        'total_quantity_type',
    	'deleted_at'
    ];

    public function event()
    {
        return $this->belongsTo('Ticketing\Models\Event');
    }

    public function redemption_tickets(){
        return $this->hasMany('Ticketing\Models\Redemption_ticket');
    }

    public function requestedQty(){
        $result='0';
        $total_qty = Event_request_detail::where('tier_id', '=', $this->id)->sum('total_qty');
        if($total_qty){
            $result= $total_qty;
        }
        return $result;

    }
    public function checkFreeAndOpenOrFreeOny(){
        $result='0';
        if($this->price_type==0 && $this->total_quantity_type==0){
            return 'free_and_unliminted_qty';
        }else if($this->price_type==0 && $this->total_quantity_type==1){
            return 'free_only_limited_qty';
        }else if($this->price_type==1 && $this->total_quantity_type==0){
            return 'not_free_unlimited_qty';
        }else if($this->price_type==1 && $this->total_quantity_type==1){
            return 'not_free_limited_qty';
        }
    }

    public function getIssuedTicketCount(){
        $result='0';
        $result = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty')
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_tickets.event_request_status_id','=',3)
            ->where('issued_ticket_details.tier_id','=',$this->id)
            ->sum('issued_ticket_details.total_qty');
            //->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            //->get();
        return $result;
    }

    public function getIssuedTicketStats(){
        $issued_tickets = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty','issued_ticket_details.vat as vat','issued_ticket_details.price as price')
        ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
        ->where('issued_tickets.event_request_status_id','=',3)
        ->where('issued_ticket_details.tier_id','=',$this->id)
        ->get();

        $total_vat = 0;
        $total_price = 0;
        foreach ($issued_tickets as $key => $issued_ticket) {

            $calculated_total = $issued_ticket->total_qty * $issued_ticket->price;
            $calculated_vat = ($issued_ticket->vat/100) * $calculated_total;

            $total_vat = $total_vat + $calculated_vat;
            $total_price = $total_price + ($calculated_total + $calculated_vat);

        }
        return ['total_price_per_tier' => number_format($total_price,2), 'total_vat_per_tier'=> number_format($total_vat,2)];
    }

    public function getIssuedTicketStatsNew(){
        $issued_tickets = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty','issued_ticket_details.vat as vat','issued_ticket_details.price as price')
        ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
        ->where('issued_tickets.event_request_status_id','=',3)
        ->where('issued_tickets.price_type','=',1)
        ->where('issued_ticket_details.tier_id','=',$this->id)
        ->get();

        $total_vat = 0;
        $total_price = 0;
        foreach ($issued_tickets as $key => $issued_ticket) {

            $calculated_total = $issued_ticket->total_qty * $issued_ticket->price;
            $calculated_vat = ($issued_ticket->vat/100) * $calculated_total;

            // $calculated_total =  $calculated_total - $calculated_vat;
            // $calculated_vat=ceil($issued_ticket->vat/100 * $calculated_total);
            // $calculated_total =  $calculated_total + $calculated_vat;

            $total_vat = $total_vat + $calculated_vat;
            $total_price = $total_price + ($calculated_total);

        }
        return ['total_price_per_tier' => number_format($total_price,2), 'total_vat_per_tier'=> number_format($total_vat,2)];
    }


    public function getEventRequestCount(){
        $result='0';
        $result = Event_request::select('event_request_details.total_qty as total_qty')
            ->join('event_request_details', 'event_request_details.event_request_id', '=', 'event_requests.id')
//            ->where('event_requests.event_request_status_id','!=',3)
//            ->where('event_requests.event_request_status_id','!=',4)
            ->where('event_request_details.tier_id','=',$this->id)

            ->sum('event_request_details.total_qty');
        //->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        //->get();
        return $result;
    }

    public function getFreeTicketAsGiftByAdminFromAdminSideCount(){
        $result='0';
        $result = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty')
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_tickets.event_request_status_id','=',3)
            ->where('issued_tickets.price_type','=',2)
            ->where('issued_ticket_details.tier_id','=',$this->id)
            ->sum('issued_ticket_details.total_qty');
        //->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        //->get();
        return $result;
    }

    public function getRedemptionCount(){
        $result='0';
        $total_qty = Redemption_ticket::where('tier_id', '=', $this->id)->where('redeem_status','=','0')->count();
        if($total_qty){
            $result= $total_qty;
           // die;
        }
        return $result;

    }


    public static function getMaxSequence() {
        $sequence = Tier::select('sequence')->order('desc')->first();
        if ($sequence && $sequence->sequence) {
            return $sequence->sequence;
        }
        return 1;
    }

    public static  function getMaxSequenceByIds($ids) {
        $ids = explode(',', $ids);
        $sequence = Tier::whereIn('id',$ids)->orderBy('sequence','desc')->first();
        if ($sequence && $sequence->sequence) {
            return $sequence->sequence;
        }
        return 1;
    }

    // public function totaAvailable()
    // {
    //     return Event_request_detail::select('*')->join('event_requests','event_requests.id','=','event_request_details.event_request_id')->where('event_requests.event_request_status_id','<>',4)->where('event_request_details.tier_id',$this->id)->sum('event_request_details.total_qty');
    // }

    public function totaAvailable()
    {
        return Event_request_detail::select('*')->join('event_requests','event_requests.id','=','event_request_details.event_request_id')->whereIn('event_requests.event_request_status_id', [1, 2])->where('event_request_details.tier_id',$this->id)->sum('event_request_details.total_qty');
    }

    public function exclusive_count()
    {
//        $issued_ticket_details = Issued_ticket_detail::select('*')
//        ->join('issued_tickets', 'issued_tickets.id','=', 'issued_ticket_details.issued_ticket_id')
//        ->where('issued_ticket_details.tier_id',$this->id)
//        ->where('issued_tickets.ticket_from',2)
//        ->where('issued_tickets.event_request_status_id',3)->count();
//        return $issued_ticket_details;
        return 0;
    }
}
