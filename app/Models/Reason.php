<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reason extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    public static $status_options = ['Hide','Show'];

    protected $fillable = [
    	'title_english',
    	'title_arabic',
    	'description_english',
    	'description_arabic',
        'type',
    	'status',
    	'deleted_at'
    ];

    public  static  $type = ['1'=>'Request' , '2' => 'Ticket'];
}