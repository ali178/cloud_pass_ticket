<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['title', 'title_arabic','description','description_arabic','show','identifier'];
}
