<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;

class VolunteerEvent extends Model
{
    protected $fillable = ['user_id','event_id'];

    public function user(){
    	return $this->belongsTo('Ticketing\Models\User');
    }
}
