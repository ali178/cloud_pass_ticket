<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Issued_ticket extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    public static $status_YN = [1 =>'Public',2 =>'Admin'];


//    public function issued_ticket_details()
//    {
//        return $this->hasMany('Ticketing\Models\issued_ticket_detail');
//    }
    public static function boot()
    {
        parent::boot();

//        self::creating(function($object){
//            $object->event_id_str = Event::genRandomId();
//        });


        self::saving(function($object){

            if($object)
            {
                $changed = $object->isDirty() ? $object->getDirty() : false;
                $changed = empty($changed) ? false : $changed;

                if ($changed) {
                    $object->last_edit_by = Auth::user()->id;
                }
            }

        });





        Issued_ticket::deleting(function($model)
        {
            foreach ($model->issued_ticket_details as $issued_ticket_detail)
            {
                $issued_ticket_detail->delete();
            }

        });
    }


    public function issued_ticket_details()
    {
        return $this->hasMany('Ticketing\Models\Issued_ticket_detail');
    }

    public function redemption_tickets()
    {
        return $this->hasMany('Ticketing\Models\Redemption_ticket');
    }

    public function event_request_status(){
        return $this->belongsTo('Ticketing\Models\Event_request_status');
    }

    public function getIndividualTicketOfTier($issued_ticket_id,$tier_id){
           return $redemption_tickets=Redemption_ticket::where('issued_ticket_id','=',$issued_ticket_id)->where('tier_id','=',$tier_id)->get();
    }

    public function customer(){
        return $this->belongsTo('Ticketing\Models\Customer');
    }
    public static function getTierColorByTierId($id){
        $tier=Tier::find($id);
        return $tier;
    }

    // public static function getTotalAdminIssuedSum($event_request_status_id,$eventid,$ticket_from)
    // {
    //     $event_requests = Issued_ticket::where('issued_tickets.event_id','=',$eventid)->where('issued_tickets.ticket_from','=',$ticket_from)->where('event_request_status_id',$event_request_status_id)
    //         ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
    //         ->join('tiers', 'tiers.id', '=', 'issued_ticket_details.tier_id')
    //         ->where('tiers.total_quantity_type', '=', 1)
    //         ->get();
    //     $total_qty = 0;
    //     foreach ($event_requests as $key => $event_request) {
    //         $total_qty = $total_qty + $event_request->total_qty;
    //     }
    //     return $total_qty;
    // }

    public static function getTotalAdminIssuedSum($event_request_status_id,$eventid,$ticket_from)
    {
        $event_requests = Issued_ticket::where('issued_tickets.event_id','=',$eventid)->whereIn('issued_tickets.ticket_from',[1,2])->where('event_request_status_id',$event_request_status_id)
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->join('tiers', 'tiers.id', '=', 'issued_ticket_details.tier_id')
            ->where('tiers.total_quantity_type', '=', 1)
            ->get();
        $total_qty = 0;
        foreach ($event_requests as $key => $event_request) {
            $total_qty = $total_qty + $event_request->total_qty;
        }
        return $total_qty;
    }

    public static function getTotalAdminIssuedSumNew($event_request_status_id,$eventid, $tier_id)
    {
        $event_requests = Issued_ticket::where('issued_tickets.event_id','=',$eventid)->where('event_request_status_id',$event_request_status_id)
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_ticket_details.tier_id', $tier_id)
            ->join('tiers', 'tiers.id', '=', 'issued_ticket_details.tier_id')
            ->where('tiers.total_quantity_type', '=', 1)
            ->get();
        $total_qty = 0;
        foreach ($event_requests as $key => $event_request) {
            $total_qty = $total_qty + $event_request->total_qty;
        }
        return $total_qty;
    }

    public static function getTotalAdminIssuedFree($event_request_status_id,$eventid, $tier_id)
    {
        $event_requests = Issued_ticket::where('issued_tickets.event_id','=',$eventid)->where('event_request_status_id',$event_request_status_id)
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_tickets.price_type','=',2)
            ->where('issued_tickets.ticket_from','=',2)
            ->where('issued_ticket_details.tier_id', $tier_id)
            ->join('tiers', 'tiers.id', '=', 'issued_ticket_details.tier_id')
            ->where('tiers.total_quantity_type', '=', 1)
            ->get();
        $total_qty = 0;
        foreach ($event_requests as $key => $event_request) {
            $total_qty = $total_qty + $event_request->total_qty;
        }
        return $total_qty;
    }
}
