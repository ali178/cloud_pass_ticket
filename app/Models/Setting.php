<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'vat_value',
    	'deleted_at',
    	'phone',
    	'email',
    	'copyright_english',
    	'copyright_arabic',
        'iban',
        'beneficiary_name_english',
        'beneficiary_name_arabic',
        'bank_name_english',
        'bank_name_arabic',
        'account'
    ];
}