<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_event_request_detail extends Model
{
    public function tier()
    {
        return $this->belongsTo('Ticketing\Models\Tier');
    }
}
