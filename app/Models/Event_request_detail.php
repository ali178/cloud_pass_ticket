<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event_request_detail extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function tier()
    {
        return $this->belongsTo('Ticketing\Models\Tier');
    }

    public function event_request(){
        return $this->belongsTo('Ticketing\Models\Event_request');
    }
}
