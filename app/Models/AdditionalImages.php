<?php

namespace Ticketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalImages extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	protected $fillable = [
    	'image',
    	'caption',
    	'sequence',
    	'imageable_id',
    	'imageable_type',
    	'deleted_at'
    ];

    public function imageable(){
        return $this->morphTo();
    }

    public static function getMaxSequence() {
        $sequence = AdditionalImages::select('sequence')->order('desc')->first();
        if ($sequence && $sequence->sequence) {
            return $sequence->sequence;
        }
        return 1;
    }

    public static  function getMaxSequenceByIds($ids) {
        $ids = explode(',', $ids);
        $sequence = AdditionalImages::whereIn('id',$ids)->orderBy('sequence','desc')->first();
        if ($sequence && $sequence->sequence) {
            return $sequence->sequence;
        }
        return 1;
    }
}
