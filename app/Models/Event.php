<?php

namespace Ticketing\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Ticketing\Traits\UploadImages;
use Auth;

class Event extends Model
{
    use SoftDeletes,UploadImages;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public static $Tags = [4 =>'NO TAG', 0 =>'HOT',1 =>'COMING SOON', 2 => 'ENDING SOON', 3 => 'SOLD OUT'];
    public static $status = [0 =>'Unpublished',1 =>'Published'];
    public static $status_YN = [0 =>'No',1 =>'Yes'];
    public static $selling = [0 =>'OFF',1 =>'ON'];

    protected $fillable = [
    	'logo_image',
    	'cover_image',
    	'title_english',
    	'title_arabic',
    	'tag',
    	'venue_english',
    	'venue_arabic',
        'from_date_english',
        'to_date_english',
        'from_time_english',
        'to_time_english',
        'from_date_arabic',
        'to_date_arabic',
        'from_time_arabic',
        'to_time_arabic',
        'is_another_dt',
        'another_date',
        'another_time',
        'description_english',
        'description_arabic',
        'tc_english',
        'tc_arabic',
        'twitter_link',
        'instagram_link',
        'url_link',
        'snapchat_link',
        'facebook_link',
        'mobile',
        'email',
        'url_link2',
        'status',
        'deleted_at',
        'map_address',
        'long',
        'lat',
        'is_draft',
        'event_id_str',
        'last_edit_by',
        'selling_off'
    ];

    public function exclamation_mark()
    {
        $event_id = $this->id;
        $pending_refunds = Issued_ticket::where('event_id',$event_id)->where('event_request_status_id',4)->count();
        return $pending_refunds > 0 ? true : false;
    }
    
    public function additional_images(){
        return $this->morphMany('Ticketing\Models\AdditionalImages','imageable')->orderBy('sequence','desc');
    }

    public function tiers()
    {
        return $this->hasMany('Ticketing\Models\Tier')->orderBy('sequence','desc');
    }
    
    // this is use for creating 6 digits random number while generating new object
    public static function boot()
    {
        parent::boot();
        self::creating(function($object){
            $object->event_id_str = Event::genRandomId();
        });

        self::saving(function($object){

            if($object)
            {   
                $changed = $object->isDirty() ? $object->getDirty() : false;
                $changed = empty($changed) ? false : $changed;
                
                if ($changed) {
                    if(Auth::user())
                    {
                        $object->last_edit_by = Auth::user()->id;
                    }
                }
            }
            
        });
    }

    public static function genRandomId()
    {
      $existed_random_ids = Event::select('event_id_str')->where('event_id_str','<>','')->pluck('event_id_str')->toArray();
      return Event::genRandomString($existed_random_ids);

    }

    public static function eventStatusById($eventid)
    {

     $event=Event::find($eventid);
        $today=date('Y-m-d H:i:s');
     if($event->another_date==''){

          $toDate = date('Y-m-d H:i:s', strtotime("$event->to_date_english $event->to_time_english"));
         if(strtotime($toDate)>strtotime($today)){
            return true;
         }else{
             return false;
         }
     }else{
         $toDate = date('Y-m-d H:i:s', strtotime("$event->another_date $event->another_time"));
         if(strtotime($toDate)>strtotime($today)){
             return true;
         }else{
             return false;
         }

     }

     // return $event->id;
      //  $existed_random_ids = Event::select('event_id_str')->where('event_id_str','<>','')->pluck('event_id_str')->toArray();
     //   return Event::genRandomString($existed_random_ids);

    }


    public static function genRandomString($existed_random_ids)
    {
      $myStr = mt_rand(100000, 999999);
      if(in_array($myStr, $existed_random_ids))
      {
        return Event::genRandomString($existed_random_ids);
      }else{
        return $myStr;
      }
    }


    public function totalQuantyByEventId(){

        $result='0';
       $total_quantity = Tier::where('event_id', '=', $this->id)->where('price_type', '=', '1')->sum('total_quantity');
        if($total_quantity){
           $result= $total_quantity;
       }
        return $result;

    }

    public function totalAmountByEventId(){

        $result='0.00';
        $total_amount = Tier::where('event_id', '=', $this->id)->where('price_type', '=', '1')->sum('price');
        if($total_amount){
            $result= $total_amount;
        }
        return $result;

    }





    public static function genRandomEventRequestNumber()
    {
        $existed_random_ids =Event_request::select('complete_request_no')->where('complete_request_no','<>','')->pluck('complete_request_no')->toArray();
        return Event::genRandomEventRequestNumberString($existed_random_ids);

    }

    public static function genRandomEventRequestNumberString($existed_random_ids)
    {
        $EmyStr = mt_rand(100000, 999999);
        $RmyStr = mt_rand(100000, 999999);
        $complete_request_no='E'.$RmyStr.' - R'.$EmyStr;
        if(in_array($complete_request_no, $existed_random_ids))
        {
            return Event::genRandomEventRequestNumberString($existed_random_ids);
        }else{
            return $complete_request_no;
        }
    }


    public static function genRandomEventRequestNumberIssuedTicket()
    {
        $existed_random_ids =Issued_ticket::select('complete_ticket_no')->where('complete_ticket_no','<>','')->pluck('complete_ticket_no')->toArray();
        return Event::genRandomEventRequestNumberStringIssuedTicket($existed_random_ids);

    }

    public static function genRandomEventRequestNumberStringIssuedTicket($existed_random_ids)
    {
        $EmyStr = mt_rand(100000, 999999);
        $RmyStr = mt_rand(100000, 999999);
        $complete_ticket_no='E'.$RmyStr.'-T'.$EmyStr;
        if(in_array($complete_ticket_no, $existed_random_ids))
        {
            return Event::genRandomEventRequestNumberStringIssuedTicket($existed_random_ids);
        }else{
            return $complete_ticket_no;
        }
    }

    public function getTotalIssuedTicket(){
        $result='0';
        $result = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty')
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_tickets.event_request_status_id','=',3)
            ->where('issued_tickets.event_id','=',$this->id)
            ->sum('issued_ticket_details.total_qty');
        //->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        //->get();
        return $result;
    }

    public static function getTotalIssuedTicket2($event_id){
        $result='0';
        $result = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty')
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_tickets.event_request_status_id','=',3)
            ->where('issued_tickets.event_id','=',$event_id)
            ->sum('issued_ticket_details.total_qty');
        //->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        //->get();
        return $result;
    }

    public function getTotalIssuedTicketAmount(){
        $result='0';
        $result = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty','issued_ticket_details.price as price')
            ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
            ->where('issued_tickets.event_request_status_id','=',3)
            ->where('issued_tickets.event_id','=',$this->id)
//            ->sum('issued_ticket_details.price');
            ->sum(DB::raw('issued_ticket_details.price * issued_ticket_details.total_qty'));
        //->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        //->get();
        return $result;
    }

    public function getTotalIssuedTicketAmountWithVat(){
        $issued_tickets = Issued_ticket::select('issued_tickets.id','issued_ticket_details.total_qty as total_qty','issued_ticket_details.vat as vat','issued_ticket_details.price as price')
        ->join('issued_ticket_details', 'issued_ticket_details.issued_ticket_id', '=', 'issued_tickets.id')
        ->where('issued_tickets.event_request_status_id','=',3)
        ->where('issued_tickets.price_type','=',1)
        ->where('issued_tickets.event_id','=',$this->id)
        ->get();

        $total_vat = 0;
        $total_price = 0;
        foreach ($issued_tickets as $key => $issued_ticket) {

            $calculated_total = $issued_ticket->total_qty * $issued_ticket->price;
            $calculated_vat = ($issued_ticket->vat/100) * $calculated_total;

            $calculated_total =  $calculated_total - $calculated_vat;
            $calculated_vat=ceil($issued_ticket->vat/100 * $calculated_total);
            $calculated_total =  $calculated_total + $calculated_vat;

            $total_vat = $total_vat + $calculated_vat;
            $total_price = $total_price + ($calculated_total);

        }
        return ['total_price_per_tier' => number_format($total_price,2), 'total_vat_per_tier'=> number_format($total_vat,2)];
    }

    public function getRedemptionCount(){
        $result='0';
        $total_qty = Redemption_ticket::where('event_id', '=', $this->id)->where('redeem_status','=','0')->count();
        if($total_qty){
            $result= $total_qty;
            // die;
        }
        return $result;

    }

    public static  function getArabicDate(Event $event) {
        $result = explode('/', $event->from_date_arabic);
        return $result;

    }
    public static  function getArabicToDate(Event $event) {
        $result = explode('/', $event->to_date_arabic);

        if(isset($result) && isset($result[1]))
        {
            $result['leading_zero'] = strlen(Event::a2e($result[1])) == 2 ? 'no' : 'yes';
        }
        
        return $result;

    }

    // Localization

    public static function arabic_hours()
    {
        $hours = [];
        // $prepend = array('','01','02','03','04','05','06','07','08','09'); 
        $prepend = array('','1','2','3','4','5','6','7','8','9'); 
        $numbers     = array_merge($prepend,range(10, 12)); 
        foreach ($numbers as $number) {
            $hours[Event::e2a($number)] = Event::e2a($number);
        }
        return $hours;
    }

    public static function arabic_minutes()
    {
        $minutes = [];
        $prepend = array('','00','01','02','03','04','05','06','07','08','09'); 
        $numbers     = array_merge($prepend,range(10, 59)); 

        foreach ($numbers as $number) {
            $minutes[Event::e2a($number)] = Event::e2a($number);
        }
        return $minutes;
    }

    public static function arabic_time_format($event)
    {
        $arabic_times = [];
        if(isset($event['from_time_arabic_minutes']) || isset($event['from_time_arabic_hours']) || isset($event['from_time_arabic_type']))
        {
            // $arabic_times['from'] = ($event['from_time_arabic_minutes'] ? $event['from_time_arabic_minutes'] : '').':'.($event['from_time_arabic_hours'] ? $event['from_time_arabic_hours'] : '').' '.($event['from_time_arabic_type'] ? $event['from_time_arabic_type'] : '');

            $arabic_times['from'] = ($event['from_time_arabic_hours'] ? $event['from_time_arabic_hours'] : '').':'.($event['from_time_arabic_minutes'] ? $event['from_time_arabic_minutes'] : '').' '.($event['from_time_arabic_type'] ? $event['from_time_arabic_type'] : '');

        }else{
            $arabic_times['from'] = '';
        }

        if(isset($event['to_time_arabic_minutes']) || isset($event['to_time_arabic_hours']) || isset($event['to_time_arabic_type']))
        {
            // $arabic_times['to'] = ($event['to_time_arabic_minutes'] ? $event['to_time_arabic_minutes'] : '').':'.($event['to_time_arabic_hours'] ? $event['to_time_arabic_hours'] : '').' '.($event['to_time_arabic_type'] ? $event['to_time_arabic_type'] : '');

            $arabic_times['to'] = ($event['to_time_arabic_hours'] ? $event['to_time_arabic_hours'] : '').':'.($event['to_time_arabic_minutes'] ? $event['to_time_arabic_minutes'] : '').' '.($event['to_time_arabic_type'] ? $event['to_time_arabic_type'] : '');

        }else{
            $arabic_times['to'] = '';
        }

        return $arabic_times;
    }

    public static function getSeperateArabicTime($time)
    {   
        $date = [];
        $timeString2Array = explode(' ', $time);
        $hours_minutes = explode(':', isset($timeString2Array[0]) ? $timeString2Array[0] : '');
        $date['hours'] = isset($hours_minutes[0]) ? $hours_minutes[0] : '';
        $date['minutes'] = isset($hours_minutes[1]) ? $hours_minutes[1] : '';
        $date['type'] = isset($timeString2Array[1]) ? $timeString2Array[1] : '';
        $date['both'] = isset($timeString2Array[0]) ? $timeString2Array[0] : '';
        // dd($date);
        return $date;
    }

    public static function getSeperateArabicTime2($time)
    {   
        $date = [];
        $timeString2Array = explode(' ', $time);
        $hours_minutes = explode(':', isset($timeString2Array[0]) ? $timeString2Array[0] : '');
        $date['hours'] = isset($hours_minutes[0]) ? $hours_minutes[0] : '';
        $date['minutes'] = isset($hours_minutes[1]) ? $hours_minutes[1] : '';
        $date['type'] = isset($timeString2Array[1]) ? $timeString2Array[1] : '';
        $date['both'] = isset($timeString2Array[0]) ? $timeString2Array[0] : '';
        $date['leading_zero'] = Event::addLeadingZero($date['hours']);
        return $date;
    }

    public static function addLeadingZero($hour)
    {   
        $english_hour = (int)Event::a2e($hour);
        $leading_zero = $english_hour < 10 ? 'yes' : 'no';
        return $leading_zero;
    }

    /**
     * Converts numbers in string from western to eastern Arabic numerals.
     *
     * @param  string $str Arbitrary text
     * @return string Text with western Arabic numerals converted into eastern Arabic numerals.
    */
    public static function e2a($str) {
        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic_western, $arabic_eastern, $str);
    }
    /**
     * Converts numbers from eastern to western Arabic numerals.
     *
     * @param  string $str Arbitrary text
     * @return string Text with eastern Arabic numerals converted into western Arabic numerals.
     */
    public static function a2e($str) {
        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic_eastern, $arabic_western, $str);
    }
    /*/ Test 
        echo arabic_w2e("1234567890"); // Outputs: ١٢٣٤٥٦٧٨٩٠
        echo arabic_e2w("١٢٣٤٥٦٧٨٩٠"); // Outputs: 1234567890
    //*/
}