<?php

namespace Ticketing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FrontendTierAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->request->get('submit')=='tier') {

           $irregulardoselist=$this->request->get('medicinedose');
        if (count($irregulardoselist) > 0){
                $count1 = count($irregulardoselist);
                for ($j = 0; $j < $count1; $j++) {
                    $rules['medicinedose.'.$j] = 'required|max:4|dynamicPickList:'.$totalquantity.','.$patientid;
                }
             }
        }else{
            $rules = [

             ];
        }



        return $rules;

    }


    public function messages()
    {

        $messages = [
            'irregulardoselist.required'=> 'The Irregular Dose field is required.',
            'totalquantity.is_totalquantity_equal'=> 'The Total Quantity not match to cycle quantity.',
            'medicinedose.dynamicPickList'=> 'The Total Quantity not match to cycle quantity.'
        ];
        $irregulardoselist=$this->request->get('medicinedose');
        if (count($irregulardoselist) > 0) {
            $count1 = count($irregulardoselist);
            for ($j = 0; $j < $count1; $j++) {
                $messages['medicinedose.' . $j . '.dynamicPickList'] = 'The field labeled "Book Title ' . $j . '" must be less than :max characters.';
            }
        }
        return $messages;



//        return [
//            'irregulardoselist.required'=> 'The Irregular Dose field is required.',
//            'totalquantity.is_totalquantity_equal'=> 'The Total Quantity not match to cycle quantity.',
//            'medicinedose.dynamicPickList'=> 'The Total Quantity not match to cycle quantity.'
//        ];
    }
}
