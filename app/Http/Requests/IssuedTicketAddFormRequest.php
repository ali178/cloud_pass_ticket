<?php

namespace Ticketing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IssuedTicketAddFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $min=1;
        foreach($this->request->get('total_qty') as $key => $val){
           if($val>0){
               $min=0;
           }
        }



        $rules = [
                  'full_name'  => 'required|max:60',
                  'email'     => 'required|email|max:60',
                   'mobile'    => 'required|min:9|max:15',
                 ];

        foreach($this->request->get('total_qty') as $key => $val)
        {
            $rules['total_qty.'.$key] = 'required|numeric|min:'.$min;
        }
        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('total_qty') as $key => $val)
        {
            $messages['total_qty.'.$key.'.min'] = 'One Tier must has Qty greater than 0';
        }
        return $messages;
    }


}
