<?php

namespace Ticketing\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Ticketing\Http\Requests\FrontendTierAddFormRequest;
use Ticketing\Mail\EventTicketConfirmation;
use Ticketing\Models\Customer;
use Ticketing\Models\Event;
use Ticketing\Http\Controllers\PublicController;
use Ticketing\Models\Event_request;
use Ticketing\Models\Event_request_detail;
use Ticketing\Models\Temp_event_request_detail;
use Ticketing\Models\Tier;

class EventController extends PublicController
{
    public function index(Request $request)
    {	
        $events = Event::where('status',1)->where('is_draft',0)->orderBy('id','desc')->get();
        return View('site.events.index',compact('events'));
    }

    public function detail($id)
    {	 
        $event = Event::find($id);
        if($event && $event->status && !$event->is_draft)
        {
            return View('site.events.detail',compact('event'));
        }
        return redirect()->back();
    }

    public function tickets($id)
    {
        $event = Event::find($id);
    	return View('site.events.view_tickets',compact('event'));
    }

    public function tickets_review($id)
    {
        $event = Event::find($id);

        session_start();
        $session_id = session_id();

        $event_request_details=Temp_event_request_detail::where('browser_session_id','=',$session_id)->get();
        return View('site.events.view_tickets_review',compact('event','event_request_details'));
    }




    public function store(Request $request, $eventid)
    {



       if($request->submit=='customer'){
           $review=  $this->validate($request, [
               'full_name'     => 'required|max:200',
               'email'     => 'required|email|max:200',
               'mobile'    => 'required|min:9|max:15',

           ]);


           //           $num=0;
//           foreach (Session::get('tierList') as $product_id){
//               echo $ddd=Session::get('tierList',$num);
//          $num++;
//           }
//           die;

           session_start();
           $session_id = session_id();

           $temp_event_request_details=Temp_event_request_detail::where('browser_session_id','=',$session_id)->get();
            $current_vat = 0;
            $event                      = Event::find($eventid);
            $event_request              = new Event_request();

            $customer                   = new Customer();

            $customer ->full_name =$request->full_name;
            $customer ->email        =$request->email;
            $customer ->mobile    =$request->mobile;
            $customer ->save();

            $genRandomNumberForEventRequest=$event->genRandomEventRequestNumber();


                $number=strstr($genRandomNumberForEventRequest, '-');

               $post_request_no = substr($number, strpos($number, "_ ") + 1);        ;
               $pre_request_no = strstr($genRandomNumberForEventRequest, ' ', true); // As of PHP 5.3.0
               $total_qty=0;
               $total_price=0.00;

               foreach ( $temp_event_request_details as $temp_event_request_detail){

                   $total_qty= $total_qty + $temp_event_request_detail->total_qty;
                   $total_price = $total_price + ($temp_event_request_detail->total_qty * $temp_event_request_detail->price);
                   $current_vat = $temp_event_request_detail->vat;

               }


            $event_request ->customer_id         =$customer ->id;
            $event_request ->event_id            =$eventid;
            $event_request ->total_qty           =$total_qty;
            $event_request ->total_price         =$total_price;
            $event_request ->pre_request_no      =$pre_request_no;
            $event_request ->post_request_no     =$post_request_no;
            $event_request ->complete_request_no =str_replace(' ','',$genRandomNumberForEventRequest);
            $event_request ->event_request_status_id =1;
            $event_request ->request_from            =1;
            $event_request ->setting_id              =1;
            $event_request->vat = $current_vat;
            $event_request ->save();


            foreach ( $temp_event_request_details as $temp_event_request_detail){
                $event_request_detail                   = new Event_request_detail();
                $event_request_detail->event_request_id = $event_request->id;
                $event_request_detail->tier_id = $temp_event_request_detail->tier_id;
                $event_request_detail->total_qty = $temp_event_request_detail->total_qty;
                $event_request_detail->price = $temp_event_request_detail->price;
                $event_request_detail->setting_id = 1;
                $event_request_detail->vat = $current_vat;
                $event_request_detail->save();
            }





       }elseif($request->submit=='tier'){
           session_start();
           $session_id = session_id();
           // print_r($request->all());
           // die;
           $tierlist=$request->tier_id;
           $total_qtylist=$request->total_qty;
            // $arrTier=array();
            // $arrTotalQty=array();
           if (count($tierlist) > 0) {
               $count = count($tierlist);
               $temp_event_request_detail=Temp_event_request_detail::where('browser_session_id','=',$session_id)->delete();
               for ($i = 0; $i < $count; $i++) {
                   if ($total_qtylist[$i] > 0) {
                       $tier = Tier::find($tierlist[$i]);
                       $temp_event_request_detail                     = new Temp_event_request_detail();
                       $temp_event_request_detail->browser_session_id = $session_id;
                       $temp_event_request_detail->tier_id            = $tierlist[$i];
                       $temp_event_request_detail->total_qty          = $total_qtylist[$i];
                       $temp_event_request_detail->price              = $tier->price ;
                       $temp_event_request_detail->vat                = $request->current_vat ;
                       $temp_event_request_detail->save();
                   }
               }
             //  die;
           }

          // Session::push('tierList', collect($arrTier));
          // Session::push('totalQtyList',  collect($arrTotalQty));





          // die;
       }

//        foreach ($event_request as $request) {
//         $event_request_detail                   = new Event_request_detail();
//         $event_request_detail->event_request_id = $event_request->id;
//         $event_request_detail->tier_id = $request->$request->tier_id;
//         $event_request_detail->total_qty = 100;
//         $event_request_detail->price = 8788.99;
//         $event_request_detail->setting_id = 1;
//         $event_request_detail->save();
//     }




        if($request->submit=='customer'){
        Session::flash('success', 'Ticketing has been successfully saved.');
        return redirect()->route('site.events.tickets.review.confirmation',$event_request->id);
        }elseif($request->submit=='tier'){
            return redirect()->route('site.events.tickets.review',$eventid);
        }
    }




    public function confirmation($event_request_id=1)
    {

        session_start();
        $session_id = session_id();

          $event_request= Event_request::find($event_request_id);
        $event = Event::find($event_request->event_id);

       // session_start();
       // $session_id = session_id();

        $event_request_details=Temp_event_request_detail::where('browser_session_id','=',$session_id)->get();



        $content = [
            'title_english'=> $event->title_english,
            'title_arabic'=> $event->title_arabic,
            'logo_image' => $event->logo_image,
            'full_name' => $event_request->customer->full_name,
            'complete_request_no' => $event_request->complete_request_no
        ];


//        $receiverAddress = 'iqbalchannar796@gmail.com';
//        Mail::to($receiverAddress)->send(new Contactus($content));
//         dd('mail send successfully');
//
//
          $email = $event_request->customer->email;
//        $data['email'] = $email;
//        $data['first_name'] =$request->input('first_name');
//        $data['password']=$request->input('password');
//        $data['msg']='Your dropper account is created and you account credentials are.';




            if ($email!='') {
                // Mail::to($email)->send(new EventTicketConfirmation($content));
                Mail::send('emails.frontend.eventticketconfirmation', ['content'=>$content], function ($m) use ($email) {
                  $m->from('no-reply@ticketpass.net', 'TicketPass');
                  $m->to($email)->subject('طلب تذاكر | Tickets Request');
                });
            }
         // $event_request_details = Event_request_detail::where('event_request_id','=',$event_request_id)->get();

       // $temp_event_request_details=Temp_event_request_detail::where('browser_session_id','=',$session_id)->get();
        return View('site.events.view_tickets_confirmation',compact('event', 'event_request','event_request_details'));
    }



    public function eventSearchOnFrontend(Request $request){
        $term = $request->q;
        if ($request->ajax())
        {
            $events = Event::where('status',1)->where('is_draft',0)->where('title_english', 'LIKE',  '%'.$term.'%')->orWhere('title_arabic', 'LIKE',  '%'.$term.'%')->orWhere('description_english', 'LIKE',  '%'.$term.'%')->orWhere('description_arabic', 'LIKE',  '%'.$term.'%')->orderBy('title_english')->get();
            // dd(DB::getQueryLog());
            // Format the result for select2

            $result = array();
            foreach($events as $key => $event) {
                $eventimage= $event['logo_image'] ? $event['logo_image'] : "no-img.png";
                $result[$key]['id'] = (int) $event['id'];
                $result[$key]['name'] = $term;
                $result[$key]['title_english'] = $event['title_english'];
                $result[$key]['avatar_url'] = $eventimage;
                $result[$key]['tier_count'] = count($event->tiers);
                $result[$key]['description_english'] =  str_limit($event['description_english'], 40);
                $result[$key]['event_id_str'] = $event['event_id_str'];
                $result[$key]['created_at'] = date('d/m/Y',strtotime($event['created_at']));
            }
            $events = $result;
            //   echo json_encode($users);
            return response()->json(['items'=>$events]);
        }
    }
    public function loadEventDetail(Request $request) {
        if ($request->ajax()) {
            $event_id= $request->eventid;
            $event=Event::find($event_id);
             $eventdetail = view('site/ajax/index',compact('event'))->render();
            return response()->json(['flag'=>true,'eventdetail'=>$eventdetail]);
        }
    }

    public function tnc(Request $request)
    {
      $input = $request->all();
      $source = 'generic';
      $tnc_body = ['tnc_arabic'=>'', 'tnc_english'=>'']; 
      if(isset($input['event_id']))
      {
        $event = Event::find($input['event_id']);
        if($event)
        {
          $tnc_body = ['tnc_arabic'=>$event->tc_arabic, 'tnc_english'=>$event->tc_english]; 
          $source = 'event';
        }
      }
      return View('site.events._tnc',compact('tnc_body','source'));
    }
}
