<?php

namespace Ticketing\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Ticketing\Models\Setting;
use Ticketing\Models\Document;
use View;

class PublicController extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct() {
        set_time_limit (60); 

        $setting  = Setting::first();

        $tnc_body = ['tnc_arabic'=>'', 'tnc_english'=>'']; 
        $document = Document::where('identifier','generic_tnc')->where('show',1)->first();
        if($document)
        {
          $tnc_body = ['tnc_arabic'=>$document->description_arabic, 'tnc_english'=>$document->description]; 
        }

        View::share('shared_site_vars', ['settings'=>$setting, 'tnc_body'=>$tnc_body]);
    }
}