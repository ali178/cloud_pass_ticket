<?php

namespace Ticketing\Http\Controllers\Admin;

use Ticketing\Models\Document;
use Ticketing\Http\Controllers\Controller;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use Session,Input,DB,Debugbar,Image,Mail,File,View;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('admin.documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'identifier' => 'required',
            'title' => 'required',
            'title_arabic' => 'required',
        ]);
        $input = $request->all();
        $input['show'] = (Integer) $input['show'];
        
        $document = Document::create($input);

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/documents');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Ticketing\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        return view('admin.documents.show')->withDocument($document);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Ticketing\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        return view('admin.documents.edit')->withDocument($document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Ticketing\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        $input = $request->all();
        $this->validate($request, [
            'identifier' => 'required',
            'title' => 'required',
            'title_arabic' => 'required',
        ]);

        $input['show'] = (Integer) $input['show'];
        $document->fill($input)->update();
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/documents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Ticketing\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        if ($document) {
            $document->delete();
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }
        return redirect()->back();
    }

    public function bulkshow(Request $request) {
        $ids = explode(',', $request['ids']);
        Document::whereIn('id', $ids)->update(['show' => '1']);
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->back();
    }

    public function bulkhide(Request $request) {
        $ids = explode(',', $request['ids']);
        Document::whereIn('id', $ids)->update(['show' => '0']);
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->back();
    }

    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);
        $deleted = 0;
        $message = '';
        foreach ($ids as $catId) {
            $document = Document::find($catId);
            if ($document) {
                $document->delete();
                $deleted++;
            }
        }
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    public function datatable() {
        return Datatables::of(Document::select('id', 'title', 'title_arabic', 'show')->orderBy('id' , 'desc'))
            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
            ->editColumn('show', '{{ $show ? "Show" : "Hide" }}')
            ->addColumn('actions', '
                    <ul class="nav quick-section ">
                      <li class="quicklinks actions"> <a id="gear-icon" class="" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                        <i class="fa fa-gear"></i>
                        </a>
                        <ul aria-labelledby="user-options" role="menu" class="dropdown-menu custom-listing-menu p-b-0 ">
                          @if(Auth::user()->can("documents-edit"))
                            <li><a class="" href="{{ URL::route(\'admin.documents.edit\',["documents"=>$id]) }}">Edit تحرير</a></li>
                            <li class="divider"></li>
                          @endif
                          @if(Auth::user()->can("documents-destroy"))
                            <li>{!! Form::open(["method" => "DELETE","route" => ["admin.documents.destroy", $id],"class" => "inline deleteaction actions-form"]) !!}<button class="btn-as-link delete-label-red">Delete حزف</button>{!! Form::close() !!}</li>
                          @endif
                        </ul>
                      </li>
                    </ul>
                    ', 4)
             ->rawColumns(['check','actions'])
            ->make(true);
    }
}
