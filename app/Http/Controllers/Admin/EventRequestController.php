<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Mail\EmailOnRejection;
use Ticketing\Mail\IssuedTicket;
use Ticketing\Mail\SendBankInfo;
use Ticketing\Models\Event;
use Ticketing\Models\Event_request;
use Ticketing\Models\Event_request_detail;
use Ticketing\Models\Issued_ticket;
use Ticketing\Models\Issued_ticket_detail;
use Ticketing\Models\Reason;
use Ticketing\Models\Redemption_ticket;
use Ticketing\Models\Rejection_of_event;
use Ticketing\Models\Rejection_of_request;
use Ticketing\Models\Tier;
use yajra\Datatables\Datatables;
use Session,Input,DB,Debugbar,Image,Mail,File,View;

class EventRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($eventid)
    {
       // echo $eventid;
       // die;
        $event=Event::find($eventid);
        $tier=Tier::where('event_id','=',$eventid)->get();
        $tier_sum = Tier::where(['event_id'=>$eventid,'total_quantity_type' => 1])->get();
        $total_tiers_qty_sum=$tier->where('event_id','=',$eventid)->where('total_quantity_type',1)->sum('total_quantity');
        $event_requests = Event_request::where('event_id','=',$eventid)->get();
//        $preordered_count= $event_requests->where('event_request_status_id',1)->count();
//        $pending_count = $event_requests->where('event_request_status_id',2)->count();
//        $issued_count = $event_requests->where('event_request_status_id',3)->count();
//        $cancelled_count = $event_requests->where('event_request_status_id',4)->count();
//        $total_count = $preordered_count + $pending_count + $issued_count + $cancelled_count;
        $preordered_count= $event_requests->where('event_request_status_id',1)->count();
        $pending_count = $event_requests->where('event_request_status_id',2)->count();
        $issued_count = $event_requests->where('event_request_status_id',3)->count();
        $cancelled_count = $event_requests->where('event_request_status_id',4)->count();
        $total_count = $preordered_count + $pending_count + $issued_count + $cancelled_count;
        $exclusive_count = Issued_ticket::getTotalAdminIssuedSum(3,$eventid,2);


        // $preordered_tiers_qty_sum = $event_requests->where('event_request_status_id',1)->sum('total_qty');
        // $pending_tiers_qty_sum = $event_requests->where('event_request_status_id',2)->sum('total_qty');
        // $issued_tiers_qty_sum= $event_requests->where('event_request_status_id',3)->sum('total_qty');

        // custom
        $preordered_tiers_qty_sum = $this->getTotalSumCustom(1,$eventid);
        $pending_tiers_qty_sum = $this->getTotalSumCustom(2,$eventid);
        $issued_tiers_qty_sum= $this->getTotalSumCustom(3,$eventid);
        
        $cancelled_tiers_qty_sum= $event_requests->where('event_request_status_id',4)->sum('total_qty');
        $available_qty= (($total_tiers_qty_sum) - ($preordered_tiers_qty_sum + $pending_tiers_qty_sum + $issued_tiers_qty_sum));
        $available_qty= $available_qty - $exclusive_count;
        return View('admin.event_requests.index',compact('eventid','event','preordered_count','pending_count','issued_count','cancelled_count','available_qty','total_count','preordered_tiers_qty_sum','pending_tiers_qty_sum','issued_tiers_qty_sum'));
    }

    private function getTotalSumCustom($event_request_status_id,$eventid)
    {
        $event_requests = Event_request::where('event_requests.event_id','=',$eventid)->where('event_request_status_id',$event_request_status_id)
        ->join('event_request_details', 'event_request_details.event_request_id', '=', 'event_requests.id')
        ->join('tiers', 'tiers.id', '=', 'event_request_details.tier_id')
        ->where('tiers.total_quantity_type', '=', 1)
        ->get();

        $total_qty = 0;
        foreach ($event_requests as $key => $event_request) {
            $total_qty = $total_qty + $event_request->total_qty;
        }
        return $total_qty;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewtickets($eventid){
        $event=Event::find($eventid);
        $tier=Tier::where('event_id','=',$eventid)->get();
        $total_tiers_qty_sum=$tier->where('event_id','=',$eventid)->sum('total_quantity');
        $event_requests = Event_request::where('event_id','=',$eventid)->get();
        $issued_count = $event_requests->where('event_request_status_id',3)->count();
        $cancelled_count = $event_requests->where('event_request_status_id',4)->count();
        $preordered_tiers_qty_sum= $event_requests->where('event_request_status_id',1)->sum('total_qty');
        $pending_tiers_qty_sum= $event_requests->where('event_request_status_id',2)->sum('total_qty');
        $issued_tiers_qty_sum= $event_requests->where('event_request_status_id',3)->sum('total_qty');
        $available_qty=$total_tiers_qty_sum - ($issued_tiers_qty_sum+$pending_tiers_qty_sum+$preordered_tiers_qty_sum);

        return View('admin.event_requests.viewtickets',compact('event','issued_count','cancelled_count','available_qty','issued_tiers_qty_sum','eventid'));
    }


    public function create($eventid)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($eventrequestid)
    {
        $event_request=Event_request::find($eventrequestid);
        $event=Event::find($event_request->event_id);
        $issued_tickets=Issued_ticket::where('event_request_id','=',$event_request->id)->first();


        // $tiers = $event_request->event_request_details->toArray();
        $tiers = $event_request->event_request_details() ->leftjoin('tiers','tiers.id','=','event_request_details.tier_id')->orderBy('tiers.sequence', 'DESC')->get();
        $tiers = $tiers->toArray();


        $tier=Tier::where('event_id','=',$event->id)->get();

        $total_tiers_qty_sum=$tier->where('event_id','=',$event->id)->sum('total_quantity');
        $preordered_tiers_qty_sum = $event_request->where('event_request_status_id',1)->sum('total_qty');
        $pending_tiers_qty_sum = $event_request->where('event_request_status_id',2)->sum('total_qty');
        $issued_tiers_qty_sum= $event_request->where('event_request_status_id',3)->sum('total_qty');
        $available_qty=$total_tiers_qty_sum - ($issued_tiers_qty_sum+$pending_tiers_qty_sum+$preordered_tiers_qty_sum);


        return View('admin.event_requests.edit',compact('event','event_request','tiers','available_qty','issued_tickets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event_request = Event_request::findOrFail($id);
        $event_id = $event_request->event_id;

        $issued_status = Issued_ticket::where('event_request_id' , $event_request->id)->first();
        $source = $event_request->event_request_status->title_english;
        $source = $source == 'Ordered' ? 'preordered' : $source;

        if($issued_status == '' || $issued_status->event_request_status_id != 3) {
            $event_request->delete();
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        else {
            Session::flash('flash_message', '<span style="color: red;margin-bottom: 20px;font-weight: 700" >Event Request can not be deleted because ticket issued against event request, first remove it then proceed</span>');
            return redirect()->back();
        }
        return redirect('admin/event_requests/'.$event_id.'?source='.$source);
    }

    public function bulkreject(Request $request) {
        $ids = explode(',', $request['ids']);
        $cancelled = 0;
        $message = '';
        foreach ($ids as $catId) {
            $event_request = Event_request::find($catId);
            $event         = Event::find($event_request->event_id);
            $event_request_status_id=$event_request->event_request_status_id;

            $event_request ->event_request_status_id =4;
            $event_request ->save();

            $cancelled++;

            $rejection_of_request=new Rejection_of_request();
            $rejection_of_request->reason_id=$request->reason_id;
            $rejection_of_request->event_request_id=$event_request->id;
            $rejection_of_request->description_arabic=$request->description_arabic;
            $rejection_of_request->description_english=$request->description_english;
            $rejection_of_request->event_request_status_id=$event_request_status_id;
            $rejection_of_request->from=1;
            $rejection_of_request->save();

            $content = [
                'title_english'=> $event->title_english,
                'title_arabic'=> $event->title_arabic,
                'logo_image' => $event->logo_image,
                'full_name' => $event_request->customer->full_name,
                'post_request_no'=>$event_request->post_request_no,
                'description_arabic'=>$rejection_of_request->description_arabic,
                'description_english'=>$rejection_of_request->description_english,
                'reason_title_arabic'=>$rejection_of_request->reason->title_arabic,
                'reason_title_english'=>$rejection_of_request->reason->title_english,
                'reason_description_arabic'=>$rejection_of_request->description_arabic,
                'reason_description_english'=>$rejection_of_request->description_english,

                'id_type'=>'Request ID: ',
                'id_type_arabic'=>'رقم الطلب',
                'complete_id'=>$event_request->complete_request_no,
            ];
            $email = $event_request->customer->email;
            // dd("s");
            try {
                // Mail::to($email)->send(new EmailOnRejection($content));
                Mail::send('emails.admin._cancelled', ['content'=>$content], function ($m) use ($email) {
                  $m->from('ticketing@gmail.com', 'TicketPass');
                  $m->to($email)->subject('إالغاء طلب | Request Cancellation');
                });
                // echo 'Mail send successfully';
            } catch (\Exception $e) {
                echo 'Error - '.$e;
                //  die;
            }

        }
        if ($cancelled < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }
        return redirect('admin/event_requests/'.$event->id.'?source=cancelled');

    }







    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);

        $deleted = 0;
        $message = '';
        foreach ($ids as $catId) {
            $event_request = Event_request::find($catId);
            $event_request->delete();
            $deleted++;
        }
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    public function pending_status($id)
    {
        $event_request = Event_request::find($id);
        $event         = Event::find($event_request->event_id);
        $event_request_details= $event_request->event_request_details;
        $message = '';
        if($event_request){
            $event_request->event_request_status_id = 2;
            $event_request->save();
            $message = 'pending';


            $content = [
                'title_english'=> $event->title_english,
                'title_arabic'=> $event->title_arabic,
                'logo_image' => $event->logo_image,
                'full_name' => $event_request->customer->full_name,
                'post_request_no'=>$event_request->post_request_no,
                'complete_request_no'=>$event_request->complete_request_no,
                'total_price'=>$event_request->total_price,
                'event_request_details'=>$event_request_details,
                'event_request'=>$event_request,
            ];
            $email = $event_request->customer->email;
            try {
                // Mail::to($email)->send(new SendBankInfo($content));
                Mail::send('emails.admin.sendbankinfo', ['content'=>$content], function ($m) use ($email) {
                  $m->from('ticketing@gmail.com', 'TicketPass');
                  $m->to($email)->subject('معلومات التحويل | Bank Transfer');
                });
               // echo 'Mail send successfully';
            } catch (\Exception $e) {
                echo 'Error - '.$e;
            }

        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/event_requests/'.$event->id.'?source='.$message);
    }

    public function reject_status(Request $request,$id)
    {
        $event_request = Event_request::find($id);
         $event         = Event::find($event_request->event_id);

        ////////// first check that the Request status which is "event_request_status_id==3" and it has ticket issued record then
        //// give alert that first delete issued ticket rcord from issued_ticket table and then proceed ?

        $message = '';
        if($event_request){
            $issued_ticket=Issued_ticket::where('event_request_id','=',$id)->where('event_request_status_id','=',3)->first();

            if(!$issued_ticket) {
                $event_request_status_id=$event_request->event_request_status_id;

                $event_request->event_request_status_id = 4;
                $event_request->save();
                $message = 'cancelled';

                $rejection_of_request=new Rejection_of_request();
                $rejection_of_request->reason_id=$request->reason_id;
                $rejection_of_request->event_request_id=$event_request->id;
                $rejection_of_request->description_arabic=$request->description_arabic;
                $rejection_of_request->description_english=$request->description_english;
                $rejection_of_request->event_request_status_id=$event_request_status_id;
                $rejection_of_request->from=1;
                $rejection_of_request->save();

                $content = [
                    'title_english'=> $event->title_english,
                    'title_arabic'=> $event->title_arabic,
                    'logo_image' => $event->logo_image,
                    'full_name' => $event_request->customer->full_name,
                    'post_request_no'=>$event_request->post_request_no,
                    'description_arabic'=>$rejection_of_request->description_arabic,
                    'description_english'=>$rejection_of_request->description_english,
                    'reason_title_arabic'=>$rejection_of_request->reason->title_arabic,
                    'reason_title_english'=>$rejection_of_request->reason->title_english,
                    'reason_description_arabic'=>$rejection_of_request->description_arabic,
                    'reason_description_english'=>$rejection_of_request->description_english,

                    'id_type'=>'Request ID: ',
                    'id_type_arabic'=>'رقم الطلب',
                    'complete_id'=>$event_request->complete_request_no,
                ];
                $email = $event_request->customer->email;
                // dd("s");
                try {
                    // Mail::to($email)->send(new EmailOnRejection($content));
                    Mail::send('emails.admin._cancelled', ['content'=>$content], function ($m) use ($email) {
                      $m->from('ticketing@gmail.com', 'TicketPass');
                      $m->to($email)->subject('إالغاء طلب | Request Cancellation');
                    });
                    // echo 'Mail send successfully';
                } catch (\Exception $e) {
                    echo 'Error - '.$e;
                    //  die;
                }

            }else{
                $message = 'issued';
            }



        }
        if($message=='issued') {
            Session::flash('flash_message', '<span style="color: red;margin-bottom: 20px;font-weight: 700" >Event Request status not set as cancelled because ticket issued againest event request, first remove it then proceed</span>');
            return redirect()->back();
        }



        Session::flash('flash_message', 'تم قبول طلبك بنجاح');


        return redirect('admin/event_requests/'.$event->id.'?source='.$message);
    }

    public function issue_status($id)
    {
        $event_request = Event_request::find($id);
        $event         = Event::find($event_request->event_id);
        $message = '';
        if($event_request){
            $event_request->event_request_status_id = 3;
            $event_request->save();

            $issued_ticket                           = new Issued_ticket();
            $issued_ticket ->event_request_id        =$event_request ->id;
            $issued_ticket ->customer_id             =$event_request->customer_id;
            $issued_ticket ->event_id                =$event_request->event_id;
            $issued_ticket ->total_qty               =$event_request->total_qty;
            $issued_ticket ->total_price             =$event_request->total_price;
            $issued_ticket ->pre_ticket_no           =$event_request->pre_request_no;
            $issued_ticket ->post_ticket_no          =str_replace('R', 'T',$event_request->post_request_no);
            $issued_ticket ->complete_ticket_no      =str_replace('R', 'T', $event_request->complete_request_no);
            $issued_ticket ->event_request_status_id =$event_request->event_request_status_id;
            $issued_ticket ->ticket_from             =$event_request->request_from;
            $issued_ticket ->setting_id              =$event_request->setting_id;
            $issued_ticket ->vat                     =$event_request->vat;
            $issued_ticket ->save();

            $event_request_details                   =$event_request->event_request_details;
            $num=1;
            foreach ($event_request_details as $event_request_detail){
                $issued_ticket_detail                   = new Issued_ticket_detail();
                $issued_ticket_detail->issued_ticket_id = $issued_ticket->id;
                $issued_ticket_detail->tier_id          = $event_request_detail->tier_id;
                $issued_ticket_detail->total_qty        = $event_request_detail->total_qty;
                $issued_ticket_detail->price            = $event_request_detail->price;
                $issued_ticket_detail->setting_id       =$event_request_detail->setting_id;
                $issued_ticket_detail->vat       =$event_request_detail->vat;
                $issued_ticket_detail->save();

                   for ($i=1;  $i<=$issued_ticket_detail->total_qty; $i++){
                       $redemption_ticket                          =new Redemption_ticket();
                       $redemption_ticket->event_id                = $event_request->event_id;
                       $redemption_ticket->issued_ticket_id        = $issued_ticket->id;
                       $redemption_ticket->issued_ticket_detail_id = $issued_ticket_detail->id;
                       $redemption_ticket->tier_id                 = $event_request_detail->tier_id;
                       $redemption_ticket->ticket_no               = $issued_ticket->complete_ticket_no.'-'.$num;
                       $redemption_ticket->save();
                       $num++;
                   }

            }

            $message = 'issued';

            $issued_ticket=Issued_ticket::find($issued_ticket->id);
            $event=Event::find($issued_ticket->event_id);
            $tiers = $issued_ticket->issued_ticket_details->toArray();

            $content = [
                'title_english'=> $event->title_english,
                'title_arabic'=> $event->title_arabic,
                'logo_image' => $event->logo_image,
                'full_name' => $issued_ticket->customer->full_name,
                'post_ticket_no'=>$issued_ticket->post_ticket_no,
                'complete_ticket_no'=>$issued_ticket->complete_ticket_no,
                'issued_ticket'=>$issued_ticket,
                'tiers'=>$tiers,
                'complete_request_no'=>$event_request->complete_request_no,
            ];
            $email = $issued_ticket->customer->email;
            try {
                // Mail::to($email)->send(new IssuedTicket($content));
                Mail::send('emails.admin.issuedticket', ['content'=>$content], function ($m) use ($email) {
                  $m->from('ticketing@gmail.com', 'TicketPass');
                  $m->to($email)->subject('التذاكر | Tickets');
                });
               // echo 'Mail send successfully';
            } catch (\Exception $e) {
                echo 'Error - '.$e;
              //  die;
            }
        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/event_requests/'.$event->id.'?source='.$message);
    }



    public function datatable(Request $request) {
      //  print_r($request->all());
       // die;
     //   $patientid = $this->route('id');
        $type = Input::get('type');
        $eventid = Input::get('eventid');
        return Datatables::of(Event_request::select('event_requests.id as id', 'event_requests.total_price as total_price','event_requests.complete_request_no as complete_request_no','event_requests.request_from as request_from','event_requests.status as status' ,'event_requests.event_request_status_id as event_request_status_id','event_requests.last_edit_by','users.name as updated_by','event_requests.created_at as created_at','event_requests.updated_at as updated_at','customers.full_name','customers.mobile','customers.email','event_requests.vat','issued_tickets.id as issued_ticket_id')
            ->where('event_requests.event_id',$eventid)
            ->where('event_requests.event_request_status_id',$type)
            ->orderBy('id','desc')
            ->leftjoin('customers','customers.id','=','event_requests.customer_id')
            ->leftjoin('users','users.id','=','event_requests.last_edit_by')
            ->leftjoin('issued_tickets','issued_tickets.event_request_id','=','event_requests.id')
        )

            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)



            ->editColumn('status',function($event_request){
                return Event_request::$status_YN[$event_request->request_from];
            })

            ->editColumn('total_price',function($event_request){

                $vat_in_decimal = 0;
                $vat_in_decimal = $event_request->vat/100;
                $vat_added_value = $vat_in_decimal * $event_request->total_price;

                // $event_request->total_price =  $event_request->total_price - $vat_added_value;
                // $vat_value=ceil($event_request->vat/100 * $event_request->total_price);
                // $event_request->total_price =  $event_request->total_price + $vat_value;

                // $total_price = number_format($event_request->total_price + $vat_added_value,2);
                $total_price = number_format($event_request->total_price,2);

                return $total_price;
            })

            ->editColumn('updated_by',function($event_request){
                return $event_request->updated_by ? $event_request->updated_by.'<br>'.$event_request->updated_at : '';
            })

            ->addColumn('info',function($event_request)
            {
                $customer_full_name = $event_request->full_name;
                $customer_mobile = $event_request->mobile;
                $customer_email = $event_request->email;
                $twelve_digit_id = $event_request->complete_request_no;
                $created_at = $event_request->created_at;

                // $tiers = $event_request->event_request_details->toArray();
                $tiers = $event_request->event_request_details() ->leftjoin('tiers','tiers.id','=','event_request_details.tier_id')->orderBy('tiers.sequence', 'DESC')->get();
                $tiers = $tiers->toArray();


                $info = View::make('admin.event_requests._tiers_tiles',['tiers'=>$tiers,'twelve_digit_id'=>$twelve_digit_id,'created_at'=>$created_at, 'customer_mobile'=>$customer_mobile, 'customer_email'=>$customer_email,'customer_full_name'=>$customer_full_name, 'event_request'=>$event_request]);

                return $info;
            })

            ->addColumn('actions', '  
                <ul class="nav quick-section ">
                    <li class="quicklinks actions"> 
                        <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu left custom-listing-menu">

                        @if(Auth::user()->can("eventrequests-sendbankinfo"))  
                            @if(in_array($event_request_status_id,array(1)))
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.pending_status", $id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}<button class="change_status">Send Bank Info إرسال معلومات التحويل</button>{!! Form::close() !!}</li>
                            <li class="divider"></li>
                           @endif
                           @if(in_array($event_request_status_id,array(2)))
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.pending_status", $id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to Re-Send Bank Info?"]) !!}<button class="change_status">Re-Send Bank Info إرسال معلومات التحويل</button>{!! Form::close() !!}</li>
                            <li class="divider"></li>
                           @endif
                         @endif
                      
                         @if(Auth::user()->can("eventrequests-issueticket"))  
                            @if(in_array($event_request_status_id,array(1,2)))
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.issue_status", $id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}<button class="change_status">Issue Tickets إصدار تذاكر</button>{!! Form::close() !!}</li>
                            <li class="divider"></li>
                           @endif
                           
                         @if(Auth::user()->can("eventrequests-eventrequestdetail"))  
                         <li><a class="" href="{{ route("admin.event_requests.edit",$id) }}">View Details نظرة على الطلب</a></li></li>
                               <li class="divider"></li>
                        
                        @endif
                        
                        @if(Auth::user()->can("issuedtickets-issuedticketdetail"))
                            @if(in_array($event_request_status_id,array(3)))

                         <li><a class="" href="{{ URL::route(\'admin.issued_tickets.viewissuedticket\',["id"=>$issued_ticket_id, "bsource"=>"request"]) }}">View Issued Ticket Details نظرة على التذكرة</a></li></li>
                                                           <li class="divider"></li>

                            @endif
                        @endif    
                          
                         @endif  
                           @if(Auth::user()->can("eventrequests-reject"))    
                            @if(in_array($event_request_status_id,array(1,2)))
                           <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.reject_status", $id],"class" => "rejectaction", "data-url"=>URL::to("admin/event_requests/$id/reject_status"),  "data-eventrequestid"=>$id, "data-msg"=>"Are you sure you want to change status?"]) !!}<button class="change_status">Reject رفض</button>{!! Form::close() !!}</li>
                              <li class="divider"></li>
                             @endif
                          @endif
                        @if(Auth::user()->can("eventrequests-destroy"))
                            <li>{!! Form::open(["method" => "DELETE","route" => ["admin.event_requests.destroy", $id],"class" => "inline deleteaction"]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                        @endif
                       
                        
              
                
                    </ul>
                  </li>
                </ul>', 3)

            ->rawColumns(['check','info','total_price','updated_by','actions'])
            ->make(true);
    }

public function getreasonbytype(Request $request){
    $event_request =Event_request::find($request->eventrequestid);
    $reasons      =Reason::where('type','=',1)->get();
    $reasonlist = view('admin/event_requests/ajax/ajax-load-reasonlist',compact('reasons'))->render();
    return response()->json(['flag'=>true,'reasonlist'=>$reasonlist]);
}

    public function getdescriptionbyreason(Request $request){
        $reason =Reason::find($request->reasonid);
        return response()->json([
                                   'flag'=>true,
                                   'description_english'=>$reason->description_english,
                                   'description_arabic'=>$reason->description_arabic
                               ]);
    }








}
