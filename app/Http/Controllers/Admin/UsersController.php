<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\User;
use Ticketing\Models\Role;
use Ticketing\Models\Event;
use yajra\Datatables\Datatables;
use Session,Input,DB,Debugbar,Image,Mail,File;

class UsersController extends Controller
{
    /*   * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // load the view and pass the users
        return View('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $events = Event::where('status',1)->where('is_draft',0)->orderBy('id','desc')->pluck('title_english','id');
        $volunteer_events = [];
        $roles = Role::pluck('display_name','id');
        return View('admin.users.create', compact('events','roles','volunteer_events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            // 'username' => 'required',
            // 'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'username' => 'required|unique:users,username,NULL,id,deleted_at,NULL',
            'password' => 'required|min:8',
            'role' => 'sometimes|required',
            'status' => 'required',
        ]);
        // dd($request->all());
        $input = $request->all();
        $input['status'] = (Integer) $input['status'];
        
        if (isset($input['assign_all_events'])) {
          $input['assign_all_events'] = 1;
        }else{
          $input['assign_all_events'] = 0;
        }
        
        if (empty($input['password'])){
            unset($input['password']);
        }else{
            $input['password']= bcrypt($input['password']);
        }    
        $user = User::create($input);
        if (!empty($input['role'])) {
            $user->attachRole($input['role']);   
        }
        $user->save();
        $service_type = $user->type == 0 ? 'systemusers' : 'volunteers';
        if(isset($input['events']) && !empty($input['events'])) {
            foreach ($input['events'] as $key=> $value){
                $user->volunteer_events()->create(['event_id'=>$key]);
            }
        }

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        return redirect('admin/users?source='.$service_type);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::findOrFail($id);
        return view('admin.users.show')->withuser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::pluck('display_name','id');
        $events = Event::where('status',1)->where('is_draft',0)->orderBy('id','desc')->pluck('title_english','id');
        $volunteer_events = $user->volunteer_events()->pluck('event_id','event_id')->toArray();

        return view('admin.users.edit', compact('user','roles','events','volunteer_events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        $input = $request->all();

        if (isset($input['assign_all_events'])) {
          $input['assign_all_events'] = 1;
        }else{
          $input['assign_all_events'] = 0;
        }
        
        if(isset($input['password']))
        {
            $this->validate($request, [
            // 'name' => 'required',
            // 'email' => 'required|email|unique:users,email,'.$user->id.',id,deleted_at,NULL',
            'username' => 'required|unique:users,username,'.$user->id.',id,deleted_at,NULL',
            'password' => 'required|min:8',
            'role' => 'sometimes|required',
            'status' => 'required',

        ]);
        }else{
            $this->validate($request, [
            // 'name' => 'required',
            // 'email' => 'required|email|unique:users,email,'.$user->id.',id,deleted_at,NULL',
            'username' => 'required|unique:users,username,'.$user->id.',id,deleted_at,NULL',
            'password' => '',
            'role' => 'sometimes|required', 
            'status' => 'required',

        ]);
        }

        $input['status'] = (Integer) $input['status'];
       
        if (empty($input['password'])){
            unset($input['password']);
        }else{
            $input['password']= bcrypt($input['password']);
        } 
        
        $user->fill($input)->update();
        if (!empty($input['role'])) {
            $user->roles()->detach();
            $user->attachRole((Integer)$input['role']);
        }

        if (isset($input['events']) && !empty($input['events'])) {
            $user->volunteer_events()->delete();
           
            if(isset($input['events']) && !empty($input['events'])) {
                foreach ($input['events'] as $key=> $value){
                    $user->volunteer_events()->create(['event_id'=>$key]);
                }
            }
        }else{
            $user->volunteer_events()->delete();
        }

        $service_type = $user->type == 0 ? 'systemusers' : 'volunteers';
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        return redirect('admin/users?source='.$service_type);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $user = User::findOrFail($id);

        if ($user) {
            if($user->type == 1)
            {
                $user->volunteer_events()->delete();
            }
            $user->delete();
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkshow(Request $request) {
        $ids = explode(',', $request['ids']);
        User::whereIn('id', $ids)->update(['status' => '1']);
        
        if (count($ids) < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkhide(Request $request) {

        $ids = explode(',', $request['ids']);
        User::whereIn('id', $ids)->update(['status' => '0']);

        if (count($ids) < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);

        $deleted = 0;
        $message = '';
        foreach ($ids as $catId) {
            $user = User::find($catId);
            if($user->type == 1)
            {
                $user->volunteer_events()->delete();
            }
            $user->delete();
            $deleted++;
        }
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    public function datatable() {
        $type = Input::get('type');
        return Datatables::of(User::select('id', 'username', 'email', 'status', 'created_at', 'type')
        ->where('type',$type))
            ->addColumn('role',function($user){
               return !empty($user->roles()) && $user->roles->first() ? $user->roles->first()->display_name : '';
            })
            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
            
            ->addColumn('actions', '
                    <ul class="nav quick-section">
                      <li class="quicklinks actions">
                        <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                        <ul aria-labelledby="user-options" role="menu" class="dropdown-menu custom-listing-menu">
                            @if(Auth::user()->can("users-edit"))
                                @if($type == 0)
                                    <li><a class="" href="{{ URL::route(\'admin.users.edit\',["users"=>$id,"source"=>"systemusers"]) }}">Edit تحرير</a></li>
                                @else
                                    <li><a class="" href="{{ URL::route(\'admin.users.edit\',["users"=>$id,"source"=>"volunteers"]) }}">Edit تحرير</a></li>
                                @endif
                                <li class="divider"></li>
                            @endif
                            @if(Auth::user()->can("users-destroy"))
                                <li>{!! Form::open(["method" => "DELETE","route" => ["admin.users.destroy", $id],"class" => "inline deleteaction"]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                            @endif
                        </ul>
                      </li>
                    </ul>
                ')
            ->editColumn('status', '{{ $status ? "Active" : "Inactive" }}')
            ->rawColumns(['check','actions'])
            ->make(true);
    }
}
