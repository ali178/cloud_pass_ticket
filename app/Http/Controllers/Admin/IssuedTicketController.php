<?php

namespace Ticketing\Http\Controllers\Admin;

use Hamcrest\Core\Is;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Ticketing\Mail\EmailOnRejection;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Http\Requests\IssuedTicketAddFormRequest;
use Ticketing\Models\Customer;
use Ticketing\Models\Event;
use Ticketing\Models\Event_request;
use Ticketing\Models\Issued_ticket;
use Ticketing\Models\Issued_ticket_detail;
use Ticketing\Models\Reason;
use Ticketing\Models\Redemption_ticket;
use Ticketing\Models\Rejection_of_request;
use Ticketing\Models\Tier;
use yajra\Datatables\Datatables;
use Session,Input,DB,Debugbar,Image,Mail,File,View;

class IssuedTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($eventid){
        $event=Event::find($eventid);
        $tier=Tier::where('event_id','=',$eventid)->get();


        $total_tiers_qty_sum=$tier->where('event_id','=',$eventid)->sum('total_quantity');
        $event_requests = Event_request::where('event_id','=',$eventid)->get();

        $issued_tickets=Issued_ticket::where('event_id','=',$eventid)->get();

       // $issued_count = $event_requests->where('event_request_status_id',3)->count();
        // exclusive count
        $exclusive_count = Issued_ticket::getTotalAdminIssuedSum(3,$eventid,2);

        // $exclusive_count = 0;
        // foreach ($tier as $key => $single_tier) {
        //     $exclusive_count = $exclusive_count + Issued_ticket::getTotalAdminIssuedSumNew(3,$eventid,$single_tier->id);
        // }

//        $exclusive_count = $issued_tickets->where('ticket_from',2)->where('event_request_status_id',3)->count();

        $issued_count = $issued_tickets->where('event_request_status_id',3)->count();
       // $cancelled_count = $event_requests->where('event_request_status_id',4)->count();

        $cancelled_count =$issued_tickets->where('event_request_status_id',4)->count();


        // $issued_tiers_qty_sum= $event_requests->where('event_request_status_id',3)->sum('total_qty');
        $issued_tiers_qty_sum = $issued_tickets->where('event_request_status_id',3)->sum('total_qty');
        // $available_qty=$total_tiers_qty_sum - $issued_tiers_qty_sum;

        // available total_quantity clone same as event request
//         $preordered_tiers_qty_sum = $event_requests->where('event_request_status_id',1)->sum('total_qty');
//         $pending_tiers_qty_sum = $event_requests->where('event_request_status_id',2)->sum('total_qty');
//         $issued_tiers_qty_sum2= $event_requests->where('event_request_status_id',3)->sum('total_qty');

        // custom
        $preordered_tiers_qty_sum = $this->getTotalSumCustom(1,$eventid);
        $pending_tiers_qty_sum = $this->getTotalSumCustom(2,$eventid);
        $issued_tiers_qty_sum2= $this->getTotalSumCustom(3,$eventid);

        // $available_qty= (($total_tiers_qty_sum) - ($preordered_tiers_qty_sum + $pending_tiers_qty_sum + $issued_tiers_qty_sum2));
        $available_qty= (($total_tiers_qty_sum) - ($preordered_tiers_qty_sum + $pending_tiers_qty_sum));
        $available_qty = $available_qty - $exclusive_count;

        // $available_qty = $total_tiers_qty_sum - $exclusive_count;
        // ----------------------------------------------------

        return View('admin.issued_tickets.index',compact('event','issued_count','cancelled_count','available_qty','issued_tiers_qty_sum','eventid'));
    }

    private function getTotalSumCustom($event_request_status_id,$eventid)
    {
        $event_requests = Event_request::where('event_requests.event_id','=',$eventid)->where('event_request_status_id',$event_request_status_id)
        ->join('event_request_details', 'event_request_details.event_request_id', '=', 'event_requests.id')
        ->join('tiers', 'tiers.id', '=', 'event_request_details.tier_id')
        ->where('tiers.total_quantity_type', '=', 1)
        ->get();

        $total_qty = 0;
        foreach ($event_requests as $key => $event_request) {
            $total_qty = $total_qty + $event_request->total_qty;
        }
        return $total_qty;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($eventid)
    {
        $event=Event::find($eventid);
        $tiers=$event->tiers;
//        $total_tiers_qty_sum=$tier->where('event_id','=',$eventid)->sum('total_quantity');
//        $event_requests = Event_request::where('event_id','=',$eventid)->get();
//        $issued_count = $event_requests->where('event_request_status_id',3)->count();
//        $cancelled_count = $event_requests->where('event_request_status_id',4)->count();
//        $issued_tiers_qty_sum= $event_requests->where('event_request_status_id',3)->sum('total_qty');
//        $available_qty=$total_tiers_qty_sum - $issued_tiers_qty_sum;

        return View('admin.issued_tickets.create',compact('event','eventid','tiers'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IssuedTicketAddFormRequest $request,$id)
    {
        $event                      = Event::find($id);
        $issued_ticket              = new Issued_ticket();

        $customer                   = new Customer();

        $customer ->full_name    =$request->full_name;
        $customer ->email        =$request->email;
        $customer ->mobile       =$request->mobile;
        $customer ->save();

        $genRandomNumberForEventRequestIssuedTicket=$event->genRandomEventRequestNumberIssuedTicket();


        $number=strstr($genRandomNumberForEventRequestIssuedTicket, '-');
        $post_ticket_no = substr($number, strpos($number, "_ ") + 1);        ;
        $pre_ticket_no = strstr($genRandomNumberForEventRequestIssuedTicket, ' ', true); // As of PHP 5.3.0
        $total_qty=0;
        $total_price=0.00;


        $tierlist=$request->tier_id;
        $total_qtylist=$request->total_qty;


        if (count($tierlist) > 0) {
            $count = count($tierlist);
            for ($i = 0; $i < $count; $i++) {
                if ($total_qtylist[$i] > 0) {
                    $tier = Tier::find($tierlist[$i]);
                    $total_qty        = $total_qty + $total_qtylist[$i];
                    // $total_price            = $total_price + $tier->price ;
                    $total_price =  $total_price + (isset($request['total_price'][$i]) ? $request['total_price'][$i] : 0);
                }
            }
        }

        $issued_ticket                           = new Issued_ticket();
        $issued_ticket ->event_request_id        =0;
        $issued_ticket ->customer_id             =$customer ->id;
        $issued_ticket ->event_id                =$id;

        $issued_ticket ->price_type               =$request->price_type;
        $issued_ticket ->tag_as_vip               =$request->has('tag_as_vip') ? '1' : '0';

        $issued_ticket ->total_qty               =$total_qty;
        $issued_ticket ->total_price             =$total_price;
        $issued_ticket ->pre_ticket_no           =$pre_ticket_no;
        $issued_ticket ->post_ticket_no          =$post_ticket_no;
        $issued_ticket ->complete_ticket_no      =$genRandomNumberForEventRequestIssuedTicket;
        $issued_ticket ->event_request_status_id =3;
        $issued_ticket ->ticket_from             =2;
        $issued_ticket ->setting_id              =1;
        $issued_ticket ->vat                     =$request->current_vat;
        $issued_ticket ->save();






        if (count($tierlist) > 0) {
            $count = count($tierlist);
            $num=1;
            for ($i = 0; $i < $count; $i++) {
                if ($total_qtylist[$i] > 0) {
                    $tier = Tier::find($tierlist[$i]);
                    $issued_ticket_detail                   = new Issued_ticket_detail();
                    $issued_ticket_detail->issued_ticket_id = $issued_ticket->id;
                    $issued_ticket_detail->tier_id          = $tierlist[$i];
                    $issued_ticket_detail->total_qty        = $total_qtylist[$i];
                    $issued_ticket_detail->price            = $tier->price ;
                    $issued_ticket_detail->setting_id       =1;
                    $issued_ticket_detail->vat              =$request->current_vat;
                    $issued_ticket_detail->save();

                    for ($j=1;  $j<=$total_qtylist[$i]; $j++){
                        $redemption_ticket                          =new Redemption_ticket();
                        $redemption_ticket->event_id                = $id;
                        $redemption_ticket->issued_ticket_id        = $issued_ticket->id;
                        $redemption_ticket->issued_ticket_detail_id = $issued_ticket_detail->id;
                        $redemption_ticket->tier_id                 = $issued_ticket_detail->tier_id;
                        $redemption_ticket->ticket_no               = $issued_ticket->complete_ticket_no.'-'.$num;
                        $redemption_ticket->redemption_from         =2;
                        $redemption_ticket->save();
                        $num++;
                    }
                }
            }
        }


        // $content = [
        //     'title_english'=> $event->title_english,
        //     'title_arabic'=> $event->title_arabic,
        //     'logo_image' => $event->logo_image,
        //     'full_name' => $customer->full_name,
        // ];

        // $email = $request->email;
        // if ($email!='') {
        //     // Mail::to($email)->send(new EventTicketConfirmation($content));
        //     Mail::send('emails.frontend.eventticketconfirmation', ['content'=>$content], function ($m) use ($email) {
        //       $m->from('no-reply@ticketpass.net', 'TicketPass');
        //       $m->to($email)->subject('طلب تذاكر | Tickets Request');
        //     });
        // }

        $email = $request->email;
        $tiers = $issued_ticket->issued_ticket_details->toArray();
        $content = [
            'title_english'=> $event->title_english,
            'title_arabic'=> $event->title_arabic,
            'logo_image' => $event->logo_image,
            'full_name' => $issued_ticket->customer->full_name,
            'post_ticket_no'=>$issued_ticket->post_ticket_no,
            'complete_ticket_no'=>$issued_ticket->complete_ticket_no,
            'issued_ticket'=>$issued_ticket,
            'tiers'=>$tiers,
        ];
        if ($email!='') {
            Mail::send('emails.admin.issuedticket', ['content'=>$content], function ($m) use ($email) {
              $m->from('ticketing@gmail.com', 'TicketPass');
              $m->to($email)->subject('التذاكر | Tickets');
            });
        }
        
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->route("admin.issued_tickets",$id);

    }





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $issued_ticket = Issued_ticket::findOrFail($id);
        $event_id=$issued_ticket->event_id;
        $source = $issued_ticket->event_request_status->title_english;
        $source = $source == 'Cancelled' ? 'cancelled' : $source;

        if ($issued_ticket) {
            $issued_ticket->delete();
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }
        return redirect('admin/issued_tickets/'.$event_id.'?source='.$source);
    }

    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);
        $deleted = 0;
        $message = '';
        foreach ($ids as $catId) {
            $issued_ticket = Issued_ticket::find($catId);
            $issued_ticket->delete();
            $deleted++;
        }
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }




    public function bulkreject(Request $request) {
        $ids = explode(',', $request['ids']);
        $cancelled = 0;
        $message = '';
        foreach ($ids as $catId) {
            $issued_ticket = Issued_ticket::find($catId);
            $event         = Event::find($issued_ticket->event_id);
            $event_request_status_id=$issued_ticket->event_request_status_id;

            $issued_ticket -> event_request_status_id =4;
            $issued_ticket -> save();
            $cancelled++;

            $rejection_of_request=new Rejection_of_request();
            $rejection_of_request->reason_id=$request->reason_id;
            $rejection_of_request->event_request_id=$issued_ticket->id;
            $rejection_of_request->description_arabic=$request->description_arabic;
            $rejection_of_request->description_english=$request->description_english;
            $rejection_of_request->event_request_status_id=$event_request_status_id;
            $rejection_of_request->from=1;
            $rejection_of_request->save();

            $content = [
                'title_english'=> $event->title_english,
                'title_arabic'=> $event->title_arabic,
                'logo_image' => $event->logo_image,
                'full_name' => $issued_ticket->customer->full_name,
                'post_request_no'=>$issued_ticket->post_ticket_no,
                'description_arabic'=>$rejection_of_request->description_arabic,
                'description_english'=>$rejection_of_request->description_english,
                'reason_title_arabic'=>$rejection_of_request->reason->title_arabic,
                'reason_title_english'=>$rejection_of_request->reason->title_english,
                'reason_description_arabic'=>$rejection_of_request->description_arabic,
                'reason_description_english'=>$rejection_of_request->description_english,

                'id_type'=>'Ticket ID: ',
                'id_type_arabic'=>'رقم التذاكر',
                'complete_id'=>$issued_ticket->complete_ticket_no,
            ];
            $email = $issued_ticket->customer->email;
            try {
                // Mail::to($email)->send(new EmailOnRejection($content));
                Mail::send('emails.admin._cancelled', ['content'=>$content], function ($m) use ($email) {
                    $m->from('ticketing@gmail.com', 'TicketPass');
                    $m->to($email)->subject('إالغاء التذاكر | Tickets Cancellation');
                });
                // echo 'Mail send successfully';
            } catch (\Exception $e) {
                echo 'Error - '.$e;
                //  die;
            }

        }
        if ($cancelled < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }
        return redirect()->back();
    }


    public function reject_status(Request $request, $id)
    {
        $issued_ticket = Issued_ticket::find($id);
        $event=Event::find($issued_ticket->event_id);

        $message = '';
        if($issued_ticket){
            $event_request_status_id=$issued_ticket->event_request_status_id;
                $issued_ticket->event_request_status_id = 4;
                $issued_ticket->save();
                $message = 'cancelled';


            $rejection_of_request=new Rejection_of_request();
            $rejection_of_request->reason_id=$request->reason_id;
            $rejection_of_request->event_request_id=$issued_ticket->id;
            $rejection_of_request->description_arabic=$request->description_arabic;
            $rejection_of_request->description_english=$request->description_english;
            $rejection_of_request->event_request_status_id=$event_request_status_id;
            $rejection_of_request->from=2;
            $rejection_of_request->save();

            $content = [
                'title_english'=> $event->title_english,
                'title_arabic'=> $event->title_arabic,
                'logo_image' => $event->logo_image,
                'full_name' => $issued_ticket->customer->full_name,
                'post_request_no'=>$issued_ticket->post_ticket_no,
                'description_arabic'=>$rejection_of_request->description_arabic,
                'description_english'=>$rejection_of_request->description_english,
                'reason_title_arabic'=>$rejection_of_request->reason->title_arabic,
                'reason_title_english'=>$rejection_of_request->reason->title_english,
                'reason_description_arabic'=>$rejection_of_request->description_arabic,
                'reason_description_english'=>$rejection_of_request->description_english,

                'id_type'=>'Ticket ID: ',
                'id_type_arabic'=>'رقم التذاكر',
                'complete_id'=>$issued_ticket->complete_ticket_no,
            ];
            $email = $issued_ticket->customer->email;
            try {
                // Mail::to($email)->send(new EmailOnRejection($content));
                Mail::send('emails.admin._cancelled', ['content'=>$content], function ($m) use ($email) {
                    $m->from('ticketing@gmail.com', 'TicketPass');
                    $m->to($email)->subject('إالغاء التذاكر | Tickets Cancellation');
                });
                // echo 'Mail send successfully';
            } catch (\Exception $e) {
                echo 'Error - '.$e;
                //  die;
            }

            }

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/issued_tickets/'.$issued_ticket->event_id.'?source='.$message);
    }

    public function resend_ticket($id)
    {
            $issued_ticket=Issued_ticket::find($id);
            $event=Event::find($issued_ticket->event_id);
            $tiers = $issued_ticket->issued_ticket_details->toArray();

            $content = [
                'title_english'=> $event->title_english,
                'title_arabic'=> $event->title_arabic,
                'logo_image' => $event->logo_image,
                'full_name' => $issued_ticket->customer->full_name,
                'post_ticket_no'=>$issued_ticket->post_ticket_no,
                'complete_ticket_no'=>$issued_ticket->complete_ticket_no,
                'issued_ticket'=>$issued_ticket,
                'tiers'=>$tiers
            ];
            $email = $issued_ticket->customer->email;
            try {
                // Mail::to($email)->send(new IssuedTicket($content));
                Mail::send('emails.admin.issuedticket', ['content'=>$content], function ($m) use ($email) {
                  $m->from('ticketing@gmail.com', 'TicketPass');
                  $m->to($email)->subject('التذاكر | Tickets');
                });
               // echo 'Mail send successfully';
            } catch (\Exception $e) {
                echo 'Error - '.$e;
              //  die;
            }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->back();
    }

    public function datatable(Request $request) {
        $type = Input::get('type');
        $eventid = Input::get('eventid');
        return Datatables::of(Issued_ticket::select('issued_tickets.id as id','issued_tickets.tag_as_vip as tag', 'issued_tickets.total_price as total_price','issued_tickets.price_type as type','issued_tickets.complete_ticket_no as complete_ticket_no','issued_tickets.ticket_from as ticket_from' ,'issued_tickets.event_request_status_id as event_request_status_id','issued_tickets.last_edit_by','users.name as updated_by','issued_tickets.created_at as created_at','issued_tickets.updated_at as updated_at','customers.full_name','customers.mobile','customers.email','issued_tickets.vat')
            ->where('issued_tickets.event_id',$eventid)
            ->where('issued_tickets.event_request_status_id',$type)
            ->orderBy('id','desc')
            ->leftjoin('customers','customers.id','=','issued_tickets.customer_id')
            ->leftjoin('users','users.id','=','issued_tickets.last_edit_by')
        )

            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)



            ->editColumn('status',function($issued_ticket){
                return Issued_ticket::$status_YN[$issued_ticket->ticket_from];
            })
            ->editColumn('type',function($issued_ticket){
                return  $issued_ticket->type == 1 ? "PAID" : "FREE";
            })

            ->editColumn('total_price',function($issued_ticket){

                $vat_in_decimal = 0;
                $vat_in_decimal = $issued_ticket->vat/100;
                $vat_added_value = $vat_in_decimal * $issued_ticket->total_price;

                // $issued_ticket->total_price =  $issued_ticket->total_price - $vat_added_value;
                // $vat_value=ceil($issued_ticket->vat/100 * $issued_ticket->total_price);
                // $issued_ticket->total_price =  $issued_ticket->total_price + $vat_value;

                $total_price = number_format($issued_ticket->total_price,2);

                return $total_price;
            })

            ->editColumn('updated_by',function($issued_ticket){
                return $issued_ticket->updated_by ? $issued_ticket->updated_by.'<br>'.$issued_ticket->updated_at : '';
            })

            ->addColumn('info',function($issued_ticket)
            {
                $customer_full_name = $issued_ticket->full_name;
                $customer_mobile = $issued_ticket->mobile;
                $customer_email = $issued_ticket->email;
                $twelve_digit_id = $issued_ticket->complete_ticket_no;
                $created_at = $issued_ticket->created_at;


             //  echo '<pre>';
               // print_r($issued_ticket->issued_ticket_details->toArray());
               // die;

                // $tiers = $issued_ticket->issued_ticket_details->toArray();
                $tiers = $issued_ticket->issued_ticket_details() ->leftjoin('tiers','tiers.id','=','issued_ticket_details.tier_id')->orderBy('tiers.sequence', 'DESC')->get();
                $tiers = $tiers->toArray();


               // $tiers =    Issued_ticket_detail::where('issued_ticket_id','=',$issued_ticket->id)->get();
               // $tiers = $tiers->toArray();
                $info = View::make('admin.issued_tickets._viewtickets_tiers_tiles',['tiers'=>$tiers,'twelve_digit_id'=>$twelve_digit_id,'created_at'=>$created_at, 'customer_mobile'=>$customer_mobile, 'customer_email'=>$customer_email,'customer_full_name'=>$customer_full_name, 'issued_ticket'=>$issued_ticket,'bsource'=>'issued_tickets']);

                return $info;
            })

            ->addColumn('actions', '  
                <ul class="nav quick-section ">
                    <li class="quicklinks actions"> 
                        <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu left custom-listing-menu">

                        @if($event_request_status_id == 3 )
                            <li><a class="" href="{{ route("admin.issued_tickets.resend_ticket",$id) }}">Re-send Ticket إعادة اصدار التذاكر</a></li></li>
                            <li class="divider"></li>
                        @endif
                         
                        @if(Auth::user()->can("issuedtickets-issuedticketdetail"))
                         
                         
                        <li><a class="" href="{{ URL::route(\'admin.issued_tickets.viewissuedticket\',["id"=>$id, "bsource"=>"issued_tickets"]) }}">View Detail نظرة على الطلب</a></li></li>
                        <li class="divider"></li>

                        @endif  
                         
                        @if(Auth::user()->can("issuedtickets-reject"))
                            @if($event_request_status_id != 4 )
                           <li>{!! Form::open(["method" => "POST","route" => ["admin.issued_tickets.reject_status", $id],"class" => "rejectaction", "data-url"=>URL::to("admin/issued_tickets/$id/reject_status"),  "data-issuedticketid"=>$id, "data-msg"=>"Are you sure you want to change status?"]) !!}<button class=" change_status">Cancel إلغاء</button>{!! Form::close() !!}</li>
                              <li class="divider"></li>
                             @endif
                         @endif
                        @if(Auth::user()->can("issuedtickets-destroy"))
                           
                            <li>{!! Form::open(["method" => "DELETE","route" => ["admin.issued_tickets.destroy", $id],"class" => "inline deleteaction"]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                        @endif
                       
                      
                    </ul>
                  </li>
                </ul>', 3)

            ->rawColumns(['check','info','total_price','updated_by','actions'])
            ->make(true);
    }
    public function getreasonbytype(Request $request){
        $issued_ticket =Issued_ticket::find($request->issuedticketid);
        $reasons      =Reason::where('type','=',2)->get();
        $reasonlist = view('admin/issued_tickets/ajax/ajax-load-reasonlist',compact('reasons'))->render();
        return response()->json(['flag'=>true,'reasonlist'=>$reasonlist]);
    }

    public function getdescriptionbyreason(Request $request){
        $reason =Reason::find($request->reasonid);
        return response()->json([
            'flag'=>true,
            'description_english'=>$reason->description_english,
            'description_arabic'=>$reason->description_arabic
        ]);
    }

    public function cancelledticket($eventid){
        $event=Event::find($eventid);
        $tier=Tier::where('event_id','=',$eventid)->get();
        $total_tiers_qty_sum=$tier->where('event_id','=',$eventid)->sum('total_quantity');
        $issued_tickets = Issued_ticket::where('event_id','=',$eventid)->get();
        $cancelled_count = $issued_tickets->where('event_request_status_id',4)->count();

        $refunded_count = $issued_tickets->where('event_request_status_id',5)->count();
        $cancelled_tiers_qty_sum= $issued_tickets->where('event_request_status_id',4)->sum('total_qty');
        // $total_pending_amount_sum= $issued_tickets->where('event_request_status_id',4)->sum('total_price');

        $total_pending_amount_sum = $issued_tickets->where('event_request_status_id',4)->map(function ($issued_ticket) {
            $vat_in_decimal = 0;
            $vat_in_decimal = $issued_ticket->vat/100;
            $vat_added_value = $vat_in_decimal * $issued_ticket->total_price;
            $total_price = $issued_ticket->total_price + $vat_added_value;
            return $issued_ticket->total_price;
        })->sum();

      //  $available_qty=$total_tiers_qty_sum - $cancelled_tiers_qty_sum;

        return View('admin.issued_tickets.cancelledticket',compact('event','cancelled_count','refunded_count','total_pending_amount_sum','cancelled_tiers_qty_sum','eventid'));
    }


    public function refunded_status($id)
    {
        $issued_ticket = Issued_ticket::find($id);
       // $event=Event::find($issued_ticket->event_id);
        $message = '';
        if($issued_ticket){
            $issued_ticket->event_request_status_id = 5;
            $issued_ticket->save();
            $message = 'refunded';
        }

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/issued_tickets/cancelledticket/'.$issued_ticket->event_id.'?source='.$message);
    }

    public function cancelled_status($id)
    {
        $issued_ticket = Issued_ticket::find($id);

        $message = '';
        if($issued_ticket){
            $issued_ticket->event_request_status_id = 4;
            $issued_ticket->save();
            $message = 'pending';
        }

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/issued_tickets/cancelledticket/'.$issued_ticket->event_id.'?source='.$message);
    }




    public function datatableCancelledTicket(Request $request) {
        $type = Input::get('type');
        $eventid = Input::get('eventid');
        return Datatables::of(Issued_ticket::select('issued_tickets.id as id','issued_tickets.tag_as_vip as tag', 'issued_tickets.total_price as total_price' , 'issued_tickets.price_type as type','issued_tickets.complete_ticket_no as complete_ticket_no','issued_tickets.ticket_from as ticket_from' ,'issued_tickets.event_request_status_id as event_request_status_id','issued_tickets.last_edit_by','users.name as updated_by','issued_tickets.created_at as created_at','issued_tickets.updated_at as updated_at','customers.full_name as full_name','customers.mobile as mobile','customers.email as email','issued_tickets.vat')
            ->where('issued_tickets.event_id',$eventid)
            ->where('issued_tickets.event_request_status_id',$type)
            ->orderBy('id','desc')
            ->leftjoin('customers','customers.id','=','issued_tickets.customer_id')
            ->leftjoin('users','users.id','=','issued_tickets.last_edit_by')
        )

            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)



            ->editColumn('status',function($issued_ticket){
                return Issued_ticket::$status_YN[$issued_ticket->ticket_from];
            })

            ->editColumn('type',function($issued_ticket){
                return  $issued_ticket->type == 1 ? "PAID" : "FREE";
            })

            ->editColumn('total_price',function($issued_ticket){

                $vat_in_decimal = 0;
                $vat_in_decimal = $issued_ticket->vat/100;
                $vat_added_value = $vat_in_decimal * $issued_ticket->total_price;

                // $issued_ticket->total_price =  $issued_ticket->total_price - $vat_added_value;
                // $vat_value=ceil($issued_ticket->vat/100 * $issued_ticket->total_price);
                // $issued_ticket->total_price =  $issued_ticket->total_price + $vat_value;

                // $total_price = number_format($issued_ticket->total_price + $vat_added_value,2);
                $total_price = number_format($issued_ticket->total_price,2);

                return $total_price;
            })

            ->editColumn('updated_by',function($issued_ticket){
                return $issued_ticket->updated_by ? $issued_ticket->updated_by.'<br>'.$issued_ticket->updated_at : '';
            })

            ->addColumn('info',function($issued_ticket)
            {
                $customer_full_name = $issued_ticket->full_name;
                $customer_mobile = $issued_ticket->mobile;
                $customer_email = $issued_ticket->email;
                $twelve_digit_id = $issued_ticket->complete_ticket_no;
                $created_at = $issued_ticket->created_at;

                //  echo '<pre>';
                // print_r($issued_ticket->issued_ticket_details->toArray());
                // die;

                // $tiers = $issued_ticket->issued_ticket_details->toArray();
                $tiers = $issued_ticket->issued_ticket_details() ->leftjoin('tiers','tiers.id','=','issued_ticket_details.tier_id')->orderBy('tiers.sequence', 'DESC')->get();
                $tiers = $tiers->toArray();



                // $tiers =    Issued_ticket_detail::where('issued_ticket_id','=',$issued_ticket->id)->get();
                // $tiers = $tiers->toArray();
                $info = View::make('admin.issued_tickets._viewtickets_tiers_tiles',['tiers'=>$tiers,'twelve_digit_id'=>$twelve_digit_id,'created_at'=>$created_at, 'customer_mobile'=>$customer_mobile, 'customer_email'=>$customer_email,'customer_full_name'=>$customer_full_name, 'issued_ticket'=>$issued_ticket,'bsource'=>'cancelledticket']);

                return $info;
            })

            ->addColumn('actions', '  
                <ul class="nav quick-section " >
                    <li class="quicklinks actions"> 
                        <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu left custom-listing-menu" style="width: 240px">

                        @if(Auth::user()->can("cancelledtickets-markaspending"))
                            @if(in_array($event_request_status_id,array(5)))
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.issued_tickets.cancelled_status", $id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}<button class=" change_status">Mark as Pending تنتظر التعويض</button>{!! Form::close() !!}</li>
                          
                           @endif
                        @endif
                           @if(Auth::user()->can("cancelledtickets-markasrefund"))
                            @if(in_array($event_request_status_id,array(4)))
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.issued_tickets.refunded_status", $id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}<button class=" change_status">Mark as Refunded تم التعويض</button>{!! Form::close() !!}</li>
                           @endif
                           @endif 

                    </ul>
                  </li>
                </ul>', 3)

            ->rawColumns(['check','info','total_price','updated_by','actions'])
            ->make(true);
    }

    function viewissuedticket($issuedticketid){
        $issued_ticket=Issued_ticket::find($issuedticketid);
        $event=Event::find($issued_ticket->event_id);

        // $tiers = $issued_ticket->issued_ticket_details->toArray();
        $tiers = $issued_ticket->issued_ticket_details() ->leftjoin('tiers','tiers.id','=','issued_ticket_details.tier_id')->orderBy('tiers.sequence', 'DESC')->get();
        $tiers = $tiers->toArray();

        return View('admin.issued_tickets.viewissuedticket',compact('event','issued_ticket','tiers'));
    }

    function genbarcodeofticket($id){
       // $tier=Tier::find($tierid);
        // $redemption_ticket=Redemption_ticket::find($id);
        $redemption_ticket=Redemption_ticket::where('random_id_string',$id)->first();
        if($redemption_ticket)
        {
            $tier=Tier::find($redemption_ticket->tier_id);
            $event=Event::find($redemption_ticket->event_id);
            $num=$redemption_ticket->ticket_no;
            $issued_ticket= Issued_ticket::find($redemption_ticket->issued_ticket_id);

            // $barcode_color = $issued_ticket->event_request_status_id == 4 || $redemption_ticket->redeem_status == 0 ? '137,137,137' : '0,0,0';
    
            // $QRCode=QrCode::format('png')->size(150)->generate($redemption_ticket->ticket_no);
            if($issued_ticket->event_request_status_id == 4 || $redemption_ticket->redeem_status == 0)
            {
                $QRCode=QrCode::format('png')->size(150)->color(137,137,137)->generate($redemption_ticket->ticket_no);
                $barcode_color = '898989';
            }else{
                $QRCode=QrCode::format('png')->size(150)->color(0,0,0)->generate($redemption_ticket->ticket_no);
                $barcode_color = '000000';
            }
           // $tiers = $issued_ticket->issued_ticket_details->toArray();
            return View('admin.issued_tickets.genbarcodeofticket',compact('event','issued_ticket','tier','num','QRCode','barcode_color'));
        }else{
            abort(404);
        }
        // return redirect()->back();
    }

}
