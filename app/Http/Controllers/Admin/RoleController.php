<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\User;
use Ticketing\Models\Role;
use Ticketing\Models\Permission;
use yajra\Datatables\Datatables;
use Session,
    Input,
    DB,
    Debugbar,Auth,Entrust;

class RoleController extends Controller
{
    public function __construct(){
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // load the view and pass the roles
        return View('admin.roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $permissionsArray = $this->permissionsArray(); 
        $role_permissions = [];
        // load the create form (app/views/roles/create.blade.php)
        return View('admin.roles.create',compact('permissionsArray','role_permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:roles',
            'display_name' => 'required',
        ]);

        $input = $request->all();

        $role = Role::create($input);

        if (!empty($input['permissions'])) {
            $pids = [];
            foreach ($input['permissions'] as $key=> $value){
                $permission = Permission::where('name',$key)->first();
                if($permission){
                   $pids= $permission->id;
                }
                $role->perms()->attach($pids);
            }
            
        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $role = Role::findOrFail($id);
        return view('admin.roles.show')->withrole($role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $role = Role::findOrFail($id);
        $permissionsArray = $this->permissionsArray(); 
        $role_permissions = $role->perms()->pluck('display_name','name')->toArray();
        return view('admin.roles.edit',compact('role','permissionsArray','role_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $role = Role::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|unique:roles,name,'.$role->id,
            'display_name' => 'required',
        ]);

        $input = $request->all();
        $role->fill($input)->save();
        if (!empty($input['permissions'])) {
            $role->perms()->detach();
            $pids = [];
            foreach ($input['permissions'] as $key=> $value){
                $permission = Permission::where('name',$key)->first();
                if($permission){
                   $pids[]= $permission->id;
                }
            }

            $role->perms()->attach($pids);
        } else {
            $role->perms()->detach();
        }
        
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $role = Role::find($id);
        
        if ($role) {
            $user  = $role->users()->first();
            if(!$user)
            {
                $role->delete();
                Session::flash('flash_message', 'تم قبول طلبك بنجاح');
            }
            else{
                Session::flash('flash_message', 'This role is associated!');
            }
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkshow(Request $request) {
        $ids = explode(',', $request['ids']);
        Role::whereIn('id', $ids)->update(['status' => '1']);

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkhide(Request $request) {

        $ids = explode(',', $request['ids']);
        Role::whereIn('id', $ids)->update(['status' => '0']);

        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);
        
        $i = 0;
        $role_name = [];
        foreach ($ids as $role_id) {
            $role = Role::find($role_id);
            
            if($role) {
                $user = $role->users()->first();
                if(!$user)
                {   
                    $role->delete();
                    Session::flash('flash_message', 'تم قبول طلبك بنجاح');
                }
                else {
                    $role_name[$i] = $role->name; 
                    $i++; 
                }
            }
        }
        $names = implode(',',$role_name);
        if($names)
        {
            Session::flash('flash_message', ''.$names.' roles are associated');
        }
    
        return redirect()->back();
    }

    public function datatable() {
        return Datatables::of(Role::select('id', 'name', 'display_name'))
        ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
        ->addColumn('actions', '
                <ul class="nav quick-section">
                  <li class="quicklinks actions">
                    <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                        <i class="fa fa-gear"></i>
                    </a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu custom-listing-menu">
                          @if(Auth::user()->can("roles-edit"))
                            <li><a class="" href="{{ URL::route(\'admin.roles.edit\',$id) }}">Edit تحرير</a></li></li>
                            <li class="divider"></li>
                          @endif
                          @if(Auth::user()->can("roles-destroy"))
                            <li>{!! Form::open(["method" => "DELETE","route" => ["admin.roles.destroy", $id],"class" => "inline deleteaction"]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                          @endif
                    </ul>
                  </li>
                </ul>
            ')
        ->rawColumns(['check','actions'])
        ->make(true);
    }
}
