<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Models\Event;
use Ticketing\Models\Event_request;
use Ticketing\Models\Event_request_detail;
use Ticketing\Models\Issued_ticket;
use Ticketing\Models\Issued_ticket_detail;
use Ticketing\Models\Tier;
use Ticketing\Models\Redemption_ticket;
use yajra\Datatables\Datatables;
use Session,Input,DB,Debugbar,Image,Mail,File,View;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $events = Event::all();
        $publish_count = $events->where('status',1)->where('is_draft',0)->count();
        $unpublish_count = $events->where('status',0)->where('is_draft',0)->count();
        return View('admin.events.index',compact('publish_count','unpublish_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = Event::create(['tag' => 4]);
        return redirect()->route('admin.events.addeditevent',['event'=>$event->id, 'q'=>'Add']);
    }

    public function addeditevent(Request $request,$id)
    {
        $tags1 = Event::$Tags;

        $tags =[];
        foreach ($tags1 as $index => $key) {

            if ($index != 3){
                $tags[$index] = $key;
                 }
               }
          //  die;



        $input = $request->all();
        $event = Event::find($id);
        if($event)

        {

            if ($request->isMethod('put')) {
                /* NOTE
                number => (e.g) h,w,h2,w2
                mode  (e.g) wh => $file->crop($width, $height, $x, $y);, hw => $file->crop($height, $width, $x, $y);]*/

                foreach (['logo_image'=>'','cover_image'=>'2'] as $image_name => $number) {
                    $uploadLogoimage = Event::uploadImage(['object' => $event,'input' => $input,'image_name' => $image_name, 'path' => 'uploads/events/', 'number' => $number, 'mode'=>'wh']);
                    if($uploadLogoimage != 'ignore' || $uploadLogoimage == '')
                    {
                        $input[$image_name] = $uploadLogoimage;
                    }else{
                        unset($input[$image_name]);
                    }
                }
                
                // save record and redirect to events index

                $input['is_another_dt'] = isset($input['is_another_dt']) ? 1 : 0;
                $input['from_date_arabic'] = isset($input['day_from_arabic'],$input['month_from_arabic'],$input['year_from_arabic']) ? $input['year_from_arabic'].'/'.$input['month_from_arabic'].'/'.$input['day_from_arabic'] : null;
                $input['to_date_arabic'] = isset($input['day_to_arabic'],$input['month_to_arabic'],$input['year_to_arabic']) ? $input['year_to_arabic'].'/'.$input['month_to_arabic'].'/'.$input['day_to_arabic'] : null;
                $input['is_draft'] = 0;

                $input['from_time_arabic'] = Event::arabic_time_format($input)['from'];
                $input['to_time_arabic'] = Event::arabic_time_format($input)['to'];

                $event->fill($input)->save();
                if(isset($input['q']) && $input['q'] == 'Add')
                {
                    Session::flash('flash_message', 'تم قبول طلبك بنجاح');
                }else{
                    Session::flash('flash_message', 'تم قبول طلبك بنجاح');
                }

                $source = $event->status ? 'published' : 'unpublished';
                return redirect('admin/events?source='.$source);
            }


            // arabic time start
            $arabic_hours = Event::arabic_hours();
            $arabic_minutes = Event::arabic_minutes();
            // arabic time end

            if (isset($input['q']) && $input['q'] == 'Add') {
                $request_type = 'Add';
                return View('admin.events.create',compact('tags','request_type','event','arabic_hours','arabic_minutes'));
            }else{
                $request_type = 'Edit';
                $arabic_date = $event->getArabicDate($event);
                $arabic_to_date = $event->getArabicToDate($event);

                $existing_arabic_from_time = Event::getSeperateArabicTime($event->from_time_arabic);
                $existing_arabic_to_time = Event::getSeperateArabicTime($event->to_time_arabic);

                return View('admin.events.edit',compact('tags','request_type','event','arabic_date','arabic_to_date','arabic_hours','arabic_minutes','existing_arabic_from_time','existing_arabic_to_time'));
            }    
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);

        if ($event) {
            $event->delete();
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        }
        return redirect()->back();
    }

    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);

        $deleted = 0;
        $message = '';
        foreach ($ids as $catId) {
            $event = Event::find($catId);
            $event->delete();
            $deleted++;
        }
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    public function status($id)
    {
        $event = Event::find($id);
        $message = '';
        if($event){
            $event->status = $event->status ? 0 : 1;
            $event->save();
            $message = $event->status ? 'published' : 'unpublished';
        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect('admin/events?source='.$message);
    }

    public function selling($id)
    {
        $event = Event::find($id);
        $message = '';
        if($event){
            $event->selling_off = $event->selling_off ? 0 : 1;
            $event->save();
            $message = $event->status ? 'Enable' : 'Disable';
        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');
        return redirect()->back();
    }

    public function datatable(Request $request) {
        $type = Input::get('type');
        return Datatables::of(Event::select('events.id as id', 'logo_image','event_id_str','from_date_english','to_date_english','from_time_english','to_time_english','title_english','title_arabic','selling_off','events.status as status','map_address','events.last_edit_by','users.name as updated_by','events.updated_at as updated_at','events.lat as lat' ,'events.long as long')
            ->where('is_draft',0)
            ->where('events.status',$type)
            ->orderBy('id','desc')
            ->leftjoin('users','users.id','=','events.last_edit_by')
            )

            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
            
            ->editColumn('logo_image','<a class="gallery-item" data-fancybox="logo" data-gallery="" href="{{ asset($logo_image)}}"><div class="img-thumbnail" style=" width:90px;height:90px; display: inline-block; vertical-align: top; position: relative;"><img src="{{ asset($logo_image)}}" class="listing-icon" style="max-width:80px; max-height:80px; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0; right: 0; margin: auto;"/></div></a>')

            ->editColumn('status',function($event){
                return Event::$status_YN[$event->status];
            })

            ->editColumn('updated_by',function($event){
//                return $event->updated_by ? $event->updated_by.'<br>'.date('d-m-Y', strtotime($event->updated_at)) : '';
                return $event->updated_by ? $event->updated_by.'<br>'.$event->updated_at : '';
            })

            ->addColumn('info',function($event)
            {
                $six_digit_id = $event->event_id_str;
                $dates = date('d-m-Y', strtotime($event->from_date_english)).' - '. date('d-m-Y', strtotime($event->to_date_english));
                $times = date('h:i A', strtotime($event->from_time_english)) .' - '. date('h:i A', strtotime($event->to_time_english));

                $tiers = $event->tiers->toArray();
                $info = View::make('admin.events._tiers_tiles',['tiers'=>$tiers,   'six_digit_id'=>$six_digit_id,'dates'=>$dates,'times'=>$times,'event'=>$event]);
                return $info;
            })
            
            ->addColumn('actions', '  
                <ul class="nav quick-section ">
                    <li class="quicklinks actions"> 
                        <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu left custom-listing-menu" style="overflow-y: auto; overflow-x: hidden;">

                        @if(Auth::user()->can("eventrequests-index"))
                   
                           <li><a class="" href="{{ route("admin.event_requests",$id) }}">Event Requests طلبات التذاكر</a></li>
                         <li class="divider"></li>
                         @endif
                         @if(Auth::user()->can("issuedtickets-index"))
                       
                       <li><a class="" href="{{ route("admin.issued_tickets",$id) }}">View Issued Tickets التذاكر المباعة</a></li>
                       <li class="divider"></li>
                       @endif
                         @if(Auth::user()->can("issuedtickets-create"))
                       
                         <li><a class="" href="{{route("admin.issued_tickets.create",$id)}}">Issue Ticket صرف تذاكر</a></li>
                          <li class="divider"></li>
                          @endif
                           @if(Auth::user()->can("cancelledtickets-index"))
                         <li><a class="" href="{{route("admin.issued_tickets.cancelledticket",$id)}}">View Cancelled Tickets التذاكر الملغاة</a></li>
                         <li class="divider"></li>

                          @endif
                          
                        @if(Auth::user()->can("events-edit"))
                           
                         
                            @if($status == 1)
                            <li>
                                <?php
                                    $enable_disable_message = $selling_off ? "اتاحة البيع؟<br>Enable sale?" : "إيقاف البيع؟ الطلبات المرسلة لن تتأثر بالايقاف<br>Disable sale? Pending requests will not be affected";
                                ?>
                                {!! Form::open(["method" => "POST","route" => ["admin.events.selling", $id],"class" => "deleteaction",\'onsubmit\'=>"$(\'#progressbar\').show()", "data-msg"=>"$enable_disable_message"]) !!}<button class=" change_status">{{$selling_off ? "Enable Selling تشغيل بيع المناسبة" : "Disable Selling ايقاف بيع المناسبة"}}</button>{!! Form::close() !!}
                            </li>
                            <li class="divider"></li>

                            @endif

                            @if($status)
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.events.status", $id],"class" => "unpublished", "data-url"=>URL::to("admin/events/$id/status"),  "data-eventid"=>$id, "data-msg"=>"Un-publish this event now? ايقاف نشر هذه المناسبة؟"]) !!}<button class=" change_status">{{"Un-publish ايقاف النشر"}}</button>{!! Form::close() !!}</li>
                            @else
                            <li>{!! Form::open(["method" => "POST","route" => ["admin.events.status", $id],"class" => "deleteaction", "data-msg"=>"نشر هذه المناسبة الان؟ <br> Publish this event now?"]) !!}<button class=" change_status">{{"Publish نشر"}}</button>{!! Form::close() !!}</li>
                            @endif
                            <li class="divider"></li>
                            <li><a class="" href="{{ URL::route(\'admin.events.addeditevent\',$id) }}">Edit تحرير</a></li>
                            
                        @endif
                        @if(Auth::user()->can("events-destroy") && $status == 0)
                            <li class="divider"></li>
                            <li>{!! Form::open(["method" => "DELETE","route" => ["admin.events.destroy", $id],"class" => "deleteEvent","data-url"=>URL::to("admin/events/$id/destroy"), "data-eventid"=>$id]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                        @endif
                     
                    </ul>
                  </li>
                </ul>', 3)

            ->rawColumns(['check','logo_image','info','updated_by','actions'])
            ->make(true);
    }
public function checkIfEventHasRunningTicket(Request $request){
        $issued_ticket =Issued_ticket::where('event_id','=',$request->eventid)->where('event_request_status_id','=',3)->get();
        $event_request =Event_request::where('event_id','=',$request->eventid)->whereNotIn('event_request_status_id', [3,4])->get();

        $getTotalIssuedTicket = (int)Event::getTotalIssuedTicket2($request->eventid);
        $event_redemption_tickets = Redemption_ticket::where('event_id', '=', $request->eventid)->where('redeem_status','=','0')->count(); // redeem tickets counts
        $final_ERT = $getTotalIssuedTicket - $event_redemption_tickets;


        $flag=false;
        if(count($issued_ticket)>0 || count($event_request)>0){
            $flag=true;
        }

        if($getTotalIssuedTicket>0 && $final_ERT==0)
        {
            $flag=false;
        }
        return response()->json([
            'flag'=>$flag,
        ]);
}

    // public function checkIfEventHasRequestOrTicket(Request $request){
    //     $issued_ticket =Issued_ticket::where('event_id','=',$request->eventid)->where('event_request_status_id','=',3)->get();
    //     $event_request =Event_request::where('event_id','=',$request->eventid)->where('event_request_status_id','!=',3)->get();
    //     $flag="false";
    //     if(count($issued_ticket)> 0 || count($event_request)> 0){
    //         $flag="true";
    //         return $flag ? "true" : "false";
    //     }
    //     return $flag;

    // }

    public function checkIfEventHasRequestOrTicket(Request $request){

        $issued_ticket =Issued_ticket::where('event_id','=',$request->eventid)->count();
        $event_request =Event_request::where('event_id','=',$request->eventid)->count();

        // dd($issued_ticket,$event_request);
        // dd($issued_ticket,$event_request);
        $total_records = $issued_ticket + $event_request;
        return $total_records > 0 ? 'true' : 'false';
    }

    public function checkIfTierHasRequestOrTicket(Request $request){

        $issued_ticket =Issued_ticket_detail::where('tier_id','=',$request->tierid)->get();
        $event_request =Event_request_detail::where('tier_id','=',$request->tierid)->get();
        $flag="false";
        if(count($issued_ticket)> 0 || count($event_request)> 0){
            $flag="true";
            return $flag ? "true" : "false";
        }
        return $flag;

    }
}
