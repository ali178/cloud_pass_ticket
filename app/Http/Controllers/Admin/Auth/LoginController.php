<?php
namespace Ticketing\Http\Controllers\Admin\Auth;

use Ticketing\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;


//    public function username()
//    {
//        return 'email';
//    }
    protected $redirectTo = '/dashboard';
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function __construct()
    {
       $this->middleware('guest.admin', ['except' => 'logout']);
    }





    protected function authenticated(Request $request, $user)
    {

       // print_r($user);

       // if ($user->hasRole('superadmin')) {

            // if($user->can('redemption-login')){
           // return redirect('/admin/dashboard');
            // }else{
            //  Auth::guard('redemptions')->logout();
            // $this->guard()->logout();
            //  return redirect('/redemption');
            // }
      //  }

    }




    /**
     * LoginController constructor.
     */


    /**
     * function to show Login Formt
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        if (Auth::guard('admins')->check())
        {
            return redirect('/admin');
        }
       // return view('admin.auth.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admins');
    }

    /**
     * Log the user out of the application.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        Auth::guard('admins')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/admin/login');
    }

    public function username()
    {
        return 'username';
    }
}