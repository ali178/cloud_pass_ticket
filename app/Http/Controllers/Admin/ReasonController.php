<?php

namespace Ticketing\Http\Controllers\Admin;

use Ticketing\Models\Reason;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use Ticketing\Http\Controllers\Controller;
use Session,Input,DB,Debugbar,Image,Mail,File;

class ReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('admin.reasons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.reasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Reason::create($input);
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        return redirect('admin/reasons');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reason = Reason::findOrFail($id);
        return view('admin.reasons.edit',compact('reason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $reason = Reason::findOrFail($id);
        $reason->fill($input)->save();
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        return redirect('admin/reasons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reason = Reason::find($id);
        if ($reason) {
            $reason->delete();
        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        return redirect()->back();
    }

    public function bulkdelete(Request $request) {
        $ids = explode(',', $request['ids']);

        $deleted = 0;
        foreach ($ids as $id) {
            $reason = Reason::find($id);
            $reason->delete();
            $deleted++;
        }

        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    public function bulkshow(Request $request) {
        $ids = explode(',', $request['ids']);
        Reason::whereIn('id', $ids)->update(['status' => 1]);

        if (count($ids) < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }

        return redirect()->back();
    }

    public function bulkhide(Request $request) {
        $ids = explode(',', $request['ids']);
        Reason::whereIn('id', $ids)->update(['status' => 0]);

        if (count($ids) < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();
    }

    public function datatable() {
        return Datatables::of(Reason::select('id', 'title_english', 'title_arabic','description_english','description_arabic','status','type'))
        ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
        ->editColumn('status',function($reason){
            return Reason::$status_options[$reason->status];
        })
        ->editColumn('type',function($reason){
            return Reason::$type[$reason->type];
        })
        ->addColumn('actions', '
            <ul class="nav quick-section">
              <li class="quicklinks actions">
                <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                    <i class="fa fa-gear"></i>
                </a>
                <ul aria-labelledby="user-options" role="menu" class="dropdown-menu custom-listing-menu">
                      @if(Auth::user()->can("reasons-edit"))
                        <li><a class="" href="{{ URL::route(\'admin.reasons.edit\',$id) }}">{!! __("reasons.menu.edit") !!}</a></li></li>
                        <li class="divider"></li>
                      @endif
                      @if(Auth::user()->can("reasons-destroy"))
                        <li>{!! Form::open(["method" => "DELETE","route" => ["admin.reasons.destroy", $id],"class" => "inline deleteaction"]) !!}<button class="delete-label-red">{!! __("reasons.menu.delete") !!}</button>{!! Form::close() !!}</li>
                      @endif
                </ul>
              </li>
            </ul>
        ')
        ->rawColumns(['check','actions'])
        ->make(true);
    }
}
