<?php

namespace Ticketing\Http\Controllers\Admin;
use Ticketing\Models\Setting;
use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Session;
class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        $setting = $setting ? $setting : Setting::create(['vat_value'=>0]);
        return view('admin.settings.index',['setting' => $setting]);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $setting = Setting::find($id);
        if($setting)
        {
            $setting->fill($input)->save();
        }
        Session::flash('flash_message', 'تم قبول طلبك بنجاح');


        return redirect()->back();
    }
}
