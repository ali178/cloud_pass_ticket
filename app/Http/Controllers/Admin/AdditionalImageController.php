<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Models\AdditionalImages;
use Ticketing\Models\Event;
use Ticketing\Models\Tier;
use yajra\Datatables\Datatables;
use Session,Input,DB,Debugbar,Image,Mail,File,Response;

class AdditionalImageController extends Controller
{
    
    public function uploadimage(Request $request){
        $input = $request->all();
        $event = Event::find($input['event_id']);
        $message = '';
        $sequence = AdditionalImages::orderBy('sequence','desc')->pluck('sequence')->first();
        if ($sequence) {
            $maxSequence = $sequence;
            $input['sequence'] = $maxSequence + 1;

        } else {
            $input['sequence'] = + 1;
        }

        if($event)
        {
            $additional_images_data = [];
            $uploadimage = Event::uploadImage(['object' => '','input' => $input,'image_name' => 'image', 'path' => 'uploads/additionalimages/', 'number' => '3', 'mode'=>'wh']);

             $additional_images_data['sequence'] = isset($input['sequence']) ? $input['sequence'] : '';
             $additional_images_data['image'] = $uploadimage;
             $additional_images_data['caption'] = isset($input['caption']) ? $input['caption'] : '';
             $event->additional_images()->create($additional_images_data);  
             $message = 'تم قبول طلبك بنجاح';


        }else{
            $message = 'event not found';
        }
        return $message;
    }

    public function editcaption(Request $request)
    {
        $input = $request->all();
        $input['caption'] = isset($input['caption']) ? $input['caption'] : '';

        $image = AdditionalImages::find($input['image_id']);
        if ($image) {
            $image->fill($input)->save();
        }
        return "data saved";
    }

    public function show($id)
    {
       //
    }

    public function delimage(Request $request)
    {
        $input = $request->all();
        $image = AdditionalImages::find($input['id']);
        if ($image) {
            $image->delete();
        }
        return 'deleted';
    }

    public function bulkimagedelete(Request $request){
        $ids = explode(',', $request['ids']);
        $deleted = 0;
        $message = '';
        foreach ($ids as $imageable_id) {
            $image = AdditionalImages::find($imageable_id);
            if ($image) {
                $image->delete();
                $deleted++;
            }
        }
         
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();

    }

    public function datatable(Request $request) {
        $imageable_id =(Integer) $request->query('imageable_id');
        $imageable_type = 'Ticketing\Models\Event';

        return Datatables::of(AdditionalImages::select('id', 'image','caption','imageable_id','sequence','imageable_type')
        	->where('imageable_type',$imageable_type)
            ->where('imageable_id',$imageable_id)
            ->orderBy('sequence', 'desc')
            )
            
            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
            ->editColumn('image','<a class="gallery-item" data-fancybox="images" data-gallery="" href="{{ asset($image)}}"><div class="img-thumbnail" style=" width:90px;height:90px; display: inline-block; vertical-align: top; position: relative;"><img src="{{ asset($image)}}" class="listing-icon" style="max-width:80px; max-height:80px; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0; right: 0; margin: auto;"/></div></a>')

            ->addColumn('actions', '  
                <ul class="nav quick-section ">
                  	<li class="quicklinks actions"> 
                    	<a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                        	<i class="fa fa-gear"></i>
                    	</a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu custom-listing-menu">

                        @if(Auth::user()->can("additionalimages-destroy"))
                            <li><a class="btn btn-link delete-label-red" onclick="$(\'#delButton\').attr(\'data-id\',\'{{$id}}\');$(\'#delImageMessage\').modal(\'toggle\');">Delete حذف</a></li>
	                    @endif
                    </ul>
                  </li>
                </ul>', 3)
             ->rawColumns(['check','actions','image'])
            ->make(true);
    }
    public function reorder(Request $request)
    {
        $input = $request->all();
        $ids = $input['ids'];
        if ($ids == '') {
            return Response::json(['Message' => 'Not Found.'], 404);
        }

        $itemsIds = explode(',', $ids);
        $maxSequence = AdditionalImages::getMaxSequenceByIds($ids);
        $sequence = $maxSequence;
        foreach ($itemsIds as $item) {
            $news_item = AdditionalImages::find($item);
            if($news_item)
            {
                $news_item->sequence = $sequence;
                $news_item->save();
                $sequence--;
            }
        }
        return Response::json(['Message' => 'Success.'], 200);
    }
}
