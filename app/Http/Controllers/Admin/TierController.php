<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Models\Event_request_detail;
use Ticketing\Models\Issued_ticket_detail;
use yajra\Datatables\Datatables;
use Ticketing\Models\Tier;
use Ticketing\Models\Event;
use Session,Input,DB,Debugbar,Image,Mail,File,Response;

class TierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bulkdelete(Request $request){
        $ids = explode(',', $request['ids']);
        $deleted = 0;
        $message = '';
        foreach ($ids as $id) {
            $tier = Tier::find($id);
            if ($tier) {
                $tier->delete();
                $deleted++;
            }
        }
         
        if ($deleted < 2) {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        } else {
            Session::flash('flash_message', 'تم قبول طلبك بنجاح');

        }
        return redirect()->back();

    }

    public function deltier(Request $request)
    {
        $input = $request->all();
        $tier = Tier::find($input['id']);
        if ($tier) {
            $tier->delete();
        }
        return 'deleted';
    }

    public function addtier(Request $request){

        $input = $request->all();
        $tier_id = $input['edit_tier_id'];
        $message = '';

        $sequence = Tier::pluck('sequence')->first();
        if ($sequence) {
            $maxSequence = $sequence;
            $input['sequence'] = $maxSequence + 1;
        } else {
            $input['sequence'] = + 1;
        }

        $input['price'] = isset($input['price']) ? $input['price'] : 0;
        $input['total_quantity'] = isset($input['total_quantity']) ? $input['total_quantity'] : 0;

        if($tier_id)
        {

            $tier = Tier::find($tier_id);

            if($tier)
            {
                $tier->fill($input)->save();
                $message = 'successfully update';
            }else{
                $message = 'tier not found';   
            }
        }else{
            $event_id = $input['event_id'];
            $event = Event::find($event_id);
            if($event)
            {

                $event->tiers()->create($input);
                
            }else{
                $message = 'event not found';
            }
        }
        return $message;
    }

    public function datatable(Request $request) {
        $event_id =(Integer) $request->query('event_id');

        return Datatables::of(Tier::select('id', 'title_english','title_arabic','description_english','description_arabic','header_color','price','total_quantity','event_id','sequence','total_quantity_type','price_type')
            ->where('event_id',$event_id)
            ->orderBy('sequence', 'desc')
            )
            
            ->addColumn('check', '<input type="checkbox" class="checkboxes" name="ids[]" value="{{ $id }}" />', 0)
            ->editColumn('title_english',function($tier){
                return '<span style="color:#'.$tier->header_color.'">'.($tier->title_english ? $tier->title_english : '').'</span>';
            })

            ->editColumn('title_arabic',function($tier){
                return '<span style="color:#'.$tier->header_color.'">'.($tier->title_arabic ? $tier->title_arabic : '').'</span>';
            })

            ->editColumn('price',function($tier){
                return $tier->price_type ? $tier->price : 'FREE';
            })

            ->editColumn('total_quantity',function($tier){
                return $tier->total_quantity_type ? $tier->total_quantity : 'OPEN';
            })

            ->addColumn('actions', '  
                <ul class="nav quick-section ">
                    <li class="quicklinks actions"> 
                        <a id="gear-icon" class="context_menu_btn" href="#" class="dropdown-toggle  pull-right " data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                        </a>
                    <ul aria-labelledby="user-options" role="menu" class="dropdown-menu custom-listing-menu">

                        @if(Auth::user()->can("tiers-edit"))
                            <li><a class="btn btn-link" onclick="editTier(\'{{$id}}\')" data-title_english="{{$title_english}}" data-title_arabic="{{$title_arabic}}" data-description_english="{{$description_english}}" data-description_arabic="{{$description_arabic}}" data-header_color="{{$header_color}}" data-price="{{$price}}" data-total_quantity="{{$total_quantity}}" data-total_quantity_type="{{$total_quantity_type}}" data-price_type="{{$price_type}}" id="{{$id}}_edit_tier">Edit تحرير</a></li>
                        @endif
                        @if(Auth::user()->can("tiers-destroy"))
                            <li class="divider"></li>
                            <li>{!! Form::open(["method" => "DELETE","route" => ["admin.tiers.deltier", $id],"class" => "deleteTier","data-tierid"=>$id]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>

                          
                        @endif
                    </ul>
                  </li>
                </ul>', 3)
             ->rawColumns(['check','actions','title_english','title_arabic'])
            ->make(true);
    }

    public function reorder(Request $request)
    {
        $input = $request->all();
        $ids = $input['ids'];
        if ($ids == '') {
            return Response::json(['Message' => 'Not Found.'], 404);
        }

        $itemsIds = explode(',', $ids);
        $maxSequence = Tier::getMaxSequenceByIds($ids);
        $sequence = $maxSequence;
        foreach ($itemsIds as $item) {

            $news_item = Tier::find($item);
            if($news_item)
            {
                $news_item->sequence = $sequence;
                $news_item->save();
                $sequence--;
            }

        }
        return Response::json(['Message' => 'Success.'], 200);
    }




}
