<?php

namespace Ticketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Models\Permission;

class PermissionController extends Controller
{
    public function index() {
        // load the view and pass the permissions
        $permissions = $this->permissionsArray();
        foreach ($permissions as $module => $actions) {
            foreach ($actions as $key => $value) {
                $permission = Permission::firstOrNew(['name'=>$key]);
                $permission->name = $key;
                $permission->display_name = $value;
                $permission->save();
            }
        }
        return redirect()->back();
    }
}
