<?php

namespace Ticketing\Http\Controllers\Redemption;

use Ticketing\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ticketing\Models\Event;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.redemption');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::guard('redemptions')->user();
        if($current_user && $current_user->assign_all_events == 0)
        {
            $events_ids = $current_user->volunteer_events()->pluck('event_id');
            $events = Event::whereIn('id', $events_ids)->where('status',1)->where('is_draft',0)->orderBy('id','desc')->get();
        }else{
            $events = Event::where('status',1)->where('is_draft',0)->orderBy('id','desc')->get();
        }
        return View('redemption.events.index',compact('events'));
    }
}
