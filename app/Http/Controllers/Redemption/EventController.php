<?php

namespace Ticketing\Http\Controllers\Redemption;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Ticketing\Http\Controllers\Controller;
use Ticketing\Models\Event;
use Ticketing\Models\Issued_ticket;
use Ticketing\Models\Redemption_ticket;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.redemption');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {

        try {
            $event = Event::where('event_id_str',$id)->first();
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'ticket_no' => 'required',
                ]);
                $redemption_ticket=Redemption_ticket::where('event_id','=',$event->id)->where('ticket_no','=',$request->ticket_no)->first();
                if($redemption_ticket){

                    $issued_ticket=Issued_ticket::find($redemption_ticket->issued_ticket_id);
                    if($issued_ticket->event_request_status_id == 3)
                    {
                        // if($redemption_ticket->redeem_status=='0'){
                        //     Session::flash('flash_message', 'Ticket has already been redeemed<br>تم بالفعل استرداد قيمة التذكرة');
                        // }else {
                        //     // $redemption_ticket->redeem_status = '0';
                        //     // $redemption_ticket->save();
                        //     // // $issued_ticket=Issued_ticket::find($redemption_ticket->issued_ticket_id);
                        //     $flag=true;
                        // }
                        $flag=true;
                    }else{
                        Session::flash('flash_message', 'Ticket is cancelled <br> إدخال معرف البطاقة غير صالح أو تم بالفعل إنهاء الحدث');
                    }

                    // if($redemption_ticket->redeem_status=='0'){
                    //     Session::flash('flash_message', 'Ticket has already been redeemed<br>تم بالفعل استرداد قيمة التذكرة');
                    // }else {
                    //     $redemption_ticket->redeem_status = '0';
                    //     $redemption_ticket->save();
                    //     $issued_ticket=Issued_ticket::find($redemption_ticket->issued_ticket_id);
                    //     $flag=true;
                    // }

                }else{
                    Session::flash('flash_message', 'Enter ticket id is invalid or the event has already been ended<br> إدخال معرف البطاقة غير صالح أو تم بالفعل إنهاء الحدث');
                }
            }
            
            return View('redemption.events.searchticket',compact('event','flag','redemption_ticket','issued_ticket'));
        }
        catch (\Exception $e) {
            abort(404);
        }
        
    }

    public function redeem(Request $request, $id)
    {
        $event = Event::where('event_id_str',$id)->first();
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'ticket_no' => 'required',
            ]);
            $redemption_ticket=Redemption_ticket::where('event_id','=',$event->id)->where('ticket_no','=',$request->ticket_no)->first();
            if($redemption_ticket){

                $issued_ticket=Issued_ticket::find($redemption_ticket->issued_ticket_id);
                if($issued_ticket->event_request_status_id == 3)
                {
                    if($redemption_ticket->redeem_status=='0'){
                        Session::flash('flash_message', 'Ticket has already been redeemed<br>تم بالفعل استرداد قيمة التذكرة');
                    }else {
                        $redemption_ticket->redeem_status = '0';
                        $redemption_ticket->save();
                        $flag=true;
                        Session::flash('flash_message', 'Ticket successfully redeemed<br>تم استبدال التذكرة بنجاح<br><br>'.$redemption_ticket->ticket_no);
                    }
                }else{
                    Session::flash('flash_message', 'Ticket is cancelled <br> إدخال معرف البطاقة غير صالح أو تم بالفعل إنهاء الحدث');
                }
            }else{
                Session::flash('flash_message', 'Enter ticket id is invalid or the event has already been ended<br> إدخال معرف البطاقة غير صالح أو تم بالفعل إنهاء الحدث');
            }
        }
        
        return redirect()->back();
        // return View('redemption.events.searchticket',compact('event','flag','redemption_ticket','issued_ticket'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
