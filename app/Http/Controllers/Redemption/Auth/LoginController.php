<?php
namespace Ticketing\Http\Controllers\Redemption\Auth;

use Ticketing\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ticketing\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;


//    public function username()
//    {
//        return 'email';
//    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function __construct()
    {
        $this->middleware('guest.redemption', ['except' => 'logout']);
    }

    protected $redirectTo = '/redemption';
    protected $guard = 'redemptions';

    public function username()
    {
        return 'username';
    }
    
    protected function authenticated(Request $request, $user)
    {

        if ( $user->hasRole('redemption') || $user->hasRole('superadmin')) {

           if($user->can('redemption-login')){
               return redirect('/redemption');
           }else{
               Auth::guard('redemptions')->logout();
              // $this->guard()->logout();
               return redirect('/redemption');
           }
        }

    }

    protected function credentials(Request $request)
    {
        return ['username' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1, 'type' => User::$TYPE['Volunteer']];
    }

    /**
     * LoginController constructor.
     */


    /**
     * function to show Login Formt
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        if (Auth::guard('redemptions')->check())
        {
            return redirect('/redemption/home');
        }
        return view('redemption.auth.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('redemptions');
    }

    /**
     * Log the user out of the application.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        $this->guard('redemptions')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/redemption/login');
    }
}