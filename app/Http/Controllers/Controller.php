<?php

namespace Ticketing\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use Ticketing\Models\Setting;
use Ticketing\Models\Document;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     public function __construct() {
        set_time_limit (60); 
        // Retun readable names for you controller names in permissions
        View::share('readable', [
            'roles' => 'Roles',
            'settings' => 'Settings',
            'documents' => 'Documents',
            'users' => 'Users',
            'events' => 'Events',
            'additionalimages' => 'Additional Images',
            'tiers' => 'Tiers',
            'reasons' => 'Reasons',
            'eventrequests' => 'Event Requests',
            'issuedtickets' => 'Issued Tickets'
        ]);

        $setting  = Setting::first();

        $tnc_body = ['tnc_arabic'=>'', 'tnc_english'=>'']; 
        $document = Document::where('identifier','generic_tnc')->where('show',1)->first();
        if($document)
        {
          $tnc_body = ['tnc_arabic'=>$document->description_arabic, 'tnc_english'=>$document->description]; 
        }

        View::share('shared_site_vars', ['settings'=>$setting, 'tnc_body'=>$tnc_body]);
     }

    public static function permissionsArray() {
        return [
           
            'dashboard' => [
                'dashboard' => ''
            ],'roles' => [
                'roles-index' => 'List',
                'roles-create' => 'Create',
                'roles-edit' => 'Edit',
                'roles-destroy' => 'Delete'
            ],
            'users' => [
                'users-index' => 'List',
                'users-create' => 'Create',
                'users-edit' => 'Edit',
                'users-destroy' => 'Delete'
            ],
            'events' => [
                'events-index' => 'List',
                'events-create' => 'Create',
                'events-edit' => 'Edit',
                'events-destroy' => 'Delete'
            ],
            'additionalimages' => [
//                'additionalimages-index' => 'List',
                'additionalimages-create' => 'Create',
                'additionalimages-edit' => 'Edit',
                'additionalimages-destroy' => 'Delete'
            ],
            'tiers' => [
//                'tiers-index' => 'List',
                'tiers-create' => 'Create',
                'tiers-edit' => 'Edit',
                'tiers-destroy' => 'Delete'
            ],
            'reasons' => [
                'reasons-index' => 'List',
                'reasons-create' => 'Create',
                'reasons-edit' => 'Edit',
                'reasons-destroy' => 'Delete'
            ],
            'eventrequests' => [
                'eventrequests-index' => 'List',
                'eventrequests-sendbankinfo' => 'Send Bank Info',
                'eventrequests-issueticket' => 'Issue Ticket',
                'eventrequests-reject' => 'Reject',
                'eventrequests-destroy' => 'Delete',
                'eventrequests-eventrequestdetail' => 'Event Request Detail'
            ],
            'issuedtickets' => [
                'issuedtickets-index' => 'List',
                'issuedtickets-create' => 'Create',
                'issuedtickets-reject' => 'Reject',
                'issuedtickets-destroy' => 'Delete',
                'issuedtickets-issuedticketdetail' => 'Issued Ticket Detail',

            ],
            'Cancelled Tickets' => [
                'cancelledtickets-index' => 'Cancelled Tictets',
                'cancelledtickets-markasrefund' => 'Mark as Refund',
                'cancelledtickets-markaspending' => 'Mark as Pending'
            ],
            'Redemption' => [
                'redemption-login' => 'Login',
                'redemption-search' => 'Search',
            ],
            'documents' => [
                'documents-index' => 'List',
                'documents-create' => 'Create',
                'documents-edit' => 'Edit',
                'documents-destroy' => 'Delete'
            ],
            'settings' => [
                'settings-index' => 'List',
                'settings-create' => 'Create',
                'settings-edit' => 'Edit',
                'settings-destroy' => 'Delete'
            ],

        ];
    }
}
