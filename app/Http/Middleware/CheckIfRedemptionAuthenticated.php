<?php

namespace Ticketing\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfRedemptionAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth=Auth::guard('redemptions');
        if (!$auth->check()) {
            return redirect('/redemption/login');
        }
        return $next($request);
    }
}
