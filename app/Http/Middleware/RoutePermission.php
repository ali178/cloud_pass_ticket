<?php

namespace Ticketing\Http\Middleware;
use Auth,Session;
use \Ticketing\Models\Role;
use Closure;

class RoutePermission
{
     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected static $except = [];
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        if (Auth::user()) {

            // if request is ajax them allow the request through
            if ($request->ajax()) {
                return $next($request);
            }
            $role = Auth::user()->get_role();
            $route_name = $route->getName();


           // echo $route_name;
           // die;

            if ($route_name) {
                $arr = explode('.', $route_name);
                $valid_actions = ['index', 'create', 'edit', 'destroy'];
                // if route is in $except array then we do not need to check permissions and also route action should be in valid actions
                if($arr[0]=='admin'){
                if (sizeof($arr)>1 && !in_array($arr[0], self::$except) && in_array($arr[1], $valid_actions) && $arr[0] != 'permissions') {
                    $has = $role->hasPermission($arr[0] . '-' . $arr[1]);
                } else {
                    $has = true;
                   // print_r($has);
                   // die;
                }
                if (!$has) {
                    Session::flash('flash_message_status', true);
                    Session::flash('flash_message', 'You are not authorized to complete this action. Please Contact to admin for further information.');
                    return redirect('/admin');
                }
                }else{
                    return redirect('/redemption');
                }
            }
           
        } else {
            return redirect('/admin');
        }
        return $next($request);
    }
}
