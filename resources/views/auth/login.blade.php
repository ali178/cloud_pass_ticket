@extends('layouts.app')

@section('content')
<style>
    body > .page-container > .page-content{
        background-color: white;
    }
</style>
<div class="row login-container" style="margin-top: 3%;">
    {{--<div class="col-md-2 col-md-offset-3" style="height: 204px;">--}}
        {{--<h2>--}}
            {{--{{ __('Login') }} to {{ config('app.name', 'Laravel') }}--}}
        {{--</h2>--}}
        {{--<p>Use your email to sign in.</p>--}}
        {{--<br>--}}
    {{--</div>--}}
    <div class="col-md-4 col-md-offset-4 col-xs-12">
        <div class="form-group" style="text-align: center">
            <img src="{{asset('assets/images/ticketing_logo.png')}}" class="bb-logo" style="width: 50%;height: auto">
            <br/>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-4 col-xs-12">

      <br>
      <form method="POST" action="{{route('login')}}">
        @csrf

        <!-- <div class="row">
          <div class="form-group col-md-12 col-sm-12">
            <label class="form-label" for="email">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
          </div>
        </div> -->
        <div class="row">
          <div class="form-group col-md-12 col-sm-12">
            <label class="form-label" for="email">Username</label>
            <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" autofocus>

                @if ($errors->has('username'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-12">
            <label class="form-label" for="password">{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
          </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="control-group  col-md-12">--}}
                {{--<div class="checkbox checkbox check-success"> --}}
                    {{--<!-- <a href="{{ route('password.request') }}">Forgot Your Password?</a>&nbsp;&nbsp; -->--}}
                    {{--<input type="checkbox" name="remember" id="checkbox1" {{ old('remember') ? 'checked' : '' }}>--}}
                    {{--<label for="checkbox1">Keep me reminded </label>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-cons pull-right" type="submit">{!!  __('Login') !!}   دخول  </button>
            </div>
        </div>

       <!--  <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </div>
        </div> -->
    </form>

    </div>
  </div>

@endsection
