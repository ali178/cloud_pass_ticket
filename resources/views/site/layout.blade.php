<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<title>Ticket Pass</title>
<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}"/>
<link href="{{ asset('css/site.css') }}" rel="stylesheet">
  <script type="text/javascript">
      var base_path = '{{URL::to('/')}}';
  </script>
</head>
<body class="public_site site-background-image" style="background-image: url('{{ asset('assets/images/background.png') }}'); background-size: 22% !important;">
	
  	@yield('content')

    <!-- general tnc modal -->
    <div class="modal fade" id="general_tnc_modal" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          
          <div class="modal-body">
            <center>
              <p class="arabic_font">حين الأراضي البولندي عن, بقعة اللا ليتسنّى كل الا. عرض لهيمنة جديداً استرجاع أي, جعل عل خلاف الحرة وباستثناء. نفس ووصف أثره، ما, حين أي غرّة، الشرقية. ذلك الضروري ابتدعها الكونجرس ثم, من لمّ فرنسا بالرّد شموليةً.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
            </center>
          </div>

          <div class="modal-footer">
            <center>
              <button type="button" class="btn btn-success  custom_btn" data-dismiss="modal" style="background: rgb(137,137,137);">
                <span>Ok</span>
                <span class="arabic_font">موافق</span></button>
            </center>
          </div>
        </div>
      </div>
    </div>

    <!-- validation error modal -->
    <div class="modal fade" id="validation_error_modal" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          
          <div class="modal-body">
            <center>
              <p class="arabic_font">يجب تعبئة معلومات التواصل بشكل كامل وصحيح. الرجاء التأكد من زن جميع المعلومات مدخلة. معلومات حديثة وصحيحة مهم جداً لإستكمال طلبك. تحذف جميع الطلبات الغير مكتملة او غير صحيحة</p>
              
              <p>Contact information fields must be filled. Please check that all fields are enetered correctly. Correct and current contact information is essential in completing requests. Requests sent with incorrect or missing details will be deleted</p>
            </center>
          </div>

          <div class="modal-footer">
            <center>
              <button type="button" class="btn btn-success  custom_btn" data-dismiss="modal" style="border: unset;outline: none;background: rgb(137,137,137);">
                <span>Ok</span>
                <span class="arabic_font">موافق</span></button>
            </center>
          </div>
        </div>
      </div>
    </div>
  	
  	<script src="{{ asset('js/admin.js') }}"></script>
  	<script type="text/javascript">
  		$.ajaxSetup({
      	headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
  		});
  	</script>
  	@yield('page_script')
</body>
</html>