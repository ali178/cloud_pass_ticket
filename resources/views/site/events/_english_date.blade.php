
<h3 class="event_listing_text margin_top_fix1">

    <div class="row">
        <div class="col-xs-5 english_date_from" style="margin-right: -10px;">
            {{ date('d/m/Y', strtotime($event->from_date_english)) }}
        </div>
        <div class="col-xs-2"> <center>&mdash;</center> </div>
        <div class="col-xs-5 english_date_to" style="margin-left: -13px;">
            {{ date('d/m/Y', strtotime($event->to_date_english)) }}
        </div>
    </div>
</h3>
