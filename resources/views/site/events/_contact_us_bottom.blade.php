<?php
    $setting = $shared_site_vars['settings'];
?>
<center>
    @if($setting && ($setting->phone || $setting->email))
        <span class="contact_us_text contact_us_header"><strong>TicketPass <span class="arabic_font">تيكيت باس</span></strong></span>
        <br>
        @if($setting && $setting->phone)
            <a href="tel:{{$setting->phone}}"><span class="contact_us_text contact_us_header">{{$setting->phone}}</span></a>
            <br>
        @endif

        @if($setting && $setting->email)
            <a href="mailto:{{$setting->email}}" target="_top"><span class="contact_us_text contact_us_header">{{$setting->email}}</span></a>
        @endif
        <br><br>
    @endif

    <span class="tnd_copyright">
        <!-- <a href="javascript:void(0)" class="underline" data-toggle="modal" data-target="#general_tnc_modal" style="color: #B1B1B1 !important;"><span class="underline">Terms & Condtions <span class="arabic_font">الأحكام والشروط</span></span></a> -->
        <a href="{{ URL::route('site.events.tnc')}}" target="_blank" class="underline {{ $shared_site_vars['tnc_body']['tnc_arabic'] == '' && $shared_site_vars['tnc_body']['tnc_english'] == '' ? 'disable_this' : '' }}" style="color: #B1B1B1 !important;"><span class="underline">Website Terms & Conditions <span class="arabic_font">شروط و احكام الموقع</span></span></a>

        <br><br>

        @if($setting && $setting->copyright_english)
            <span style="color: #9B9B9B;">{{$setting->copyright_english}}</span>
            <br>
        @endif
        @if($setting && $setting->copyright_arabic)
            <span class="arabic_font" style="color: #9B9B9B;">{{$setting->copyright_arabic}}</span>
        @endif
    </span>
</center>