@if( ($event->to_time_arabic && $event->from_time_arabic) || ($event->to_date_arabic && $event->from_date_arabic))
    <div class="row grey_color dt_class">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <?php echo View::make('site.events._english_date',['event'=>$event]); ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            @if($event->to_date_arabic && $event->from_date_arabic)
                <h3 class="arabic_font right-align event_listing_text margin_top_fix1" style="margin-right: -24px;">

                    <div class="row">
                        <div class="col-xs-5 right_date_time_from arabic_date_to">
                            {{ $event->to_date_arabic }}
                        </div>
                        <div class="col-xs-2"> <center>&mdash;</center> </div>
                        <div class="col-xs-5 right_date_time_to arabic_date_from">
                            {{ $event->from_date_arabic }}
                        </div>
                    </div>

                </h3>
            @endif
        </div>
    </div>

    <div class="row grey_color dt_class">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <?php echo View::make('site.events._english_time',['event'=>$event]); ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 ">
            @if($event->to_time_arabic && $event->from_time_arabic)
                <h3 class="arabic_font right-align event_listing_text margin_top_fix2" style="margin-right: -24px;">

                    <div class="row">
                        <div class="col-xs-5 right_date_time_from arabic_time_to">
                            {{ $event->to_time_arabic }}
                        </div>
                        <div class="col-xs-2"> <center>&mdash;</center> </div>
                        <div class="col-xs-5 right_date_time_to arabic_time_from">
                            {{ $event->from_time_arabic }}
                        </div>
                    </div>

                </h3>
            @endif
        </div>
    </div>
@else
    <div class="row dt_class">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
            <?php echo View::make('site.events._english_date',['event'=>$event]); ?>
            <?php echo View::make('site.events._english_time',['event'=>$event]); ?>
        </div>
    </div>
@endif