@extends('site.layout')
@section('content')
<style type="text/css">
  button:focus {outline:0;}
</style>
<div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container">
        <div class="elevation-3dp" style="padding-top: 0px;">
        @include('site.sections.header')
      	@include('site.events._slider')


      	<div class="listing-div detail_main_div tickets_view" style="padding-top: 20px !important;">
            {!! Form::open(array('route' => ['events.store',$event->id], 'id'=>'personal_info_form')) !!}
            <input type="hidden" name="submit" value="tier">
              <div class="row">
                  <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                      <!-- <center>

                          <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px">
                      </center> -->
                      @include('site.events._banner_logo')
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
                      <span>{{date('d/m/Y h:i A',strtotime($event->updated_at))}}</span>
                  </div>
              </div>

              <center>
                  <h3 class="arabic_font event_detail_title" style="margin-bottom: -10px;">
                      {{$event->title_arabic}}
                  </h3>
                  <h3 class="event_detail_title">
                      {{$event->title_english}}
                  </h3>

                  <a href="{{ URL::route('site.events.detail',['event_id'=>$event->id]) }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;margin: 20px 0px 30px 0px;">
                      <span>Back</span>
                      <span class="arabic_font">السابق</span>
                  </a>
                  <span>&nbsp;</span>
                  <button type="submit" class="btn btn-success btn-lg btnNext" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; margin: 20px 0px 30px 0px;  font-size: 14px;outline: none;"  disabled="disabled">
                      <span>Next</span>
                      <span class="arabic_font">التالى</span>
                  </button>

              </center>


              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <center>
                   <h5 class="arabic_font content_text">مجموع التذاكر</h5>
                   <h5 class="content_text">Total Tickets</h5>
                   {{--<h3 style="margin-top: 10px;"><strong id="total_qty_all_selected_tickets">{{$event->totalQuantyByEventId()}}</strong></h3>--}}
                      <h3 style="margin-top: 10px;"><strong id="total_qty_all_selected_tickets">0</strong></h3>
                      <input type="hidden" id="hide_total_qty_all_selected_tickets" value="0">
                  </center>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <center>
                   <h5 class="arabic_font content_text">المبلغ الإجمالي</h5>
                   <h5 class="content_text">Total Amount</h5>
                   {{--<h3 style="margin-top: 10px;"><strong>{{$event->totalAmountByEventId()}}</strong></h3>--}}
                      <h3 style="margin-top: 10px;"><strong id="total_price_all_selected_tickets">0.00</strong></h3>
                      <input type="hidden" id="hide_total_price_all_selected_tickets" value="0">
                  </center>
                </div>
                <center>
                  <span class="vat_inclusive arabic_font">جميع الاسعار تتضمن الضريبة المضافة</span>
                  <br>
                  <span class="vat_inclusive">All Prices Are Inclusive of VAT</span>
                </center>
              </div>
              <?php
                $vat = isset($shared_site_vars['settings']) && $shared_site_vars['settings']['vat_value'] ? $shared_site_vars['settings']['vat_value'] : 0;
              ?>
              <input type="hidden" name="current_vat" value="{{$vat}}">
              <br>
              @include('site.events._tiers')

              <center>

                <a href="{{ URL::route('site.events.detail',['event_id'=>$event->id]) }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;margin: 20px 0px 25px 0px;">
                    <span>Back</span>
                    <span class="arabic_font">السابق</span>
                </a>
                <span>&nbsp;</span>
                <button type="submit" class="btn btn-success btn-lg btnNext" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; margin: 15px 0px 20px 0px; font-size: 14px;outline: none;" disabled="disabled">
                  <span>Next</span>
                  <span class="arabic_font">التالى</span>
                </button>
              </center>

            {!! Form::close() !!}

      	</div>

        @include('site.events._contact_us_bottom')
        <br>
      </div>
      @include('site.sections.footer')
    </div>
</div> 
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function ($) {
  spinnerInitialise();

  $('.minus').attr("src","{{asset('assets/images/minus_disable.png')}}");
  $('.minus').css("pointer-events", "none");
  $('.delete_icon').attr("src","{{asset('assets/images/delete_disable.png')}}");
  $('.delete_icon').css("pointer-events", "none");
});

function clearTierInput(tier_id)
{
     // alert(tier_id);
    var per_ticket_price= Number($('#price'+tier_id).val());
    var ticket_count= Number($('#total_qty'+tier_id).val());

   // alert(per_ticket_price);
   // alert(ticket_count);

    var total_subtruct_sum= per_ticket_price * ticket_count;
    var total_qty =Number($('#hide_total_qty_all_selected_tickets').val());
    var total_price =Number($('#hide_total_price_all_selected_tickets').val());

   // alert(total_qty);
   // alert(total_price);

    $('#hide_total_qty_all_selected_tickets').val(total_qty - ticket_count);
    $('#hide_total_price_all_selected_tickets').val(total_price - total_subtruct_sum);
    $('#total_qty_all_selected_tickets').html(total_qty - ticket_count);
    $('#total_price_all_selected_tickets').html(Number(total_price - total_subtruct_sum).toFixed(2));

  $('#tier'+tier_id+' input[type=number]').val(0);
  calculate_total_amount_of_tickets_menus(tier_id);

  $('.minus_'+tier_id).attr("src","{{asset('assets/images/minus_disable.png')}}");
  $('.minus_'+tier_id).css("pointer-events", "none"); 

  $('#delete_'+tier_id).attr("src","{{asset('assets/images/delete_disable.png')}}");
  $('#delete_'+tier_id).css("pointer-events", "none");

  $('.plus_'+tier_id).attr("src","{{asset('assets/images/plus.png')}}");
  $('.plus_'+tier_id).css("pointer-events", "auto");
}

function spinnerInitialise()
{
  jQuery('.quant-input').each(function() {
    var spinner = jQuery(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find('.plus'),
      btnDown = spinner.find('.minus'),
      min = input.attr('min'),
      max = input.attr('max');
      tierid = input.attr('tierid');

    //  max = input.attr('max');
    //  tierid = this.data('tierid');
     // alert(max)

    btnUp.click(function() {
      var oldValue = parseFloat(input.val());
        tierid= input.attr('tierid');
      if (oldValue >= max) {
        var newVal = oldValue;
         // calculate_total_amount_of_tickets_plus(tierid)
      } else {
        var newVal = oldValue + 1;
          calculate_total_amount_of_tickets_plus(tierid)
      }
      // spinner.find("input").attr('value', newVal);
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");

    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
        tierid= input.attr('tierid');
      if (oldValue <= min) {
        var newVal = oldValue;
         // calculate_total_amount_of_tickets_menus(tierid)
      } else {
        var newVal = oldValue - 1;
          calculate_total_amount_of_tickets_menus(tierid)
      }
      // spinner.find("input").attr('value', newVal);
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");

    });
  });
}
</script>


<script>
    function calculate_total_amount_of_tickets_plus(tierid){


        var per_ticket_price= Number($('#price'+tierid).val());
         var ticket_count= Number($('#total_qty'+tierid).val());



       // alert(tierid);
        // alert(per_ticket_price);
       //  alert(ticket_count);

         if(ticket_count>=0) {
            // $('.btnNext').removeAttr("disabled");
            // alert(ticket_count);
            var current_total_amount = per_ticket_price; // * ticket_count;
            var total_qty = Number($('#hide_total_qty_all_selected_tickets').val());
            var total_price = Number($('#hide_total_price_all_selected_tickets').val());
            $('#hide_total_qty_all_selected_tickets').val(total_qty + 1);
            $('#hide_total_price_all_selected_tickets').val(Number(total_price + current_total_amount).toFixed(2));
            $('#total_qty_all_selected_tickets').html(total_qty + 1);
            $('#total_price_all_selected_tickets').html(Number(total_price + current_total_amount).toFixed(2));
        }else{
           // $('.btnNext').attr("disabled", true);
         }
        var total_qty = Number($('#hide_total_qty_all_selected_tickets').val());
        if(total_qty >0){
            $('.btnNext').removeAttr("disabled");
        }else{
            $('.btnNext').attr("disabled", true);
        }

        var newValue = $('#total_qty'+tierid).val();
        newValue = parseInt(newValue);
        newValue = newValue + 1;
        if(newValue > 0)
        {
          $('.minus_'+tierid).attr("src","{{asset('assets/images/minus.png')}}");
          $('.minus_'+tierid).css("pointer-events", "auto");

          $('#delete_'+tierid).attr("src","{{asset('assets/images/delete.png')}}");
          $('#delete_'+tierid).css("pointer-events", "auto");
        }

        var getMax = $('#total_qty'+tierid).attr('max');
        getMax = parseInt(getMax);
        if(newValue == getMax)
        {
          $('.plus_'+tierid).attr("src","{{asset('assets/images/plus_disable.png')}}");
          $('.plus_'+tierid).css("pointer-events", "none"); 
        }

       // alert(per_ticket_price * ticket_count);
//        var payablefee= Number($('#payablefee'+feeheadid).val());
//        $('#payablefeeinput'+feeheadid).val(Number(payablefee - discount).toFixed(2));
//        var sum=0;
//        $( ".actualfee" ).each(function( index , value ) {
//            sum=Number(sum) + Number($( this ).val());
//        });
//        $('#totalactualfee').val(Number(sum).toFixed(2));
//        var sum=0;
//        $( ".totaldiscount" ).each(function( index , value ) {
//            sum=Number(sum) + Number($( this ).val());
//        });
//        $('#totaldiscount').val(Number(sum).toFixed(2));
//        sum=0;
//        $( ".totalpayable" ).each(function( index , value ) {
//            sum=Number(sum) + Number($( this ).val());
//        });
//        $('#totalpayablefee').val(Number(sum).toFixed(2));
    }

    function calculate_total_amount_of_tickets_menus(tierid){


        // alert(tierid);

        var per_ticket_price= Number($('#price'+tierid).val());
        var ticket_count= Number($('#total_qty'+tierid).val());


      //  alert(ticket_count);
       if(ticket_count>0){
          // $('.btnNext').removeAttr("disabled");
        var current_total_amount=per_ticket_price; // * ticket_count;
        var total_qty =Number($('#hide_total_qty_all_selected_tickets').val());
        var total_price =Number($('#hide_total_price_all_selected_tickets').val());
        $('#hide_total_qty_all_selected_tickets').val(total_qty-1);
        $('#hide_total_price_all_selected_tickets').val(total_price-current_total_amount);
        $('#total_qty_all_selected_tickets').html(total_qty-1);
        $('#total_price_all_selected_tickets').html(Number(total_price-current_total_amount).toFixed(2));
       }else{
          // $('.btnNext').attr("disabled", true);
       }
        var total_qty = Number($('#hide_total_qty_all_selected_tickets').val());
        if(total_qty >0){
            $('.btnNext').removeAttr("disabled");
        }else{
            $('.btnNext').attr("disabled", true);
        }

        var newValue = $('#total_qty'+tierid).val();
        newValue = parseInt(newValue);
        newValue = newValue - 1;
        if(newValue == 0)
        {
          $('.minus_'+tierid).attr("src","{{asset('assets/images/minus_disable.png')}}");
          $('.minus_'+tierid).css("pointer-events", "none"); 

          $('#delete_'+tierid).attr("src","{{asset('assets/images/delete_disable.png')}}");
          $('#delete_'+tierid).css("pointer-events", "none");
        }

        var getMax = $('#total_qty'+tierid).attr('max');
        getMax = parseInt(getMax);
        if(newValue < getMax)
        {
          $('.plus_'+tierid).attr("src","{{asset('assets/images/plus.png')}}");
          $('.plus_'+tierid).css("pointer-events", "auto"); 
        }

        // alert(per_ticket_price * ticket_count);
//        var payablefee= Number($('#payablefee'+feeheadid).val());
//        $('#payablefeeinput'+feeheadid).val(Number(payablefee - discount).toFixed(2));
//        var sum=0;
//        $( ".actualfee" ).each(function( index , value ) {
//            sum=Number(sum) + Number($( this ).val());
//        });
//        $('#totalactualfee').val(Number(sum).toFixed(2));
//        var sum=0;
//        $( ".totaldiscount" ).each(function( index , value ) {
//            sum=Number(sum) + Number($( this ).val());
//        });
//        $('#totaldiscount').val(Number(sum).toFixed(2));
//        sum=0;
//        $( ".totalpayable" ).each(function( index , value ) {
//            sum=Number(sum) + Number($( this ).val());
//        });
//        $('#totalpayablefee').val(Number(sum).toFixed(2));
    }
</script>
@endsection