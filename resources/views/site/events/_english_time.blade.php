<h3 class="event_listing_text margin_top_fix2">
    <div class="row" >
        <div class="col-xs-5 english_time_from" style="padding: 0px; text-align: right; margin-right: -10px;">
            {{ date('g:i A', strtotime($event->from_time_english))}}
        </div>
        <div class="col-xs-2"> <center>&mdash;</center> </div>
        <div class="col-xs-5 english_time_to" style="padding-right: 0px; margin-left: -13px;">
            {{ date('g:i A', strtotime($event->to_time_english))}}
        </div>
    </div>
</h3>