@extends('site.layout')
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container">
        <div class="elevation-3dp">
        @include('site.sections.header')

            @if(count($events))
                <div class="inner-addon" style="padding-left: 15px; padding-right: 15px;">
                  <i class="fa fa-search search_icn"></i>
                  <input class="form-control" type="text" id="events_serach" width='100%' style="border-radius: unset; direction: rtl;" placeholder="ابحث">
                </div>
                    
            	<div class="listing-div">

                    @foreach($events as $event)

                        <?php
                            $remaining = 0;
                            foreach ($event->tiers as $tier) {
                                $price_type=$tier->checkFreeAndOpenOrFreeOny();
                                $adminTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedSumNew(3,$tier->event_id,$tier->id);
                                if($price_type=='free_and_unliminted_qty' || $price_type=='not_free_unlimited_qty'){
                                   $remainingQty=1;
                                }else{
                                   $remainingQty=$tier->total_quantity - $tier->totaAvailable();
                                   $remainingQty=$tier->total_quantity - ($tier->totaAvailable() + $adminTicketCount);
                               }

                               $remaining = $remaining + $remainingQty;
                            }
                        ?>

                        @if($event->cover_image)
                            <?php echo View::make('site.events._image_card',['event'=>$event, 'source'=>'public','remaining'=>$remaining]); ?>
                        @else
                            <?php echo View::make('site.events._info_card',['event'=>$event, 'source'=>'public','remaining'=>$remaining]); ?>
                        @endif
                    @endforeach
                    
            	</div>
            @else
                <div class="listing-div" style="margin-top: -13px;">
                    <div class="row card" style="height: auto; border: none;">
                        <img class="img-responsive" src="{{asset('assets/images/place-holder-image.png')}}" style="width: 100%;height: 100%; max-height: auto;">
                    </div>
                </div>
            @endif
            
            @include('site.events._contact_us_bottom')
            <br>
        </div>
        @include('site.sections.footer')
    </div>
</div>




@endsection


@section('page_script')
    @parent

    <script>

        $.fn.select2.defaults.set(
            "theme", "bootstrap"
        );
        $(document).ready(function () {
            $("#eventSearchOnFrontend").select2({

                ajax: {
                    type: 'post',
                    dataType: 'JSON',
                    url: 'events/eventSearchOnFrontend',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            _token: $('input[name=_token]').val(),
                            page: params.page
                        };
                    },
                    beforeSend: function (xhr) {
                        // overlay_ajax();
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    error: function () {
                        // $('#modal-default').modal('show');
                    },
                    complete: function () {
                        // $('.box-body').unblock();
                    },
                    cache: true
                },
                placeholder: 'SEARCH AN EVENT',
                allowClear: true,
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepo (repo) {
                if (repo.loading) {
                    return repo.text;
                }

                var markup = "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__avatar'><img src='"+base_path+'/'+ repo.avatar_url + "' /></div>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'>" + repo.title_english + "</div>";

                if (repo.description_english) {
                    markup += "<div class='select2-result-repository__description'>" + repo.description_english + "</div>";
                }

                markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-repository__forks'> Event #:" + repo.event_id_str + " </div>" +
                    "<div class='select2-result-repository__forks'> Tiers:" + repo.tier_count + " </div>" +
                    "<div class='select2-result-repository__stargazers'>Created:" + repo.created_at + " </div>" +
                    //"<div class='select2-result-repository__watchers'>Company Name : " + repo.companyname + " </div>" +
                    "</div>" +
                    "</div></div>";

                return markup;
            }

            function formatRepoSelection (repo) {
                return repo.title_english || repo.text;
            }

            $("#eventSearchOnFrontend").on('change', function(){
                var event_id=this.value;
                // alert(medicine_id);
                if(event_id!='') {
                    $.ajax({
                        type: 'post',
                        dataType: 'JSON',
                        url: 'events/loadEventDetail',
                        delay: 250,
                        data: {
                             eventid: event_id,
                            _token: $('input[name=_token]').val(),
                        },
                        beforeSend: function (xhr) {
                            //overlay_ajax();
                        },
                        success: function (data) {
                            if (data.flag) {
                                $('.listing-div').html(data.eventdetail);

                            }else{
                               // $('#medicineunit').val('');
                            }
                        },
                        error: function () {
                           // $('#modal-default').modal('show');
                        },
                        complete: function () {
                          //  $('.box-body').unblock();
                        }
                    });
                }else{
                   // $('.unit').html('');
                   // $('#medicineunit').val('');
                }
            });

            //search and filter content
            document.getElementById("events_serach").oninput=function(){
              var matcher = new RegExp(document.getElementById("events_serach").value, "gi");
              for (var i=0;i<document.getElementsByClassName("connect-event").length;i++) {
                if (matcher.test(document.getElementsByClassName("event_title_english")[i].innerHTML) || matcher.test(document.getElementsByClassName("event_title_arabic")[i].innerHTML) || matcher.test(document.getElementsByClassName("event_venue_english")[i].innerHTML) || matcher.test(document.getElementsByClassName("event_venue_arabic")[i].innerHTML) || matcher.test(document.getElementsByClassName("event_description_english")[i].innerHTML) || matcher.test(document.getElementsByClassName("event_description_arabic")[i].innerHTML) ) {
                  document.getElementsByClassName("connect-event")[i].style.display="inline-block";
                } else {
                  document.getElementsByClassName("connect-event")[i].style.display="none";
                }
                  
              }
            }

        });
    </script>


@endsection