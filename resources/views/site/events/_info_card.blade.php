<style type="text/css">
    a, a:hover { color: inherit; }


</style>
<div class="card-div connect-event" style="margin-top: {{ $event->tag !=4 ? 'unset' : '-30px' }}">
    <!-- <a href="{{ URL::route('site.events.detail',['event_id'=>$event->id]) }}"> -->
    <a href="{{ URL::route($source == 'redemption' ? 'events' : 'site.events.detail',['event_id'=> $source == 'redemption' ? $event->event_id_str : $event->id]) }}">
    @if($event->tag !=4)
        @if($remaining == 0 && count($event->tiers))
            <img class="img-responsive right-corner-image" src="{{asset('assets/images/event_status_3.png')}}" style="width: auto;height: 30px;">
        @else
            <img class="img-responsive right-corner-image" src="{{asset('assets/images/event_status_'.$event->tag.'.png')}}" style="width: auto;height: 30px;">
        @endif
    @endif
    <div class="row card card-simple">

        <!-- <div class="row" style="margin: 0px;">
            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-3" style="padding: 0px; width: 18%;">
                <img class="img-responsive inner_logo" src="{{asset($event->logo_image)}}" style="width: auto;height: 90px;">
            </div>
            <div class="col-lg-9 col-md-10 col-sm-10 col-xs-9" style="width: 82%;">
                <center>
                    <div style="margin-left: 3px;">
                        <h3 class="arabic_font event_listing_title event_title_arabic" style="margin-top: 14px;">
                            {{ $event->title_arabic }}
                        </h3>
                        <h3 class="event_listing_title event_title_english" style="margin-top: -5px;">
                            {{ $event->title_english }}
                        </h3>
                    </div>
                </center>
            </div>
        </div> -->

         <div class="row" style="margin: 0px;">
            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-3" style="padding: 0px; width: 18%;">
                <img class="img-responsive inner_logo" src="{{asset($event->logo_image)}}" style="width: auto;height: 90px;">
            </div>
            <div class="col-lg-9 col-md-10 col-sm-10 col-xs-9" style="width: 82%; display: block;">
                <center>
                    <div style="margin-left: 3px; display: table-cell; vertical-align: middle; height: 58px;">
                        <h3 class="arabic_font event_listing_title event_title_arabic" style="margin: 0; margin-bottom: 4px;">
                            {{ $event->title_arabic }}
                        </h3>
                        <h3 class="event_listing_title event_title_english" style="margin: 0;">
                            {{ $event->title_english }}
                        </h3>
                    </div>
                </center>
            </div>
        </div>

        <p class="event_description_english hidden">{{$event->description_english}}</p>
        <p class="event_description_arabic hidden">{{$event->description_arabic}}</p>
        <!-- <div class="row">
            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-3">
                <img class="img-responsive inner_logo" src="{{asset($event->logo_image)}}" style="width: auto;height: 90px;">
            </div>
            <div class="col-lg-9 col-md-10 col-sm-10 col-xs-9">
                <center>
                    <h3 class="arabic_font event_listing_title">
                        {{ $event->title_arabic }}
                    </h3>
                    <h3 class="event_listing_title" style="margin-top: -5px;">
                        {{ $event->title_english }}
                    </h3>
                </center>
            </div>
        </div> -->
        
        <div class="row grey_color">
            <div class="col-md-6 col-sm-6 col-xs-6 padding-right-5px">
                <h3 class="event_listing_text event_venue_english">
                    {{ $event->venue_english }}
                </h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 padding-left-5px">
                <h3 class="arabic_font right-align event_listing_text event_venue_arabic">
                    {{ $event->venue_arabic }}
                </h3>
            </div>
        </div>
        <br>

        <!-- @include('site.events._event_date_times') -->
        <?php echo View::make('site.events._event_date_times',['event'=>$event]); ?>

        <!-- <div class="row grey_color">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h3 class="event_listing_text margin_top_fix1">
                   {{ date('d/m/Y', strtotime($event->from_date_english)) }} - {{ date('d/m/Y', strtotime($event->to_date_english)) }}
                </h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h3 class="arabic_font right-align event_listing_text margin_top_fix1">
                     {{ $event->from_date_arabic }} - {{ $event->to_date_arabic }}
                </h3>
            </div>
        </div>

        <div class="row grey_color">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h3 class="event_listing_text margin_top_fix2">
                    {{ date('h:i A', strtotime($event->from_time_english))}} - {{ date('h:i A', strtotime($event->to_time_english))}}
                </h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 ">
                <h3 class="arabic_font right-align event_listing_text margin_top_fix2">
                    {{ $event->from_time_arabic }} - {{ $event->to_time_arabic }}
                </h3>
            </div>
        </div> -->

    </div>
    <div class="row" style="margin: 2% -15px 0px -15px">
        <hr class="card_bottom_line">
        <img class="img-responsive" src="{{asset('assets/images/card_bottom.png')}}" style="width: auto;height: auto;">
    </div>
    </a>
</div>