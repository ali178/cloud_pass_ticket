@extends('site.layout')
@section('content')
<div class="row">
    <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 main-col-container"> -->
     <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container"> -->
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container">
        <div class="elevation-3dp" style="padding-top: 0px;">
        @include('site.sections.header')
      	@include('site.events._slider')
      	<div class="listing-div detail_main_div">
      		@include('site.events._social_icons')

              <div class="row">
                  <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                      <!-- <center>

                      <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px">
                      </center> -->
                      @include('site.events._banner_logo')
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
                      <!-- <span>{{date('d/m/Y H:m',strtotime($event->created_at))}}</span> -->
                      <span>{{date('d/m/Y g:i A',strtotime($event->updated_at))}}</span>
                  </div>
              </div>

              <center>
                  <h3 class="arabic_font event_detail_title" style="margin-bottom: -10px;">
                      {{$event->title_arabic}}
                  </h3>
                  <h3 class="event_detail_title">
                      {{$event->title_english}}

                  </h3>

                  <br>
                  {{-- <a href="{{ URL::route('site.events.index') }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;">
                      <span>Back</span>
                      <span class="arabic_font">السابق</span>
                  </a> --}}
                  @if($event->selling_off == 0 && count($event->tiers) > 0)
                  <a href="{{ URL::route('site.events.tickets',['event_id'=>$event->id]) }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;">
                      <span>View tickets</span>
                      <span class="arabic_font">عرض التذاكر</span>
                  </a>
                  @endif

                  {{--<button type="button" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: 1px solid rgb(137,137,137);">--}}
                      {{--<span>View tickets</span>--}}
                      {{--<span class="arabic_font">عرض تذاكر</span>--}}
                  {{--</button>--}}
              </center>

              <br><br>
              @include('site.events._descriptions')

              <?php
                $allow_venue_info = true;
                $map_exist = $event->lat && $event->long;
                if($map_exist)
                {
                  $venues_exist = $event->venue_arabic && $event->venue_english;
                  $allow_venue_info = $venues_exist ? true : false;
                }
              ?>
              @if($allow_venue_info)
                <center>

                  @if($event->map_address)
                    <a href="javascript:void(0)" onclick="showGoogleMap('{{$event->lat }}','{{$event->long}}')"  class="{{ $event->map_address ? '' : 'disable_this' }}"><img src="{{asset('assets/images/location-icon.png')}}" style="width: auto;height: 30px; margin-bottom: 8px;">
                    </a>
                    <br>
                  @endif

                  @if($event->venue_arabic )
                    <span class="arabic_font location_text">{{$event->venue_arabic }}</span>
                    <br>
                  @endif

                  @if($event->venue_english)
                    <span class="location_text">{{$event->venue_english }}</span>
                  <br>
                  @endif
                </center>
                <br><br>
              @endif

              @if($event->to_date_arabic || $event->from_date_arabic || $event->to_time_arabic || $event->from_time_arabic)
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 calender_dates">
                  <center>
                    <span><img src="{{asset('assets/images/calender_icn.png')}}" width="22"></span>
                    <!-- <br>
                    <br> -->

                    <ul class="list-inline" style="margin-top: 8px;">
                      <li class="list-inline-item" style="width: 60px;"><span class="calender_text">{{date('d/m/Y',strtotime($event->from_date_english))}}</span></li>
                      <li class="list-inline-item"  style="width: 16px;">&mdash;</li>
                      <li class="list-inline-item"  style="width: 62px;"><span class="calender_text">{{date('d/m/Y',strtotime($event->to_date_english))}}</span></li>
                    </ul>

                    <ul class="list-inline">
                      <li class="list-inline-item" style="width: 59px;"><span class="calender_text" style="margin-right: -15px;">{{date('g:i A',strtotime($event->from_time_english))}}</span></li>
                      <li class="list-inline-item"  style="width: 15px;">&mdash;</li>
                      <li class="list-inline-item"  style="width: 62px;"><span class="calender_text">{{date('g:i A',strtotime($event->to_time_english))}}</span></li>
                    </ul>

                    <!-- <span class="calender_text">{{date('d/m/Y',strtotime($event->from_date_english))}} - {{date('d/m/Y',strtotime($event->to_date_english))}}</span>
                    <br>
                    <span class="calender_text">{{date('h:m A',strtotime($event->from_time_english))}} - {{date('h:m A',strtotime($event->to_time_english))}}</span> -->
                  </center>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 calender_dates">
                  <center>
                    <!-- <span><i class="fa fa-calendar icon-outline" style="font-size: 40px;"></i></span> -->
                    <span><img src="{{asset('assets/images/calender_icn.png')}}" width="22"></span>
                    <!-- <br>
                    <br> -->
                    <?php
                      $arabic_date_exist = $event->to_date_arabic && $event->from_date_arabic;
                      $arabic_date_exist = $arabic_date_exist ? 'unset' : '8px';
                    ?>
                    @if($event->to_date_arabic && $event->from_date_arabic)
                      <ul class="list-inline" style="margin-top: 8px;margin-left: -6px;">
                        <li class="list-inline-item"  style="width: 60px;"><span class="calender_text">{{$event->to_date_arabic}}</span></li>
                        <li class="list-inline-item"  style="width: 16px;">&mdash;</li>
                        <li class="list-inline-item" style="width: 62px;"><span class="calender_text">{{$event->from_date_arabic}}</span></li>
                      </ul>
                    @endif

                    @if($event->to_time_arabic && $event->from_time_arabic)
                      <ul class="list-inline" style="margin-left: -6px;margin-top: {{$arabic_date_exist}};">
                        <li class="list-inline-item" style="width: 59px;"><span class="calender_text" style="margin-right: -15px;">{{$event->to_time_arabic }}</span></li>
                        <li class="list-inline-item"  style="width: 15px;">&mdash;</li>
                        <li class="list-inline-item"  style="width: 62px;"><span class="calender_text">{{$event->from_time_arabic}}</span></li>
                      </ul>
                    @endif

                    <!-- <span class="arabic_font calender_text">{{$event->from_date_arabic}}  -  {{$event->to_date_arabic}}</span>
                    <br>
                    <span class="arabic_font calender_text">{{$event->from_time_arabic}} -  {{$event->to_time_arabic }} </span> -->
                  </center>
                </div>
              </div>
              @else
                <center>
                    <span><img src="{{asset('assets/images/calender_icn.png')}}" width="22"></span>
                    <!-- <br>
                    <br> -->

                    <ul class="list-inline" style="margin-top: 8px;margin-bottom: 0px;">
                      <li class="list-inline-item" style="width: 60px;"><span class="calender_text">{{date('d/m/Y',strtotime($event->from_date_english))}}</span></li>
                      <li class="list-inline-item"  style="width: 16px;">&mdash;</li>
                      <li class="list-inline-item"  style="width: 62px;"><span class="calender_text">{{date('d/m/Y',strtotime($event->to_date_english))}}</span></li>
                    </ul>

                    <ul class="list-inline">
                      <li class="list-inline-item" style="width: 59px;"><span class="calender_text" style="margin-right: -15px;">{{date('h:m A',strtotime($event->from_time_english))}}</span></li>
                      <li class="list-inline-item"  style="width: 15px;">&mdash;</li>
                      <li class="list-inline-item"  style="width: 62px;"><span class="calender_text">{{date('h:m A',strtotime($event->to_time_english))}}</span></li>
                    </ul>

                    <!-- <span class="calender_text">{{date('d/m/Y',strtotime($event->from_date_english))}} - {{date('d/m/Y',strtotime($event->to_date_english))}}</span>
                    <br>
                    <span class="calender_text">{{date('h:m A',strtotime($event->from_time_english))}} - {{date('h:m A',strtotime($event->to_time_english))}}</span> -->
                  </center>
              @endif

                <center>
                  
                  {{-- <a href="{{ URL::route('site.events.index') }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;">
                      <span>Back</span>
                      <span class="arabic_font">السابق</span>
                  </a> --}}
                  <br><br>
                  @if($event->selling_off == 0 && count($event->tiers) > 0)
                    <a href="{{ URL::route('site.events.tickets',['event_id'=>$event->id]) }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;margin-bottom: 5px;">
                        <span>View tickets</span>
                        <span class="arabic_font">عرض التذاكر</span>
                    </a>
                  <br>
                </center>
                @else
              <!-- <br><br><br> -->
              @endif
              @if( ($event->selling_off == 0 && count($event->tiers) > 0) && ($event->mobile || $event->email || $event->url_link2) )
                <br><br>
              @endif
              @if($event->mobile || $event->email || $event->url_link2)
                <center>
                  <span class="arabic_font event_organizer_text">تواصل المناسبة</span><br>
                <span class="event_organizer_text">Event Contact</span>
                </center>
                <br>
                <div class="row content_text">
                  <!-- <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2"> -->
                  <div class="col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0">


                    <center>
                      <ul class="list-inline social_icons_list" style="padding-left: 6px;">

                        <li class="list-inline-item {{ $event->mobile ? '' : 'hidden' }}">
                            <a href="tel:{{$event->mobile}}"><img src="{{asset('assets/images/phone_icon.png')}}" class="detail_contact_icons detail_contact_phone_icon" style="height: 27px; "></a>
                        </li>

                          <li class="list-inline-item {{ $event->email ? '' : 'hidden' }}">
                            <a href="mailto:{{$event->email}}" target="_top"><img src="{{asset('assets/images/email_icon.png')}}" class="detail_contact_icons"></a>
                          </li>

                          <li class="list-inline-item {{ $event->url_link2 ? '' : 'hidden' }}">
                            <!-- <a href="{{$event->url_link2}}" target="_blank"><img src="{{asset('assets/images/website_icon.png')}}" class="detail_contact_icons"></a> -->
                            <a href="{{$event->url_link2}}" target="_blank"><img src="{{asset('assets/images/icon-web.png')}}" class="detail_contact_icons" style="height: 27px;"></a>
                          </li>

                      </ul>
                    </center>

                    <!-- <div class="row">
                      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 icon-col">
                        <img src="{{asset('assets/images/phone_icon.png')}}" class="detail_contact_icons detail_contact_phone_icon" style="height: 40px;">
                      </div>
                      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="margin-top: 5px;">
                        <span style="vertical-align: -webkit-baseline-middle;">{{$event->mobile}}</span>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 icon-col">
                        <img src="{{asset('assets/images/email_icon.png')}}" class="detail_contact_icons">
                      </div>
                      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <span>{{$event->email}}</span>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 icon-col">
                        <img src="{{asset('assets/images/website_icon.png')}}" class="detail_contact_icons">
                      </div>
                      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <span>{{$event->url_link2}}</span>
                      </div>
                    </div> -->

                  </div>
                </div>
              @endif

      	</div>

        <br>
          @include('site.events._contact_us_bottom')
          <br>
      </div>
      @include('site.sections.footer')
    </div>
</div>





<!-- maps modal -->
<div class="modal fade" id="googleMapModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Map</h4>
            </div> -->
            <div class="modal-body">
                <div class="form-group" id="map" style="width: 100%;height: 300px;"></div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </center>
            </div>
        </div>

    </div>
</div>




@endsection

@section('page_script')
@parent

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAe4ISmDhVvG-BXiqwXI2EkL3GdDFDxgM&libraries=places" async defer></script>--}}
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAe4ISmDhVvG-BXiqwXI2EkL3GdDFDxgM&sensor=false&libraries=places"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng('{{$event->lat }}','{{$event->long}}');
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 13
        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: false,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function() {
            var iwContent = '<div id="iw_container">' +
                '<div class="iw_title"><b>Location</b> : Noida</div></div>';
            // including content to the infowindow
            infowindow.setContent(iwContent);
            // opening the infowindow in the current map and at the current marker location
            infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>




<script type="text/javascript" language="javascript" class="init">

  function showGoogleMap(lat,long){
  $('#googleMapModal').modal('show');

  }

  $(document).ready(function ($) {

    var desc_type = '{{Request::get("source")}}';
    if(desc_type == 'english_desc'){
        $( "#english_description_tab" ).addClass( "active" );
        $( "#english_description" ).addClass( "active in" );
        $( "#arabic_description_tab" ).removeClass( "active" );
    }else if(desc_type == 'arabic_desc'){
        $( "#arabic_description_tab" ).addClass( "active" );
        $( "#arabic_description" ).addClass( "active in" );
        $( "#english_description_tab" ).removeClass( "active" );
    }else{
        $( "#english_description_tab" ).addClass( "active" );
        $( "#english_description" ).addClass( "active in" );
        $( "#arabic_description_tab" ).removeClass( "active" );
    }

    $('#english_description_tab').click(function(event){
        history.pushState({}, null, "{{URL::route('site.events.detail',$event->id)}}?source=english_desc");
    });
    $('#arabic_description_tab').click(function(event){
        history.pushState({}, null, "{{URL::route('site.events.detail',$event->id)}}?source=arabic_desc");
    });

  });

</script>
@endsection
