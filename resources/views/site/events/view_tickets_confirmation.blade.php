@extends('site.layout')
@section('content')
<?php
    $setting = $shared_site_vars['settings'];
?>
<style type="text/css">
  .custom_back_btn,.custom_back_btn:hover,.custom_back_btn:active,.custom_back_btn:focus,.custom_back_btn:active:focus{
    color: rgb(0, 0, 0);
    background: rgba(0, 0, 0, 0);
    border: 1px solid;
    font-size: 14px;
    text-shadow: unset;
    font-weight: bold;
    outline: none;
  }
</style>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container tickets_review_container">
    <div class="elevation-3dp" style="padding-top: 0px;">
      @include('site.sections.header')
      @include('site.events._slider')
      <div class="listing-div detail_main_div tickets_view" style="padding-top: 20px !important;">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
            <!-- <center>
              <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px">
            </center> -->
            @include('site.events._banner_logo')
          </div>
          <!-- <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
            <span>{{$event->created_at }}</span>
          </div> -->
          <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
              <span>{{date('d/m/Y h:i A',strtotime($event->updated_at))}}</span>
          </div>
        </div>
        <div class="row">
          <center>
            <h3 class="arabic_font event_detail_title" style="margin-bottom: -10px;">
                {{$event->title_arabic}}
            </h3>
            <h3 class="event_detail_title">
                {{$event->title_english}}
            </h3>

            <br>
            
            <h4 style="font-weight: bold;" class="arabic_font">ليس اثبات شراء</h4>
            <h4 style="font-weight: bold;">NOT A PROOF OF PURCHASE</h4>
            <br>

            <h5 class="arabic_font confirmation_info_description">تم استلام طلبك بنجاح. الرجاء الاحتفاظ برقم الطلب في حال الرغبة بالتواصل مع منظم المناسبة.</h5>
            <h5 class="confirmation_info_description">Your request has been sent successfully. Please note and save this request ID in case you need to communicate with event organizer</h5>

            <h2 style="font-weight: bold;">{{$event_request->complete_request_no}}</h2>
          </center>
        </div>
        <br>

        @include('site.events._buying_tickets_table')

        <div class="row">
          <center>
            <h5 class="arabic_font confirmation_info_description" style="margin-top: -15px;">سيتم الرد على طلبك بأقرب وقت ممكن. في حال قبول الطلب، ستصلك رسالة الكترونية تتضمن معلومات الدفع. في حال عدم حصولك على اي رسائل بشأن طلبك قبل او بعد الدفع يرجى الاتصال بنا على البريد الالكتروني ادناه مع ذكر رقم الطلب المكتوب اعلى هذه الرسالة</h5>

            @if($setting && $setting->email)
              <h5><a href="mailto:{{$setting->email}}" target="_top" style="color: rgb(51, 122, 183);">{{$setting->email}}</a></h5>
            @endif

            <h5 class="confirmation_info_description">You will receive a reply as soon as possible. If your order has been accepted, you will receive an email containing payment information. Please contact us on the email above in case you don't receive an communication from us before or after payment. Please include your given request ID</h5>

            <br>

            <a href="{{ URL::route('site.events.index') }}" class="btn btn-success btn-lg custom_back_btn">
              <span>Back to home page</span>
              <span>&nbsp;&nbsp;</span>
              <span class="arabic_font">العودة للصفحة الرئيسية</span>
            </a>
          </center>
        </div>

      </div>

      <br>
      @include('site.events._contact_us_bottom')
      <br>
    </div>
    @include('site.sections.footer')
  </div>
</div>
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function ($) {

});
</script>
@endsection