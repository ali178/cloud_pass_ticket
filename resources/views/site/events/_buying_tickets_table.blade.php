<div class="card-div">
  <div class="row card card-simple tickets_review_table_div">
    <div class="table-responsive">          
      <table class="table" id="review_tickets_table">
        <thead>
          <tr>
            <th>
              <span class="arabic_font">النوع</span>
              <br>
              <span>Type</span>
            </th>
            <th></th>
            <th class="qty">
              <span class="arabic_font" style="display: block;margin-bottom: -15px;">الكمية</span>
              <br>
              <span>Qty</span>
            </th>
            <th class="price">
              <span class="arabic_font" style="display: block;margin-bottom: -15px;">السعر</span>
              <br>
              <span>Price</span>
            </th>
          </tr>
        </thead>
        <tbody style="font-size: 12px;">
        <?php 
          $subTotalAmount=0.00;
          $vat = isset($shared_site_vars['settings']) && $shared_site_vars['settings']['vat_value'] ? $shared_site_vars['settings']['vat_value'] : 0;
          $arabic_vat = \Ticketing\Models\Event::e2a($vat);
          $temp_vat = 0;
        ?>
        @foreach($event_request_details as $event_request_detail)
          <tr>
            <td>{{$event_request_detail->tier->title_english}}</td>
            <td>{{$event_request_detail->tier->title_arabic}}</td>
            <td class="qty">{{$event_request_detail->total_qty  }}</td>
            <td class="price">
              <?php
                $item_price = $event_request_detail->price * $event_request_detail->total_qty;
                $temp_vat = $temp_vat + (($vat/100)*$item_price);
                $item_price = $item_price - ($vat/100)*$item_price;
                $subTotalAmount=$subTotalAmount + $item_price;

              ?>
              @if($event_request_detail->price>0)
                {{number_format($item_price,2)}}
              @else {{'Free'}} @endif
            </td>
          </tr>

          @endforeach
        </tbody>

        <tfoot style="font-size: 12px;">
          <tr class="border_top">
            <td colspan="2" class="arabic_font">
              المجموع الفرعي
            </td>
            <td>Sub Total</td>
            <td>{{number_format($subTotalAmount,2)}}</td>
          </tr>
          <tr>
            <td colspan="2" class="arabic_font" style="direction: rtl;">
              ضريبة مضافة {{$arabic_vat}}٪
            </td>
            <td>Vat {{$vat}}%</td>
            <td>
                <?php  
                  $vat_value = $temp_vat;
                ?>
              {{number_format($vat_value,2)}}
            </td>
          </tr>
          <tr>
            <td colspan="2" class="arabic_font">
              الإجمالي
            </td>
            <td>Total</td>
            <td>

              {{number_format($subTotalAmount + $vat_value,2)}}
              <!-- {{$subTotalAmount + $vat_value}} -->
            </td>
          </tr>
        </tfoot>

      </table>
    </div>
  </div>
</div>