@extends('site.layout')
@section('content')
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container tickets_review_container">
    <div class="elevation-3dp" style="padding-top: 0px;">
      @include('site.sections.header')
    	@include('site.events._slider')
    	<div class="listing-div detail_main_div tickets_view" style="padding-top: 20px !important;">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
            <!-- <center>
              <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px">
            </center> -->
            @include('site.events._banner_logo')
          </div>
         <!--  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
            <span>{{date('d/m/Y H:m',strtotime($event->created_at))}}</span>
          </div> -->
          <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
              <span>{{date('d/m/Y h:i A',strtotime($event->updated_at))}}</span>
          </div>
        </div>

        <center>
          <h3 class="arabic_font event_detail_title" style="margin-bottom: -10px;">
              {{$event->title_arabic}}
          </h3>
          <h3 class="event_detail_title">
              {{$event->title_english}}
          </h3>


          <br>
          
          <h5 class="arabic_font">ملخص الطلب</h5>
          <h5>Request Summary</h5>
        </center>


        @include('site.events._buying_tickets_table')

        <center>
          <h5 class="arabic_font">معلومات التواصل</h5>
          <h5>Contact Information</h5>
        </center>

        {{--<form action="/action_page.php" id="personal_info_form">--}}

          {!! Form::open(array('route' => ['events.store',$event->id], 'id'=>'personal_info_form')) !!}
            <input type="hidden" name="submit" value="customer">
          <div class="card-div">
            <div class="row card card-simple tickets_review_table_div">



              {{--<div class="form-group">--}}
                {{--<label for="full_name"><span>Full Name</span> <span class="arabic_font label_right">اسم كامل</span></label>--}}
                {{--<input type="text" name="full_name" class="form-control" id="full_name">--}}
                {{--<span class="alertrequired">{!!$errors->first('full_name')!!}</span>--}}
              {{--</div>--}}



              <div class="form-group">
                <label for="email" style="font-size: 12px; color: rgb(74, 74, 74);">

                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><span class="full_name_class {{ $errors->first('full_name') ? 'redText' : ''}}">Full Name</span></div>
                    <div class="col-lg-2 col-md-1 col-sm-2 col-xs-2">
                      <center>
                        <span class="asterik">*</span>
                      </center>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="arabic_font label_right full_name_class {{ $errors->first('full_name') ? 'redText' : ''}}">الاسم الكامل</span></div>
                  </div>

                </label>

                <input type="text" name="full_name" class="form-control" id="full_name" maxlength="40" value="{{ old('full_name') }}">
                <!-- <span class="alertrequired">{!!$errors->first('full_name')!!}</span> -->
              </div>








              <div class="form-group">
                <label for="email" style="font-size: 12px; color: rgb(74, 74, 74);">
                  
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><span class="email_class {{ $errors->first('email') ? 'redText' : ''}}">Email</span></div>
                    <div class="col-lg-2 col-md-1 col-sm-2 col-xs-2">
                      <center>
                        <span class="asterik">*</span>
                      </center>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><span class="arabic_font label_right email_class {{ $errors->first('email') ? 'redText' : ''}}">البريد الكتروني</span></div>
                  </div>
                
                </label>

                <input type="text" name="email" class="form-control" value="{{ old('email') }}" id="email" maxlength="60">
                <!-- <span class="alertrequired">{!!$errors->first('email')!!}</span> -->
              </div>

              <div class="form-group">
                <label for="mobile" style="font-size: 12px; color: rgb(74, 74, 74);">
                
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><span class="mobile_class {{ $errors->first('mobile') ? 'redText' : ''}}">Mobile Number</span></div>
                    <div class="col-lg-2 col-md-1 col-sm-2 col-xs-2">
                      <center>
                        <span class="asterik">*</span>
                      </center>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><span class="arabic_font label_right mobile_class {{ $errors->first('mobile') ? 'redText' : ''}}">الهاتف المتحرك</span></div>
                  </div>

                </label>
                <input type="text" name="mobile" class="form-control" id="mobile" value="{{ old('mobile') }}" onkeypress="return isNumberKey(event)" maxlength="15">
                <!-- <span class="alertrequired">{!!$errors->first('mobile')!!}</span> -->
              </div>
            </div>

            <center>
              <br>
              <div class="checkbox tnc_checkbox_parent">
                <input type="checkbox" value="0" style="margin-left: -8px; width: 20px;height: 20px;" id="tnc_checkbox">
              </div>

              <h5 class="arabic_font checkbox_info_txt">ضغطك على هذا المربع يعني قبولك شروط و احكام الموقع ومنظم المناسبة</h5>
              <h5 class="checkbox_info_txt">By checking this box you agree to website and event terms & conditions</h5>

              <br>
              <!-- <a href="javascript:void(0)" class="underline" data-toggle="modal"  data-target="#tnc_modal" style="font-size: 12px;">Terms & Condtions <span class="arabic_font">الأحكام والشروط</span></a> -->

              @if($event && $event->tc_arabic != '' && $event->tc_english != '')
                <a href="{{ URL::route('site.events.tnc',['event_id'=>$event->id]) }}" target="_blank" class="underline {{ $event && $event->tc_arabic == '' && $event->tc_english == '' ? 'disable_this' : '' }}" style="font-size: 12px;">Event Terms & Conditions <span class="arabic_font">شروط و احكام المناسبة</span></a>
                <br><br>
              @endif

              <br>

              {{--<a href="{{ URL::route('site.events.tickets.review.confirmation',['event_id'=>$event->id]) }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset;">--}}
                {{--<span>SubmitNext</span>--}}
                {{--<span class="arabic_font">ارسال</span>--}}
              {{--</a>--}}


              <a href="{{ URL::route('site.events.tickets',['event_id'=>$event->id]) }}" class="btn btn-success btn-lg" style="color:rgb(247,231,0); background: rgb(137,137,137); border: unset; font-size: 14px;">
                  <span>Back</span>
                  <span class="arabic_font">السابق</span>
              </a>
              <span>&nbsp;</span>

              <button type="submit" class="btn btn-success btn-lg custom_btn" id="submit-personal-info-btn" disabled="true" style="color:rgb(247,231,0);font-size: 14px; border: unset;outline: none;background: rgb(137,137,137);">
                <span>Request</span>
                <span class="arabic_font">إرسال</span>

              </button>
            </center>
          </div>
          {!! Form::close() !!}
    	</div>

      <br>
      @include('site.events._contact_us_bottom')
      <br>
    </div>
    @include('site.sections.footer')

    <!-- agree tnc modal -->
    <div class="modal fade" id="agree_tnc_modal" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          
          <div class="modal-body tnc_popup_text">
            <center>
              <p class="arabic_font">الضغط على نعم يعني قبولك شروط و احكام الموقع والمناسبة</p>
              <p>By clicking <strong>YES</strong> you agree to website and event terms and conditions</p>
            </center>
          </div>

          <div class="modal-footer">
            <center>
              <button type="button" class="btn btn-success  custom_btn" data-dismiss="modal" style="font-size: 14px;border: unset;outline: none;background: rgb(137,137,137);"><span>No</span>
                <span class="arabic_font">لا</span></button>  
              <button type="button" class="btn btn-success custom_btn" onclick="check_tnc_checkbox();"  style="font-size: 14px;border: unset;outline: none;background: rgb(137,137,137);"><span>Yes</span>
              <span class="arabic_font">نعم</span></button>
            </center>
          </div>
        </div>
      </div>
    </div>

    <!-- tnc modal -->
    <div class="modal fade" id="tnc_modal" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          
          <div class="modal-body tnc_popup_text">
            <center>
              <p class="arabic_font">حين الأراضي البولندي عن, بقعة اللا ليتسنّى كل الا. عرض لهيمنة جديداً استرجاع أي, جعل عل خلاف الحرة وباستثناء. نفس ووصف أثره، ما, حين أي غرّة، الشرقية. ذلك الضروري ابتدعها الكونجرس ثم, من لمّ فرنسا بالرّد شموليةً.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
            </center>
          </div>

          <div class="modal-footer">
            <center>
              <button type="button" class="btn btn-success  custom_btn" data-dismiss="modal" style="font-size: 14px;"><span>Ok</span>
                <span class="arabic_font">موافق</span></button>
            </center>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">

jQuery('#personal_info_form').submit(function (event) {
    form = this;
    //some validations
    var full_name = $('#full_name').val();
    var email = $('#email').val();
    var mobile = $('#mobile').val();

    if(full_name && email && mobile && isEmail(email))
    {
      return true;
    }else{
      updateInputsError(full_name,email,mobile);
      $('#validation_error_modal').modal('show');
      return false;
    }
    return true;
});

function updateInputsError(full_name,email,mobile)
{
  if(!full_name)
  {
    $('.full_name_class').css('color','red');
  }else{
    $('.full_name_class').css('color','rgb(74, 74, 74)');
  }

  if(!mobile)
  {
    $('.mobile_class').css('color','red');
  }else{
    $('.mobile_class').css('color','rgb(74, 74, 74)');
  }

  if(!email || !isEmail(email))
  {
    $('.email_class').css('color','red');
  }else{
    $('.email_class').css('color','rgb(74, 74, 74)');
  }
}

$(document).ready(function ($) {

  document.querySelector("#tnc_checkbox").addEventListener("click", function(event) {
    if(this.checked)
    {
      $("#agree_tnc_modal").modal("show");
      $(".tickets_review_container").addClass("after_modal_appended");
      $('.modal-backdrop').appendTo('.tickets_review_container');   
      $('body').removeClass("modal-open");
      event.preventDefault();
    }else{
      document.getElementById("submit-personal-info-btn").disabled = true;
    }
  }, false);

});

function check_tnc_checkbox(){
  $( "#tnc_checkbox" ).prop( "checked", true );
  $("#agree_tnc_modal").modal("hide");
  document.getElementById("submit-personal-info-btn").disabled = false;
}
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}



        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
@endsection