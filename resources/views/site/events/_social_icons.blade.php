@if($event->twitter_link || $event->instagram_link || $event->snapchat_link || $event->facebook_link || $event->url_link )
	<center>
		<ul class="list-inline social_icons_list" style="margin-bottom: 30px;padding-left: 6px;">
			
			<li class="list-inline-item {{ $event->twitter_link ? '' : 'hidden' }}">
			  	<a href="{{ $event->twitter_link}}" target="_blank" class="{{ $event->twitter_link ? '' : 'disable_this' }}"><img src="{{asset('assets/images/twitter_icn.png')}}" width="32"></a>
			</li>

		  	<li class="list-inline-item {{ $event->instagram_link ? '' : 'hidden' }}">
		  		<a href="{{ $event->instagram_link}}" target="_blank" class="{{ $event->instagram_link ? '' : 'disable_this' }}"><img src="{{asset('assets/images/instagram_icn.png')}}" width="32"></a>
		  	</li>

		  	<li class="list-inline-item {{ $event->snapchat_link ? '' : 'hidden' }}">
		  		<a href="{{ $event->snapchat_link}}" target="_blank" class="{{ $event->snapchat_link ? '' : 'disable_this' }}"><img src="{{asset('assets/images/snapchat_icn.png')}}" width="32"></a>
		  	</li>

		  	<li class="list-inline-item {{ $event->facebook_link ? '' : 'hidden' }}">
		  		<a href="{{ $event->facebook_link}}" target="_blank" class="{{ $event->facebook_link ? '' : 'disable_this' }}"><img src="{{asset('assets/images/icon-fb.png')}}" width="16"></a>
		  	</li>

		  	<li class="list-inline-item {{ $event->url_link ? '' : 'hidden' }}">
		  		<a href="{{ $event->url_link}}" target="_blank" class="{{ $event->url_link ? '' : 'disable_this' }}"><img src="{{asset('assets/images/icon-web.png')}}" width="32"></a>
		  	</li>

		</ul>
	</center>
@endif

<!-- <div class="row" style="margin-bottom: 30px;">
	<div class="col-md-2 col-sm-2 col-xs-2 social_icon_col_div">
		<center>
			<a href="{{ $event->twitter_link}}" target="_blank" class="{{ $event->twitter_link ? '' : 'disable_this' }}"><i class="fa fa-twitter icon-outline social_icon"></i></a>
		</center>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 social_icon_col_div">
		<center>
			<a href="{{ $event->instagram_link}}" target="_blank" class="{{ $event->instagram_link ? '' : 'disable_this' }}"><i class="fa fa-instagram icon-outline social_icon"></i></a>
		</center>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 social_icon_col_div">
		<center>
			<a href="{{ $event->snapchat_link}}" target="_blank" class="{{ $event->snapchat_link ? '' : 'disable_this' }}"><i class="fa fa-snapchat-ghost icon-outline social_icon"></i></a>
		</center>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 social_icon_col_div">
		<center>
			<a href="{{ $event->facebook_link}}" target="_blank" class="{{ $event->facebook_link ? '' : 'disable_this' }}"><i class="fa fa-facebook-square icon-outline social_icon"></i></a>
		</center>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 social_icon_col_div">
		<center>
			<a href="{{ $event->url_link}}" target="_blank" class="{{ $event->url_link ? '' : 'disable_this' }}"><i class="fa fa-globe icon-outline social_icon"></i></a>
		</center>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 social_icon_col_div">
		<center>
			<a href="javascript:void(0)" target="_blank"><i class="fa fa-square-o icon-outline social_icon"></i></a>
		</center>
	</div>
</div>
    		 -->