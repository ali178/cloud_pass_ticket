@extends('site.layout')
@section('content')

<?php
  $tnc_body = $source == 'event' ? $tnc_body : $shared_site_vars['tnc_body'];
?>
<div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container">
        <div class="elevation-3dp" style="padding-top: 0px;">
        @include('site.sections.header')
      	<div class="listing-div detail_main_div">
      		<center>

            @if($tnc_body['tnc_arabic'] != '')
              <p class="arabic_font tnc_heading">شروط و احكام الموقع</p>
              <br>
              <p class="arabic_font tnc_body">
                {{$tnc_body['tnc_arabic']}}
              </p>
              <br>
            @endif

            @if($tnc_body['tnc_english'] != '')
              <p class="tnc_heading">Website Terms & Conditions</p>
              <br>
              <p class="tnc_body">
                {{$tnc_body['tnc_english']}}
              </p>
            @endif

          </center>      
      	</div>
        <br>
          @include('site.events._contact_us_bottom')
          <br>
      </div>
      @include('site.sections.footer')
    </div>
</div>
@endsection
