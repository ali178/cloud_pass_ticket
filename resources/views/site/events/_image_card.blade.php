<div class="card-div connect-event" style="margin-top: {{ $event->tag !=4 ? 'unset' : '-30px' }}">
    <!-- <a href="{{ URL::route('site.events.detail',['event_id'=>$event->id]) }}"> -->
    <a href="{{ URL::route($source == 'redemption' ? 'events' : 'site.events.detail',['event_id'=> $source == 'redemption' ? $event->event_id_str : $event->id]) }}">
    @if($event->tag !=4)
       @if($remaining == 0 && count($event->tiers))
            <img class="img-responsive right-corner-image" src="{{asset('assets/images/event_status_3.png')}}" style="width: auto;height: 30px;">
        @else
            <img class="img-responsive right-corner-image" src="{{asset('assets/images/event_status_'.$event->tag.'.png')}}" style="width: auto;height: 30px;">
        @endif
    @endif
    <div class="row card" style="height: auto; border: none;">
        <img class="img-responsive" src="{{asset($event->cover_image)}}" style="width: 100%;height: 100%; max-height: 402px;">

       <!--  <div class="middle">
        	<div class="middle-info-div">
        		<p class="middle_text">{{ date('d.m.y', strtotime($event->from_date_english)) }}<br>THE PARTY<br>To End All Parties</p>
        	</div>    
		</div> -->
      
    </div>

    <p class="event_title_arabic hidden">{{$event->title_arabic}}</p>
    <p class="event_title_english hidden">{{$event->title_english}}</p>
    <p class="event_description_english hidden">{{$event->description_english}}</p>
    <p class="event_description_arabic hidden">{{$event->description_arabic}}</p>
    <p class="event_venue_english hidden">{{$event->venue_english}}</p>
    <p class="event_venue_arabic hidden">{{$event->venue_arabic}}</p>

    <div class="row" style="margin: 2% -15px 0px -15px">
        <hr class="card_bottom_line">
        <img class="img-responsive" src="{{asset('assets/images/card_bottom.png')}}" style="width: auto;height: auto;">
    </div>
    </a>
</div>