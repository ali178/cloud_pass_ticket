<?php use Ticketing\Models\Issued_ticket;$num=1; ?>
@foreach($event->tiers as $tier)
<div class="card-div"  id="tier{{$tier->id }}">
    <div class="row card card-simple" style="height: auto !important; border:1px solid rgb(216, 216, 216);background-color: rgb(250, 250, 250);">
        <header class="tier_header" style="background-color: {{'#'.$tier->header_color }}">
            <span>{{$tier->title_english }}</span>
            <span class="arabic_font right">{{$tier->title_arabic }}</span>
        </header>
       <?php
        $price_type=$tier->checkFreeAndOpenOrFreeOny();
        // $adminTicketCount = Issued_ticket::getTotalAdminIssuedSum(3,$tier->event_id,2);
        $adminTicketCount = Issued_ticket::getTotalAdminIssuedSumNew(3,$tier->event_id,$tier->id);
        if($price_type=='free_and_unliminted_qty' || $price_type=='not_free_unlimited_qty'){
           $remainingQty=1;
        }else{
           $remainingQty=$tier->total_quantity - $tier->totaAvailable();
           $remainingQty=$tier->total_quantity - ($tier->totaAvailable() + $adminTicketCount);
            // $remainingQty=$tier->total_quantity - $tier->getIssuedTicketCount();
       }
       
        ?>
        @if($remainingQty > 0)


            <div class="tier_info">
    		<span>
    			<span class="amount">
    				<strong> @if($price_type=='free_and_unliminted_qty' || $price_type=='free_only_limited_qty') {{'Free'}} @else {{$tier->price }} @endif</strong>

                      @if($price_type=='free_and_unliminted_qty' || $price_type=='free_only_limited_qty')  @else <strong class="sr">SR</strong> @endif

    			</span>
                 <input type="hidden" name="tier_id[]" value="{{$tier->id }}" >
                 <input type="hidden" id="price{{$tier->id }}" value="@if($price_type=='free_and_unliminted_qty' || $price_type=='free_and_liminted_qty') {{'0'}} @else  {{$tier->price }} @endif" >
    			<span class="quantity right quant-input">
    				<img src="{{asset('assets/images/minus.png')}}"  class="minus minus_{{$tier->id }}">
                    <input type="number" name="total_qty[]" id="total_qty{{$tier->id }}" value="0" tierid="{{$tier->id }}" min="0"  @if($price_type=='free_and_unliminted_qty' || $price_type=='not_free_unlimited_qty')   @else 
                    max="{{$tier->total_quantity - ($tier->totaAvailable() + $adminTicketCount) }}" 
                    @endif readonly>
                    <img src="{{asset('assets/images/plus.png')}}" class="plus plus_{{$tier->id }}">
    			</span>
    		</span>
            <center>
                <p class="arabic_font tier_description_text"> {{$tier->description_arabic }}</p>
                <p class="tier_description_text">{{$tier->description_english }}</p>
            </center>
        </div>

         @else

            <div class="tier_info">
                <img class="img-responsive" src="{{asset('assets/images/soldout.png')}}">
            </div>

          @endif



    </div>
    @if($remainingQty > 0)
    <img class="img-responsive bottom-right-corner-image delete_icon" src="{{asset('assets/images/delete.png')}}" onclick="clearTierInput('{{$tier->id }}');" id="delete_{{$tier->id }}">
    @else
        <img class="img-responsive bottom-right-corner-image" src="{{asset('assets/images/delete_disable.png')}}">
    @endif
</div>
<?php $num++; ?>
@endforeach

{{--<div class="card-div">--}}
    {{--<div class="row card card-simple" style="height: auto !important;">--}}
        {{--<header class="tier_header" style="background-color: #367BCD;">--}}
            {{--<span>Executive</span>--}}
            {{--<span class="arabic_font right">تنفيذي</span>--}}
        {{--</header>--}}
        {{----}}
        {{--<div class="tier_info">--}}
            {{--<img class="img-responsive" src="{{asset('assets/images/soldout.png')}}">--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<img class="img-responsive bottom-right-corner-image" src="{{asset('assets/images/delete_disable.png')}}">--}}
{{--</div>--}}

