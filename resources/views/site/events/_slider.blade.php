@if(count($event->additional_images))
  <div id="fawesome-carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">


      @foreach($event->additional_images as $index => $image)
        <li data-target="#fawesome-carousel" data-slide-to="{{$index}}" class="{{ $index == 0 ? 'active' : ''}}"></li>
      @endforeach

  <!--     <li data-target="#fawesome-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#fawesome-carousel" data-slide-to="1"></li>
      <li data-target="#fawesome-carousel" data-slide-to="2"></li> -->
    </ol>

    <div class="carousel-inner" role="listbox" style="height: 175px;">

      @foreach($event->additional_images as $index => $image)
        <div class="item {{ $index == 0 ? 'active' : ''}}">
          <img src="{{asset($image->image)}}" style="width:100%;">
        </div>
      @endforeach

      <!-- <div class="item active">
        <img src="{{asset('assets/images/slider_sample1.jpg')}}" style="width:100%;">
      </div>

      <div class="item">
        <img src="{{asset('assets/images/slider_sample2.jpeg')}}" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="{{asset('assets/images/slider_sample3.jpg')}}" style="width:100%;">
      </div> -->
    </div>

    <a class="left fawesome-slide-control" href="#fawesome-carousel" role="button" data-slide="prev">
      <!-- <i class="fa fa-angle-left"></i> -->
      <img src="{{asset('assets/images/left-arrow.png')}}" style="width:40%;">
    </a>
    <a class="right fawesome-slide-control" href="#fawesome-carousel" role="button" data-slide="next">
      <!-- <i class="fa fa-angle-right"></i> -->
      <img src="{{asset('assets/images/right-arrow.png')}}" style="width:40%;">
    </a>
  </div>
@endif