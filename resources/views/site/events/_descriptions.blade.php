<div class="card-div" style="margin-bottom: 20px !important;">
  <div class="row card card-simple" style="height: auto !important;">
    <br><br>
    <div class="nav-navbar-center">
      <ul class="nav nav-pills">
        <li id="english_description_tab"><a href="#english_description" data-toggle="tab"><span class=" ">English</span></a></li>
        <li id="arabic_description_tab"><a href="#arabic_description " data-toggle="tab"><span class="arabic_font  ">العربية</span></a></li>
      </ul>
    </div>

    <div class="tab-content" style="margin-top: -22px;">
        <div id="english_description" class="tab-pane fade">
            <span style="white-space: pre-line;">
               {{$event->description_english}}
            </span>
        </div>

        <div id="arabic_description" class="tab-pane fade" style="direction: rtl;">
          <span class="arabic_font" style="white-space: pre-line;">
              {{$event->description_arabic}}
          </span>
        </div>
    </div>

  </div>
</div>