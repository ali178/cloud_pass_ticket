@extends('layouts.app')
@section('content')

<style type="text/css">
    table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }
</style>
<div class="page-title">
    <center>
        <h3>{{ __('documents.listing.title.english') }} <span class="arabic_font">{{ __('documents.listing.title.arabic') }}</span></h3>
    </center>
</div>
<div class="tab-content">
<div class="tab-pane fade active in">
    <div class='control' style="margin-left: 20px;margin-top: 0;margin-bottom: 0;">

        @if(Auth::user()->can("documents-create"))
            <a href="{{ URL::to('admin/documents/create') }}" class="btn btn-primary">Add إضافة +</a>
        @endif

        <div class="pull-right markaction">

            @if(Auth::user()->can("documents-edit"))
                <a id="show" href="javascript:void(0)" class="btn btn-success" data-url="{{ URL::to('admin/documents/bulkshow') }}">{{ __('documents.form.field.status_active') }} <span class="arabic_font">{{ __('documents.form.field.status_active_trans') }}</a>
                <a id="hide" href="javascript:void(0)" class="btn btn-warning" data-url="{{ URL::to('admin/documents/bulkhide') }}">{{ __('documents.form.field.status_inactive') }} <span class="arabic_font">{{ __('documents.form.field.status_inactive_trans') }}</a>
            @endif

            @if(Auth::user()->can("documents-destroy"))
                <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/documents/bulkdelete') }}">{{ __('documents.listing.delete.english') }} <span class="arabic_font">{{ __('documents.listing.delete.arabic') }}</span></a>
            @endif
        </div>
    </div>
    <br>

    <div class="grid simple vertical green mobile-scroll">
        <div class="grid-title no-border"></div>
        <div class="grid-body no-border">
            <table class="table table-hover table-condensed" id="datatable-example" >
                <thead>
                <tr>
                    <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                    <th>Id</th>
                    <th>{!! __('documents.table.name') !!}</th>
                    <th>{!! __('documents.table.name_arabic') !!}</th>
                    <th>{!! __('documents.table.visible') !!}</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
</div>
{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
    <input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!} 
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {

        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/documents/datatable') }}",
                "type": "POST"
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'title_arabic', name: 'title_arabic'},
                {data: 'show', name: 'show'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
               {
                    "targets": [1],
                    "visible": false,
                    "searchable": false

                },
                {
                    "targets": [0],
                    "searchable": false,
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5],
                    "orderable": false
                },
            ]
        });
    });
</script>

@endsection