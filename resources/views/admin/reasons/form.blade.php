@extends('layouts.app')
@section('content')

<style type="text/css">
    .page-title .title_arabic{
        margin-bottom: 0px;
    }
    .page-title .title_english{
        margin-top: 0px;
    }
</style>

<div class="page-title">
    <center>
        <h3 class="title_arabic"><span class="arabic_font">{{ __('reasons.form.title.arabic') }}</span> - <span class="semi-bold"><span class="arabic_font">{{ $reason ? __('edit.arabic') : __('add.arabic')}}</span></span></h3>
        <h3 class="title_english">{{ __('reasons.form.title.english') }} - <span class="semi-bold">{{ $reason ? __('edit.english') : __('add.english')}}</span></h3>

    </center>
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border"> <br>
                
                @if($errors->any())
                <div class="row">
                    <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                        <button data-dismiss="alert" class="close"></button>
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif

                {!! Form::model($reason,['route' => $reason ? ['admin.reasons.update','id'=>$reason->id] : 'admin.reasons.store','method'=> $reason ? 'PUT' : 'POST', 'id'=>'form_reason']) !!}

                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">Type <br> <span class="arabic_font">نوع</span> <span style="color:red">*</span></label>
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">
                        {!! Form::select('type',['1'=>'Request','2'=>'Ticket'], $reason ? $reason->type : null, ['class' => 'form-control','required'=>true]) !!}
                    </div>
                </div>


                <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('reasons.form.field.title_english') }} <br> <span class="arabic_font">{{ __('reasons.form.field.title_english_trans') }}</span> <span style="color:red">*</span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('title_english', null, ['class' => 'form-control','required'=>true,'id'=>'title_eng']) !!}
                            <span class="help-block"><span id="char_title_english">40</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('reasons.form.field.title_arabic') }} <br> <span class="arabic_font">{{ __('reasons.form.field.title_arabic_trans') }}</span> <span style="color:red">*</span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('title_arabic', null, ['class' => 'form-control','required'=>true,'id'=>'title_arabic']) !!}
                            <span class="help-block"><span id="char_title_arabic">40</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('reasons.form.field.description_english') }} <br> <span class="arabic_font">{{ __('reasons.form.field.description_english_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::textarea('description_english', null, ['class' => 'form-control','rows'=>10,'id'=>'description_eng']) !!}
                            <span class="help-block"><span id="char_description_english">5000</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('reasons.form.field.description_arabic') }} <br> <span class="arabic_font">{{ __('reasons.form.field.description_arabic_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::textarea('description_arabic', null, ['class' => 'form-control','rows'=>10,'id'=>'description_arabic']) !!}
                            <span class="help-block"><span id="char_description_arabic">5000</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('reasons.form.field.status') }} <br> <span class="arabic_font">{{ __('reasons.form.field.status_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12 radio-group inline">                                       
                            <div class="radio" style="margin-top: 3px;">
                                <input type="radio" {{$reason ?  ($reason->status==1 ? 'checked' : ''):'checked' }} value="1" name="status" id="yes">
                                <label for="yes">{{ __('reasons.form.field.status_active') }} <span class="arabic_font">{{ __('reasons.form.field.status_active_trans') }}</label>
                                <input type="radio" value="0" {{$reason ?  ($reason->status==0 ? 'checked' : ''):'' }}  name="status" id="no">
                                <label for="no">{{ __('reasons.form.field.status_inactive') }} <span class="arabic_font">{{ __('reasons.form.field.status_inactive_trans') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions" style="padding-top: 20px; padding-bottom: 15px; text-align: right;">

                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary" id="btn-submit">{!! $reason ? __('reasons.form.update_english').' <span class="arabic_font">'.__('reasons.form.update_arabic').'</span>' : __('reasons.form.save_english').' <span class="arabic_font">'.__('reasons.form.save_arabic').'</span>' !!} </button>
                        </div>
                        <div class="pull-left">
                            <a href="{{URL::to('admin/reasons')}}" class="btn btn-default">{{ __('reasons.form.cancel_english') }} <span class="arabic_font">{{ __('reasons.form.cancel_arabic') }}</span></a>
                        </div>

                    </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection
@section('page_script')
@parent
<script>
    jQuery('#form_reason').submit(function (event) {
        var pass = true;
        //some validations

        if (pass == false) {
            return false;
        }
        form = this;
        jQuery('#btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();

        return true;
    });
    $(document).ready(function(){

        $("#title_eng").keyup(function (event) {
            updateFeatureCharLimitNum('title_eng', 'char_title_english', 40);
        });
        $("#title_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('title_arabic', 'char_title_arabic', 40);
        });
        $("#description_eng").keyup(function (event) {
            updateFeatureCharLimitNum('description_eng', 'char_description_english', 5000);
        });
        $("#description_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('description_arabic', 'char_description_arabic', 5000);
        });
        updateFeatureCharLimitNum('title_eng', 'char_title_english', 40);
        updateFeatureCharLimitNum('title_arabic', 'char_title_arabic', 40);
        updateFeatureCharLimitNum('description_eng', 'char_description_english', 5000);
        updateFeatureCharLimitNum('description_arabic', 'char_description_arabic', 5000);


    });
</script>

@endsection
