@extends('layouts.app')
@section('content')

<style type="text/css">
    .page-title .title_arabic{
        margin-bottom: 0px;
    }
    .page-title .title_english{
        margin-top: 0px;
    }
</style>

<div class="page-title">
    <center>
    {{--<h3 class="title_arabic"><span class="arabic_font">{{ __('settings.form.title.arabic') }}</span> <span class="semi-bold"><span class="arabic_font">{{ $setting ? __('edit.arabic') : __('add.arabic')}}</span></span></h3>--}}
    <h3>{{ __('settings.form.title.english') }} <span class="arabic_font">{{ __('settings.form.title.arabic') }}</span></h3>
    </center>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border"> <br>
                
                @if($errors->any())
                <div class="row">
                    <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                        <button data-dismiss="alert" class="close"></button>
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif

                {!! Form::model($setting,['route' => ['admin.settings.update','id'=>$setting->id],'method'=> 'PUT', 'id'=>'form_settings']) !!}

                
                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.phone') }} <br> <span class="arabic_font">{{ __('settings.form.field.phone_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('phone', null, ['class' => 'form-control','onkeypress'=>'return isNumberKey(event)', 'maxlength'=>'15','id'=>'phone']) !!}
                            <span class="help-block"><span id="char_phone_english">15</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.email') }} <br> <span class="arabic_font">{{ __('settings.form.field.email_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::email('email', null, ['class' => 'form-control','id'=>'email']) !!}
                            <span class="help-block"><span id="char_email_english">60</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.copyright_english') }} <br> <span class="arabic_font">{{ __('settings.form.field.copyright_english_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('copyright_english', null, ['class' => 'form-control','id'=>'copyright_eng']) !!}
                            <span class="help-block"><span id="char_copyright_english">80</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.copyright_arabic') }} <br> <span class="arabic_font">{{ __('settings.form.field.copyright_arabic_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('copyright_arabic', null, ['style' => 'direction: rtl','class' => 'form-control','id'=>'copyright_arabic']) !!}
                            <span class="help-block"><span id="char_copyright_arabic">80</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.vat') }} <br> <span class="arabic_font">{{ __('settings.form.field.vat_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                             {!! Form::input('number','vat_value', null, ['class' => 'form-control','min'=>'0','max'=>'99999']) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.beneficiary_name_english') }} <br> <span class="arabic_font">{{ __('settings.form.field.beneficiary_name_english_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('beneficiary_name_english', null, ['style' => '','class' => 'form-control','id'=>'beneficiary_name_english']) !!}
                            <span class="help-block"><span id="char_beneficiary_name_english">80</span> <?php echo __('general_characters_left'); ?></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.beneficiary_name_arabic') }} <br> <span class="arabic_font">{{ __('settings.form.field.beneficiary_name_arabic_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('beneficiary_name_arabic', null, ['style' => 'direction: rtl','class' => 'form-control','id'=>'beneficiary_name_arabic']) !!}
                            <span class="help-block"><span id="char_beneficiary_name_arabic">80</span> <?php echo __('general_characters_left'); ?></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.bank_name_english') }} <br> <span class="arabic_font">{{ __('settings.form.field.bank_name_english_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('bank_name_english', null, ['style' => '','class' => 'form-control','id'=>'bank_name_english']) !!}
                            <span class="help-block"><span id="char_bank_name_english">80</span> <?php echo __('general_characters_left'); ?></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.bank_name_arabic') }} <br> <span class="arabic_font">{{ __('settings.form.field.bank_name_arabic_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('bank_name_arabic', null, ['style' => 'direction: rtl','class' => 'form-control','id'=>'bank_name_arabic']) !!}
                            <span class="help-block"><span id="char_bank_name_arabic">80</span> <?php echo __('general_characters_left'); ?></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.account') }} <br> <span class="arabic_font">{{ __('settings.form.field.account_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('account', null, ['style' => '','class' => 'form-control','id'=>'account']) !!}
                            <span class="help-block"><span id="char_account">40</span> <?php echo __('general_characters_left'); ?></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.iban') }} <br> <span class="arabic_font">{{ __('settings.form.field.iban_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('iban', null, ['style' => '','class' => 'form-control','id'=>'iban']) !!}
                            <span class="help-block"><span id="char_iban_english">40</span> <?php echo __('general_characters_left'); ?></span>

                        </div>
                    </div>

                    <div class="form-actions" style="padding-top: 20px; padding-bottom: 15px; text-align: right;">
                        <button type="submit" class="btn btn-primary" id="btn-submit">{!! $setting ? __('reasons.form.update_english').' <span class="arabic_font">'.__('reasons.form.update_arabic').'</span>' : __('reasons.form.save_english').' / <span class="arabic_font">'.__('reasons.form.save_arabic').'</span>' !!} </button>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection
@section('page_script')
@parent
<script>
    jQuery('#form_settings').submit(function (event) {

        form = this;
        jQuery('#btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();
        return true;
    });
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    $(document).ready(function(){
        $("#phone").keyup(function (event) {
            updateFeatureCharLimitNum('phone', 'char_phone_english', 15);
        });
        $("#email").keyup(function (event) {
            updateFeatureCharLimitNum('email', 'char_email_english', 60);
        });
        $("#copyright_eng").keyup(function (event) {
            updateFeatureCharLimitNum('copyright_eng', 'char_copyright_english', 80);
        });
        $("#copyright_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('copyright_arabic', 'char_copyright_arabic', 80);
        });
        $("#iban").keyup(function (event) {
            updateFeatureCharLimitNum('iban', 'char_iban_english', 40);
        });
        updateFeatureCharLimitNum('phone', 'char_phone_english', 15);
        updateFeatureCharLimitNum('email', 'char_email_english', 60);
        updateFeatureCharLimitNum('copyright_eng', 'char_copyright_english', 80);
        updateFeatureCharLimitNum('copyright_arabic', 'char_copyright_arabic', 80);
        updateFeatureCharLimitNum('iban', 'char_iban_english', 40);

        $("#beneficiary_name_english").keyup(function (event) {
            updateFeatureCharLimitNum('beneficiary_name_english', 'char_beneficiary_name_english', 80);
        });
        updateFeatureCharLimitNum('beneficiary_name_english', 'char_beneficiary_name_english', 80);

        $("#beneficiary_name_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('beneficiary_name_arabic', 'char_beneficiary_name_arabic', 80);
        });
        updateFeatureCharLimitNum('beneficiary_name_arabic', 'char_beneficiary_name_arabic', 80);

        $("#bank_name_english").keyup(function (event) {
            updateFeatureCharLimitNum('bank_name_english', 'char_bank_name_english', 80);
        });
        updateFeatureCharLimitNum('bank_name_english', 'char_bank_name_english', 80);

        $("#bank_name_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('bank_name_arabic', 'char_bank_name_arabic', 80);
        });
        updateFeatureCharLimitNum('bank_name_arabic', 'char_bank_name_arabic', 80);

        $("#account").keyup(function (event) {
            updateFeatureCharLimitNum('account', 'char_account', 40);
        });
        updateFeatureCharLimitNum('account', 'char_account', 40);
    });
</script>

@endsection
