@extends('layouts.app')
@section('content')

    <style type="text/css">
    table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }
</style>
<div class="page-title">
    <center>
        <h3>Users المدراء</h3>
    </center>
</div>

<div class="tab-content">
    <div style="margin-top: 15px"></div>
    <div class='control pull-left' style="margin-left: 20px;margin-bottom: 0">
        <div class="nav-navbar-center">
            <ul class="nav nav-pills">
                <li id="systemusers_tab"><a href="#systemusers" data-toggle="tab">System Users<span style="margin-left: 10px"></span><span class="heading_arabic" style="margin-left: 10px">مستخدمي النظام</span></a></li>
                <li id="volunteers_tab"><a href="#volunteers" data-toggle="tab">Volunteers<span style="margin-left: 10px"></span><span class="heading_arabic" style="margin-left: 10px">المتطوعين</span></a></li>
            </ul>
        </div>
    </div>

    <div id="systemusers" class="tab-pane fade">

        <div class='control' style="margin-left: 20px;">
            @if(Auth::user()->can('users-create'))
            <a href="{{ URL::to('admin/users/create?source=systemusers') }}" class="btn btn-primary">Add إضافة +</a>
            @endif
            <div class="pull-right markaction">
                @if(Auth::user()->can('users-edit'))
                <a id="show" href="javascript:void(0)" class="btn btn-success" data-url="{{ URL::to('admin/users/bulkshow?source=systemusers') }}">Activate تنشيط</a>
                <a id="hide" href="javascript:void(0)" class="btn btn-warning" data-url="{{ URL::to('admin/users/bulkhide?source=systemusers') }}">De-activate إيقاف</a>
                @endif
                @if(Auth::user()->can('users-destroy'))
                <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/users/bulkdelete?source=systemusers') }}">Delete حذف</a>
                @endif
            </div>
        </div>
        <br>
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-body no-border">
                <table class="table table-hover table-condensed" id="datatable-example" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                            <th>Id</th>
                            <th>{!! __('users.table.username') !!}</th>
                            <th>{!! __('users.table.email') !!}</th>
                            <th>{!! __('users.table.role') !!}</th>
                            <th id="status-head">{!! __('users.table.status') !!}</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="volunteers" class="tab-pane fade">

        <div class='control' style="margin-left: 20px;">
            @if(Auth::user()->can('users-create'))
            <a href="{{ URL::to('admin/users/create?source=volunteers') }}" class="btn btn-primary">Add إضافة +</a>
            @endif
            <div class="pull-right markaction">
                @if(Auth::user()->can('users-edit'))
                <a id="show2" href="javascript:void(0)" class="btn btn-success" data-url="{{ URL::to('admin/users/bulkshow?source=volunteers') }}">Activate تنشيط</a>
                <a id="hide2" href="javascript:void(0)" class="btn btn-warning" data-url="{{ URL::to('admin/users/bulkhide?source=volunteers') }}">De-activate إيقاف</a>
                @endif
                @if(Auth::user()->can('users-destroy'))
                <a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/users/bulkdelete?source=volunteers') }}">Delete حذف</a>
                @endif
            </div>
        </div>
        <br>
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-body no-border">
                <table class="table table-hover table-condensed" id="datatable-example2" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                            <th>Id</th>
                            <th>{!! __('users.table.username') !!}</th>
                            <th>{!! __('users.table.email') !!}</th>
                            <th id="status-head">{!! __('users.table.status') !!}</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
    <input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!}

@endsection

@section('page_script')
@parent


<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {

        var users_type = '{{Request::get("source")}}';
        if(users_type == 'systemusers'){
            $( "#systemusers_tab" ).addClass( "active" );
            $( "#systemusers" ).addClass( "active in" );
            $( "#volunteers_tab" ).removeClass( "active" );
        }else if(users_type == 'volunteers'){
            $( "#volunteers_tab" ).addClass( "active" );
            $( "#volunteers" ).addClass( "active in" );
            $( "#systemusers_tab" ).removeClass( "active" );
        }else{
            $( "#systemusers_tab" ).addClass( "active" );
            $( "#systemusers" ).addClass( "active in" );
            $( "#volunteers_tab" ).removeClass( "active" );
        }

        $('#systemusers_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.users.index')}}?source=systemusers");
        });
        $('#volunteers_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.users.index')}}?source=volunteers");
        });

        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/users/datatable') }}",
                "type": "POST",
                "data": {'type':0}
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'role', name: 'role'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0, 5],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5,6],
                    "orderable": false
                },
               
            ]
        });

        $('#datatable-example2').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/users/datatable') }}",
                "type": "POST",
                "data": {'type':1}
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,4,5],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5],
                    "orderable": false
                },
               
            ]
        });
    });
</script>
@endsection