@extends('layouts.app')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" type="text/css">

<?php
    $source = Request::get('source') == 'volunteers' ? 1 : 0;
    $source = $user ? $user->type : $source;
?>
<div class="page-title">
    <!-- <h3>Users - <span class="semi-bold">{{$user ? 'Edit' : 'Add'}}</span></h3> -->
    <center>
        <h3>المدراء - <span class="arabic_font">{{ $user ? __('edit.arabic') : __('add.arabic')}}</span></h3>
        <h3>User - <span class="semi-bold">{{ $user ? 'Edit' : 'Add'}}</span></h3>
    </center>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <!-- <h4>{{$user ? 'Edit' : 'Add'}} <span class="semi-bold">User</span></h4> -->
                <!-- <div class="tools"> 
                    <a class="collapse" href="javascript:;"></a> 
                    <a class="reload" href="javascript:;"></a> 
                </div> -->
            </div>
            <div class="grid-body no-border"> <br>
                @if($errors->any())
                <div class="row">
                    <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                        <button data-dismiss="alert" class="close"></button>
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif
                {!! Form::model($user, ['method' => $user ? 'PATCH' : 'POST','route' => $user ? ['admin.users.update', $user->id] : 'admin.users.store','id'=>'form_users',"enctype"=>"multipart/form-data"]) !!}
                <!-- <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        {!! Form::label('first_name', 'First Name', ['class' => 'form-label']) !!}
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                    </div>
                </div> -->
                <!-- <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        {!! Form::label('last_name', 'Last Name', ['class' => 'form-label']) !!}
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                    </div>
                </div> -->
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('users.form.field.name') }} <br> <span class="arabic_font">{{ __('users.form.field.name_trans') }}</span> <span style="color:red"></span></label>

                    </div>
                    <!-- <span class="help">e.g. "Jonh Smith"</span> -->
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::text('name', null, ['class' => 'form-control','id'=>'name']) !!}
                        <span class="help-block"><span id="char_name_english">40</span> <?php echo __('general_characters_left'); ?></span>

                    </div>
                </div>
                {!! Form::hidden('type', $source , ['class' => 'form-control']) !!}
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('users.form.field.username') }} <span style="color:red">*</span><br> <span class="arabic_font">{{ __('users.form.field.username_trans') }}</span></label>

                    </div>
                    <!-- <span class="help">e.g. "Jonh Smith"</span> -->
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::text('username', null, ['class' => 'form-control','id'=>'username']) !!}
                        <span class="help-block"><span id="char_username_english">20</span> <?php echo __('general_characters_left'); ?></span>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('users.form.field.email') }} <br> <span class="arabic_font">{{ __('users.form.field.email_trans') }}</span> <span style="color:red"></span></label>

                        <!-- <span class="help">e.g. "john@examp.com"</span> -->
                    </div>  
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::email('email', null, ['class' => 'form-control','id'=>'email']) !!}
                        <span class="help-block"><span id="char_email_english">60</span> <?php echo __('general_characters_left'); ?></span>

                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('users.form.field.password') }} <span style="color:red">*</span><br> <span class="arabic_font">{{ __('users.form.field.password_trans') }}</span> <span style="color:red"></span></label>

                    </div>  
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::input('password','password', "", ['class' => 'form-control','id'=>'password']) !!}
                        <span class="help-block"><span id="char_pass_english">10</span> <?php echo __('general_characters_left'); ?></span>

                        <span class="help">{{$user ? "If you don't want to change password leave it empty." : ''}}</span>
                        
                    </div>
                </div>
                
                @if($source == 0)
                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('users.form.field.role') }} <br> <span class="arabic_font">{{ __('users.form.field.role_trans') }}</span> <span style="color:red"></span></label>

                        </div>  
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                   

                            {!! Form::select('role', $roles,$user && $user->roles()->first() ? $user->roles()->first()->id : '', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                @elseif($source == 1)
                    <div class="form-group row">
                      <div class="col-md-3 col-sm-12 text-right">
                        <label class="control-label">Assign Events<br>إختيار المناسبات</label>
                      </div>
                      <div class="col-md-6 col-sm-12 text">
                        <label for='assign_all_events' class="check">
                            <input name='assign_all_events' type="checkbox" id="assign_all_events" {{ $user && $user->assign_all_events ? 'checked' : '' }}/> All Events جميع المناسبات 
                        </label>
                      </div>
                      <div class="col-md-2 col-sm-12">
                        <!-- <a href="{{ URL::to('admin/permissions')}}" class='btn btn-primary'>Reload Permissions</a> -->
                      </div>
                    </div>
                    
                    <div class="form-group row event-list {{ $user && $user->assign_all_events ? 'hidden' : '' }}">  
                        <label class="col-md-3 col-xs-12 text-right control-label"></label>
                        <div class="col-md-8 col-xs-12">
                            <label class="check">
                                <input name='check_all' type="checkbox" id="check_all_events"/> Select All Events اختر جميع المناسبات
                            </label>
                        </div>
                    </div>
                    @foreach ($events as $event_id => $event_title)
                        <div class="form-group row event-list {{ $user && $user->assign_all_events ? 'hidden' : '' }}">
                            
                            <label class="col-md-3 col-xs-12 text-right control-label"></label>
                            <div class="col-md-8 col-xs-12">
                                <label class="check">
                                    <input type="checkbox" <?php echo array_key_exists($event_id,$volunteer_events)? 'checked': '' ?> name="events[<?php echo  $event_id; ?>]" class="event_check"/> {{$event_title}}
                                </label>
                            </div>
                        </div>
                    @endforeach
                @endif

                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('users.form.field.status') }} <br> <span class="arabic_font">{{ __('users.form.field.status_trans') }}</span> <span style="color:red"></span></label>

                    </div>
                    <div class='radio-group inline col-md-6 col-sm-12'>    
                        <div class="radio">
                            <input type="radio" {{$user ?  ($user->status==1 ? 'checked' : ''):'checked' }} value="1" name="status" id="yes">
                            <label for="yes">Active نشيط</label>
                            <input type="radio" value="0" {{$user ?  ($user->status==0 ? 'checked' : ''):'' }}  name="status" id="no">
                            <label for="no">In-active متوقف</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-actions">  
                    <div class="pull-left">
                        <a class="btn btn-default btn-cons" href="{{url('/admin/users?source=').Request::get('source')}}" >Cancel إلغاء</a>
                    </div>
                    <div class="pull-right">
                        {!! Form::submit($user ? 'Update تحديث' : 'Save حفظ', ['class' => 'btn btn-primary','id' => 'btn-submit']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
@parent

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" language="javascript" class="init">

    jQuery('#form_users').submit(function (event) {
        var pass = true;
        //some validations

        if (pass == false) {
            return false;
        }
        form = this;
        jQuery('#btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();

        return true;
    });
    $(document).ready(function(){
        $('.selectbox').select2({placeholder: 'Select Role'});
        $('#check_all_events').bind('change',function(){
            if(this.checked){
                $('.event_check').prop('checked',true);
            } else {
                $('.event_check').prop('checked',false);
            }
        });

        $('#assign_all_events').bind('change',function(){
            if(this.checked){
                $('.event-list').addClass('hidden');
            } else {
                $('.event-list').removeClass('hidden');
            }
        });

        $("#name").keyup(function (event) {
            updateFeatureCharLimitNum('name', 'char_name_english', 40);
        });
        $("#username").keyup(function (event) {
            updateFeatureCharLimitNum('username', 'char_username_english', 20);
        });
        $("#email").keyup(function (event) {
            updateFeatureCharLimitNum('email', 'char_email_english', 60);
        });
        $("#password").keyup(function (event) {
            updateFeatureCharLimitNum('password', 'char_pass_english', 10);
        });
        updateFeatureCharLimitNum('name', 'char_name_english', 40);
        updateFeatureCharLimitNum('username', 'char_username_english', 20);
        updateFeatureCharLimitNum('email', 'char_email_english', 60);
        updateFeatureCharLimitNum('password', 'char_pass_english', 10);




    });
  
</script>

@endsection