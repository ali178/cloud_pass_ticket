@extends('layouts.app')
@section('content')
<style type="text/css">
    table#datatable-example4,table#datatable-example3,table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }

    .nav-navbar-center {
        margin-top: 0px;
    }
</style>
<div class="page-title">
    <center>
{{--       @if($event->eventStatusById($event->id))--}}
       @if($event->status == 1 && $event->selling_off == 0)
        <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
       @else
        <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
       @endif
        <br/>
        <span style="font-size: 16px;margin-top: 5px;display: inline-block;font-weight: bold">{{$event->title_arabic}}</span>
        <br/>
        <span style="font-size: 16px;margin-bottom: 15px;display: inline-block;font-weight: bold">{{$event->title_english}}</span>
        <br/>
    </center>
</div>


<div class="page-title">
    <center>
        <h3>All Event Requests  جميع طلبات الحدث</h3>
        {{--<br/>--}}
        {{--<span>Foie gras is a luxury food product made of the liver of a duck or goose that has been specially fattened. By French law</span>--}}
        {{--<br/>--}}
        {{--<span>هو منتج غذائي فاخر مصنوع من كبد البط أو الأوز الذي تم تسمينه بشكل خاص. بموجب القانون الفرنسي</span>--}}
        {{--<br>--}}
    </center>
</div>










<div style="font-size: 18px;background-color: white;margin-bottom: 10px">
    {{--<div class="nav-navbar-center">--}}
        {{--<ul class="nav nav-pills" style="margin-top: 15px;">--}}
            {{--<li style="margin-right: 60px">--}}
                {{--<div class="row" style="padding-bottom: 10px">--}}
                    {{--Total Ordered حضرها--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<span style="font-size: 26px; color: black" >{{$preordered_tiers_qty_sum}}</span>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<div class="row" style="padding-bottom: 10px">--}}
                    {{--Total Pending مجموع--}}
                {{--</div>--}}
                {{--<div class="row" >--}}
                {{--<span style="font-size: 26px; color: black" >{{$pending_tiers_qty_sum}}</span>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li style="margin-left: 60px">--}}
                {{--<div class="row" style="padding-bottom: 10px">--}}
                    {{--Total Issued مجموع--}}
                {{--</div>--}}
                {{--<div class="row" >--}}
                {{--<span style="font-size: 26px; color: black" >{{$issued_tiers_qty_sum}}</span>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<li style="margin-left: 60px">--}}
                {{--<div class="row" style="padding-bottom: 10px">--}}
                    {{--Total Available تم البيع</div>--}}
                {{--<div class="row">--}}
                {{--<span style="font-size: 26px; color: black" >{{ $available_qty}}</span>--}}
                {{--</div>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}

        <table class="table table-hover">

            <tr style="padding: 5px">
                <th style="text-align: center">النوع<br>Tiers</th>
                <th style="text-align: center">التذاكر المتاحة<br>Offered</th>
                <th style="text-align: center">التذاكر المباعة<br>Issued</th>

                <th style="text-align: center">التذاكر المطلوبة<br>Requests</th>
                <th style="text-align: center">التذاكر المجانية<br>Free</th>
                <th style="text-align: center">التذاكر المتبقية<br>Left</th>

                <th style="text-align: center">الحضور<br>Attended</th>
                <th style="text-align: center">سعر التذكرة<br>Price</th>
                <th style="text-align: center">الاجمالي<br>Total</th>
                <th style="text-align: center">الضريبة<br>VAT</th>
            </tr>

            <tbody>
            @foreach($event->tiers as $tier)
                <tr style="padding: 5px">
                    <td style="background-color:#{{$tier->header_color}}; text-align: right;color: white;font-size: 14px;">{{$tier->title_arabic}}</td>
                    <?php $total_quantity = $tier->total_quantity ?>
                    <td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0) {{'Open'}} @else {{$total_quantity}}@endif</td>
                    <?php

                    $getIssuedTicketCount = $tier->getIssuedTicketCount();
                    $getFreeIssuedTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedFree(3,$event->id,$tier->id);

                    // $getIssuedTicketStats = $tier->getIssuedTicketStats();
                    $getIssuedTicketStats = $tier->getIssuedTicketStatsNew();
                    $getTotalTicketStats = $tier->totaAvailable();
//                    $adminTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedSum(3,$event->id,2);
                    $adminTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedSumNew(3,$event->id,$tier->id);


                    ?>
                    <td style="text-align: center;font-size: 14px;">{{$getIssuedTicketCount - $getFreeIssuedTicketCount}}</td>

                <?php $getEventRequestCount = $tier->totaAvailable() ?>
                    <td style="text-align: center;font-size: 14px;">{{$getEventRequestCount}}</td>
                    <td style="text-align: center;font-size: 14px;">{{$tier->getFreeTicketAsGiftByAdminFromAdminSideCount()}}</td>
                    {{--<td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0){{'Open'}}@else{{$total_quantity - ($getEventRequestCount + $getIssuedTicketCount)}}@endif</td>--}}
                    <td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0){{'Open'}}@else
                        {{$total_quantity - ($getTotalTicketStats + $adminTicketCount) }}
                    @endif</td>
                    <td style="text-align: center;font-size: 14px;">{{$tier->getRedemptionCount()}}</td>
                    <td style="text-align: center;font-size: 14px;">@if($tier->price_type==0){{'Free'}}@else{{$tier->price}}@endif</td>
                <!-- <td style="text-align: center;font-size: 14px;">{{$getIssuedTicketCount*$tier->price}}</td>
                        <td style="text-align: center;font-size: 14px;">{{ 5/100 * ($getIssuedTicketCount*$tier->price)}}</td> -->

                    <td style="text-align: center;font-size: 14px;">{{ $getIssuedTicketStats['total_price_per_tier'] }}</td>
                    <td style="text-align: center;font-size: 14px;">{{ $getIssuedTicketStats['total_vat_per_tier'] }}</td>

                </tr>
            @endforeach
            </tbody>
        </table>

    {{--<br>--}}
</div>


<div class="tab-content">

    <div class='control pull-left' style="margin-top: 20px;margin-left: 20px;margin-bottom: 0">
        <div class="nav-navbar-center">
            <ul class="nav nav-pills">
                <li id="preordered_tab" style="margin-right: 7px">
                    <a href="#preodered_event_requests" data-toggle="tab">

                        Ordered

                        <span class="heading_arabic" style="margin-left: 10px">تحت الطلب</span>

                    </a>
                </li>
                <li id="pending_tab" style="margin-right: 7px"><a href="#pending_event_requests " data-toggle="tab">Pending <span class="heading_arabic" style="margin-left: 10px">تنتظر الدفع</span></a></li>
                <li id="issued_tab" style="margin-right: 7px"><a href="#issued_event_requests" data-toggle="tab">Accepted  <span class="heading_arabic" style="margin-left: 10px">مقبولة</span></a></li>
                <li id="cancelled_tab" style="margin-right: 7px"><a href="#cancelled_event_requests " data-toggle="tab">Rejected  <span class="heading_arabic" style="margin-left: 10px">مرفوضة</span></a></li>

            </ul>
        </div>
    </div>


    <div id="preodered_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">

            <div class="grid-body no-border">
                <a href="{{ URL::to('admin/events') }}" class="btn btn-default" >Cancel إلغاء</a>


                <div class="pull-right markaction">
                    @if(Auth::user()->can("eventrequests-reject"))
                        <a id="reject" href="javascript:void(0)" class="btn btn-reject" data-url="{{ URL::to('admin/event_requests/bulkreject') }}">Reject رفض</a>
                    @endif
                    {{--@if(Auth::user()->can("eventrequests-destroy"))--}}
                        {{--<a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete حذف</a>--}}
                    {{--@endif--}}
                </div>
                <br><br><br>
                <table class="table table-hover table-condensed" id="datatable-example" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                            <th>Id</th>
                            <th><center>معلومات الطلب <br> Request Information</center></th>
                            <th><center>إجمالي المبلغ <br> Total(SR)</center></th>
                            <th><center>المصدر <br> Source</center></th>
                            <th><center>آخر تحديث <br> Last Edit</center></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="pending_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border" style="margin-top: -21px">
                <div class="pull-right markaction">
                    @if(Auth::user()->can("eventrequests-reject"))
                        <a id="reject1" href="javascript:void(0)" class="btn btn-reject" data-url="{{ URL::to('admin/event_requests/bulkreject') }}">Reject رفض</a>
                    @endif
                    {{--@if(Auth::user()->can("eventrequests-destroy"))--}}
                        {{--<a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete حذف</a>--}}
                    {{--@endif--}}
                </div><br><br><br>

                <table class="table table-hover table-condensed" id="datatable-example2" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                            <th>Id</th>

                            <th><center>معلومات الطلب <br> Request Information</center></th>
                            <th><center>إجمالي المبلغ <br> Total(SR)</center></th>
                            <th><center>المصدر <br> Source</center></th>
                            <th><center>آخر تحديث <br> Last Edit</center></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div id="issued_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                {{--<div class="pull-right markaction">--}}
                    {{--@if(Auth::user()->can("eventrequests-destroy"))--}}
                        {{--<a id="del3" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete حذف</a>--}}
                    {{--@endif--}}
                {{--</div><br><br><br>--}}

                <table class="table table-hover table-condensed" id="datatable-example3" >
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="group-checkable" data-set="#datatable-example3 .checkboxes" /></th>
                        <th>Id</th>

                        <th><center>معلومات الطلب <br> Request Information</center></th>
                        <th><center>إجمالي المبلغ <br> Total(SR)</center></th>
                        <th><center>المصدر <br> Source</center></th>
                        <th><center>آخر تحديث <br> Last Edit</center></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div id="cancelled_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-right markaction">
                    {{--@if(Auth::user()->can("eventrequests-destroy"))--}}
                        {{--<a id="del4" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete حذف</a>--}}
                    {{--@endif--}}
                </div>

                <table class="table table-hover table-condensed" id="datatable-example4" >
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="group-checkable" data-set="#datatable-example4 .checkboxes" /></th>
                        <th>Id</th>

                        <th><center>معلومات الطلب <br> Request Information</center></th>
                        <th><center>إجمالي المبلغ <br> Total(SR)</center></th>
                        <th><center>المصدر <br> Source</center></th>
                        <th><center>آخر تحديث <br> Last Edit</center></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- maps modal -->
<div class="modal fade" id="rejectionOnReasonModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Request Rejection<br/>طلب الرفض</h4>
            </div>
            {!! Form::open(array('route' =>["admin.event_requests.reject_status", 5], 'id'=>'addRejectionForm', 'role'=>'form','onsubmit'=>"$('#progressbar').show()")) !!}

            <input type="hidden" name="event_request_id" id="event_request_id" value="">

            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">Reason<br><span class="arabic_font">السبب</span><span style="color:red">*</span></label>
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">
                        {!! Form::select('reason_id',[''=>'--- SELECT ---'],null, ['class' => 'form-control','id'=>'reason_id', 'placeholder'=>'--- SELECT ---', 'required'=>'required']) !!}
                    </div>
                </div>

               <div class="row">
                   <div class="col-md-12">
                <div class="form-group">
                    <label class="form-label">Description Arabic &emsp; <span class="arabic_font">التفاصيل بالعربي</span> <span style="color:red"></span></label>
                    {!! Form::textarea('description_arabic', null, ['class' => 'form-control','id'=>'description_arabic','rows'=>3,'style'=>'direction: rtl']) !!}
                    <span class="help-block"><span id="char_description_arabic">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                </div>
                 </div>
               </div>

                <div class="row">
                    <div class="col-md-12">
                <div class="form-group">
                 <label class="form-label">Description English &emsp; <span class="arabic_font">التفاصيل بالانجليزي</span> <span style="color:red"></span></label>
                  {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'description_english','rows'=>3]) !!}
                    <span class="help-block"><span id="char_description_english">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                </div>
                </div>
                </div>


            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel إالغاء</button>

                    {!! Form::submit('Send إرسال', ['class' => 'btn btn-green tier-form-submit','id' => 'add_tier_btn']) !!}
                </center>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>


<div class="modal fade" id="rejectBulkMessage" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Request Rejection<br/>طلب الرفض</h4>
            </div>
            {!! Form::open(array('route' =>["admin.event_requests.reject_status", 5], 'id'=>'bulkRejectionForm', 'role'=>'form','onsubmit'=>"$('#progressbar').show()")) !!}
            <input type="hidden" value="" name="ids" id="rejectids" />
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">Rejection Reason <br> <span class="arabic_font">سبب الرفض</span> <span style="color:red">*</span></label>
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">
                        {!! Form::select('reason_id',[''=>'--- SELECT ---'],null, ['class' => 'form-control','id'=>'bulk_reason_id','placeholder'=>"-- SELECT --"]) !!}
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description Arabic &emsp; <span class="arabic_font">التفاصيل بالعربي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_arabic', null, ['class' => 'form-control','id'=>'bulk_description_arabic','rows'=>3,'style'=>'direction: rtl']) !!}
                            <span class="help-block"><span id="bulk_char_description_arabic">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description English &emsp; <span class="arabic_font">التفاصيل بالانجليزي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'bulk_description_english','rows'=>3]) !!}
                            <span class="help-block"><span id="bulk_char_description_english">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel إالغاء</button>

                    {!! Form::submit('Send إرسال', ['class' => 'btn btn-green tier-form-submit','id' => 'add_tier_btn']) !!}
                </center>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>


</div>
{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
<input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!}
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {
        var event_type = '{{Request::get("source")}}';
        if(event_type == 'preordered'){
            $( "#preordered_tab" ).addClass( "active" );
            $( "#preodered_event_requests" ).addClass( "active in" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'pending'){
            $( "#pending_tab" ).addClass( "active" );
            $( "#pending_event_requests" ).addClass( "active in" );
            $( "#preordered_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'issued'){
            $( "#issued_tab" ).addClass( "active" );
            $( "#issued_event_requests" ).addClass( "active in" );
            $( "#preordered_tab" ).removeClass( "active" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'cancelled'){
            $( "#cancelled_tab" ).addClass( "active" );
            $( "#cancelled_event_requests" ).addClass( "active in" );
            $( "#preordered_tab" ).removeClass( "active" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
        } else{
            $( "#preordered_tab" ).addClass( "active" );
            $( "#preodered_event_requests" ).addClass( "active in" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }

        $('#preordered_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=preordered");
        });
        $('#pending_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=pending");
        });

        $('#issued_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=issued");
        });
        $('#cancelled_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=cancelled");
        });



        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':1,
                         'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4, 5]
                }
            ]
        });

        $('#datatable-example2').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':2,
                         'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4, 5]
                }
            ]
        });


        $('#datatable-example3').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':3,
                         'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4, 5]
                }
            ]
        });


        $('#datatable-example4').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':4,
                          'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4, 5]
                }
            ]
        });


        // by iqbal
    });

    $(document).on('submit', '.rejectaction', function() {
       // var ret = confirm("Are you sure you want to delete?");
       // if (ret) {
           // $("#progressbar").show();
           // return true;
       // }
       // return false;

        $("#progressbar").show();
        return false;
    });
   // var form = '';
    $(document).on('click', '.rejectaction', function() {
        eventrequestid = $(this).attr('data-eventrequestid');
       // alert(eventrequestid);
        urlrequest = $(this).attr('data-url');
        $.ajax({
            dataType: 'JSON',
            url: base_path+"/admin/event_requests/getreasonbytype",
            type: 'POST',
            data: {
               // _token: $('input[name=_token]').val(),
                eventrequestid:eventrequestid,
            },
            beforeSend: function () {
               // overlay_ajax();
            },
            success: function (data) {
               // alert(data.reasonlist);

                $('#addRejectionForm').attr('action', urlrequest);//'http://localhost/ticketing/public/admin/event_requests/'+eventrequestid+'/reject_status'
                $('#event_request_id').val('');
                $('#char_description_english,#char_description_arabic').text(250);
                $('#addRejectionForm')[0].reset();
                $('#rejectionOnReasonModal').modal('show');
                $('#reason_id').html(data.reasonlist);
                $('#event_request_id').val(eventrequestid);
                $("#progressbar").hide();

            },
            error: function () {
               // $('#modal-default').modal('show');
            },
            complete: function () {
               // $('.box-body').unblock();
            }
        });

       // return false;
    });


    $(document).ready(function(){
        $("#description_english").keyup(function (event) {
            updateFeatureCharLimitNum('description_english','char_description_english',250);
        });

        $("#description_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
        });

        $("#bulk_description_english").keyup(function (event) {
            updateFeatureCharLimitNum('bulk_description_english','bulk_char_description_english',250);
        });

        $("#bulk_description_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('bulk_description_arabic','bulk_char_description_arabic',250);
        });
        updateFeatureCharLimitNum('description_english','char_description_english',250);
        updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
        updateFeatureCharLimitNum('bulk_description_english','bulk_char_description_english',250);
        updateFeatureCharLimitNum('bulk_description_arabic','bulk_char_description_arabic',250);
    });



    $(document).on('click', '.markaction #reject', function (e) {
       // alert('4')
        e.preventDefault();
        // $("#progressbar").show();
        bulkActionReject(this,'',true);

    });

    $(document).on('click', '.markaction #reject1', function (e) {
        e.preventDefault();
        bulkActionPendingReject(this,'',true);
    });
    var errorMessageStr = 'Nothing Selected';
    var rejmsg = "Are you sure you want to reject ?";
    function bulkActionReject(element,count,confirm_reject){
        url = $(element).attr('data-url');
        var selectedCheckboxItems = $('#datatable-example'+count+' tbody :checkbox:checked').map(function () {
            return this.value;
        }).get();

        if (selectedCheckboxItems.length > 0) {
            selectedItem = selectedCheckboxItems.join(',');
            selectedItems = selectedItem.replace('on,', '');
            delMsg = '';

            if (confirm_reject) {
                delMsg = $(element).attr('data-msg');

                if (delMsg) {
                    rejectBulkModal(delMsg);
                } else {
                    rejectBulkModal(rejmsg);
                }
            }else{
                $("#progressbar").show();
                $('#rejectids').attr('value', selectedItems);
                $('#bulkaction').attr('action', url);
                $('#bulkaction').submit();


            }

        }
        else {
            showErrorMessage(errorMessageStr);
        }

    }
    function bulkActionPendingReject(element,count,confirm_reject){
        url = $(element).attr('data-url');
        var selectedCheckboxItems2 = $('#datatable-example2'+count+' tbody :checkbox:checked').map(function () {
            return this.value;
        }).get();

        if (selectedCheckboxItems2.length > 0) {
            selectedItem = selectedCheckboxItems2.join(',');
            selectedItems = selectedItem.replace('on,', '');
            delMsg = '';

            if (confirm_reject) {
                delMsg = $(element).attr('data-msg');

                if (delMsg) {
                    rejectBulkModal(delMsg);
                } else {
                    rejectBulkModal(rejmsg);
                }
            }else{

                $("#progressbar").show();
                $('#rejectids').attr('value', selectedItems);
                $('#bulkaction').attr('action', url);
                $('#bulkaction').submit();
            }

        }
        else {
            showErrorMessage(errorMessageStr);
        }

    }
    function rejectBulkModal(message, action) {
      //  $('#rejectBulkMessage .message-content').text(message);

        $.ajax({
            dataType: 'JSON',
            url: base_path+"/admin/event_requests/getreasonbytype",
            type: 'POST',
            data: {
                // _token: $('input[name=_token]').val(),
            },
            beforeSend: function () {
                // overlay_ajax();
            },
            success: function (data) {
                // alert(data.reasonlist);
                $('#rejectids').attr('value', selectedItems);
                $('#bulkRejectionForm').attr('action', url);
                $('#char_description_english,#char_description_arabic').text(250);
                $('#bulkRejectionForm')[0].reset();
                $('#bulk_reason_id').html(data.reasonlist);
                $('#rejectBulkMessage').modal('show');


                $('#bulk_description_english').val('');
                $('#bulk_description_arabic').val('');
                $("#progressbar").hide();

            },
            error: function () {
                // $('#modal-default').modal('show');
            },
            complete: function () {
                // $('.box-body').unblock();
            }
        });


    }

//    $(document).on('click', '.submitRejectBulkModal', function() {
//        $("#progressbar").show();
//        $('#ids').attr('value', selectedItems);
//        $('#bulkaction').attr('action', url);
//        $('#bulkaction').submit();
//    });

    function showErrorMessage(msg) {
        var style = 'flat';

        Messenger.options = {extraClasses: 'messenger-fixed messenger-on-top', theme: style};
        Messenger().post({
            message: msg,
            type: 'error',
            showCloseButton: true
        });
    }



    $("#bulk_reason_id").change(function(){
        reason_id=$("#bulk_reason_id").val();
        if(reason_id!=''){
            $.ajax({
                dataType: 'JSON',
                url: base_path+"/admin/event_requests/getdescriptionbyreason",
                type: 'POST',
                data: {
                    'reasonid':reason_id
                    // _token: $('input[name=_token]').val(),
                },
                beforeSend: function () {
                    // overlay_ajax();
                },
                success: function (data) {

                    $('#bulk_char_description_arabic').text(250 - data.description_arabic.length);
                    $('#bulk_char_description_english').text(250 - data.description_english.length);
                   // $('#bulkRejectionForm')[0].reset();
                    $('#bulk_description_english').val(data.description_english);
                    $('#bulk_description_arabic').val(data.description_arabic);

                    // $('#bulkRejectionForm')[0].reset();
                },
                error: function () {
                    // $('#modal-default').modal('show');
                },
                complete: function () {
                    // $('.box-body').unblock();
                }
            });
        }else{
            $('#bulkRejectionForm')[0].reset();
        }
    });

    $("#reason_id").change(function(){
        reason_id=$("#reason_id").val();
        if(reason_id!=''){
            $.ajax({
                dataType: 'JSON',
                url: base_path+"/admin/event_requests/getdescriptionbyreason",
                type: 'POST',
                data: {
                   'reasonid':reason_id
                    // _token: $('input[name=_token]').val(),
                },
                beforeSend: function () {
                    // overlay_ajax();
                },
                success: function (data) {
                    $('#char_description_arabic').text(250 - data.description_arabic.length);
                    $('#char_description_english').text(250 - data.description_english.length);
                  //  $('#addRejectionForm')[0].reset();
                    $('#description_english').text(data.description_english);
                    $('#description_arabic').text(data.description_arabic);
                },
                error: function () {
                    // $('#modal-default').modal('show');
                },
                complete: function () {
                    // $('.box-body').unblock();
                }
            });
        }else{
            $('#addRejectionForm')[0].reset();
        }
    });
</script>





@endsection