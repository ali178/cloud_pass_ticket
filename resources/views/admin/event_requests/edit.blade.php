@extends('layouts.app')
@section('content')
<style type="text/css">
    .nav-navbar-center {
        margin-top: 0px;
    }
</style>
<div class="page-title" style="letter-spacing:unset;">
    <div class="row">
   <div class="col-md-2" style="text-align: left">

           <ul class="nav quick-section ">
               <li class="quicklinks actions">
                   <a id="gear-icon" class="context_menu_btn" href="#" data-toggle="dropdown" aria-expanded="false">
                       <i class="fa fa-gear"></i>
                   </a>
                   <ul aria-labelledby="user-options" role="menu" class="dropdown-menu left custom-listing-menu" style="left: 40px !important;">
                       @if(in_array($event_request->event_request_status_id,array(1)))
                           <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.pending_status",$event_request->id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}<button class="change_status">Send Bank Info إرسال معلومات التحويل</button>{!! Form::close() !!}</li>
                           <li class="divider"></li>

                       @endif
                       @if(in_array($event_request->event_request_status_id,array(2)))
                               <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.pending_status", $event_request->id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to Re-Send Bank Info?"]) !!}<button class="change_status">Re-Send Bank Info إرسال معلومات التحويل</button>{!! Form::close() !!}</li>
                               <li class="divider"></li>
                       @endif
                       @if(Auth::user()->can("eventrequests-issueticket"))
                           @if(in_array($event_request->event_request_status_id,array(1,2)))
                               <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.issue_status", $event_request->id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}<button class="change_status">Issue Tickets إصدار تذاكر</button>{!! Form::close() !!}</li>
                                   <li class="divider"></li>

                           @endif

                       @endif
                       @if(in_array($event_request->event_request_status_id,array(3)))

                               <li><a class="" href="{{ URL::route('admin.issued_tickets.viewissuedticket',["id"=>$issued_tickets->id, "bsource"=>"request"]) }}">View Issued Ticket Details نظرة على التذكرة</a></li></li>
                               <li class="divider"></li>

                       @endif
                       @if(Auth::user()->can("eventrequests-reject"))
                           @if(in_array($event_request->event_request_status_id,array(1,2)))
                               <li>{!! Form::open(["method" => "POST","route" => ["admin.event_requests.reject_status", $event_request->id],"class" => "rejectaction", "data-url"=>URL::to("admin/event_requests/$event_request->id/reject_status"),  "data-eventrequestid"=>$event_request->id, "data-msg"=>"Are you sure you want to change status?"]) !!}<button class="change_status">Reject رفض</button>{!! Form::close() !!}</li>
                                   <li class="divider"></li>

                           @endif
                       @endif
                       @if(Auth::user()->can("eventrequests-destroy"))
                           <li>{!! Form::open(["method" => "DELETE","route" => ["admin.event_requests.destroy", $event_request->id],"class" => "inline deleteaction"]) !!}<button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                       @endif
                   </ul>
               </li>
           </ul>
   </div>
    <div class="col-md-2">

    </div>
   <div class="col-md-4">
    <center>
      <div class="row" style="padding-bottom: 5px;">
        @if($event->status == 1 && $event->selling_off == 0)
            <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @else
            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @endif
      </div>
        <span style="font-size: 16px;font-weight: bold;">{{$event->title_arabic}}</span>
        <br/>
        <span style="font-size: 16px;font-weight: bold">{{$event->title_english}}</span>
        <br/>
    </center>
    </div>
        <div class="col-md-4" style="text-align: right">
            <div class="col-md-9" style="text-align: right;font-weight: 700;letter-spacing: 0"> </div> <div class="col-md-3">
            
                <span style="font-size: 16px">
                    <strong style="float: right">{{$event_request->event_request_status->title_arabic}}<br/>{{$event_request->event_request_status->title_english}}
                    </strong>
                </span>
            
        </div>
        </div>
    </div>
</div>


<div class="page-title">
    <center>
        <br/>
        <span style="font-size: 20px"><strong>{{$event_request->complete_request_no}}</strong></span>
        <br/>
        <span style="letter-spacing: 0;">{{date('d/m/Y h:i A',strtotime($event_request->created_at))}}</span>
        <br>
    </center>
</div>




<div class="row">
    <div class="col-md-12">
        <div class="grid simple" style="margin-bottom: 0 !important;">
            <div class="grid-body no-border"> <br>
                {{--@if($errors->any())--}}
                    {{--<div class="row">--}}
                        {{--<div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">--}}
                            {{--<button data-dismiss="alert" class="close"></button>--}}
                            {{--@foreach($errors->all() as $error)--}}
                                {{--<p>{{ $error }}</p>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endif--}}

                <div class="panel-group" id="accordion">
                    <a href="{{ URL::to('admin/event_requests/'.$event->id) }}" class="btn btn-default" >Back السابق</a>
                    <br/>
                    <br/>
                    <div class="panel panel-default" style="padding-top: 5px">
                        <div class="panel-heading" >
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Contact Information<span class="heading_arabic">معلومات التواصل</span></a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-4 text-right">
                                                <label class="form-label">Full Name <br> الاسم الكامل <span style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8">
                                                {!! Form::text('title_english', $event_request->customer->full_name , ['class' => 'form-control', 'id'=>'title_english','readonly'=>true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-4 text-right">
                                                <label class="form-label">Email <br> البريد الالكتروني <span style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8">
                                                {!! Form::text('title_arabic', $event_request->customer->email  , ['class' => 'form-control', 'id'=>'title_arabic','readonly'=>true]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-4 text-right">
                                                <label class="form-label">Mobile No <br> الهاتف المتحرك <span style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8">
                                                {!! Form::text('title_arabic', $event_request->customer->mobile , ['class' => 'form-control', 'id'=>'title_arabic','readonly'=>true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            {{--<div class="grid-title no-border">--}}
                {{--<h4> <span class="semi-bold"></span></h4>--}}
            {{--</div>--}}
            <div class="grid-body no-border"> <br>
                @if($errors->any())
                    <div class="row">
                        <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                            <button data-dismiss="alert" class="close"></button>
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
                @endif

                <div class="panel-group" id="accordion2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2">Request Information<span class="heading_arabic">معلومات الطلب</span></a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class='control' style="font-size: 18px">
                                            <div class="nav-navbar-center">
                                                <ul class="nav nav-pills">
                                                    <li style="margin-right: 60px;">
                                                        <div class="row"><span>Total Tickets حضرها</span></div>
                                                        <div class="row" style="padding-top: 6px;">
                                                            <span style="font-size: 26px; color: black" >
                                                                {{$event_request->total_qty}}
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <?php  
                                                        // $vat_value=5/100 * $event_request->total_price  
                                                        use Ticketing\Models\Issued_ticket;
                                                        $vat = $event_request->vat ? $event_request->vat : 0;
                                                        $vat_value=$vat/100 * $event_request->total_price;  

                                                        // $event_request->total_price =  $event_request->total_price - $vat_value;
                                                        // $vat_value=ceil($vat/100 * $event_request->total_price);
                                                        // $event_request->total_price =  $event_request->total_price + $vat_value;
                                                    ?>
                                                    <li >
                                                        <div class="row">Total Price مجموع</div>
                                                        <div class="row" style="padding-top: 6px;">
                                                            <!-- <span style="font-size: 26px; color: black" ><strong>SR</strong> {{ number_format($event_request->total_price + $vat_value,2) }}</span> -->
                                                            <span style="font-size: 26px; color: black" ><strong>SR</strong> {{ number_format($event_request->total_price,2) }}</span>
                                                        </div>
                                                    </li>
                                                    <li style="margin-left: 60px">
                                                        <div class="row">Total VAT تم البيع</div>
                                                        <div class="row" style="padding-top: 6px;">
                                                            <span style="font-size: 26px; color: black" >{{ number_format($vat_value,2) }}</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>

                @if(count($tiers)>0)
                    @foreach (array_chunk($tiers,3) as $chunked_tiers)
                            @foreach ($chunked_tiers as $tier)
                                <?php
                                    $tl=$event_request->getTierColorByTierId($tier['tier_id']);

                                    $total_price = $tier['total_qty'] * $tier['price'];
                                    $vat = $tier['vat'] ? $tier['vat'] : 0;
                                    $vat_value=$vat/100 * $total_price;

                                    $adminTicketCount = Issued_ticket::getTotalAdminIssuedSumNew(3,$event->id,$tier['tier_id']);


                                ?>

                                    @if($tl)

                                    <div class="grid simple">
                                    {{--<div class="grid-title" style="background-color:#{{$tl->header_color}};color:white;">--}}
                                        <div class="panel-heading" style="background-color:#{{$tl->header_color}};color:white;">
                                            <h4 class="panel-title" style="color: white">
                                                <strong><span style="color: white">{{$tl->title_english }}<span class="heading_arabic">{{$tl->title_arabic }}</span></span></strong> <div style="float: right"> <span style="color: white">Available<span class="heading_arabic">لعنوان أرابيك</span></span>  <span style="color: white"><span class="heading_arabic" STYLE="float: right"><strong>@if($tl->total_quantity_type) {{$tl->total_quantity - ($tl->totaAvailable() + $adminTicketCount) }} @else Open @endif</strong></span></span></div>
                                            </h4>
                                        </div>


                                        {{--<div><h4><span class="semi-bold" style="color: white">{{$tl->title_english }}<span class="heading_arabic">{{$tl->title_arabic }}</span></span></h4></div>--}}
                                     {{--</div>--}}
                                    <div class="grid-body sub-section">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-4 text-right">
                                                        <label class="form-label">Qty <br> العدد </label>
                                                    </div>
                                                    <div class="input-with-icon  right col-md-6 col-sm-12">
                                                        {!! Form::text('title_english', $tier['total_qty'] , ['class' => 'form-control', 'id'=>'title_english', 'readonly'=>true, "style"=>"text-align:center"]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-4 text-right">
                                                        <label class="form-label">Price Per Ticket <br> سعر التذكرة </label>
                                                    </div>
                                                    <div class="input-with-icon  right col-md-6 col-sm-12">
                                                        {!! Form::text('title_arabic', $tier['price'] , ['class' => 'form-control', 'id'=>'title_arabic','readonly'=>true, "style"=>"text-align:center"]) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-4 text-right">
                                                        <label class="form-label">Total Amount <br> الاجمالي </label>
                                                    </div>
                                                    <div class="input-with-icon  right col-md-6 col-sm-12">
                                                        {!! Form::text('title_arabic', number_format($total_price - $vat_value,2) , ['class' => 'form-control', 'id'=>'title_arabic','readonly'=>true, "style"=>"text-align:center"]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    @endif
                            @endforeach
                        @endforeach
                    @endif


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="rejectionOnReasonModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Request Rejection<br/>طلب الرفض</h4>
            </div>
            {!! Form::open(array('route' =>["admin.event_requests.reject_status", 5], 'id'=>'addRejectionForm', 'role'=>'form','onsubmit'=>"$('#progressbar').show()")) !!}

            <input type="hidden" name="event_request_id" id="event_request_id" value="">

            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">Reason<br><span class="arabic_font">السبب</span><span style="color:red">*</span></label>
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">
                        {!! Form::select('reason_id',[''=>'--- SELECT ---'],null, ['class' => 'form-control','id'=>'reason_id', 'placeholder'=>'--- SELECT ---', 'required'=>'required']) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description Arabic &emsp; <span class="arabic_font">التفاصيل بالعربي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_arabic', null, ['class' => 'form-control','id'=>'description_arabic','rows'=>3,'style'=>'direction: rtl']) !!}
                            <span class="help-block"><span id="char_description_arabic">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description English &emsp; <span class="arabic_font">التفاصيل بالانجليزي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'description_english','rows'=>3]) !!}
                            <span class="help-block"><span id="char_description_english">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel إالغاء</button>
                    {!! Form::submit('Send إرسال', ['class' => 'btn btn-green tier-form-submit','id' => 'add_tier_btn']) !!}

                </center>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>

@endsection

@section('page_script')
    @parent

    <script type="text/javascript" language="javascript" class="init">

        $(document).on('submit', '.rejectaction', function() {
            // var ret = confirm("Are you sure you want to delete?");
            // if (ret) {
            // $("#progressbar").show();
            // return true;
            // }
            // return false;
            $("#progressbar").show();
            return false;
        });
        // var form = '';
        $(document).on('click', '.rejectaction', function() {
            issuedticketid = $(this).attr('data-eventrequestid');
            urlrequest = $(this).attr('data-url');
            console.log(issuedticketid);
            console.log(urlrequest);
            $.ajax({
                dataType: 'JSON',
                url: base_path+"/admin/event_requests/getreasonbytype",
                type: 'POST',
                data: {
                    // _token: $('input[name=_token]').val(),
                    issuedticketid:issuedticketid,
                },
                beforeSend: function () {
                    // overlay_ajax();
                },
                success: function (data) {
                    // alert(data.reasonlist);
                    $('#addRejectionForm').attr('action',urlrequest);//'http://localhost/ticketing/public/admin/issued_tickets/'+issuedticketid+'/reject_status'
                    $('#issued_ticket_id').val('');
                    $('#char_description_english,#char_description_arabic').text(250);
                    // $('#addRejectionForm')[0].reset();
                    $('#rejectionOnReasonModal').modal('show');
                    $('#reason_id').html(data.reasonlist);
                    $('#issued_ticket_id').val(issuedticketid);
                    $("#progressbar").hide();


                },
                error: function () {
                    // $('#modal-default').modal('show');
                },
                complete: function () {
                    // $('.box-body').unblock();
                }
            });

            // return false;
        });
        $(document).ready(function(){
            $("#description_english").keyup(function (event) {
                updateFeatureCharLimitNum('description_english','char_description_english',250);
            });

            $("#description_arabic").keyup(function (event) {
                updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
            });

            updateFeatureCharLimitNum('description_english','char_description_english',250);
            updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
        });

        $("#reason_id").change(function(){
            reason_id=$("#reason_id").val();
            if(reason_id!=''){
                $.ajax({
                    dataType: 'JSON',
                    url: base_path+"/admin/event_requests/getdescriptionbyreason",
                    type: 'POST',
                    data: {
                        'reasonid':reason_id
                        // _token: $('input[name=_token]').val(),
                    },
                    beforeSend: function () {
                        // overlay_ajax();
                    },
                    success: function (data) {
                        $('#char_description_arabic').text(250 - data.description_arabic.length);
                        $('#char_description_english').text(250 - data.description_english.length);
                        //$('#addRejectionForm')[0].reset();
                        $('#description_english').val(data.description_english);
                        $('#description_arabic').val(data.description_arabic);
                    },
                    error: function () {
                        // $('#modal-default').modal('show');
                    },
                    complete: function () {
                        // $('.box-body').unblock();
                    }
                });
            }else{
                $('#addRejectionForm')[0].reset();
            }
        });
    </script>
@endsection