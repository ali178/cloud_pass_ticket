@extends('layouts.app')
@section('content')

<div class="page-title">
    <h3>Event - <span class="semi-bold">{{$request_type}}</span></h3>
</div>
<style type="text/css">
  table#datatable-example2,table#datatable-example{
    width: 100% !important;
  }
  .pick-a-color-markup .input-group-btn .color-dropdown{
    padding: 7.5px 5px !important;
  }
  .error{
    display: -webkit-inline-box;
  }
  #header_color_tier-error{
    display: none !important;
  }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>{{$request_type}} <span class="semi-bold">Event</span></h4>
            </div>
            <div class="grid-body no-border"> <br>
                @if($errors->any())
                <div class="row">
                    <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                        <button data-dismiss="alert" class="close"></button>
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif
                {!! Form::model($event,['route' => ['admin.events.addeditevent','id'=>$event->id],'method'=> $event ? 'PUT' : 'POST', 'id'=>'form_events', "enctype"=>"multipart/form-data"]) !!}
                    
                    <div class="form-actions" style="background-color: transparent; margin-bottom: 10px !important; margin-top: -45px !important;">  
                      <a href="{{ URL::to('admin/events') }}" class="btn btn-default">Cancel</a>
                      <!-- <a href="javascript:void(0)" class="btn btn-primary form-submit" style="float: right;">{{ $request_type == 'Add' ? 'Save' : 'Update' }}</a> -->
                      {!! Form::submit($request_type == 'Add' ? 'Save' : 'Update', ['class' => 'btn btn-primary btn-cons form-submit','style' => 'float:right']) !!}
                    </div>

                    <div id="images_div">
                      <div class="row">
                        <div class="col-md-5">
                          <center>
                          <?php $return = $event ? $event->logo_image : false ?>
                            <div class="form-group row " style="margin-bottom: 0px;">
                              <label><span class="semi-bold">Logo Image<span class="heading_arabic">العنوان إنجليش</span></span></label>
                              <div class="col-md-12 col-xs-12 controls">
                                  <div class="fileupload <?php  echo ($return != '') ? 'fileupload-exists' : 'fileupload-new'; ?>" data-provides="fileupload">

                                    <?php if ($return != '') { ?>
                                        <div class="fileupload-new thumbnail image_thumbnail_size">
                                            <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail image_parent_div" id="image1">
                                            <a data-gallery="" class="gallery-item" href="{{ asset($event->logo_image)}}"><img src="{{ asset($event->logo_image)}}" class="existed_image" /></a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="fileupload-new thumbnail image_thumbnail_size">
                                            <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail image_parent_div" id="image1">
                                        </div>
                                    <?php } ?>

                                    <div>
                                      <span class="btn btn-file">
                                          <span class="fileupload-new">Select image</span>
                                          <span class="fileupload-exists">Change</span>
                                          {!! Form::file('logo_image', ['class' => 'selectimage','id'=>'logo_image']) !!}
                                      </span>
                                      <a class="btn hide" style="" id="startCrop">Crop</a>
                                      <a href="javascript:void(0)" class="btn remove_picture fileupload-exists" data-dismiss="fileupload" >Remove</a>
                                      <p id="image1-dim" class="label label-success" style="display:none;">
                                      </p>
                                      <p id="image1-size" class="label label-success" style="display:none;">
                                      </p>
                                    </div>
                                  </div>
                                  
                                  <code>Allowed Image size: </code> 1MB <br>
                                  <code>Allowed Dimension</code>1:1<br />

                                  <br>
                              </div>
                              <div class="img-container" class="input">
                                <input type="hidden" class="x" name="logo_x" id="x" />
                                <input type="hidden" class="y" name="logo_y" id="y"/>
                                <input type="hidden" class="w" name="logo_w" id="w"/>
                                <input type="hidden" class="h" name="logo_h" id="h"/>
                              </div>
                            </div>
                            </center>
                        </div>
                        <div class="col-md-5">

                          <div class="grid simple">
                            <div class="grid-title" style="padding: 0;"></div>
                            <div class="grid-body sub-section" style="padding: 0;">

                              <div class="row">
                                <div class="col-md-6" style="padding-top: 10px;">

                                  <center>
                                    <?php $return = $event ? $event->cover_image : false ?>
                                    <div class="form-group row " style="margin-bottom: 0px;">
                                      <!-- <label class="col-md-3 col-xs-12 form-label text-right">Cover Image</label> -->
                                      <label><span class="semi-bold">Covr Image<span class="heading_arabic">العنوان إنجليش</span></span></label>
                                      <div class="col-md-12 col-xs-12 controls">
                                          <div class="fileupload <?php  echo ($return != '') ? 'fileupload-exists' : 'fileupload-new'; ?>" data-provides="fileupload">

                                            <?php if ($return != '') { ?>
                                                <div class="fileupload-new thumbnail image_thumbnail_size">
                                                    <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail image_parent_div" id="image2">
                                                    <a data-gallery="" class="gallery-item" href="{{ asset($event->cover_image)}}"><img src="{{ asset($event->cover_image)}}" class="existed_image" /></a>
                                                </div>
                                            <?php } else { ?>
                                                <div class="fileupload-new thumbnail image_thumbnail_size">
                                                    <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail image_parent_div" id="image2">
                                                </div>
                                            <?php } ?>

                                            <div>
                                              <span class="btn btn-file">
                                                  <span class="fileupload-new">Select image</span>
                                                  <span class="fileupload-exists">Change</span>
                                                  {!! Form::file('cover_image', ['class' => 'selectimage','id'=>'cover_image']) !!}
                                              </span>
                                              <a class="btn hide" style="" id="startCrop2">Crop</a>
                                              <a href="javascript:void(0)" class="btn remove_picture fileupload-exists" data-dismiss="fileupload" >Remove</a>
                                              <p id="image2-dim" class="label label-success" style="display:none;">
                                              </p>
                                              <p id="image2-size" class="label label-success" style="display:none;">
                                              </p>
                                            </div>
                                          </div>
                                          
                                          <code>Allowed Image size: </code> 1MB <br>
                                          <code>Allowed Dimension</code>1:1<br />
                                          
                                          <br>
                                      </div>
                                      <div class="img-container" class="input">
                                          <input type="hidden" class="x" name="logo_x2" id="x2" />
                                          <input type="hidden" class="y" name="logo_y2" id="y2"/>
                                          <input type="hidden" class="w" name="logo_w2" id="w2"/>
                                          <input type="hidden" class="h" name="logo_h2" id="h2"/>
                                      </div>
                                    </div>
                                  </center>
                                  
                                </div>
                                <div class="col-md-6">
                                  <div style="padding: 10px;">
                                    <center>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <p style="direction: rtl;">تمهيد بالرّغم دار تم. دول الآلاف وبولندا والكوري عن, لكل تُصب أمدها الحكومة و, دون بقسوة والتي</p>
                                    
                                      <img src="{{asset('assets/images/sample_cover.png')}}" style="width: 192px;height: auto;">
                                    </center>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                    
                    <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Basic Information<span class="heading_arabic">العنوان إنجليش</span></a>
                          </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-md-8">

                                <div class="form-group row">
                                    <div class="col-md-3 col-sm-12 text-right">
                                      <label class="form-label">Title English <br> العنوان إنجليش <span style="color:red">*</span></label>
                                    </div>
                                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                      {!! Form::text('title_english', null , ['class' => 'form-control', 'id'=>'title_english','required'=>true]) !!}
                                      <span class="help-block"><span id="char_title_english">60</span> <?php echo 'characters left'; ?></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3 col-sm-12 text-right">
                                      <label class="form-label">Title Arabic <br> العنوان أرابيك <span style="color:red">*</span></label>
                                    </div>
                                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                      {!! Form::text('title_arabic', null , ['class' => 'form-control', 'id'=>'title_arabic','required'=>true]) !!}
                                      <span class="help-block"><span id="char_title_arabic">60</span> <?php echo 'characters left'; ?></span>
                                    </div>
                                </div>

                              </div>
                              <div class="col-md-4">
                                
                                <div class="form-group row">
                                    <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Tag <br> العنوان أرابيك</label>
                                    </div>
                                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::select('tag',[''=>'']+ $tags, $event ? $event->tag : null, ['class' => 'form-control','id'=>'tag_dropdown']) !!}
                                    </div>
                                </div>

                              </div>
                            </div>

                            <div class="grid simple">
                              <div class="grid-title">
                                <div><h4><span class="semi-bold">Venue<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                              </div>
                              <div class="grid-body sub-section"> 
                                <div class="row">
                                  <div class="col-md-6 col-sm-12">

                                    <div class="form-group row">
                                        <div class="col-md-3 col-sm-12 text-right">
                                            <label class="form-label">Venue English <br> العنوان أرابيك <span style="color:red">*</span></label>
                                        </div>
                                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                             {!! Form::text('venue_english', null , ['class' => 'form-control','required'=>true]) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3 col-sm-12 text-right">
                                            <label class="form-label">Venue Arabic <br> العنوان أرابيك <span style="color:red">*</span></label>
                                        </div>
                                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                             {!! Form::text('venue_arabic', null , ['class' => 'form-control','required'=>true]) !!}
                                        </div>
                                    </div>
                                    
                                  </div>
                                   <div class="col-md-6 col-sm-12">
                                    
                                    <div class="form-group row">
                                        <div class="col-md-3 col-sm-12 text-right">
                                          <label class="form-label">Map <br> العنوان أرابيك</label>
                                        </div>
                                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                          <!-- {!! Form::text('map_address', null , ['class' => 'form-control','id'=>'map_address', 'disabled'=>true]) !!} -->
                                          <input type="text" class="form-control" name="map_address" id="map_address" value="{{$event && $event->map_address != '' ? $event->map_address : ''}}" readonly="readonly">
                                          <br>
                                          <a href="javascript:void(0)" class="btn btn-success" id="select_from_map" data-toggle="modal" data-target="#googleMapModal">From Map</a>
                                          <a href="javascript:void(0)" class="btn btn-danger clear_marker" id="map_clear">Clear</a>
                                        </div>

                                        <input type="hidden" name="lat" id="lat_input" value="{{$event && $event->lat != 0 ? $event->lat : ''}}">
                                        <input type="hidden" name="long" id="lng_input" value="{{$event && $event->long != 0 ? $event->long : ''}}">
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- DATE TIME -->
                            <div class="row">
                              <div class="col-md-6 col-sm-12">
                                <div class="grid simple">
                                  <div class="grid-title">
                                    <div><h4><span class="semi-bold">Date time English<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                                  </div>
                                  <div class="grid-body sub-section"> 
                                   
                                    <div class="row">
                                      <div class="col-md-6 col-sm-12">
                                        <!-- sub section -->
                                        <div class="grid simple">
                                          <div class="grid-title"></div>
                                          <div class="grid-body sub-section"> 
                                            <div class="row">
                                              <!-- dates handling -->
                                              <div class="col-md-12 col-sm-12">

                                                <div class="form-group row">
                                                  <label for="date_from_english"><span class="semi-bold">From Date<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('from_date_english', null , ['class' => 'form-control date_picker', 'id'=>'date_from_english','required'=>true]) !!}
                                                </div>
                                                <div class="form-group row">
                                                  <label for="date_to_english"><span class="semi-bold">To Date<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('to_date_english', null , ['class' => 'form-control date_picker', 'id'=>'date_to_english','required'=>true]) !!}
                                                </div>
                                                
                                              </div>
                                              
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <!-- sub section ends here -->
                                      </div>
                                      <div class="col-md-6 col-sm-12">
                                        <!-- sub section -->
                                        <div class="grid simple">
                                          <div class="grid-title"></div>
                                          <div class="grid-body sub-section"> 
                                            <div class="row">
                                              <!-- dates handling -->
                                              <div class="col-md-12 col-sm-12">

                                                <div class="form-group row">
                                                  <label for="time_from_english"><span class="semi-bold">From Time<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('from_time_english', null , ['class' => 'form-control time_picker', 'id'=>'time_from_english','required'=>true]) !!}
                                                </div>
                                                <div class="form-group row">
                                                  <label for="time_to_english"><span class="semi-bold">To Time<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('to_time_english', null , ['class' => 'form-control time_picker', 'id'=>'time_to_english','required'=>true]) !!}
                                                </div>
                                                
                                              </div>
                                              
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <!-- sub section ends here -->
                                      </div>
                                    </div>

                                    <!-- another event date and time -->
                                    <div class="grid simple">
                                      <div class="grid-title"></div>
                                      <div class="grid-body sub-section"> 
                                        <div class="row">
                                          <!-- dates handling -->
                                          <div class="col-md-12 col-sm-12">

                                            <input type="checkbox" name="is_another_dt" id="is_another_dt" {{$event && $event->is_another_dt ? 'checked':''}}>
                                            <span class="semi-bold">Select another event date & time<span class="heading_arabic"> العنوان إنجليش العنوان إنجليش</span></span>
                                            <div id="another_dt_div" class="{{$event && $event->is_another_dt ? '':'hidden'}}">
                                              <br><br>
                                              <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                  <label for="another_date"><span class="semi-bold">Date<span class="heading_arabic">العنوان إنجليش</span></span></label>
                                                  {!! Form::text('another_date', null , ['class' => 'form-control date_picker', 'id'=>'another_date']) !!}
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                  <label for="another_time"><span class="semi-bold">Time<span class="heading_arabic">العنوان إنجليش</span></span></label>
                                                  {!! Form::text('another_time', null , ['class' => 'form-control time_picker', 'id'=>'another_time']) !!}
                                                </div>
                                              </div>
                                            </div>
                                           
                                          </div>
                                          
                                        </div>
                                        
                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>
                              
                              <div class="col-md-6 col-sm-12">
                                <div class="grid simple">
                                  <div class="grid-title">
                                    <div><h4><span class="semi-bold">Date time Arabic<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                                  </div>
                                  <div class="grid-body sub-section"> 
                                   
                                    <div class="row">
                                      <div class="col-md-6 col-sm-12">
                                        <!-- sub section -->
                                        <div class="grid simple">
                                          <div class="grid-title"></div>
                                          <div class="grid-body sub-section"> 
                                            <div class="row">
                                              <!-- dates handling -->
                                              <div class="col-md-12 col-sm-12">

                                                <div class="form-group row">
                                                  <label for="date_from_arabic"><span class="semi-bold">From Date<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('from_date_arabic', null , ['class' => 'form-control', 'id'=>'date_from_arabic','required'=>true]) !!}
                                                </div>
                                                <div class="form-group row">
                                                  <label for="date_to_arabic"><span class="semi-bold">To Date<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('to_date_arabic', null , ['class' => 'form-control', 'id'=>'date_to_arabic','required'=>true]) !!}
                                                </div>
                                                
                                              </div>
                                              
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <!-- sub section ends here -->
                                      </div>
                                      <div class="col-md-6 col-sm-12">
                                        <!-- sub section -->
                                        <div class="grid simple">
                                          <div class="grid-title"></div>
                                          <div class="grid-body sub-section"> 
                                            <div class="row">
                                              <!-- dates handling -->
                                              <div class="col-md-12 col-sm-12">

                                                <div class="form-group row">
                                                  <label for="time_from_arabic"><span class="semi-bold">From Time<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('from_time_arabic', null , ['class' => 'form-control', 'id'=>'time_from_arabic','required'=>true]) !!}
                                                </div>
                                                <div class="form-group row">
                                                  <label for="time_to_arabic"><span class="semi-bold">To Time<span class="heading_arabic">العنوان إنجليش</span> <span style="color:red">*</span></span></label>
                                                  {!! Form::text('to_time_arabic', null , ['class' => 'form-control', 'id'=>'time_to_arabic','required'=>true]) !!}
                                                </div>
                                                
                                              </div>
                                              
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <!-- sub section ends here -->
                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="grid simple">
                              <div class="grid-title">
                                <div><h4><span class="semi-bold">Description<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                              </div>
                              <div class="grid-body sub-section"> 
                                <div class="row">
                                  <div class="col-md-6 col-sm-12">
                                    <label for="description_english"><span class="semi-bold">English<span class="heading_arabic">الإنجليزية</span> <span style="color:red">*</span></span></label>
                                    {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'description_english','rows'=>5,'required'=>true]) !!}
                                    <span class="help-block"><span id="char_description_english">250</span> <?php echo 'characters left'; ?></span>
                                  </div>
                                   <div class="col-md-6 col-sm-12">
                                    <label for="description_arabic"><span class="semi-bold">Arabic<span class="heading_arabic">العربية</span> <span style="color:red">*</span></span></label>
                                    {!! Form::textarea('description_arabic', null, ['class' => 'form-control','id'=>'description_arabic','rows'=>5,'required'=>true]) !!}
                                    <span class="help-block"><span id="char_description_arabic">250</span> <?php echo 'characters left'; ?></span>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="grid simple">
                              <div class="grid-title">
                                <div><h4><span class="semi-bold">Terms & Condtions<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                              </div>
                              <div class="grid-body sub-section"> 
                                <div class="row">
                                  <div class="col-md-6 col-sm-12">
                                    <label for="tc_arabic_english"><span class="semi-bold">English<span class="heading_arabic">العربية</span></span></label>
                                    {!! Form::textarea('tc_english', null, ['class' => 'form-control','id'=>'tc_arabic_english','rows'=>5]) !!}
                                    <span class="help-block"><span id="char_tc_arabic_english">250</span> <?php echo 'characters left'; ?></span>
                                  </div>
                                   <div class="col-md-6 col-sm-12">
                                    <label for="tc_arabic_arabic"><span class="semi-bold">Arabic<span class="heading_arabic">العربية</span></span></label>
                                    {!! Form::textarea('tc_arabic', null, ['class' => 'form-control','id'=>'tc_arabic_arabic','rows'=>5]) !!}
                                    <span class="help-block"><span id="char_tc_arabic_arabic">250</span> <?php echo 'characters left'; ?></span>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                      
                      <br>
                      
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Contact Information<span class="heading_arabic">العنوان إنجليش</span></a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                          <div class="panel-body">

                            <div class="grid simple">
                              <div class="grid-title">
                                <div><h4><span class="semi-bold">Event Organiser<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                              </div>
                              <div class="grid-body sub-section"> 
                                <div class="row">
                                  <div class="col-md-6 col-sm-12">
                                    
                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Twitter <br>العنوان أرابيك</label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('twitter_link', null , ['class' => 'form-control']) !!}
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Instagram <br>العنوان أرابيك</label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('instagram_link', null , ['class' => 'form-control']) !!}
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">URL <br>العنوان أرابيك</label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('url_link', null , ['class' => 'form-control']) !!}
                                      </div>
                                    </div>

                                  </div>
                                  <div class="col-md-6 col-sm-12">
                                    
                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Snapchat <br>العنوان أرابيك</label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('snapchat_link', null , ['class' => 'form-control']) !!}
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Facebook <br>العنوان أرابيك</label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('facebook_link', null , ['class' => 'form-control']) !!}
                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>

                             <div class="grid simple">
                              <div class="grid-title">
                                <div><h4><span class="semi-bold">Event Organiser<span class="heading_arabic">العنوان أرابيك</span></span></h4></div>
                              </div>
                              <div class="grid-body sub-section"> 
                                <div class="row">
                                  <div class="col-md-6 col-sm-12">
                                    
                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Mobile <br>العنوان أرابيك <span style="color:red">*</span></label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('mobile', null , ['class' => 'form-control','required'=>true]) !!}
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">Email <br>العنوان أرابيك <span style="color:red">*</span></label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('email', null , ['class' => 'form-control','required'=>true]) !!}
                                      </div>
                                    </div>

                                  </div>
                                  <div class="col-md-6 col-sm-12">
                                    
                                    <div class="form-group row">
                                      <div class="col-md-3 col-sm-12 text-right">
                                        <label class="form-label">URL <br>العنوان أرابيك</label>
                                      </div>
                                      <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                                        {!! Form::text('url_link2', null , ['class' => 'form-control']) !!}
                                      </div>
                                    </div>

                                  </div>

                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                      
                      <br>
                      
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Tiers<span class="heading_arabic">العنوان إنجليش</span></a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                          <div class="panel-body">

                            <div class="form-group row">
                              <div class="col-md-10 col-md-offset-1">
                                <div class="controls">
                                  @if(Auth::user()->can('tiers-create'))
                                    <a href="javascript:void(0)" id="addEditTierModalBtn" class="btn btn-success">Add Tier</a>
                                  @endif
                                  <div class="pull-right markaction">
                                    @if(Auth::user()->can('tiers-destroy'))
                                      <a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/tiers/bulkdelete') }}">Delete</a>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-10 col-md-offset-1">
                                <table class="table sortable table-hover table-bordered" id="datatable-example2">
                                  <thead>
                                    <tr>
                                      <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                                      <th>Id</th>
                                      <th><center>English title<br>طبقات</center></th>
                                      <th><center>Arabic title<br>طبقات</center></th>
                                      <th><center>Price<br>طبقات</center></th>
                                      <th><center>Total Qty<br>طبقات</center></th>
                                      <th><center>Actions<br>طبقات</center></th>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                      
                      <br>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Images<span class="heading_arabic">العنوان إنجليش</span></a>
                          </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                          <div class="panel-body">

                            <div class="form-group row">
                              <div class="col-md-10 col-md-offset-1">
                                <div class="controls">
                                  @if(Auth::user()->can('events-create'))
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#additionaImageModal" class="btn btn-success">Add Image</a>
                                  @endif
                                  <div class="pull-right markaction">
                                    @if(Auth::user()->can('events-destroy'))
                                      <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/additionalimages/bulkimagedelete') }}">Delete</a>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-10 col-md-offset-1">
                                <table class="table sortable table-hover table-bordered" id="datatable-example">
                                  <thead>
                                    <tr>
                                      <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                                      <th>Id</th>
                                      <th>Image</th>
                                      <th>Caption</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                  
                    </div>

                    <input type="hidden" name="q" value="{{$request_type}}">
                    <div class="form-actions">  
                      <a href="{{ URL::to('admin/events') }}" class="btn btn-default">Cancel</a>
                      <!-- <a href="javascript:void(0)" class="btn btn-primary form-submit" style="float: right;">{{ $request_type == 'Add' ? 'Save' : 'Update' }}</a> -->
                      {!! Form::submit($request_type == 'Add' ? 'Save' : 'Update', ['class' => 'btn btn-primary btn-cons form-submit','style' => 'float:right']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- additional images modal -->
<div class="modal fade" id="additionaImageModal" role="dialog">
  <div class="modal-dialog modal-md">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Additional Image</h4>
      </div>
      <div class="modal-body">
        <form>
        <div class="row">
            
            <div class="form-group row">
              <div class="col-md-4 col-sm-12 text-right">
                <label class="form-label">Image <br>العنوان أرابيك</label>
              </div>
              <div class="input-with-icon  right col-md-6 col-sm-12">
                <div id="image_additional_div">                                    
                  <div class="fileupload <?php  echo ($return != '') ? 'fileupload-exists' : 'fileupload-new'; ?>" data-provides="fileupload">
           
                    <div class="fileupload-new thumbnail image_thumbnail_size">
                      <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail image_parent_div" id="image3">
                      <a data-gallery="" class="gallery-item" ><img src="http://via.placeholder.com/200x150?text=no+image" class="existed_image" /></a>
                    </div>

                    <div>
                      <span class="btn btn-file">
                        <span class="fileupload-new">Select image</span>
                        <span class="fileupload-exists">Change</span>
                          {!! Form::file('additional_image', ['class' => 'selectimage','id'=>'additional_image']) !!}
                      </span>
                      <a class="btn hide" style="" id="startCrop3">Crop</a>
                      <a href="javascript:void(0)" class="btn remove_picture fileupload-exists hide" data-dismiss="fileupload" >Remove</a>
                      <p id="image3-dim" class="label label-success" style="display:none;">
                      </p>
                      <p id="image3-size" class="label label-success" style="display:none;">
                      </p>
                    </div>
                  </div>

                  <div class="img-container" class="input">
                    <input type="hidden" class="x" name="logo_x3" id="x3" />
                    <input type="hidden" class="y" name="logo_y3" id="y3"/>
                    <input type="hidden" class="w" name="logo_w3" id="w3"/>
                    <input type="hidden" class="h" name="logo_h3" id="h3"/>
                  </div>
                </div>
            </div>
            </div>

            <div class="form-group row">
              <div class="col-md-4 col-sm-12 text-right">
                <label class="form-label">Caption <br>العنوان أرابيك</label>
              </div>
              <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                {!! Form::text('caption', null , ['class' => 'form-control','id'=>'caption']) !!}
              </div>
            </div>
        </div>
      </div>
        
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-default" data-dismiss="modal" id="add_picture_btn">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" id="close_picture_btn">Close</button>
        </center>
      </div>
    </form>
    </div>
    
  </div>
</div>
<!-- additional images modal end -->

<!-- maps modal -->
<div class="modal fade" id="googleMapModal" role="dialog">
  <div class="modal-dialog modal-lg">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Map</h4>
      </div>
      <div class="modal-body">
        <input id="pac-input" class="controls pac-input" type="text" placeholder="Search Box" style="width: 200px;">
        <div class="form-group" id="location_map" style="width: 100%;height: 300px;"></div>

        <div class="row">
          <div class="text-right col-md-12 col-sm-12">
            <a href="javascript:void(0)" class="btn btn-danger clear_marker">Clear</a>
            <a href="javascript:void(0)" class="btn btn-success" onclick="copyMapLink()">Copy Map Link</a>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div>
    
  </div>
</div>
<!-- maps modal end -->

<!-- edit caption modal -->
<div class="modal fade" id="editCaptionModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="editDescriptionModalLabel">Edit Caption</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="caption_image_id" id="edit_caption_id" value="">

        <div class="form-group row">
          <div class="col-md-4 col-sm-12 text-right">
            <label class="form-label">Caption <br>العنوان أرابيك</label>
          </div>
          <div class="input-with-icon  right col-md-6 col-sm-12">                                       
            {!! Form::text('edit_caption', null , ['class' => 'form-control','id'=>'edit_caption_input']) !!}
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="update_caption_btn">Update</button>
        <button type="button" class="btn  btn-default" onclick='closeModal("#editCaptionModal")' aria-label="Close"">Cancel</button>
      </div>
    </div>
  </div>
</div>

<!-- add tiers modal -->
<div class="modal fade" id="addEditTierModal" role="dialog">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <form id="addEditTierForm">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tier_modal_title">Add Tier</h4>
        <input type="hidden" name="edit_tier_id" id="edit_tier_id" value="">
      </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <div class="col-md-4 col-sm-12 ">
                  <label class="form-label">Title English <br> العنوان إنجليش <span style="color:red">*</span></label>
                </div>
                <div class="input-with-icon  right col-md-8 col-sm-12">                                       
                  {!! Form::text('title_english_tier', null , ['class' => 'form-control', 'id'=>'title_english_tier','required'=>true]) !!}
                  <span class="help-block"><span id="char_title_english_tier">15</span> <?php echo 'characters left'; ?></span>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4 col-sm-12 ">
                  <label class="form-label">Header Color <br> العنوان أرابيك <span style="color:red">*</span></label>
                </div>
                <div class="input-with-icon  right col-md-8 col-sm-12">
                  <div id="header_color_div">
                    <input type="text" class="form-control" id="header_color_tier" value="" required>
                  </div>                               
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <div class="col-md-4 col-sm-12 ">
                  <label class="form-label">Title Arabic <br> العنوان أرابيك <span style="color:red">*</span></label>
                </div>
                <div class="input-with-icon  right col-md-8 col-sm-12">                                       
                  {!! Form::text('title_arabic_tier', null , ['class' => 'form-control', 'id'=>'title_arabic_tier','required'=>true]) !!}
                  <span class="help-block"><span id="char_title_arabic_tier">15</span> <?php echo 'characters left'; ?></span>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-8">
              <div class="form-group row">
                <div class="col-md-3 col-sm-12 ">
                  <label class="form-label">Price <br> العنوان إنجليش</label>
                </div>
                <div class="input-with-icon  right col-md-8 col-sm-12">                                       
                  <label class="radio-inline">
                    <input type="radio" name="priceRadioBtn" value="0" checked id="free_price_radio">Free مجانيا
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="priceRadioBtn" value="1" id="value_price_radio">Value ثابت
                  </label>
                  <label class="radio-inline hidden" id="value_price_div">
                    <input type="number" min="0" name="value_price" id="value_price" style="padding: 0px 10px 0px 10px !important;" class="form-control" value="0">
                  </label>
                  
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-8">
              <div class="form-group row">
                <div class="col-md-3 col-sm-12 ">
                  <label class="form-label">Units <br> العنوان أرابيك</label>
                </div>
                <div class="input-with-icon  right col-md-8 col-sm-12">                                       
                  <label class="radio-inline" style="margin-right: 9px;">
                    <input type="radio" name="unitRadioBtn" value="0" checked id="open_unit_radio">Open فتح
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="unitRadioBtn" value="1" id="value_unit_radio">Value ثابت
                  </label>
                  <label class="radio-inline hidden" id="value_unit_div">
                    <input type="number" min="0" name="value_unit" id="value_unit" style="padding: 0px 10px 0px 10px !important;" class="form-control" value="0">
                  </label>
                  
                </div>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="input-with-icon col-md-12 col-sm-12">
              <label for="description_english_tier"><span class="semi-bold">Description English<span class="heading_arabic">الإنجليزية</span></span></label>
              {!! Form::textarea('description_english_tier', null, ['class' => 'form-control','id'=>'description_english_tier','rows'=>5]) !!}
              <span class="help-block"><span id="char_description_english_tier">250</span> <?php echo 'characters left'; ?></span>
            </div>
          </div>

          <div class="form-group row">
            <div class="input-with-icon col-md-12 col-sm-12">
              <label for="description_arabic_tier"><span class="semi-bold">Description Arabic<span class="heading_arabic">الإنجليزية</span></span></label>
              {!! Form::textarea('description_arabic_tier', null, ['class' => 'form-control','id'=>'description_arabic_tier','rows'=>5]) !!}
              <span class="help-block"><span id="char_description_arabic_tier">250</span> <?php echo 'characters left'; ?></span>
            </div>
          </div>
          
        </div>
          
        <div class="modal-footer">
          <center>
            <!-- <button type="button" class="btn btn-default" id="add_tier_btn">Submit</button> -->
            {!! Form::submit('Submit', ['class' => 'btn btn-default tier-form-submit','id' => 'add_tier_btn']) !!}
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </center>
        </div>
      </form>
    </div>
    
  </div>
</div>

{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
  <input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!} 

@endsection
@section('page_script')
@parent
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAe4ISmDhVvG-BXiqwXI2EkL3GdDFDxgM&libraries=places" async defer></script>
<script type="text/javascript" language="javascript" class="init">
  var divClone = $("#image_additional_div").clone();
  $(window).load(function(){
    initAutocomplete();
  });
  $(document).ready(function(){

    initialise_colorpicker();
    priceRadioProcessing();
    unitRadioProcessing();

    $('#close_picture_btn').click(function(){
        contentHandeling(divClone);
    });

    $('input[name=priceRadioBtn]').on('change', function() {
      priceRadioProcessing();
    });

    $('input[name=unitRadioBtn]').on('change', function() {
      unitRadioProcessing();
    });

    $('#tag_dropdown').select2({placeholder: 'Select Tag'});
    $('.date_picker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $('.time_picker').clockpicker({
      autoclose: true,
      placement: 'top',
      align: 'left',
    });

    $("#description_english").keyup(function (event) {
        updateFeatureCharLimitNum('description_english','char_description_english',250);
    });
    
    $("#description_arabic").keyup(function (event) {
        updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
    });

    $("#tc_arabic_english").keyup(function (event) {
        updateFeatureCharLimitNum('tc_arabic_english','char_tc_arabic_english',250);
    });
    
    $("#tc_arabic_arabic").keyup(function (event) {
        updateFeatureCharLimitNum('tc_arabic_arabic','char_tc_arabic_arabic',250);
    });

    $("#title_english").keyup(function (event) {
        updateFeatureCharLimitNum('title_english','char_title_english',60);
    });

    $("#title_arabic").keyup(function (event) {
        updateFeatureCharLimitNum('title_arabic','char_title_arabic',60);
    });

    $("#description_english_tier").keyup(function (event) {
        updateFeatureCharLimitNum('description_english_tier','char_description_english_tier',250);
    });

    $("#description_arabic_tier").keyup(function (event) {
        updateFeatureCharLimitNum('description_arabic_tier','char_description_arabic_tier',250);
    });

    $("#title_arabic_tier").keyup(function (event) {
        updateFeatureCharLimitNum('title_arabic_tier','char_title_arabic_tier',15);
    });

    $("#title_english_tier").keyup(function (event) {
        updateFeatureCharLimitNum('title_english_tier','char_title_english_tier',15);
    });

    updateFeatureCharLimitNum('title_english','char_title_english',60);
    updateFeatureCharLimitNum('title_arabic','char_title_arabic',60);
    updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
    updateFeatureCharLimitNum('description_english','description_english_arabic',250);
    updateFeatureCharLimitNum('tc_arabic_english','char_tc_arabic_english',250);
    updateFeatureCharLimitNum('tc_arabic_arabic','char_tc_arabic_arabic',250);

    updateFeatureCharLimitNum('title_arabic_tier','char_title_arabic_tier',15);
    updateFeatureCharLimitNum('title_english_tier','char_title_english_tier',15);
    updateFeatureCharLimitNum('description_english_tier','char_description_english_tier',250);
    updateFeatureCharLimitNum('description_arabic_tier','char_description_arabic_tier',250);

    // another dt toggle
    $('#is_another_dt').change(function() {
      this.checked ? $('#another_dt_div').removeClass('hidden') : $('#another_dt_div').addClass('hidden');
    });

    // submitting events format
    // jQuery('.form-submit').click(function (event) {
    //     form = $('#form_events');
    //     event.preventDefault();
    //     setTimeout(function () {
    //         form.submit();
    //     }, 500);
    //     jQuery('#progressbar').show();
    //     return true;
    // });

    jQuery('#form_events').submit(function (event) {
      var logo_image = $("#images_div #image1 img").attr('src');
      if(!logo_image){
        fileSizeMessage('Logo Image is required');
        return false;
      }

      var tiers_count = tiers_table.page.info().end;
      if(tiers_count > 0)
      {
        var pass = true;
        //some validations

        if (pass == false) {
            return false;
        }
        form = this;
        jQuery('.btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();

        return true;
      }else{
        fileSizeMessage('At leat one tier must add');
      }
      return false;
    });

    //additional images datatable
    $('#datatable-example').DataTable({
        serverSide: false,
        'pageLength': 25,
        "order": [[1, "desc"]],
        ajax: {
            "url": "{{ URL::to('admin/additionalimages/datatable?imageable_id='.($event ? $event->id : 0)) }}",
            "type": "POST"
        },
        columns: [
            {data: 'check', name: 'check'},
            {data: 'id', name: 'id'},
            {data: 'image', name: 'image'},
            {data: 'caption', name: 'caption'},
            {data: 'actions', name: 'actions'}
        ],
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [0,1,2,4],
                "searchable": false
            },
            {
                "targets": [0, 1, 2, 3, 4],
                "orderable": false
            },
           
        ]
    });

    //tiers datatable
    var tiers_table = $('#datatable-example2').DataTable({
        serverSide: false,
        'pageLength': 25,
        "order": [[1, "desc"]],
        ajax: {
            "url": "{{ URL::to('admin/tiers/datatable?event_id='.($event ? $event->id : 0)) }}",
            "type": "POST"
        },
        columns: [
            {data: 'check', name: 'check'},
            {data: 'id', name: 'id'},
            {data: 'title_english', name: 'title_english'},
            {data: 'title_arabic', name: 'title_arabic'},
            {data: 'price', name: 'price'},
            {data: 'total_quantity', name: 'total_quantity'},
            {data: 'actions', name: 'actions'}
        ],
        "columnDefs": [
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [0,1,6],
                "searchable": false
            },
            {
                "targets": [0, 1, 2, 3, 4,5,6],
                "orderable": false
            },
           
        ]
    });

    $(document).on("click","#add_picture_btn",function() {
        var image = $('#additional_image').get(0).files[0];
        var formData = new FormData();
        
          formData.append('image', image);
          formData.append('event_id',  "{{ $event ? $event->id : 0 }}");
          formData.append('logo_h3', $('#h3').val());
          formData.append('logo_w3', $('#w3').val());
          formData.append('logo_x3', $('#x3').val());
          formData.append('logo_y3', $('#y3').val());
          formData.append('caption', $('#caption').val());
        
        if(image != undefined)
        {
          $('#progressbar').show();
          $.ajax({
            url : "{{ URL::route('admin.additionalimages.uploadimage')}}",
            type: "POST",
            data : formData,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR){
              if($.isEmptyObject(data.error)){
                  $('#datatable-example').DataTable().ajax.reload();
                  contentHandeling(divClone);
                }
              }
            });  
        }
        else{
            alert("please upload images first!");
        }
    });

    //add tier
    $(document).on("click","#add_tier_btn",function() {

      $("#addEditTierForm").submit(function(e){
        e.preventDefault();
      });

      var title_english_tier = $('#title_english_tier').val();
      var title_arabic_tier = $('#title_arabic_tier').val();
      var header_color_tier = $('#header_color_tier').val();

      if(title_english_tier != '' && title_arabic_tier != '' && header_color_tier != ''){
        var priceRadioVal = $('input[name=priceRadioBtn]:checked').val();
        var unitRadioVal = $('input[name=unitRadioBtn]:checked').val();

        var formData = new FormData();
        formData.append('event_id',  "{{ $event ? $event->id : 0 }}");
        formData.append('title_english',title_english_tier);
        formData.append('title_arabic',title_arabic_tier);
        formData.append('description_english',$('#description_english_tier').val());
        formData.append('description_arabic',$('#description_arabic_tier').val());
        formData.append('header_color',header_color_tier);
        formData.append('price_type',priceRadioVal);
        formData.append('total_quantity_type',unitRadioVal);
        formData.append('price',$('#value_price').val());
        formData.append('total_quantity',$('#value_unit').val());

        var edit_tier_id = $('#edit_tier_id').val();
        formData.append('edit_tier_id',edit_tier_id ? edit_tier_id : 0);
        
        $.ajax({
          url : "{{ URL::route('admin.tiers.addtier')}}",
          type: "POST",
          data : formData,
          processData: false,
          contentType: false,
          success:function(data, textStatus, jqXHR){
            if($.isEmptyObject(data.error)){
                $('#datatable-example2').DataTable().ajax.reload();
              }
            }
        });
        $('#addEditTierModal').modal('hide');
      }
    });

    $('#additionaImageModal').on('hidden.bs.modal', function () {
      // $(".imgareaselect-outer, .imgareaselect-handle").addClass('hidden');
      // $(".imgareaselect-selection").parents('div').addClass('hidden');
      // document.getElementById("additional_image").value = "";
      // document.getElementById("additional_image").value = "";
      // document.getElementById("additional_image").value = null;
      $('#caption').val('');
    });

    $('#addEditTierModalBtn').on('click',function(){
      $('#tier_modal_title').text('Add Tier');
      $('#value_price_div,#value_unit_div').addClass('hidden');
      $('.color-preview.current-color').css("background-color", "");

      $('#char_title_arabic_tier,#char_title_english_tier').text(250);
      $('#char_description_english_tier,#char_description_arabic_tier').text(250);

      $('#addEditTierForm')[0].reset();
      $('#addEditTierModal').modal('show');
    });
  });
  
  function initialise_colorpicker()
  {
    $("#header_color_tier").pickAColor({
      showSpectrum            : true,
      showSavedColors         : true,
      saveColorsPerElement    : false,
      fadeMenuToggle          : true,
      showHexInput            : true,
      showBasicColors         : true,
      allowBlank              : true,
      inlineDropdown          : false
    });
  }

  function priceRadioProcessing()
  {
    var priceRadioVal = $('input[name=priceRadioBtn]:checked').val();
    priceRadioVal == 1 ? $('#value_price_div').removeClass('hidden') : $('#value_price_div').addClass('hidden');
  }

  function unitRadioProcessing()
  {
    var unitRadioVal = $('input[name=unitRadioBtn]:checked').val();
    unitRadioVal == 1 ? $('#value_unit_div').removeClass('hidden') : $('#value_unit_div').addClass('hidden');
  }

  function closeModal(id) {
    $(id).modal('hide');
  }
  
  function delAdditionalImage(id)
  {
    if(id)
    {
      var formData = new FormData();
      formData.append('id', id);
      $.ajax({
        url : "{{ URL::route('admin.additionalimages.delimage')}}",
        type: "POST",
        data : formData,
        processData: false,
        contentType: false,
        success:function(data, textStatus, jqXHR){
          if($.isEmptyObject(data.error)){
              $('#datatable-example').DataTable().ajax.reload();
            }
          }
        });  
    }
  }

  function delTier(id)
  {
    if(id)
    {
      var formData = new FormData();
      formData.append('id', id);
      $.ajax({
        url : "{{ URL::route('admin.tiers.deltier')}}",
        type: "POST",
        data : formData,
        processData: false,
        contentType: false,
        success:function(data, textStatus, jqXHR){
          if($.isEmptyObject(data.error)){
              $('#datatable-example2').DataTable().ajax.reload();
            }
          }
        });  
    }
  }

  $( "#update_caption_btn" ).click(function() {
    // console.log("sadas");
    var formData = new FormData();
    formData.append('caption', $('#edit_caption_input').val());
    formData.append('image_id', $('#edit_caption_id').val());
      $.ajax({
        url : "{{ URL::route('admin.additionalimages.editcaption')}}",
        type: "POST",
        data : formData,
        processData: false,
        contentType: false,
        success:function(data, textStatus, jqXHR){
            if($.isEmptyObject(data.error)){    
                $('#editCaptionModal').modal('hide');
                $('#datatable-example').DataTable().ajax.reload();
            }
        }
    });  
  });

  function editCaption(id){
      var caption = $('#'+id+'_additional_edit_caption').data('caption');
      $('#edit_caption_input').val(caption);
      $('#edit_caption_id').val(id);
      $('#editCaptionModal').modal('show');
  }

  function editTier(id){
      var data = $('#'+id+'_edit_tier').data();

      $('#tier_modal_title').text('Edit Tier');
      $('#edit_tier_id').val(id);
      $('#title_english_tier').val(data.title_english);
      $('#title_arabic_tier').val(data.title_arabic);
      $('#description_english_tier').val(data.description_english);
      $('#description_arabic_tier').val(data.description_arabic);
      $('#value_price').val(data.price);
      $('#value_unit').val(data.total_quantity);

      $('#header_color_div').empty();
      $('#header_color_div').append('<input type="text" class="form-control" id="header_color_tier" value="">');
      $('#header_color_tier').val(data.header_color);
      initialise_colorpicker();

      var price_type = data.price_type;
      var total_quantity_type = data.total_quantity_type;

      price_type == 1 ? $("#value_price_radio").prop("checked", true) : $("#free_price_radio").prop("checked", true);
      total_quantity_type == 1 ? $("#value_unit_radio").prop("checked", true) : $("#open_unit_radio").prop("checked", true);
      
      priceRadioProcessing();
      unitRadioProcessing();

      updateFeatureCharLimitNum('title_arabic_tier','char_title_arabic_tier',15);
      updateFeatureCharLimitNum('title_english_tier','char_title_english_tier',15);
      updateFeatureCharLimitNum('description_english_tier','char_description_english_tier',250);
      updateFeatureCharLimitNum('description_arabic_tier','char_description_arabic_tier',250);
      
      $('#addEditTierModal').modal('show');
  }

  $(document).on('click', '#startCrop', function (e) {

      var image = $('#image1 .cropbox');
      AspectRatio = '1:1';
      maxWidth = MaxHeight = 4000;
      var value = $("#beacon_target").val();
      var originalWidth = image[0].naturalWidth;
      var originalHeight = image[0].naturalHeight;
      $('#image1 .cropbox').imgAreaSelect({
          handles: true,
          fadeSpeed: 200,
          onSelectChange: updateCoords,
          imageHeight: originalHeight,
          imageWidth: originalWidth,
          aspectRatio: AspectRatio,
          maxHeight: MaxHeight,
          maxWidth: maxWidth,
          parent: '#crop_parent_1'
      });
      $("#image1-dim").html('width: 0, height: 0');
      $("#image1-dim").show();

  });
  function updateCoords(img, selection)
  {
      if (!selection.width || !selection.height)
          return;

      $('#x').val(selection.x1);
      $('#y').val(selection.y1);
      $('#w').val(selection.width);
      $('#h').val(selection.height);
      $("#image1-dim").html('width: ' + selection.width + ', height: ' + selection.height);
  }

  $(document).on('click', '#startCrop2', function (e) {

      var image = $('#image2 .cropbox');
      AspectRatio = '2.13:1';
      maxWidth = MaxHeight = 4000;
      var value = $("#beacon_target").val();
      // console.log(image[0]);
      var originalWidth = image[0].naturalWidth;
      var originalHeight = image[0].naturalHeight;
      $('#image2 .cropbox').imgAreaSelect({
          handles: true,
          fadeSpeed: 200,
          onSelectChange: updateCoords2,
          imageHeight: originalHeight,
          imageWidth: originalWidth,
          aspectRatio: AspectRatio,
          maxHeight: MaxHeight,
          maxWidth: maxWidth,
          parent: '#crop_parent_2'
      });
      $("#image2-dim").html('width: 0, height: 0');
      $("#image2-dim").show();

  });
  function updateCoords2(img, selection)
  {
      if (!selection.width || !selection.height)
          return;

      $('#x2').val(selection.x1);
      $('#y2').val(selection.y1);
      $('#w2').val(selection.width);
      $('#h2').val(selection.height);
      $("#image2-dim").html('width: ' + selection.width + ', height: ' + selection.height);
  }

  $(document).on('click', '#startCrop3', function (e) {

      var image = $('#image3 .cropbox');
      AspectRatio = '2.12:1';
      maxWidth = MaxHeight = 4000;
      var value = $("#beacon_target").val();
      // console.log(image[0]);
      var originalWidth = image[0].naturalWidth;
      var originalHeight = image[0].naturalHeight;
      $('#image3 .cropbox').imgAreaSelect({
          handles: true,
          fadeSpeed: 200,
          onSelectChange: updateCoords3,
          imageHeight: originalHeight,
          imageWidth: originalWidth,
          aspectRatio: AspectRatio,
          maxHeight: MaxHeight,
          maxWidth: maxWidth,
          classPrefix: 'additional_image imgareaselect',
          parent: $('#additionaImageModal .modal-content')
      });
      $("#image3-dim").html('width: 0, height: 0');
      $("#image3-dim").show();

  });
  function updateCoords3(img, selection)
  {
      if (!selection.width || !selection.height)
          return;

      $('#x3').val(selection.x1);
      $('#y3').val(selection.y1);
      $('#w3').val(selection.width);
      $('#h3').val(selection.height);
      $("#image3-dim").html('width: ' + selection.width + ', height: ' + selection.height);
  }

  function contentHandeling(divClone){
    $(".additional_image.imgareaselect-outer, .additional_image.imgareaselect-handle").addClass('hidden');
    $("#image_additional_div").replaceWith(divClone.clone());
  }

</script>
@endsection
