@extends('layouts.app')
@section('content')
<style type="text/css">
    table#datatable-example4,table#datatable-example3,table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }

    .nav-navbar-center {
        margin-top: 0px;
    }
</style>
<div class="page-title">
    <center>
        @if($event->eventStatusById($event->id))
            <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @else
            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @endif
        <br/>
        <span style="font-weight: bold">{{$event->title_english}}</span>
        <br/>
        <span style="font-weight: bold">{{$event->title_arabic}}</span>
        <br/>
    </center>
</div>


<div class="page-title">
    <center>
        <h3>View Tickets جميع طلبات الحدث</h3>
        <br/>
        <span>Translate between 103 languages by typing</span>
        <br/>
        <span>ترجم بين 103 لغة عن طريق الكتابة</span>
        <br/>
    </center>
</div>


<br>


<div class='control' style="font-size: 18px">
    <div class="nav-navbar-center">
        <ul class="nav nav-pills">
            <li style="margin-right: 20px">Total Sold حضرها<br/><span style="font-size: 24px; color: #0078d7" >{{$issued_tiers_qty_sum}}</span> </li>
            <li style="margin-left: 20px">Total Available تم البيع<br/><span style="font-size: 24px; color: #6b6b6c" >{{$available_qty}}</span></li>
        </ul>
    </div>
</div>
<br>

<div class='control'>
    <div class="nav-navbar-center">
        <ul class="nav nav-pills">
            <li id="issued_tab"><a href="#issued_event_requests" data-toggle="tab">Issued ({{$issued_count}})</a></li>
            <li id="cancelled_tab"><a href="#cancelled_event_requests " data-toggle="tab">Cancelled ({{$cancelled_count}})</a></li>
        </ul>
    </div>
</div>

<br/>


<div class="tab-content">

    <div id="issued_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-left markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/events/bulkdelete') }}">+ Issue exclusive tickets</a>
                    @endif
                </div>
                <div class="pull-right markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/events/bulkdelete') }}">Delete</a>
                    @endif
                </div>
                <br><br><br>
                <table class="table table-hover table-condensed" id="datatable-example" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                            <th>Id</th>
                            <th>عنوان <br> Information</th>
                            <th>شعار <br> Total(SR)</th>
                            <th>نشرت <br> Status</th>
                            <th>آخر تحديث <br> Last Edit</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="cancelled_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-right markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/events/bulkdelete') }}">Delete</a>
                    @endif
                </div><br><br><br>

                <table class="table table-hover table-condensed" id="datatable-example2" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                            <th>Id</th>

                            <th>عنوان <br> Information</th>
                            <th>شعار <br> Total(SR)</th>
                            <th>نشرت <br> Status</th>
                            <th>آخر تحديث <br> Last Edit</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


</div>
{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
<input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!}
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {
        var event_type = '{{Request::get("source")}}';
        if(event_type == 'issued'){
            $( "#issued_tab" ).addClass( "active" );
            $( "#issued_event_requests" ).addClass( "active in" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'cancelled'){
            $( "#cancelled_tab" ).addClass( "active" );
            $( "#cancelled_event_requests" ).addClass( "active in" );
            $( "#issued_tab" ).removeClass( "active" );
        } else{
            $( "#issued_tab" ).addClass( "active" );
            $( "#issued_event_requests" ).addClass( "active in" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }


        $('#issued_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests.viewtickets',$event->id)}}?source=issued");
        });
        $('#cancelled_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests.viewtickets',$event->id)}}?source=cancelled");
        });

        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable1') }}",
                "type": "POST",
                "data": {
                    'eventid':'{{$eventid}}',
                    'type':3
                     }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,2,6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false
                }
            ]
        });

        $('#datatable-example2').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable1') }}",
                "type": "POST",
                "data": {
                         'eventid':'{{$eventid}}',
                         'type':4
                    }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,2,6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false
                }
            ]
        });
    });
</script>

@endsection