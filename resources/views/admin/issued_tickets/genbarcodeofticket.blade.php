@extends('site.layout')
@section('content')
<link href="{{ asset('css/site.css') }}" rel="stylesheet">
<div class="row">
    <div style="background-color: white" class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container">
        <div class="elevation-3dp">
                 
                
        	<div class="listing-div" style="margin-top: -15px;">

<style type="text/css">
    a, a:hover { color: inherit; }
.head-top {
    /*border: solid 1px #254072;*/
    height: 35px;
    position: relative;
}
.head-top:before {
    content: "";
    position: absolute;
    right: 42%;
    height: 50px;
    width: 50px;
    border-radius: 100%;
    background: #fff;
    left: 42%;
    top: -30px;
}
.head-top p span {
    color: #fff;
    padding: 6px 15px;
}
.head-bottom {
    background: #9b9b9b;
    border: solid 1px #9b9b9b;
    height: 35px;
    position: relative;
}
.head-bottom:before {
    content: "";
    position: absolute;
    right: 42%;
    height: 50px;
    width: 50px;
    border-radius: 100%;
    background: #fff;
    left: 42%;
    bottom: -30px;
}
p.dt-in-eng {
    float: left;
    margin: 0;
}
p.dt-in-eng span:nth-child(1) {
    font-size: 14px;
}
p.dt-in-eng span:last-child {
    color: #a7a7a7;
    display: block;
    padding-left: 4px;
    margin-top: -5px;
    font-size: 11px;
}
.dt-line {
    float: left;
    border-bottom: solid 2px;
    width: 10px;
    display: block;
    margin: 21px 6px;
}
.time p.dt-in-eng span:nth-child(1) {
    font-size: 9px;
    margin-left: 2px;
}
.time p.dt-in-eng span:last-child{
    padding-top: 5px;
}
.date , .time{
    float: left;
}
.dtb-eng {
    float: left;
    width: 90%;
    padding: 5px;
    border: solid 1px #cecece7e;
}
.blocks {
    float: left;
    width: 100px;
    /*margin-left: 10px;*/
    margin-left: 5px;
}
.ord-sec span{
    display: block;
    color: #a7a7a7;
    font-size: 15px;
}
.rdr-name{
    font-weight: bold;
    font-size: 13px;
}

    .card {
        height: auto !important;
    }
    .media-heading{
        font-size: 18.91px;
    }

    .vip-tag {
        position: absolute;
        top: 21px;
        right: 20px;
    }
</style>


<div class="card-div">
    <div class="row" style="margin: 0px -15px 2% -15px">
        <div class="head-top" style="background-color:#{{$tier->header_color}}">
            <p>
                <span class="pull-left">{{$tier->title_english }}</span>
                <span class="pull-right arabic_font">{{$tier->title_arabic}}</span>
            </p>
        </div>
    </div>
    <div class="row card card-simple" style="border: 1px dashed #{{$tier->header_color}}">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                @if($issued_ticket->tag_as_vip)
                    <img src="{{asset('assets/images/vip_tag.png')}}" style="width: auto; height: 25px;" class="vip-tag">
                @endif

                <center>
                    <img src="data:image/png;base64, {!! base64_encode($QRCode) !!} " style="width: 150px;height: 150px;"><br>
                    <p class="text-center" style="font-size: 9px;margin-top: -22px; color: #{{$barcode_color}}">{{$num}}</p>
                </center>

                <!-- <figure style="text-align: center;">
                    <?php
                    print_r($QRCode)
                    ?>
                    <center><figcaption class="text-center" style="margin-top: -30px;position: relative;font-size: 9px;">{{$num}}</figcaption></center>
                </figure> -->
                
            </div>
            
        </div>
        
        <br>
        <div class="row" style="margin-top: -5px;">
            <div class="col-lg-8 col-ms-8 col-sm-8 col-xs-8" style="padding-right: 0px;">
                <h4 class="media-heading text-right arabic_font"> {{$event->title_arabic}}</h4>
                <h4 class="text-right" style="font-size: 13.34px;">{{$event->title_english}}</h4>

                <br>

                <h4 class="media-heading text-right arabic_font" style="font-size: 15.13px;"> {{$event->venue_arabic}}</h4>
                <h4 class="text-right" style="font-size: 10.82px;">{{$event->venue_english}}</h4>
            </div>
            <div class="col-lg-4 col-ms-4 col-sm-4 col-xs-4" style="padding-left: 11px;">
                <img src="{{asset($event->logo_image)}}" class="media-object" style="width:90px">
            </div>
        </div>
        <br>

        <style type="text/css">
            .big_txt{
                font-size: 22.69px !important;
            }
            .small_txt{
                font-size: 9.45px !important;
                margin-left: -5px;
            }
            .bottom_year{
                padding-left: 10px !important;
                font-size: 10.08px !important;
                color: #9B9B9B !important;
            }

            .bottom_time{
                font-size: 7.56px !important;
                color: #9B9B9B !important;
                margin-left: 10px;
                margin-top: 0px !important;
                padding-left: 6px !important;
            }
            .big_txt_time{
                font-size: 12.68px !important;
                margin-left: 7px !important;
            }

            .orderd_by{
                font-size: 12.61px;
                color: #9B9B9B;
                text-align: right;
            }
        </style>
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-lg-8 col-ms-8 col-sm-8 col-xs-8">

                <div class="row">
                    <div class=" col-lg-6 col-ms-6 col-sm-6 col-xs-6 {{ ($event->from_date_arabic || $event->to_date_arabic) || ($event->from_time_arabic || $event->to_time_arabic) ? '' : 'col-lg-offset-6 col-md-offset-6 col-sm-offset-6 col-xs-offset-6'}}">
                        <div class="blocks">
                            <div class="dtb-eng" style="width: 112px; height: 78px;">
                                <div class="date" style="margin-top: -6px;">
                                    <p class="dt-in-eng">
                                        <span class="big_txt">
                                            {{date('d',strtotime($event->from_date_english))}}
                                        </span>
                                        <span class="small_txt">
                                            /{{date('m',strtotime($event->from_date_english))}}
                                        </span>
                                        <span>&mdash;</span>
                                        <span class="bottom_year">
                                            {{date('Y',strtotime($event->from_date_english))}}
                                        </span>
                                    </p>
                                    <p class="dt-in-eng" style="margin-left: 4px;">
                                        <span class="big_txt">
                                            {{date('d',strtotime($event->to_date_english))}}
                                        </span>
                                        <span class="small_txt">
                                            /{{date('m',strtotime($event->to_date_english))}}
                                        </span>
                                        <span class="bottom_year">
                                            {{date('Y',strtotime($event->to_date_english))}}
                                        </span>
                                    </p>
                                </div>

                                <div class="date" style="margin-top: 5px;">
                                    <p class="dt-in-eng">
                                        <span class="big_txt_time" style="margin-left: 14px !important;">
                                            {{date('g:i',strtotime($event->from_time_english))}}
                                        </span>
                                       
                                        <span>&mdash;</span>
                                        <span class="bottom_time" style="padding-left: 11px !important;">
                                            {{date('A',strtotime($event->from_time_english))}}
                                        </span>
                                    </p>
                                    <p class="dt-in-eng" style="margin-left: -10px;">
                                        <span class="big_txt_time" style="margin-left: 14px !important;">
                                            {{date('g:i',strtotime($event->to_time_english))}}
                                        </span>
                                        
                                        <span class="bottom_time" style="padding-left: 11px !important;">
                                            {{date('A',strtotime($event->to_time_english))}}
                                        </span>
                                    </p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <?php
                        $existing_arabic_from_time = Ticketing\Models\Event::getSeperateArabicTime2($event->from_time_arabic);
                        $existing_arabic_to_time = Ticketing\Models\Event::getSeperateArabicTime2($event->to_time_arabic);

                        $arabic_from_date = $event->getArabicDate($event);
                        $arabic_to_date = $event->getArabicToDate($event);

                        // dd($arabic_from_date,$arabic_to_date);
                    ?>
                    <!-- <div class="col-lg-6 col-ms-6 col-sm-6 col-xs-6">
                        <div class="blocks">
                            <div class="dtb-eng" style="width: 112px; height: 78px;">
                                @if($event->from_date_arabic || $event->to_date_arabic)
                                    <div class="date" style="margin-top: -6px;">
                                        <p class="dt-in-eng">
                                            <span class="small_txt">
                                                {{ isset($arabic_to_date[1]) ? $arabic_to_date[1] : ''}}/
                                            </span>
                                            <span class="big_txt">
                                                {{ isset($arabic_to_date[2]) ? $arabic_to_date[2] : ''}}
                                            </span>
                                            <span>&mdash;</span>
                                            <span class="bottom_year">
                                                {{ isset($arabic_to_date[0]) ? $arabic_to_date[0] : ''}}
                                            </span>
                                        </p>
                                        <p class="dt-in-eng" style="margin-left: 4px;">
                                            <span class="small_txt">
                                                {{ isset($arabic_from_date[1]) ? $arabic_from_date[1] : ''}}/
                                            </span>
                                            <span class="big_txt">
                                               {{ isset($arabic_from_date[2]) ? $arabic_from_date[2] : ''}}
                                            </span>
                                            <span class="bottom_year">
                                                {{ isset($arabic_from_date[0]) ? $arabic_from_date[0] : ''}}
                                            </span>
                                        </p>
                                    </div>
                                @endif
                                @if($event->from_time_arabic || $event->to_time_arabic)
                                    <div class="date" style="margin-top: 5px;">
                                        <p class="dt-in-eng">
                                            <span class="big_txt_time">
                                                {{$existing_arabic_to_time['both']}}
                                            </span>
                                           
                                            <span>&mdash;</span>
                                            <span class="bottom_time" style="margin-top: -1px !important;">
                                                @if($existing_arabic_to_time['type'] == 'ص' || $existing_arabic_to_time['type'] == 'م')
                                                    {{$existing_arabic_to_time['type'] == 'ص' ? 'صباحاً' : 'مساءً'}}
                                                @else
                                                    {{$existing_arabic_to_time['type']}}
                                                @endif
                                            </span>
                                        </p>
                                        <p class="dt-in-eng" style="margin-left: 2px;">
                                            <span class="big_txt_time">
                                                {{$existing_arabic_from_time['both']}}
                                            </span>
                                            
                                            <span class="bottom_time" style="margin-top: -1px !important;">
                                                @if($existing_arabic_from_time['type'] == 'ص' || $existing_arabic_from_time['type'] == 'م')
                                                    {{$existing_arabic_from_time['type'] == 'ص' ? 'صباحاً' : 'مساءً'}}
                                                @else
                                                    {{$existing_arabic_from_time['type']}}
                                                @endif
                                            </span>
                                        </p>
                                    </div>
                                @endif
                                
                            </div>
                        </div>
                    </div> -->
                    @if( ($event->from_date_arabic || $event->to_date_arabic) || ($event->from_time_arabic || $event->to_time_arabic) )
                    <div class="col-lg-6 col-ms-6 col-sm-6 col-xs-6">
                        <div class="blocks">
                            <div class="dtb-eng" style="width: 112px; height: 78px;">
                                @if($event->from_date_arabic || $event->to_date_arabic)
                                    <div class="date" style="margin-top: -6px;margin-left: 5px;">
                                        <p class="dt-in-eng">
                                            <span class="big_txt" style="margin-left: -5px;">
                                                {{ isset($arabic_to_date[2]) ? $arabic_to_date[2] : ''}}
                                            </span>
                                            <span class="small_txt" style="margin-left: -3px;">
                                                @if($arabic_to_date['leading_zero'] == 'yes')
                                                    /{{ isset($arabic_to_date[1]) ? $arabic_to_date[1] : ''}}&nbsp;&nbsp;
                                                @else
                                                    /{{ isset($arabic_to_date[1]) ? $arabic_to_date[1] : ''}}
                                                @endif
                                            </span>
                                            <span>&mdash;</span>
                                            <span class="bottom_year" style="padding-left: 8px !important;">
                                                {{ isset($arabic_to_date[0]) ? $arabic_to_date[0] : ''}}
                                            </span>
                                        </p>
                                        <p class="dt-in-eng" style="margin-left: 4px;">
                                            <span class="big_txt" style="margin-left: -5px;">
                                               {{ isset($arabic_from_date[2]) ? $arabic_from_date[2] : ''}}
                                            </span>
                                            <span class="small_txt" style="margin-left: 0px;">
                                                /{{ isset($arabic_from_date[1]) ? $arabic_from_date[1] : ''}}
                                            </span>
                                            <span class="bottom_year" style="padding-left: 8px !important;">
                                                {{ isset($arabic_from_date[0]) ? $arabic_from_date[0] : ''}}
                                            </span>
                                        </p>
                                    </div>
                                @endif
                                @if($event->from_time_arabic || $event->to_time_arabic)
                                    <div class="date" style="margin-top: 5px;">
                                        <p class="dt-in-eng">
                                            <span class="big_txt_time" style="margin-left: {{$existing_arabic_to_time['leading_zero'] == 'yes' ? '10px !important' : '7px'}}">
                                                @if($existing_arabic_to_time['leading_zero'] == 'yes')
                                                    {{$existing_arabic_to_time['both']}}&nbsp;
                                                @else
                                                    {{$existing_arabic_to_time['both']}}
                                                @endif
                                            </span>
                                           
                                            <span style="margin-left: 2px;">&mdash;</span>
                                            <span class="bottom_time" style="margin-top: -1px !important;padding-left: unset !important;margin-left: -10px !important;text-align: center;">
                                                @if($existing_arabic_to_time['type'] == 'ص' || $existing_arabic_to_time['type'] == 'م')
                                                    {{$existing_arabic_to_time['type'] == 'ص' ? 'صباحاً' : 'مساءً'}}
                                                @else
                                                    {{$existing_arabic_to_time['type']}}
                                                @endif
                                            </span>
                                        </p>
                                        <p class="dt-in-eng" style="margin-left: 2px;">
                                            <span class="big_txt_time">
                                                {{$existing_arabic_from_time['both']}}
                                            </span>
                                            
                                            <span class="bottom_time" style="margin-top: -1px !important;padding-left: unset !important;margin-left: 7px !important;text-align: center;">
                                                @if($existing_arabic_from_time['type'] == 'ص' || $existing_arabic_from_time['type'] == 'م')
                                                    {{$existing_arabic_from_time['type'] == 'ص' ? 'صباحاً' : 'مساءً'}}
                                                @else
                                                    {{$existing_arabic_from_time['type']}}
                                                @endif
                                            </span>
                                        </p>
                                    </div>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
               
            </div>

            
            <div class="col-lg-4 col-ms-4 col-sm-4 col-xs-4" style="padding-right: 19px;">
                {{--<p class="orderd_by arabic_font" style="margin-bottom: 5px;">أمر من قبل</p>--}}
                {{--<p class="orderd_by" style="font-size: 8.82px;">Ordered by</p>--}}
                <p style="font-size: 10.08px;float: right; direction: rtl;">{{$issued_ticket->customer->full_name}}</p>
            </div>
        </div>
       <!--  <div class="row" style="margin: 5% 0 0 0">
            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                <div class="media">
                    <div class="media-body">
                        <h4 class="media-heading text-right"> {{$event->title_arabic}}</h4>
                        <p class="text-right">{{$event->title_english}}</p>
                        <h4 class="media-heading text-right" style="margin-top: 20px">{{$event->description_arabic}} </h4>
                        <p class="text-right">{{$event->description_english}}</p>
                    </div>
                    <div class="media-right">
                        <img src="{{asset($event->logo_image)}}" class="media-object" style="width:90px">
                    </div>
                </div>
            </div>
            </div> -->


            <!-- <div  style="margin-top: 20px;">
                <div class="row" style="padding-left: 5px;">
                    



              <div class="blocks" style="padding-left: 0px;margin-left: 0px;">
                <div class="dtb-eng ">
                    <div class="date">
                    <p class="dt-in-eng">
                        <span style="float: left">
                            {{mb_substr($event->from_date_arabic,0,2)}}
                        </span>
                        <span style="font-size: 9px; color: black">
                            &ensp;{{mb_substr($event->from_date_arabic,2,2)}}
                        </span>
                         <br/>
                         <span style="color: grey; font-size: 12px">
                          {{mb_substr($event->from_date_arabic,5,5)}}
                         </span>
                    </p>
                        <div class="dt-line"></div>
                        <p class="dt-in-eng">
                        <span style="float: left">
                            {{mb_substr($event->to_date_arabic,0,2)}}
                        </span>
                        <span style="font-size: 9px; color: black">
                             &ensp;{{mb_substr($event->to_date_arabic,2,2)}}
                        </span>
                            <br/>
                            <span style="color: grey; font-size: 12px">
                          {{mb_substr($event->to_date_arabic,5,5)}}
                         </span>
                        </p>
                        <hr>
                </div>
                <div class="time">
                    <p class="dt-in-eng">
                   <span>
                        {{mb_substr($event->from_time_arabic,0,5)}}
                   </span>
                     <br>
                   <span style="text-align: center">
                       {{mb_substr($event->from_time_arabic,5,6)}}
                   </span>
                   </p>
                    <div class="dt-line"></div>
                <p class="dt-in-eng">
                    <span>
                    {{mb_substr($event->to_time_arabic,0,5)}}
                    </span>
                    <br>
                    <span style="text-align:center">
                       {{mb_substr($event->to_time_arabic,5,6)}}
                   </span>
                </p>
                </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;padding-left: 0px;">
                <p style="text-align: center; font-size: 9px">
                     <span>والورود</span>
                     <span>Order by</span>
                </p>
                <p class="text-right rdr-name" style="text-align: left ; font-size: 9px">
                    {{$issued_ticket->customer->full_name}}<br/>
                    {{$issued_ticket->customer->email}}<br/>
                    {{$issued_ticket->customer->mobile}}
                </p>
            </div>
        </div>
            </div> -->

        </div>



<div class="row" style="margin: 2% -15px 0px -15px">
      {{--<hr class="card_bottom_line">--}}
        {{--<!-- <img class="img-responsive" src="./Ticketing Pass_files/card_bottom.png" style="width: auto;height: auto;"> -->--}}

    {{--<img class="img-responsive" src="{{asset('assets/images//card_bottom.png')}}" style="width: auto;height: auto;">--}}


        <div class="head-bottom">

        </div>
    </div>
    <footer style="background-color: white; color: #9B9B9B; margin-bottom: -25px;">
        <center style="padding-top:10px">
            {{--<p><span style="font-size: 6.3px;">Powered by</span><span style="font-size: 7.56px" class="arabic_font"> مشغل بواسطة</span></p>--}}
            <!-- <div style="width: 140px; height: 26px; vertical-align: middle; font-style: italic;background-color: yellow"> <h4 style="font-style: italic;font-size: 28px; color: grey">TicketPass</h4></div> -->
            <a href="{{url('http://tickpass.net/')}}" target="_blank">
                <img src="{{asset('assets/images/bottom_logo.png')}}" style="margin-bottom: -16px; width: auto; height: 22px;">
            </a>
            <br>
        <!-- <span style="font-style: italic;; color: grey">www.ticketpass.com</span> -->
        <span style="font-size: 8px; display: block;margin-top: 12px;">www.tickpass.net</span>
        </center>
    </footer>
</div>

</div>
        	</div>
            

        </div>
      
   </div>

@endsection

@section('page_script')
    @parent
@endsection