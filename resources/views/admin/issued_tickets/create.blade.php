@extends('layouts.app')
@section('content')
<style type="text/css">
    .nav-navbar-center {
        margin-top: 0px;
    }
    input[type="radio"], input[type="checkbox"] {
        margin-top: 16px !important;
    }
</style>
<div class="page-title">
    <center>
        @if($event->status == 1 && $event->selling_off == 0)
            <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @else
            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @endif
        <br/>
            <span style="font-size: 16px;font-weight: bold">{{$event->title_arabic}}</span>
            <br/>
            <span style="font-size: 16px;font-weight: bold">{{$event->title_english}}</span>
        <br/>
    </center>
</div>


<div class="page-title">
    <center>
        <h3 style="padding-top: 10px;">Add Ticket  جميع طلبات الحدث </h3>
        {{--<br/>--}}
        {{--<span>Translate between 103 languages by typing</span>--}}
        {{--<br/>--}}
        {{--<span>ترجم بين 103 لغة عن طريق الكتابة</span>--}}
        {{--<br/>--}}
    </center>
</div>

<?php
use Ticketing\Models\Issued_ticket;$vat = isset($shared_site_vars['settings']) && $shared_site_vars['settings']['vat_value'] ? $shared_site_vars['settings']['vat_value'] : 0;
?>

{!! Form::open(array('route' => ['admin.issued_tickets.store',$eventid], 'role'=>'form', 'id'=>'form_create_ticket')) !!}
<div class="row">
    <div class="col-md-12">
        <div class="grid simple" style="margin-bottom: 0 !important;">
            <div class="grid-body no-border"> <br>
                <div class="panel-group" id="accordion">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-4">
                            <div class="col-md-8">
                                <div style="border: 1px gainsboro">
                                {{--<div class="form-group" style="float: left">--}}
                                    {{--<div class="text-right">--}}
                                        {{--<label class="form-label" style="margin-right: 0px;"><span style="float: left">Price</span> <br> العنوان إنجليش </label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="col-md-1" style="padding-left: 29px;">
                                    <div class="form-group" style="float: left">
                                    <input type="radio" name="price_type" value="1">
                                    </div>
                                </div>

                                <div class="form-group" style="float: left">
                                    <div class="text-right">
                                        <label class="form-label">
                                            <span style="float: left">Price Ticket</span> <br> تسجل ايراد </label>
                                    </div>

                                </div>

                                <div class="col-md-1" style="padding-left: 29px;">
                                    <div class="form-group" style="float: left">
                                        <input type="radio" name="price_type"  value="2" checked>
                                    </div>
                                </div>

                                <div class="form-group" style="float: left">
                                    <div class="text-right" style="float: left">
                                        <label class="form-label">
                                            <span style="float: left">Gift Ticket</span> <br> تسجل مجاني </label>
                                    </div>
                                </div>

                                </div>



                                <div class="col-md-1" style="padding-left: 29px;">
                                    <div class="form-group" style="float: left">
                                        <input type="checkbox" name="tag_as_vip" value="0">
                                    </div>
                                </div>

                                <div class="form-group" style="float: left">
                                    <div class="text-right" style="float: left">
                                        <label class="form-label">
                                            <span style="float: left">Tag as VIP</span> </label>
                                    </div>
                                </div>







                            </div>
                        </div>
                        </div>
                    <br/>
                    <br/>





                    <a href="{{ URL::to('admin/issued_tickets/'.$event->id) }}" class="btn btn-default" >Cancel إلغاء</a>
                    <input class="btn btn-primary btn-cons form-submit pull-right" style="float:right" value="Save حفظ" type="submit">
                        <br/>
                    <br/>
                         <div class="panel panel-default" style="padding-top: 5px">
                        <div class="panel-heading" >
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Basic Information<span class="heading_arabic">العنوان إنجليش</span></a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }}">
                                            <div class="col-md-4 text-right">
                                                <label class="form-label">Full Name <br> العنوان إنجليش <span style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8">
                                                {!! Form::text('full_name', null , ['class' => 'form-control', 'id'=>'title_english']) !!}
                                                <span class="help-block"><span id="char_title_english">60</span> <?php echo 'characters left'; ?></span>

                                            </div>
                                            <span class="alertrequired">{!!$errors->first('full_name')!!}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="col-md-4 text-right">
                                                <label class="form-label">Email <br> العنوان أرابيك <span style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8">
                                                {!! Form::text('email', null  , ['class' => 'form-control', 'id'=>'email_english']) !!}
                                                <span class="help-block"><span id="char_email_english">60</span> <?php echo 'characters left'; ?></span>

                                            </div>
                                            <span class="alertrequired">{!!$errors->first('email')!!}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
                                            <div class="col-md-4 text-right">
                                                <label class="form-label">Mobile No <br> العنوان أرابيك <span style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8">
                                                {!! Form::text('mobile', null , ['class' => 'form-control', 'id'=>'mobile_num','onkeypress'=>'return isNumberKey(event)',]) !!}
                                                <span class="help-block"><span id="char_mobile_english">15</span> <?php echo 'characters left'; ?></span>

                                            </div>
                                            <span class="alertrequired">{!!$errors->first('mobile')!!}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="current_vat" value="{{$vat}}">

<div class="row">
    <div class="col-md-12">
        <div class="grid simple" >
            {{--<div class="grid-title no-border">--}}
                {{--<h4> <span class="semi-bold"></span></h4>--}}
            {{--</div>--}}
            <div class="grid-body no-border"> <br>
                {{--@if($errors->any())--}}
                    {{--<div class="row">--}}
                        {{--<div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">--}}
                            {{--<button data-dismiss="alert" class="close"></button>--}}
                            {{--@foreach($errors->all() as $error)--}}
                                {{--<p>{{ $error }}</p>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endif--}}

                <div class="panel-group" id="accordion2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2">Ticket Information<span class="heading_arabic">العنوان إنجليش</span></a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class='control' style="font-size: 18px">
                                            <div class="nav-navbar-center">
                                                <ul class="nav nav-pills">
                                                    <li style="margin-right: 60px">
                                                        <div class="row">
                                                        Total Tickets حضرها
                                                        </div>
                                                        <div class="row" style="padding-top: 6px;">
                                                        <span style="font-size: 24px; color: black;" >
                                                            <strong id="total_qty_all_selected_tickets">0</strong>
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li >
                                                        <div class="row"> Total Price مجموع</div>
                                                        <div class="row" style="padding-top: 6px;">
                                                            <span style="font-size: 24px;font-weight: bold">SR</span>
                                                            &emsp;<span style="font-size: 24px; color: black" >
                                                            <strong id="total_price_all_selected_tickets">0.00</strong>
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li style="margin-left: 60px">
                                                        <div class="row">  Total VAT تم البيع </div>
                                                        <div class="row" style="padding-top: 6px;">
                                                        <span style="font-size: 24px;font-weight: bold">SR</span>&emsp;<span style="font-size: 24px; color: black" ><strong id="total_vat_price_all_selected_tickets">0.00</strong></span>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>

                          @if(count($tiers)>0)
                              <?php $num=0 ?>
                            @foreach ($tiers as $tier)
                                <div class="grid simple">
                                    {{--<div class="grid-title" style="background-color:#{{$tl->header_color}};color:white;">--}}
                                        <div class="panel-heading" style="background-color:#{{$tier->header_color}};color:white;">
                                            <h4 class="panel-title" style="color: white">
                                                <?php
                                                 $adminTicketCount = Issued_ticket::getTotalAdminIssuedSumNew(3,$tier->event_id,$tier->id);
//                                                $adminTicketCount = Issued_ticket::getTotalAdminIssuedSum(3,$tier->event_id,2);
                                                ?>
                                                <strong><span style="color: white">{{$tier->title_english }}<span class="heading_arabic">{{$tier->title_arabic }}</span></span></strong> <div style="float: right"> <span style="color: white">Available<span class="heading_arabic">لعنوان أرابيك</span></span>  <span style="color: white"><span class="heading_arabic" STYLE="float: right"><strong>@if($tier->total_quantity_type){{$tier->total_quantity - ($tier->totaAvailable() + $adminTicketCount) }}@else Open @endif</strong></span></span></div>
                                            </h4>
                                        </div>


                                        {{--<div><h4><span class="semi-bold" style="color: white">{{$tl->title_english }}<span class="heading_arabic">{{$tl->title_arabic }}</span></span></h4></div>--}}
                                     {{--</div>--}}
                                    <div class="grid-body sub-section">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('total_qty.'.$num) ? ' has-error' : '' }}">
                                                    <div class="col-md-4 text-right">
                                                        <label class="form-label">Qty <br> العنوان إنجليش </label>
                                                    </div>
                                                    <div class="input-with-icon  right col-md-6 col-sm-12">
                                                        <input type="hidden" name="tier_id[]" value="{{$tier->id }}" >
                                                        <input type="hidden" id="price{{$tier->id }}" value="@if($tier->price_type!=0){{$tier->price }}@else {{'0'}} @endif" >
                                                        <?php
                                                        $available=999999;
                                                        if($tier->total_quantity_type!=0){
                                                            $available =$tier->total_quantity - ($tier->totaAvailable() + $adminTicketCount);
                                                        }
                                                        ?>
                                                        {!! Form::text("total_qty[$num]", 0 , ['class' => 'form-control total_quantity_by_tier','id'=>"total_qty{{$tier->id}}", 'data-tierid'=>"{{$tier->id}}" ,'data-max'=>"$available" , 'onkeypress'=>'return isNumberKey(event)', 'onkeyup'=>'return isNumberKeyNotZero(event)', "style"=>"text-align:center"]) !!}
                                                        <span class="alertrequired">{!!$errors->first('total_qty.'.$num)!!}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-4 text-right">
                                                        <label class="form-label">Price/Ticket <br> العنوان أرابيك </label>
                                                    </div>
                                                    <?php
                                                        $perTicketPrice='';
                                                          if($tier->price_type!=0){
                                                            $perTicketPrice=$tier->price;
                                                           }else{
                                                           echo  $perTicketPrice='Free';
                                                            }
                                                        ?>

                                                    <div class="input-with-icon  right col-md-6 col-sm-12">
                                                        {!! Form::text("yuyuyy[$num]", $perTicketPrice , ['class' => 'form-control','readonly'=>true, "style"=>"text-align:center"]) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-4 text-right">
                                                        <label class="form-label">Total <br> العنوان أرابيك </label>
                                                    </div>
                                                    <div class="input-with-icon  right col-md-6 col-sm-12">

                                                        <input name="" value="0.00" id="temp_total{{$tier->id}}" class="form-control" style="text-align:center" readonly>

                                                        <input type="hidden" name="total_price[<?php echo $num?>]" value="0.00" id="total{{$tier->id}}" class="form-control total_amount_by_tier" style="text-align:center" readonly>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $num++ ?>
                            @endforeach
                             @endif


                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <a href="{{ URL::to('admin/issued_tickets/'.$event->id) }}" class="btn btn-default">Cancel إلغاء</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-primary form-submit" style="float: right;">Save</a> -->
                        <input class="btn btn-primary btn-cons form-submit" style="float:right" value="Save حفظ" type="submit">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@section('page_script')
@parent

<script>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    jQuery('#form_create_ticket').submit(function (event) {
        var pass = true;
        //some validations

        if (pass == false) {
            return false;
        }
        form = this;
        jQuery('#btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();

        return true;
    });

    // function isNumberKeyNotZero(evt)
    // {
    //     s=evt.target.value;
    //     v=Number(s);
    //     evt.target.value=v;

    //     var current_vat = parseInt('{{$vat}}');
    //     var tierid=$(evt.currentTarget).data('tierid').replace(/[^0-9]/g, '');
    //     var price= $('#price'+tierid).val();
    //     total_amount_per_tier= v * price;

    //     $('#total'+tierid).val(total_amount_per_tier);

    //     temp_vat_value=current_vat/100 * total_amount_per_tier;
    //     $('#temp_total'+tierid).val(total_amount_per_tier + temp_vat_value);

    //     var total_amount_by_tier=0;
    //     $( ".total_amount_by_tier" ).each(function( index , value ) {
    //         total_amount_by_tier=Number(total_amount_by_tier) + Number($( this ).val());
    //     });

    //     var total_quantity_by_tier=0;
    //     $( ".total_quantity_by_tier" ).each(function( index , value ) {
    //         total_quantity_by_tier=Number(total_quantity_by_tier) + Number($( this ).val());
    //     });


    //     vat_value=current_vat/100 * total_amount_by_tier;

    //     total_amount_by_tier=total_amount_by_tier + vat_value;


    //     $('#total_qty_all_selected_tickets').html(total_quantity_by_tier);
    //     $('#total_price_all_selected_tickets').html(Number(total_amount_by_tier).toFixed(2));
    //     $('#total_vat_price_all_selected_tickets').html(Number(vat_value).toFixed(2));







    //     //  alert(dd);



    //    // alert(tierid);

    // }

    function isNumberKeyNotZero(evt)
    {
        s=evt.target.value;
        v=Number(s);
        evt.target.value=v;
        var temp_vat = 0;
        var current_vat = parseInt('{{$vat}}');
        var tierid=$(evt.currentTarget).data('tierid').replace(/[^0-9]/g, '');
        var price= $('#price'+tierid).val();
        total_amount_per_tier= v * price;

        $('#total'+tierid).val(total_amount_per_tier);

        temp_vat_value=current_vat/100 * total_amount_per_tier;
        $('#temp_total'+tierid).val(Number(total_amount_per_tier - temp_vat_value).toFixed(2));

        var total_amount_by_tier=0;
        $( ".total_amount_by_tier" ).each(function( index , value ) {
            total_amount_by_tier=Number(total_amount_by_tier) + Number($( this ).val());
            temp_vat = temp_vat + (current_vat/100 * Number($( this ).val()));
        });

        var total_quantity_by_tier=0;
        $( ".total_quantity_by_tier" ).each(function( index , value ) {
            total_quantity_by_tier=Number(total_quantity_by_tier) + Number($( this ).val());
        });

        vat_value=current_vat/100 * total_amount_by_tier;
        total_amount_by_tier = total_amount_by_tier - vat_value;
        vat_value=current_vat/100 * total_amount_by_tier;
        vat_value = Math.ceil(vat_value);
        total_amount_by_tier = total_amount_by_tier + temp_vat;

                
        // console.log(vat_value,total_amount_by_tier);

        $('#total_qty_all_selected_tickets').html(total_quantity_by_tier);
        $('#total_price_all_selected_tickets').html(Number(total_amount_by_tier).toFixed(2));
        $('#total_vat_price_all_selected_tickets').html(Number(temp_vat).toFixed(2));
    }

    $(document).ready(function () {

        $("#title_english").keyup(function (event) {
            updateFeatureCharLimitNum('title_english', 'char_title_english', 40);
        });
        $("#mobile_num").keyup(function (event) {
            updateFeatureCharLimitNum('mobile_num', 'char_mobile_english', 15);
        });
        $("#email_english").keyup(function (event) {
            updateFeatureCharLimitNum('email_english', 'char_email_english', 60);
        });
        updateFeatureCharLimitNum('title_english', 'char_title_english', 40);
        updateFeatureCharLimitNum('mobile_num', 'char_mobile_english', 15);
        updateFeatureCharLimitNum('email_english', 'char_email_english', 60);


        $('.total_quantity_by_tier').on('input', function () {

            var max = $(this).data('max');

            var value = $(this).val();

            if ((value !== '') && (value.indexOf('.') === -1)) {

                $(this).val(Math.max(Math.min(value, max), -max));
            }
        });

    });

</script>
@endsection