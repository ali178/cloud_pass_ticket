
<div class="row">
	<div class="col-md-10">
		<div class="event_listing_info" >
		    <div class="col-md-12 info-basic">
		         <span class="right-space">{{$twelve_digit_id}}</span> <span class="right-space">{{$created_at}}</span>
		    </div>
			<div class="col-md-12 info-basic">
				<span class="right-space">{{$customer_full_name}}</span>
			</div>
			<div class="col-md-12 info-basic">
				<span class="right-space">{{$customer_mobile}}</span> <span class="right-space">{{$customer_email}}</span>
			</div>

			<?php
			// echo 'pre>';
			// print_r($tiers);
             //  use App\Http\Helpers\Custom;
			?>

			@if(count($tiers)>0)
			@foreach (array_chunk($tiers,3) as $chunked_tiers)
				<div>
					@foreach ($chunked_tiers as $tier)

						{{--{{ $tier['tier_id']}}--}}
					   {{--{{'dddd'}}--}}
					{{--die;--}}

						<?php
					   $num=0;
                        $tl=$event_request->getTierColorByTierId($tier['tier_id']);



                       // $tl=\App\Http\Helpers\Custom::getTierColorByTierId($tier['tier_id']);
                       // print_r($tl);
						?>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							<div class="tier_tiles">
								<div style="margin-right:-18px;">
									<div class="col-sm-4 tile-stats-div" style="font-size: 20px; padding-top: 13px;padding-left: 4px;">
				                        <span class="tile-stats" >
				                            <span class="tile-margin-right" style="font-size: 20px;">R <strong> {{$tier['total_qty']}}</strong></span>
				                        </span>

									</div>
									<div class="col-sm-7 tile-border-left" style="background-color:#{{$tl->header_color}};color:white;">
										<center>
											<span class="tile-title" style="display: block;direction: rtl;">{{$tl->title_arabic }}</span>
										</center>
									</div>
								</div>
							</div>
						</div>
					@endforeach
					<br><br><br>
				</div>
			@endforeach
			@endif

		</div>
	</div>


</div>
