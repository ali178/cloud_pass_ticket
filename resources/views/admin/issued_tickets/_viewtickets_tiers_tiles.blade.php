<div class="row">
    <div class="col-md-10 col-sm-12">
        <div class="event_listing_info">
            <div class="col-md-12 info-basic">
                @if(Route::currentRouteName() == 'issued_tickets.cancelledticket')
                    <span class="right-space"
                          style="color: black;font-size: 16px;font-weight: 700">{{$twelve_digit_id}}</span><span
                            class="right-space">{{$created_at}}</span>
                @else
                    <a style="color: black;font-size: 16px;font-weight: 700"
                       href="{{ URL::route('admin.issued_tickets.viewissuedticket',['id'=>$issued_ticket->id, 'bsource'=>$bsource]) }}"><span
                                class="right-space"
                                style="color: black;font-size: 16px;font-weight: 700">{{$twelve_digit_id}}</span></a>
                    <span class="right-space">{{$created_at}}</span>
                @endif
            </div>
            <div class="col-md-12 info-basic">
                <span class="right-space">{{$customer_full_name}}</span>
            </div>
            <div class="col-md-12 info-basic">
                <span class="right-space">{{$customer_mobile}}</span> <span
                        class="right-space">{{$customer_email}}</span>
                @if($issued_ticket->tag == 1)
                    <span class="right-space" style="color:red;font-weight: bold">VIP</span>
                @endif
            </div>

            <?php
            // echo 'pre>';
            // print_r($tiers);
            //  use App\Http\Helpers\Custom;
            ?>

            @if(count($tiers)>0)
                @foreach (array_chunk($tiers,3) as $chunked_tiers)
                    <div>
                        @foreach ($chunked_tiers as $tier)

                            {{--{{ $tier['tier_id']}}--}}
                            {{--{{'dddd'}}--}}
                            {{--die;--}}

                            <?php
                            $num = 0;
                            $tl = $issued_ticket->getTierColorByTierId($tier['tier_id']);


                            // $tl=\App\Http\Helpers\Custom::getTierColorByTierId($tier['tier_id']);
                            // print_r($tl);
                            ?>
                            @if($tl)

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-bottom: 5px">
                                    <div class="tier_tiles">
                                        <div style="margin-right:-18px;">

                                        <div class="col-sm-4 tile-stats-div"
                                             style="padding-top: 5px;padding-left: 4px;font-size: 20px">
				                                    <span class="tile-stats" style="font-size: 16px">
				                                        R <strong> {{$tier['total_qty']}}</strong>
                                                    </span>

                                        </div>
                                        <div class="col-sm-8 tile-border-left"
                                             style="background-color:#{{$tl->header_color}};color:white;height: 42px;margin-left: -18px">
                                            <center>
                                                <span class="tile-title" style="display: block;direction: rtl;">{{$tl->title_arabic }}</span>
                                            </center>
                                        </div>

                                        </div>

                                    </div>
                                </div>


                            @endif

                        @endforeach
                        <br><br><br>
                    </div>
                @endforeach
            @endif

        </div>
    </div>


</div>
