@extends('layouts.app')
@section('content')
<style type="text/css">
    table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }

    .nav-navbar-center {
        margin-top: 0px;
    }
</style>
{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
<input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!}
<div class="page-title" style="margin-top: -33px ">
    <center>
{{--        @if($event->eventStatusById($event->id))--}}
        @if($event->status == 1 && $event->selling_off == 0)
            <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text" style="font-weight: bold;">{{$event->event_id_str}}</span></span>
        @else
            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text" style="font-weight: bold;">{{$event->event_id_str}}</span></span>
        @endif
        <br/>
        <span style="font-size: 16px;margin-top: 5px;display: inline-block;font-weight: bold;">{{$event->title_arabic}}</span>
        <br/>
        <span style="font-size: 16px;margin-bottom: 16px;display: inline-block;font-weight: bold;">{{$event->title_english}}</span>
        <br/>
    </center>
</div>


<div class="page-title">
    <center>
        <h3>Tickets  التذاكر</h3>
        {{--<br/>--}}
        {{--<span>Translate between 103 languages by typing</span>--}}
        {{--<br/>--}}
        {{--<span>ترجم بين 103 لغة عن طريق الكتابة</span>--}}
        {{--<br/>--}}
    </center>
</div>

<div style="font-size: 18px;background-color: white">
    <div class="nav-navbar-center">
        <ul class="nav nav-pills" style="margin-top: 15px">
            <li style="margin-right: 20px"><span style="padding-bottom: 10px;display: inline-block;">Total Sold مباعة </span><br/><span style="font-size: 28px; color: black" >{{$issued_tiers_qty_sum}}</span> </li>
            <li style="margin-left: 20px"><span style="padding-bottom: 10px;display: inline-block;">Total Available متاحة</span><br/><span style="font-size: 28px; color: black" >{{$available_qty}}</span></li>
        </ul>
    </div>
</div>
<br>


<div class="tab-content">

    <div class='control pull-left' style="margin-top: 20px;margin-bottom: 0;margin-left: 20px">
        <div class="nav-navbar-center">
            <ul class="nav nav-pills">
                <li id="issued_tab" style="margin-right: 7px"><a href="#issued_event_requests" data-toggle="tab">Issued <span class="heading_arabic" style="margin-left: 10px">مصدرة</span> <span class="heading_arabic" style="display:inline-block;margin-left: 10px">({{$issued_count}})</span></a></li>
                <li id="cancelled_tab" style="margin-right: 7px"><a href="#cancelled_event_requests " data-toggle="tab">Cancelled <span class="heading_arabic" style="margin-left: 10px">ملغاة</span> <span class="heading_arabic" style="display: inline-block;margin-left: 10px">({{$cancelled_count}})</span></a></li>
            </ul>
        </div>
    </div>

    <div id="issued_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-body no-border">
                <div class="pull-left markaction">
                    @if(Auth::user()->can("issuedtickets-create"))
                        <a href="{{ URL::to('admin/issued_tickets/create/'.$event->id) }}" class="btn btn-primary">+ Issue Tickets صرف تذاكر</a>
                    @endif
                </div>
                <div class="pull-right markaction">
                    @if(Auth::user()->can("issuedtickets-reject"))
                        <a id="reject" href="javascript:void(0)" class="btn btn-reject" data-url="{{ URL::to('admin/issued_tickets/bulkreject') }}">Cancel إالغاء</a>
                    @endif

                </div>
                <br><br><br>
                <table class="table table-hover table-condensed" id="datatable-example" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                            <th>Id</th>
                            <th><center>معلومات التذاكر <br> Ticket Information</center></th>
                            <th><center>إجمالي المبلغ <br> Total (SR)</center></th>
                            <th><center>المصدر <br> Source</center></th>
                            <th width="42px"><center>اشارة <br> Tag</center></th>
                            <th><center>آخر تحديث <br> Last Edit</center></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="cancelled_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-body no-border">
                {{--<div class="pull-right markaction">--}}
                    {{--@if(Auth::user()->can("issuedtickets-destroy"))--}}
                        {{--<a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/issued_tickets/bulkissuedticketsdelete') }}">Delete حذف</a>--}}
                    {{--@endif--}}
                {{--</div><br><br><br>--}}

                <table class="table table-hover table-condensed" id="datatable-example2" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                            <th>Id</th>

                            <th><center>معلومات التذاكر <br> Ticket Information</center></th>
                            <th><center>إجمالي المبلغ <br> Total (SR)</center></th>
                            <th><center>المصدر <br> Source</center></th>
                            <th width="42px"><center>اشارة <br> Tag</center></th>
                            <th><center>آخر تحديث <br> Last Edit</center></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="rejectBulkMessage" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cancellation Reason<br/>سبب الالغاء</h4>
            </div>
            {!! Form::open(array('route' =>["admin.issued_tickets.reject_status", 5], 'id'=>'bulkRejectionForm', 'role'=>'form', 'novalidate' ,'onsubmit'=>"$('#progressbar').show()")) !!}
            <input type="hidden" value="" name="ids" id="rejectids" />
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">Reason<br><span class="arabic_font">السبب</span><span style="color:red">*</span></label>
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">
                        {!! Form::select('reason_id',[''=>'--- SELECT ---'],null, ['class' => 'form-control','id'=>'bulk_reason_id','placeholder'=>"-- SELECT --"]) !!}
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description Arabic &emsp; <span class="arabic_font">التفاصيل بالعربي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_arabic', null, ['class' => 'form-control','id'=>'bulk_description_arabic','rows'=>3,'style'=>'direction: rtl']) !!}
                            <span class="help-block"><span id="bulk_char_description_arabic">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description English &emsp; <span class="arabic_font">التفاصيل بالانجليزي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'bulk_description_english','rows'=>3]) !!}
                            <span class="help-block"><span id="bulk_char_description_english">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel إالغاء</button>

                    {!! Form::submit('Send إرسال', ['class' => 'btn btn-green tier-form-submit','id' => 'add_tier_btn']) !!}
                </center>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>

<!-- maps modal -->
<div class="modal fade" id="rejectionOnReasonModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cancellation Reason<br/>سبب الالغاء</h4>
            </div>
            {!! Form::open(array('route' =>["admin.issued_tickets.reject_status", 5], 'id'=>'addRejectionForm', 'role'=>'form', 'novalidate' ,'onsubmit'=>"$('#progressbar').show()")) !!}

            <input type="hidden" name="issued_ticket_id" id="issued_ticket_id" value="">

            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">Reason<br><span class="arabic_font">السبب</span> <span style="color:red">*</span></label>
                    </div>
                    <div class="input-with-icon  right col-md-6 col-sm-12">
                        {!! Form::select('reason_id',[],null, ['class' => 'form-control','id'=>'reason_id' , 'required'=>true]) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description Arabic &emsp; <span class="arabic_font">التفاصيل بالعربي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_arabic', null, ['class' => 'form-control','id'=>'description_arabic','rows'=>3,'style'=>'direction: rtl']) !!}
                            <span class="help-block"><span id="char_description_arabic">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Description English &emsp; <span class="arabic_font">التفاصيل بالانجليزي</span> <span style="color:red"></span></label>
                            {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'description_english','rows'=>3]) !!}
                            <span class="help-block"><span id="char_description_english">250</span> <?php echo 'characters left الحروف المتبقية'; ?></span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel إالغاء</button>

                    {!! Form::submit('Send إرسال', ['class' => 'btn btn-green tier-form-submit','id' => 'add_tier_btn']) !!}
                </center>
            </div>
            {{--{!! Form::close() !!}--}}
        </div>

    </div>
</div>



@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {
        var event_type = '{{Request::get("source")}}';
        if(event_type == 'issued'){
            $( "#issued_tab" ).addClass( "active" );
            $( "#issued_event_requests" ).addClass( "active in" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'cancelled'){
            $( "#cancelled_tab" ).addClass( "active" );
            $( "#cancelled_event_requests" ).addClass( "active in" );
            $( "#issued_tab" ).removeClass( "active" );
        } else{
            $( "#issued_tab" ).addClass( "active" );
            $( "#issued_event_requests" ).addClass( "active in" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }


        $('#issued_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.issued_tickets',$event->id)}}?source=issued");
        });
        $('#cancelled_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.issued_tickets',$event->id)}}?source=cancelled");
        });

        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/issued_tickets/datatable') }}",
                "type": "POST",
                "data": {
                    'eventid':'{{$eventid}}',
                    'type':3
                     }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'type', name: 'type'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6,7],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4,5,6]
                }
            ]
        });

        $('#datatable-example2').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/issued_tickets/datatable') }}",
                "type": "POST",
                "data": {
                         'eventid':'{{$eventid}}',
                         'type':4
                    }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'type', name: 'type'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6,7],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4,5,6]
                }
            ]
        });
    });


    $(document).on('submit', '.rejectaction', function() {
        // var ret = confirm("Are you sure you want to delete?");
        // if (ret) {
        // $("#progressbar").show();
        // return true;
        // }
        // return false;
        $("#progressbar").show();
        return false;
    });
    // var form = '';
    $(document).on('click', '.rejectaction', function() {
        issuedticketid = $(this).attr('data-issuedticketid');
        urlrequest = $(this).attr('data-url');

        $.ajax({
            dataType: 'JSON',
            url: base_path+"/admin/issued_tickets/getreasonbytype",
            type: 'POST',
            data: {
                // _token: $('input[name=_token]').val(),
                issuedticketid:issuedticketid,
            },
            beforeSend: function () {
                // overlay_ajax();
            },
            success: function (data) {
                // alert(data.reasonlist);
                $('#addRejectionForm').attr('action',urlrequest);//'http://localhost/ticketing/public/admin/issued_tickets/'+issuedticketid+'/reject_status'
                $('#issued_ticket_id').val('');
                $('#char_description_english,#char_description_arabic').text(250);
               // $('#addRejectionForm')[0].reset();
                $('#rejectionOnReasonModal').modal('show');
                $('#reason_id').html(data.reasonlist);
                $('#issued_ticket_id').val(issuedticketid);
                $("#progressbar").hide();


            },
            error: function () {
                // $('#modal-default').modal('show');
            },
            complete: function () {
                // $('.box-body').unblock();
            }
        });

        // return false;
    });


    $(document).ready(function(){
        $("#description_english").keyup(function (event) {
            updateFeatureCharLimitNum('description_english','char_description_english',250);
        });

        $("#description_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
        });

        $("#bulk_description_english").keyup(function (event) {
            updateFeatureCharLimitNum('bulk_description_english','bulk_char_description_english',250);
        });

        $("#bulk_description_arabic").keyup(function (event) {
            updateFeatureCharLimitNum('bulk_description_arabic','bulk_char_description_arabic',250);
        });

        updateFeatureCharLimitNum('description_english','char_description_english',250);
        updateFeatureCharLimitNum('description_arabic','char_description_arabic',250);
        updateFeatureCharLimitNum('bulk_description_english','bulk_char_description_english',250);
        updateFeatureCharLimitNum('bulk_description_arabic','bulk_char_description_arabic',250);
    });

    var errorMessageStr = 'Nothing Selected';
    var rejmsg = "Are you sure you want to reject ?";
    $(document).on('click', '.markaction #reject', function (e) {
        // alert('4')
        e.preventDefault();
        // $("#progressbar").show();

        bulkActionReject(this,'',true);
    });

    function bulkActionReject(element,count,confirm_reject){
        url = $(element).attr('data-url');
        var selectedCheckboxItems = $('#datatable-example'+count+' tbody :checkbox:checked').map(function () {
            return this.value;
        }).get();

        if (selectedCheckboxItems.length > 0) {
            selectedItem = selectedCheckboxItems.join(',');
            selectedItems = selectedItem.replace('on,', '');
            delMsg = '';

            if (confirm_reject) {
                delMsg = $(element).attr('data-msg');

                if (delMsg) {
                    rejectBulkModal(delMsg);
                } else {
                    rejectBulkModal(rejmsg);
                }
            }else{

                $("#progressbar").show();
                $('#rejectids').attr('value', selectedItems);
                $('#bulkaction').attr('action', url);
                $('#bulkaction').submit();
            }

        }
        else {
            showErrorMessage(errorMessageStr);
        }

    }

    function rejectBulkModal(message, action) {
        //  $('#rejectBulkMessage .message-content').text(message);

        $.ajax({
            dataType: 'JSON',
            url: base_path+"/admin/issued_tickets/getreasonbytype",
            type: 'POST',
            data: {
                // _token: $('input[name=_token]').val(),
            },
            beforeSend: function () {
                // overlay_ajax();
            },
            success: function (data) {
                // alert(data.reasonlist);
                $('#rejectids').attr('value', selectedItems);
                $('#bulkRejectionForm').attr('action', url);
                $('#char_description_english,#char_description_arabic').text(250);
                // $('#bulkRejectionForm')[0].reset();
                $('#bulk_reason_id').html(data.reasonlist);
                $('#rejectBulkMessage').modal('show');

                $('#bulk_description_english').val('');
                $('#bulk_description_arabic').val('');

                $("#progressbar").hide();


            },
            error: function () {
                // $('#modal-default').modal('show');
            },
            complete: function () {
                // $('.box-body').unblock();
            }
        });


    }
    $("#bulk_reason_id").change(function(){
        bulk_reason_id=$("#bulk_reason_id").val();

        if(bulk_reason_id!=''){
            $.ajax({
                dataType: 'JSON',
                url: base_path+"/admin/issued_tickets/getdescriptionbyreason",
                type: 'POST',
                data: {
                    'reasonid':bulk_reason_id
                    // _token: $('input[name=_token]').val(),
                },
                beforeSend: function () {
                    // overlay_ajax();
                },
                success: function (data) {

                    $('#bulk_char_description_arabic').text(250 - data.description_arabic.length);
                    $('#bulk_char_description_english').text(250 - data.description_english.length);
                    // $('#bulkRejectionForm')[0].reset();
                    $('#bulk_description_english').val(data.description_english);
                    $('#bulk_description_arabic').val(data.description_arabic);

                    // $('#bulkRejectionForm')[0].reset();
                },
                error: function () {
                    // $('#modal-default').modal('show');
                },
                complete: function () {
                    // $('.box-body').unblock();
                }
            });
        }else{
            $('#bulkRejectionForm')[0].reset();
        }
    });
    $("#reason_id").change(function(){
        reason_id=$("#reason_id").val();
        if(reason_id!=''){
            $.ajax({
                dataType: 'JSON',
                url: base_path+"/admin/issued_tickets/getdescriptionbyreason",
                type: 'POST',
                data: {
                    'reasonid':reason_id
                    // _token: $('input[name=_token]').val(),
                },
                beforeSend: function () {
                    // overlay_ajax();
                },
                success: function (data) {
                    $('#char_description_arabic').text(250 - data.description_arabic.length);
                    $('#char_description_english').text(250 - data.description_english.length);
                    //$('#addRejectionForm')[0].reset();
                    $('#description_english').val(data.description_english);
                    $('#description_arabic').val(data.description_arabic);
                },
                error: function () {
                    // $('#modal-default').modal('show');
                },
                complete: function () {
                    // $('.box-body').unblock();
                }
            });
        }else{
            $('#addRejectionForm')[0].reset();
        }
    });
</script>

@endsection