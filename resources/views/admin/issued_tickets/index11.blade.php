@extends('layouts.app')
@section('content')
<style type="text/css">
    table#datatable-example4,table#datatable-example3,table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }

    .nav-navbar-center {
        margin-top: 0px;
    }
</style>
<div class="page-title">
    <center>
       @if($event->eventStatusById($event->id))
        <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text">{{$event->event_id_str}}</span></span>
       @else
        <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text">{{$event->event_id_str}}</span></span>
       @endif
        <br/>
        <span>{{$event->title_english}}</span>
        <br/>
        <span>{{$event->title_arabic}}</span>
        <br/>
    </center>
</div>


<div class="page-title">
    <center>
        <h3>All Event Requests جميع طلبات الحدث</h3>
        <br/>
        <span>Foie gras is a luxury food product made of the liver of a duck or goose that has been specially fattened. By French law</span>
        <br/>
        <span>هو منتج غذائي فاخر مصنوع من كبد البط أو الأوز الذي تم تسمينه بشكل خاص. بموجب القانون الفرنسي</span>
        <br>
    </center>
</div>

<div class='control' style="font-size: 18px">
    <div class="nav-navbar-center">
        <ul class="nav nav-pills">
            <li style="margin-right: 20px">Total Ordered حضرها<br/><span style="font-size: 24px; color: #0078d7" >{{$preordered_count}}</span> </li>
            <li >Total Pending مجموع<br/><span style="font-size: 24px; color: #f35958" >{{$pending_count}}</span></li>
            <li style="margin-left: 20px">Total Issued مجموع<br/><span style="font-size: 24px; color: #f35958" >{{$issued_count}}</span></li>
            <li style="margin-left: 20px">Total Available تم البيع<br/><span style="font-size: 24px; color: #6b6b6c" >{{ $available_qty }}</span></li>
        </ul>
    </div>
</div>


<br>

<div class="tab-content">

    <div class='control pull-left'>
        <div class="nav-navbar-center">
            <ul class="nav nav-pills">
                <li id="preordered_tab"><a href="#preodered_event_requests" data-toggle="tab">Ordered ({{$preordered_count}})</a></li>
                <li id="pending_tab"><a href="#pending_event_requests " data-toggle="tab">Pending ({{$pending_count}})</a></li>
                <li id="issued_tab"><a href="#issued_event_requests" data-toggle="tab">Issued ({{$issued_count}})</a></li>
                <li id="cancelled_tab"><a href="#cancelled_event_requests " data-toggle="tab">Cancelled ({{$cancelled_count}})</a></li>

            </ul>
        </div>
    </div>


    <div id="preodered_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-right markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkreject') }}">Reject</a>
                    @endif
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete</a>
                    @endif
                </div>
                <br><br><br>
                <table class="table table-hover table-condensed" id="datatable-example" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                            <th>Id</th>
                            <th>عنوان <br> Information</th>
                            <th>شعار <br> Total(SR)</th>
                            <th>نشرت <br> Status</th>
                            <th>آخر تحديث <br> Last Edit</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="pending_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-right markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkreject') }}">Reject</a>
                    @endif
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete</a>
                    @endif
                </div><br><br><br>

                <table class="table table-hover table-condensed" id="datatable-example2" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                            <th>Id</th>

                            <th>عنوان <br> Information</th>
                            <th>شعار <br> Total(SR)</th>
                            <th>نشرت <br> Status</th>
                            <th>آخر تحديث <br> Last Edit</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div id="issued_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-right markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete</a>
                    @endif
                </div><br><br><br>

                <table class="table table-hover table-condensed" id="datatable-example3" >
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="group-checkable" data-set="#datatable-example3 .checkboxes" /></th>
                        <th>Id</th>

                        <th>عنوان <br> Information</th>
                        <th>شعار <br> Total(SR)</th>
                        <th>نشرت <br> Status</th>
                        <th>آخر تحديث <br> Last Edit</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div id="cancelled_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <div class="pull-right markaction">
                    @if(Auth::user()->can("events-destroy"))
                        <a id="del2" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/event_requests/bulkdelete') }}">Delete</a>
                    @endif
                </div><br><br><br>

                <table class="table table-hover table-condensed" id="datatable-example4" >
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="group-checkable" data-set="#datatable-example4 .checkboxes" /></th>
                        <th>Id</th>

                        <th>عنوان <br> Information</th>
                        <th>شعار <br> Total(SR)</th>
                        <th>نشرت <br> Status</th>
                        <th>آخر تحديث <br> Last Edit</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


</div>
{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
<input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!}
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {
        var event_type = '{{Request::get("source")}}';
        if(event_type == 'preordered'){
            $( "#preordered_tab" ).addClass( "active" );
            $( "#preodered_event_requests" ).addClass( "active in" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'pending'){
            $( "#pending_tab" ).addClass( "active" );
            $( "#pending_event_requests" ).addClass( "active in" );
            $( "#preordered_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'issued'){
            $( "#issued_tab" ).addClass( "active" );
            $( "#issued_event_requests" ).addClass( "active in" );
            $( "#preordered_tab" ).removeClass( "active" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }else if(event_type == 'cancelled'){
            $( "#cancelled_tab" ).addClass( "active" );
            $( "#cancelled_event_requests" ).addClass( "active in" );
            $( "#preordered_tab" ).removeClass( "active" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
        } else{
            $( "#preordered_tab" ).addClass( "active" );
            $( "#preodered_event_requests" ).addClass( "active in" );
            $( "#pending_tab" ).removeClass( "active" );
            $( "#issued_tab" ).removeClass( "active" );
            $( "#cancelled_tab" ).removeClass( "active" );
        }

        $('#preordered_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=preordered");
        });
        $('#pending_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=pending");
        });

        $('#issued_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=issued");
        });
        $('#cancelled_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.event_requests',$event->id)}}?source=cancelled");
        });



        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':1,
                         'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,2,6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false
                }
            ]
        });

        $('#datatable-example2').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':2,
                         'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,2,6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false
                }
            ]
        });


        $('#datatable-example3').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':3,
                         'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,2,6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false
                }
            ]
        });


        $('#datatable-example4').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/event_requests/datatable') }}",
                "type": "POST",
                "data": {
                         'type':4,
                          'eventid':'{{$eventid}}'
                        }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,2,6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6],
                    "orderable": false
                }
            ]
        });



    });





</script>





@endsection