@extends('layouts.app')
@section('content')
<style type="text/css">
    table#datatable-example2,table#datatable-example{
        width: 100% !important;
    }

    .nav-navbar-center {
        margin-top: 0px;
    }
</style>
<div class="page-title">
    <center>
        @if($event->status == 1 && $event->selling_off == 0)
            <span class="legends"><i class="fa fa-circle selling_on_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @else
            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
        @endif
        <br/>
            <span style="font-size: 16px;margin-top: 5px;display: inline-block;font-weight: bold">{{$event->title_arabic}}</span>
            <br/>
            <span style="font-size: 16px;margin-bottom: 5px;display: inline-block;font-weight: bold">{{$event->title_english}}</span>
        <br/>
    </center>
</div>


<div class="page-title">
    <center>
        <h3 style="padding-top: 10px">Cancelled Tickets التذاكر الملغاة</h3>
        {{--<br/>--}}
        {{--<span>Translate between 103 languages by typing</span>--}}
        {{--<br/>--}}
        {{--<span>ترجم بين 103 لغة عن طريق الكتابة</span>--}}
        {{--<br/>--}}
    </center>
</div>


<div style="font-size: 18px; background-color: white">
    <div class="nav-navbar-center">
        <ul class="nav nav-pills" style="margin-top: 15px">
            <li style="margin-right: 20px"><span style="padding-bottom: 10px;display: inline-block;">Total Qty عدد التذاكر</span><br/><span style="font-size: 28px; color: black" >{{$cancelled_tiers_qty_sum}}</span> </li>
            <li style="margin-left: 20px"><span style="padding-bottom: 10px;display: inline-block;">Total Amount Pending اجمالي مبلع التعويض</span><br/><span style="font-size: 28px; color: black" >{{ number_format($total_pending_amount_sum,2) }}</span></li>
        </ul>
    </div>
</div>
<br>

<div class="tab-content">

    <div class='control pull-left' style="margin-top: 20px;margin-left: 20px;margin-bottom: 0">
        <div class="nav-navbar-center">
            <ul class="nav nav-pills">
                <li id="pending_tab" style="margin-right: 7px;"><a href="#pending_event_requests" data-toggle="tab">Pending <span class="heading_arabic" style="margin-left: 5px">تنتظر التعويض</span><span class="heading_arabic" style="display: inline-block;margin-left: 10px">({{$cancelled_count}})</span></a></li>
                <li id="refunded_tab"><a href="#refunded_event_requests " data-toggle="tab">Refunded <span class="heading_arabic" style="margin-left: 5px">تم التعويض</span> <span class="heading_arabic" style="display: inline-block;margin-left: 5px">({{$refunded_count}})</span></a></li>
            </ul>
        </div>
    </div>

    <div id="pending_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <table class="table table-hover table-condensed" id="datatable-example" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                            <th>Id</th>
                            <th><center>معلومات التذاكر <br> Ticket Information</center></th>
                            <th><center>إجمالي المبلغ <br> Total (SR)</center></th>
                            <th><center>المصدر <br> Source</center></th>
                            <th width="42px"><center>اشارة <br> Tag</center></th>
                            <th><center>آخر تحديث <br> Last Edit</center></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="refunded_event_requests" class="tab-pane fade">
        <div class="grid simple vertical green mobile-scroll">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border">
                <table class="table table-hover table-condensed" id="datatable-example2" >
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable" data-set="#datatable-example2 .checkboxes" /></th>
                            <th>Id</th>
                            <th><center>معلومات التذاكر <br> Ticket Information</center></th>
                            <th><center>إجمالي المبلغ <br> Total (SR)</center></th>
                            <th><center>المصدر <br> Source</center></th>
                            <th width="42px"><center>اشارة <br> Tag</center></th>
                            <th><center>آخر تحديث <br> Last Edit</center></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


</div>
{!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
<input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!}
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {
        var event_type = '{{Request::get("source")}}';
        if(event_type == 'pending'){
            $( "#pending_tab" ).addClass( "active" );
            $( "#pending_event_requests" ).addClass( "active in" );
            $( "#refunded_tab" ).removeClass( "active" );
        }else if(event_type == 'refunded'){
            $( "#refunded_tab" ).addClass( "active" );
            $( "#refunded_event_requests" ).addClass( "active in" );
            $( "#pending_tab" ).removeClass( "active" );
        } else{
            $( "#pending_tab" ).addClass( "active" );
            $( "#pending_event_requests" ).addClass( "active in" );
            $( "#refunded_tab" ).removeClass( "active" );
        }


        $('#pending_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.issued_tickets.cancelledticket',$event->id)}}?source=pending");
        });
        $('#refunded_tab').click(function(event){
            history.pushState({}, null, "{{URL::route('admin.issued_tickets.cancelledticket',$event->id)}}?source=refunded");
        });

        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/issued_tickets/datatableCancelledTicket') }}",
                "type": "POST",
                "data": {
                    'eventid':'{{$eventid}}',
                    'type':4
                     }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'type', name: 'type'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6,7],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4,5,6]
                }
            ]
        });

        $('#datatable-example2').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/issued_tickets/datatableCancelledTicket') }}",
                "type": "POST",
                "data": {
                         'eventid':'{{$eventid}}',
                         'type':5
                    }
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'info', name: 'info'},
                {data: 'total_price', name: 'total_price'},
                {data: 'status', name: 'status'},
                {data: 'type', name: 'type'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [0,2,6],
                    "searchable": true
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5, 6,7],
                    "orderable": false,
                    "searchable": true

                },
                {
                    "className": "dt-center",
                    "targets": [3,4, 5,6]
                }
            ]
        });
    });
</script>

@endsection