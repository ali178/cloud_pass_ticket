<div class="row">
    <div class="col-md-10">
        <div class="event_listing_info">
            <div class="col-md-12 info-basic">
                {{--		        @if($event->selling_off == 0)--}}
                @if($event->status == 1 && $event->selling_off == 0)
                    <span class="selling_on_icon right-space"><i class="fa fa-circle"></i></span>
                @else
                    <span class="selling_of_icon right-space"><i class="fa fa-circle-o"></i></span>
                @endif
                <a class="" style="color: #576475" href="{{ URL::route('admin.events.addeditevent',$event->id) }}">
                    <span class="right-space id_font_size bold ">{{$six_digit_id}}</span></a>
                <span class="right-space">{{$dates}}</span>
                <span class="right-space">{{$times}}</span>
            </div>
            <div class="col-md-12 info-basic">
                <a class="" style="color: #576475" href="{{ URL::route('admin.events.addeditevent',$event->id) }}">
                    <div class="info-titles">
                        <span class="right-space title_font_size">{{$event->title_arabic}}</span>
                        <br>
                        <span class="right-space title_font_size">{{$event->title_english}}</span>
                    </div>
                </a>
            </div>

            @foreach (array_chunk($tiers,3) as $chunked_tiers)
                <div>
                    @foreach ($chunked_tiers as $tier)
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-bottom: 5px">
                            <div class="tier_tiles">
                                <div style="margin-right:-23px;">
                                    <?php $tierfunction = \Ticketing\Models\Tier::find($tier['id']) ?>
                                    <div class="col-sm-4 tile-stats-div">
				                        <span class="tile-stats" style="margin-left: -13px">
				                            T {{ $tier['total_quantity_type'] ? $tier['total_quantity'] : 'Open'}}
                                            <hr class="tile_line_break"/>
                                        </span>

                                        <?php $getEventRequestCount = $tierfunction->getEventRequestCount() ?>
                                        <span class="tile-stats" style="margin-left: -13px">
                                            S {{$getIssuedTicketCount=$tierfunction->getIssuedTicketCount()}}
                                            <hr class="tile_line_break"/>
                                        </span>

                                        {{--<span class="tile-margin-right">R <strong class="bold"--}}
                                        {{--style="margin-left: 2px;color: black"> {{ $tier['total_quantity_type'] ? $tier['total_quantity'] - ($getEventRequestCount + $getIssuedTicketCount) : 'Open'}}</strong></span><hr--}}
                                        {{--class="tile_line_break"/>--}}
                                        <span class="tile-stats" style="margin-left: -13px">

                                        {{--R <strong class="bold" style="color: black"> {{ $tier['total_quantity_type'] ? $tier['total_quantity'] - $getIssuedTicketCount=$tierfunction->getIssuedTicketCount() : 'Open'}}</strong></span>--}}
                                        {{--<hr class="tile_line_break"/> --}}

                                        R <strong class="bold" style="color: black"> {{ $tierfunction->totaAvailable() }}</strong></span>
                                        <hr class="tile_line_break"/>

                                        </span>

                                    </div>
                                    <a class="" style="color: white"
                                       href="{{ route("admin.event_requests",$event->id) }}">
                                        <div class="col-sm-8 tile-border-left"
                                             style="background-color:#{{$tier['header_color']}};color:white;margin-left: -23px;height: 42px;">
                                            <center>
                                                <span class="tile-title bold" style="display: block;direction: rtl;">{{$tier['title_arabic']}}</span>
                                            </center>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <br><br><br>
                </div>
            @endforeach
        </div>
    </div>

    <div class="col-md-2" style="text-align: right;">
        <div style="display: inline-block;">
            <span style="margin-right: 10px;font-size: 15px">A: <strong>{{$event->getRedemptionCount()}}</strong> </span>

            @if($event->exclamation_mark())
                <img src="{{asset('assets/images/!.png')}}" style="width: auto;height: 22px; margin-right: 10px;">
            @endif

            @if($event->map_address)

                <a href="javascript:void(0)" data-toggle="modal" data-target="#googleMapModal"
                   onclick="showMapLocation('{{$event->lat}}','{{$event->long}}')"
                   class="{{ $event->map_address ? '' : 'disable_this' }}">
                    <img src="{{asset('assets/images/location-icon-fill.png')}}" style="width: auto;height: 22px;">
                </a>
                {{--<input type="hidden" name="lat" id="lat_input"--}}
                {{--value="{{$event && $event->lat != 0 ? $event->lat : ''}}">--}}

                {{--<input type="hidden" name="long" id="lng_input"--}}
                {{--value="{{$event && $event->long != 0 ? $event->long : ''}}">--}}

            @else
                <img src="{{asset('assets/images/location-icon-empty.png')}}" style="width: auto;height: 19px;">
            @endif
        </div>

    </div>
</div>

