@extends('layouts.app')
@section('content')
<style type="text/css">
    .hide_column{
        display: none;
    }
</style>
    @if($request_type=='Edit')


        <div class="page-title" style="letter-spacing: unset;">
            <div class="row">
                <div class="col-md-2" style="text-align: left">

                    <ul class="nav quick-section ">
                        <li class="quicklinks actions">
                            <a id="gear-icon" class="context_menu_btn" href="#" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-gear"></i>
                            </a>
                            <ul aria-labelledby="user-options" role="menu"
                                class="dropdown-menu left custom-listing-menu" style="left: 40px !important;">

                                @if(Auth::user()->can("eventrequests-index"))

                                    <li><a class="" href="{{ route("admin.event_requests",$event->id) }}">Event Requests طلبات التذاكر</a></li>
                                    <li class="divider"></li>
                                @endif

                                    @if(Auth::user()->can("issuedtickets-index"))
                                        <li><a class="" href="{{ route("admin.issued_tickets",$event->id) }}">View Issued Tickets التذاكر المباعة</a></li>
                                        <li class="divider"></li>
                                    @endif
                                    @if(Auth::user()->can("issuedtickets-create"))

                                        <li><a class="" href="{{route("admin.issued_tickets.create",$event->id)}}">Issue Ticket صرف تذاكر</a></li>
                                        <li class="divider"></li>
                                    @endif
                                    @if(Auth::user()->can("cancelledtickets-index"))
                                        <li><a class="" href="{{route("admin.issued_tickets.cancelledticket",$event->id)}}">View Cancelled Tickets التذاكر الملغاة</a></li>
                                        <li class="divider"></li>

                                    @endif


                                @if(Auth::user()->can("events-edit"))
                                        <li>{!! Form::open(["method" => "POST","route" => ["admin.events.selling", $event->id],"class" => ""]) !!}
                                            <button class=" change_status">{{$event->selling_off ? "Enable Selling تشغيل بيع المناسبة" : "Disable Selling ايقاف بيع المناسبة"}}

                                            </button>{!! Form::close() !!}</li>
                                        <li class="divider"></li>

                                    <li>{!! Form::open(["method" => "POST","route" => ["admin.events.status", $event->id],"class" => "deleteaction", "data-msg"=>"Are you sure you want to change status?"]) !!}
                                        <button class=" change_status">{{$event->status ? "Un-publish ايقاف النشر" : "Publish نشر"}}</button>{!! Form::close() !!}
                                    </li>
                                    <li class="divider"></li>

                                @endif
                                @if(Auth::user()->can("events-destroy"))
                                    <li>{!! Form::open(["method" => "DELETE","route" => ["admin.events.destroy", $event->id],"class" => "inline deleteaction"]) !!}
                                        <button class="delete-label-red">Delete حذف</button>{!! Form::close() !!}</li>
                                @endif


                            </ul>
                        </li>
                    </ul>
                </div>
                {{--                {{dd($event->eventStatusById($event->id))}}--}}
                <div class="col-md-2">

                </div>
                <div class="col-md-4" style="margin-top: 0">
                    <center>
                        @if($event->status == 1 && $event->selling_off == 0)
                            <span class="legends "><i class="fa fa-circle selling_on_icon legend_icon"></i><span
                                        class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
                        @else
                            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span
                                        class="legend_text" style="font-weight: bold">{{$event->event_id_str}}</span></span>
                        @endif
                        <br/>
                        <span class="title_font_size" style="font-size: 16px;margin-top: 5px;display: inline-block;font-weight: bold">{{$event->title_arabic}}</span>
                        <br/>
                        <span class="title_font_size" style="font-size: 16px;margin-top: 5px;display: inline-block;font-weight: bold">{{$event->title_english}}</span>
                        <br/>
                    </center>
                </div>
                <div class="col-md-4" style="text-align: right;">
                    <div class="col-md-6" style="text-align: right"></div>
                    <div class="col-md-6">
                        <center><span><strong> <br/></strong></span></center>
                    </div>
                </div>
            </div>
        </div>





        <div class="col-md-12" style="background-color: white;margin-top: 15px">

            <div class="col-md-10 col-md-offset-1" style="margin-top: 12px">
                <div class='control' style="font-size: 18px; margin-top: 30px">
                    <div class="nav-navbar-center">
                        <ul class="nav nav-pills">
                            <li style="margin-right: 70px;margin-top: 7px"> التذاكر المباعة <br/>Total Sold <br/><span
                                        style="font-size: 27px; color: black;display: inline-block; margin-top: 7px">{{$event->getTotalIssuedTicket()}}</span>
                            </li>
                            <?php
                            $getTotalIssuedTicketAmount = $event->getTotalIssuedTicketAmount();
                                $getTotalIssuedTicketAmountWithVat = $event->getTotalIssuedTicketAmountWithVat();
                            ?>
                            <!-- <li style="margin-top: 7px"> مجموع<br/>Total Amount<br/><span
                                        style="font-size: 27px; color: black;display: inline-block; margin-top: 7px">{{$getTotalIssuedTicketAmount}}</span>
                            </li>
                            <li style="margin-left: 70px;margin-top: 7px"> مجموع<br/>Total VAT<br/><span
                                        style="font-size: 27px; color: black;display: inline-block; margin-top: 7px">{{number_format((float) 5/100 * $getTotalIssuedTicketAmount ,2,'.','')}}</span>
                            </li> -->

                            <li style="margin-top: 7px"> اجمالي المبيعات<br/>Total Amount<br/><span
                                        style="font-size: 27px; color: black;display: inline-block; margin-top: 7px">{{$getTotalIssuedTicketAmountWithVat['total_price_per_tier']}}</span>
                            </li>
                            <li style="margin-left: 70px;margin-top: 7px"> الضريبة المضافة<br/>Total VAT<br/><span
                                        style="font-size: 27px; color: black;display: inline-block; margin-top: 7px">{{$getTotalIssuedTicketAmountWithVat['total_vat_per_tier']}}</span>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="background-color: white">
            <table class="table table-hover">

                <tr style="padding: 5px">
                    <th style="text-align: center">النوع<br>Tiers</th>
                    <th style="text-align: center">التذاكر المتاحة<br>Offered</th>
                    <th style="text-align: center">التذاكر المباعة<br>Issued</th>

                    <th style="text-align: center">التذاكر المطلوبة<br>Requests</th>
                    <th style="text-align: center">التذاكر المجانية<br>Free</th>
                    <th style="text-align: center">التذاكر المتبقية<br>Left</th>

                    <th style="text-align: center">الحضور<br>Attended</th>
                    <th style="text-align: center">سعر التذكرة<br>Price</th>
                    <th style="text-align: center">الاجمالي<br>Total</th>
                    <th style="text-align: center">الضريبة<br>VAT</th>
                </tr>

                <tbody>
                @foreach($event->tiers as $tier)
                    <tr style="padding: 5px">
                        <td style="background-color:#{{$tier->header_color}}; text-align: right;color: white;font-size: 14px;">{{$tier->title_arabic}}</td>
                        <?php $total_quantity = $tier->total_quantity ?>
                        <td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0) {{'Open'}} @else {{$total_quantity}}@endif</td>
                        <?php

                        $getIssuedTicketCount = $tier->getIssuedTicketCount();
                        $getFreeIssuedTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedFree(3,$event->id,$tier->id);

                        $getIssuedTicketStats = $tier->getIssuedTicketStatsNew();
                            $getTotalTicketStats = $tier->totaAvailable();
//                            dd($tier);
//                        $adminTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedSum(3,$event->id,2);
                             $adminTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedSumNew(3,$event->id,$tier->id);

                        ?>
                        <td style="text-align: center;font-size: 14px;">{{$getIssuedTicketCount - $getFreeIssuedTicketCount}}</td>
                        <?php $getEventRequestCount = $tier->totaAvailable() ?>
                        <td style="text-align: center;font-size: 14px;">{{$getEventRequestCount}}</td>
                        <td style="text-align: center;font-size: 14px;">{{$tier->getFreeTicketAsGiftByAdminFromAdminSideCount()}}</td>
                        {{--<td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0){{'Open'}}@else{{$total_quantity - ($getEventRequestCount + $getIssuedTicketCount)}}@endif</td>--}}
                        <!-- <td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0){{'Open'}}@else{{$total_quantity - ($getTotalTicketStats + $adminTicketCount)}}@endif</td> -->
                        <td style="text-align: center;font-size: 14px;">@if($tier->total_quantity_type==0){{'Open'}}@else
                            {{$total_quantity - ($getTotalTicketStats + $adminTicketCount) }}
                            @endif</td>
                        <td style="text-align: center;font-size: 14px;">{{$tier->getRedemptionCount()}}</td>
                        <td style="text-align: center;font-size: 14px;">@if($tier->price_type==0){{'Free'}}@else{{$tier->price}}@endif</td>
                        <!-- <td style="text-align: center;font-size: 14px;">{{$getIssuedTicketCount*$tier->price}}</td>
                        <td style="text-align: center;font-size: 14px;">{{ 5/100 * ($getIssuedTicketCount*$tier->price)}}</td> -->

                        <td style="text-align: center;font-size: 14px;">{{ $getIssuedTicketStats['total_price_per_tier'] }}</td>
                        <td style="text-align: center;font-size: 14px;">{{ $getIssuedTicketStats['total_vat_per_tier'] }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <br>
    @endif



    <div class="page-title">
        {{--<h3>Event - <span class="semi-bold">{{$request_type}}</span></h3>--}}
    </div>
    <style type="text/css">
        table#datatable-example2, table#datatable-example {
            width: 100% !important;
        }

        .pick-a-color-markup .input-group-btn .color-dropdown {
            padding: 7.5px 5px !important;
        }

        .error {
            display: -webkit-inline-box;
        }

        #header_color_tier-error {
            display: none !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    {{--<h4>{{$request_type}} <span class="semi-bold">Event</span></h4>--}}
                </div>
                <div class="grid-body no-border"><br>
                    @if($errors->any())
                        <div class="row">
                            <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                                <button data-dismiss="alert" class="close"></button>
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    {!! Form::model($event,['route' => ['admin.events.addeditevent','id'=>$event->id],'method'=> $event ? 'PUT' : 'POST', 'id'=>'form_events', "enctype"=>"multipart/form-data"]) !!}

                    <div class="form-actions"
                         style="background-color: #f6f7f8; margin-bottom: 10px !important; margin-top: -45px !important;">
                        <a href="{{ URL::to('admin/events') }}" class="btn btn-default" style="float: left">Cancel إلغاء</a>
                    <!-- <a href="javascript:void(0)" class="btn btn-primary form-submit" style="float: right;">{{ $request_type == 'Add' ? 'Save إلغاء' : 'Update إلغاء' }}</a> -->
                        {!! Form::submit($request_type == 'Add' ? 'Save حفظ' : 'Update تحديث', ['class' => 'btn btn-primary btn-cons form-submit','style' => 'float:right']) !!}
                    </div>

                    <div id="images_div">
                        <div class="row">
                            <div class="col-md-5">
                                <center>
                                    <?php $return = $event ? $event->logo_image : false ?>
                                    <div class="form-group row " style="margin-bottom: 0px;">
                                        <label><span class="semi-bold">Event Logo<span class="heading_arabic">شعار المناسبة</span></span></label>
                                        <div class="col-md-12 col-xs-12 controls">
                                            <div class="fileupload <?php  echo ($return != '') ? 'fileupload-exists' : 'fileupload-new'; ?>"
                                                 data-provides="fileupload">

                                                <?php if ($return != '') { ?>
                                                <div class="fileupload-new thumbnail image_thumbnail_size">
                                                    <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail image_parent_div"
                                                     id="image1">
                                                    <a data-gallery="" data-fancybox="logo" class="gallery-item"
                                                       href="{{ asset($event->logo_image)}}"><img
                                                                src="{{ asset($event->logo_image)}}"
                                                                class="existed_image"/></a>
                                                </div>
                                                <?php } else { ?>
                                                <div class="fileupload-new thumbnail image_thumbnail_size">
                                                    <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail image_parent_div"
                                                     id="image1">
                                                </div>
                                                <?php } ?>

                                                <div>
                                      <span class="btn btn-file">
                                          <span class="fileupload-new">Select اختر الصورة </span>
                                          <span class="fileupload-exists">Change تغيير الصورة </span>
                                          {!! Form::file('logo_image', ['class' => 'selectimage','id'=>'logo_image','data-size_in_bytes'=>'512000']) !!}
                                      </span>
                                                    <a class="btn hide" style="" id="startCrop">Crop قص الصورة</a>
                                                    <a href="javascript:void(0)"
                                                       class="btn remove_picture fileupload-exists"
                                                       data-dismiss="fileupload">Remove حذف الصورة </a>
                                                    <p id="image1-dim" class="label label-success"
                                                       style="display:none;">
                                                    </p>
                                                    <p id="image1-size" class="label label-success"
                                                       style="display:none;">
                                                    </p>
                                                </div>
                                            </div>

                                            <code style="margin-left: 10px;display: -webkit-inline-box;">Allowed Image size: الحجم الاقصى</code>  500KB <br>


                                            <code>Allowed Dimension</code>1:1<br/>

                                            <br>
                                        </div>
                                        <div class="img-container" class="input">
                                            <input type="hidden" class="x" name="logo_x" id="x"/>
                                            <input type="hidden" class="y" name="logo_y" id="y"/>
                                            <input type="hidden" class="w" name="logo_w" id="w"/>
                                            <input type="hidden" class="h" name="logo_h" id="h"/>
                                        </div>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-5">

                                <div class="grid simple">
                                    <div class="grid-title" style="padding: 0;"></div>
                                    <div class="grid-body sub-section" style="padding: 0;">

                                        <div class="row">
                                            <div class="col-md-6" style="padding-top: 10px;">

                                                <center>
                                                    <?php $return = $event ? $event->cover_image : false ?>
                                                    <div class="form-group row " style="margin-bottom: 0px;">
                                                        <!-- <label class="col-md-3 col-xs-12 form-label text-right">Cover Image</label> -->
                                                        <label><span class="semi-bold">Cover Image<span
                                                                        class="heading_arabic">غلاف المناسبة</span></span></label>
                                                        <div class="col-md-12 col-xs-12 controls">
                                                            <div class="fileupload <?php  echo ($return != '') ? 'fileupload-exists' : 'fileupload-new'; ?>"
                                                                 data-provides="fileupload">

                                                                <?php if ($return != '') { ?>
                                                                <div class="fileupload-new thumbnail image_thumbnail_size">
                                                                    <img alt=""
                                                                         src="http://via.placeholder.com/200x150?text=no+image"
                                                                         style="width: 218px;">
                                                                </div>
                                                                <div class="fileupload-preview fileupload-exists thumbnail image_parent_div"
                                                                     id="image2">
                                                                    <a data-gallery="" data-fancybox="cover"
                                                                       class="gallery-item"
                                                                       href="{{ asset($event->cover_image)}}"><img
                                                                                src="{{ asset($event->cover_image)}}"
                                                                                class="existed_image"/></a>
                                                                </div>
                                                                <?php } else { ?>
                                                                <div class="fileupload-new thumbnail image_thumbnail_size">
                                                                    <img alt=""
                                                                         src="http://via.placeholder.com/200x150?text=no+image"
                                                                         style="width: 212px;height: 170px">
                                                                </div>
                                                                <div class="fileupload-preview fileupload-exists thumbnail image_parent_div"
                                                                     id="image2">
                                                                </div>
                                                                <?php } ?>

                                                                <div>
                                              <span class="btn btn-file">
                                                  <span class="fileupload-new">Select اختر الصورة </span>
                                                  <span class="fileupload-exists">Change تغيير الصورة </span>
                                                  {!! Form::file('cover_image', ['class' => 'selectimage', 'id'=>'cover_image','data-size_in_bytes'=>'512000']) !!}
                                              </span>
                                                                    <a class="btn hide" style=""
                                                                       id="startCrop2">Crop قص الصورة</a>
                                                                    <a href="javascript:void(0)"
                                                                       class="btn remove_picture fileupload-exists"
                                                                       data-dismiss="fileupload">Remove حذف الصور </a>
                                                                    <p id="image2-dim" class="label label-success"
                                                                       style="display:none;">
                                                                    </p>
                                                                    <p id="image2-size" class="label label-success"
                                                                       style="display:none;">
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <code style="margin-left: 10px;display: -webkit-inline-box;">Allowed Image size: الحجم الاقصى</code>  500KB <br>

                                                            <code>Allowed Dimension</code>2:1<br/>

                                                            <br>
                                                        </div>
                                                        <div class="img-container" class="input">
                                                            <input type="hidden" class="x" name="logo_x2" id="x2"/>
                                                            <input type="hidden" class="y" name="logo_y2" id="y2"/>
                                                            <input type="hidden" class="w" name="logo_w2" id="w2"/>
                                                            <input type="hidden" class="h" name="logo_h2" id="h2"/>
                                                        </div>
                                                    </div>
                                                </center>

                                            </div>
                                            <div class="col-md-6">
                                                <div style="padding: 10px;">
                                                    <center>
                                                        <p style="direction: rtl;">غلاف المناسبة يظهر في صفحة الموقع الرئيسية</p>
                                                        <p>Cover image shows in site home page</p>

                                                        <img src="{{asset('assets/images/sample_cover.png')}}"
                                                             style="width: 192px;height: auto;border-bottom: 1px solid rgb(189,189,189);">
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title ">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><span
                                                class="bold">Event Information</span><span class="heading_arabic bold">معلومات المناسبة</span></a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">

                                <div class="panel-body">
                                    <div class="grid simple">
                                        <div class="grid-title">
                                            <div><h4><span class="semi-bold">Name<span class="heading_arabic">الاسم</span></span>
                                                </h4></div>
                                        </div>
                                        <div class="grid-body sub-section">

                                            <div class="row">

                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group row" style="margin-left: 31px">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Title English <br>الاسم باللغة الانجليزية
                                                                <span
                                                                        style="color:red">*</span></label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::text('title_english', null , ['class' => 'form-control', 'id'=>'title_english','required'=>true]) !!}
                                                            <span class="help-block"><span
                                                                        id="char_title_english">60</span> <?php echo __('general_characters_left'); ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row" style="margin-left: 22px">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label right">Title Arabic <br>الاسم باللغة العربية
                                                                <span style="color:red">*</span></label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::text('title_arabic', null , ['style' => 'direction: rtl', 'class' => 'form-control', 'id'=>'title_arabic','required'=>true]) !!}
                                                            <span class="help-block"><span
                                                                        id="char_title_arabic">60</span> <?php echo __('general_characters_left'); ?></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-5 col-sm-12 pull-right">

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right tags">
                                                            <label class="form-label">Tag <br>اشارة</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-8 col-sm-12"
                                                             style="margin-left: 16px">
                                                            {!! Form::select('tag', $tags, $event ? $event->tag : null, ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid simple">
                                        <div class="grid-title">
                                            <div><h4><span class="semi-bold">Venue<span class="heading_arabic">الموقع</span></span>
                                                </h4></div>
                                        </div>
                                        <div class="grid-body sub-section">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">

                                                    <div class="form-group row" style="margin-left: 31px">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Venue English <br>الموقع باللغة الانجليزية
                                                                {{--<span style="color:red">*</span>--}}
                                                            </label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::textarea('venue_english', null , ['class' => 'form-control','id'=>'venue_text_english','rows'=> 2]) !!}
                                                            <span class="help-block"><span
                                                                        id="char_venue_english">150</span> <?php echo __('general_characters_left'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" style="margin-left: 31px">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Venue Arabic <br>الموقع باللغة العربية
                                                                {{--<span style="color:red">*</span>--}}
                                                            </label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::textarea('venue_arabic', null , ['class' => 'form-control','id'=>'venue_text_arabic','style'=>'direction: rtl','rows'=> 2]) !!}
                                                            <span class="help-block"><span
                                                                        id="char_venue_arabic">150</span> <?php echo __('general_characters_left'); ?></span>

                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-5 col-sm-12 pull-right">

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right maps">
                                                            <label class="form-label">Map <br>الخريطة</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-8 col-sm-12" style="margin-left: 16px;">
                                                        <!-- {!! Form::text('map_address', null , ['class' => 'form-control','id'=>'map_address', 'disabled'=>true]) !!} -->
                                                            <input type="text" class="form-control" name="map_address"
                                                                   id="map_address"
                                                                   value="{{$event && $event->map_address != '' ? $event->map_address : ''}}"
                                                                   readonly="readonly">
                                                            <br>
                                                            <a href="javascript:void(0)" class="btn btn-green"
                                                               id="select_from_map" data-toggle="modal"
                                                               data-target="#googleMapModal">Add اضافة</a>
                                                            <a href="javascript:void(0)"
                                                               class="btn btn-danger clear_marker"
                                                               id="map_clear">Clear مسح</a>
                                                        </div>

                                                        <input type="hidden" class="form-control" name="map_address_original" id="map_address_original" value="{{$event && $event->map_address != '' ? $event->map_address : ''}}" readonly="readonly">

                                                        <input type="hidden" class="form-control" name="map_address_temp" id="map_address_temp" value="{{$event && $event->map_address != '' ? $event->map_address : ''}}" readonly="readonly">

                                                        <input type="hidden" name="lat_original" id="lat_original"
                                                               value="{{$event && $event->lat != 0 ? $event->lat : ''}}">
                                                        <input type="hidden" name="long_original" id="long_original"
                                                               value="{{$event && $event->long != 0 ? $event->long : ''}}">

                                                        <input type="hidden" name="lat_temp" id="lat_temp"
                                                               value="{{$event && $event->lat != 0 ? $event->lat : ''}}">
                                                        <input type="hidden" name="long_temp" id="long_temp"
                                                               value="{{$event && $event->long != 0 ? $event->long : ''}}">

                                                        <input type="hidden" name="lat" id="lat_input"
                                                               value="{{$event && $event->lat != 0 ? $event->lat : ''}}">
                                                        <input type="hidden" name="long" id="lng_input"
                                                               value="{{$event && $event->long != 0 ? $event->long : ''}}">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- DATE TIME -->
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="grid simple">
                                                <div class="grid-title">
                                                    <div><h4><span class="semi-bold">Date time English<span
                                                                        class="heading_arabic">التاريخ والوقت الانجليزية</span></span>
                                                        </h4></div>
                                                </div>
                                                <div class="grid-body sub-section">

                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12">
                                                            <!-- sub section -->
                                                            <div class="grid simple">
                                                                <div class="grid-title"></div>
                                                                <div class="grid-body sub-section">
                                                                    <div class="row">
                                                                        <!-- dates handling -->
                                                                        <div class="col-md-12 col-sm-12">

                                                                            <div class="form-group row">
                                                                                <label for="date_from_english"><span
                                                                                            class="semi-bold">From Date<span
                                                                                                class="heading_arabic">من</span>
                                                                                        <span style="color:red">*</span>
                                                                                    </span></label>
                                                                                {!! Form::text('from_date_english', null , ['class' => 'form-control event_date_picker', 'id'=>'date_from_english' ,'required' => true]) !!}
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="date_to_english"><span
                                                                                            class="semi-bold">To Date<span
                                                                                                class="heading_arabic">الى</span>
                                                                                        <span style="color:red">*</span>
                                                                                    </span></label>
                                                                                {!! Form::text('to_date_english', null , ['class' => 'form-control event_date_picker', 'id'=>'date_to_english'  ,'required' => true]) !!}
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- sub section ends here -->
                                                        </div>
                                                        <div class="col-md-6 col-sm-12">
                                                            <!-- sub section -->
                                                            <div class="grid simple">
                                                                <div class="grid-title"></div>
                                                                <div class="grid-body sub-section">
                                                                    <div class="row">
                                                                        <!-- dates handling -->
                                                                        <div class="col-md-12 col-sm-12">

                                                                            <div class="form-group row">
                                                                                <label for="time_from_english"><span
                                                                                            class="semi-bold">From Time<span
                                                                                                class="heading_arabic">من</span>
                                                                                        <span style="color:red">*</span>
                                                                                    </span></label>
                                                                                {!! Form::text('from_time_english', null , ['class' => 'form-control time_picker', 'id'=>'time_from_english' ,'required' => true]) !!}
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="time_to_english"><span
                                                                                            class="semi-bold">To Time<span
                                                                                                class="heading_arabic">الى</span>
                                                                                        <span style="color:red">*</span>
                                                                                    </span></label>
                                                                                {!! Form::text('to_time_english', null , ['class' => 'form-control time_picker', 'id'=>'time_to_english' ,'required' => true]) !!}
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- sub section ends here -->
                                                        </div>
                                                    </div>

                                                    <!-- another event date and time -->
                                                    <div class="grid simple">
                                                        <div class="grid-title"></div>
                                                        <div class="grid-body sub-section">
                                                            <div class="row">
                                                                <!-- dates handling -->
                                                                <div class="col-md-12 col-sm-12">

                                                                    <input type="checkbox" name="is_another_dt"
                                                                           id="is_another_dt" {{$event && $event->is_another_dt ? 'checked':''}}>
                                                                    <span class="semi-bold">Hidden stoppage time<span
                                                                                class="heading_arabic">وقت ايقاف البيع الغير معلن</span></span>
                                                                    <div id="another_dt_div"
                                                                         class="{{$event && $event->is_another_dt ? '':'hidden'}}">
                                                                        <br><br>
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-sm-12">
                                                                                <label for="another_date"><span
                                                                                            class="semi-bold">Date<span
                                                                                                class="heading_arabic">التاريخ</span></span></label>
                                                                                {!! Form::text('another_date', null , ['class' => 'form-control event_date_picker', 'id'=>'another_date']) !!}
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-12">
                                                                                <label for="another_time"><span
                                                                                            class="semi-bold">Time<span
                                                                                                class="heading_arabic">الوقت</span></span></label>
                                                                                {!! Form::text('another_time', null , ['class' => 'form-control time_picker', 'id'=>'another_time']) !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-12">
                                            <div class="grid simple">
                                                <div class="grid-title">
                                                    <div><h4><span class="semi-bold">Date time Arabic<span
                                                                        class="heading_arabic">التاريخ والوقت العربية</span></span>
                                                        </h4></div>
                                                </div>
                                                <div class="grid-body sub-section">

                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12">
                                                            <!-- sub section -->
                                                            <div class="grid simple">
                                                                <div class="grid-title"></div>
                                                                <div class="grid-body sub-section">
                                                                    <div class="row">
                                                                        <!-- dates handling -->
                                                                        <div class="col-md-12 col-sm-12">

                                                                            <div class="form-group row">
                                                                                <label for="date_from_arabic"><span
                                                                                            class="semi-bold">From Date<span
                                                                                                class="heading_arabic">من</span>
                                                                                        {{--<span style="color:red">*</span>--}}
                                                                                    </span></label>
                                                                                {!! Form::text('year_from_arabic', isset($arabic_date[0]) ? $arabic_date[0] : null, ['class' => 'col-md-3 col-sm-3', 'id'=>'year_from_arabic' , 'style' => 'direction: rtl', 'placeholder'=>'سنة']) !!}
                                                                                {!! Form::text('month_from_arabic', isset($arabic_date[1]) ? $arabic_date[1] : null , ['class' => 'col-md-3 col-sm-3', 'id'=>'month_from_arabic' , 'style' => 'direction: rtl;margin-left: 15px', 'placeholder'=>'شهر']) !!}
                                                                                {!! Form::text('day_from_arabic', isset($arabic_date[2]) ? $arabic_date[2] : null , ['class' => 'col-md-3 col-sm-3', 'id'=>'day_from_arabic' , 'style' => 'direction: rtl;margin-left: 15px', 'placeholder'=>'يوم']) !!}

                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="date_to_arabic"><span
                                                                                            class="semi-bold">To Date<span
                                                                                                class="heading_arabic">الى</span>
                                                                                        {{--<span style="color:red">*</span>--}}
                                                                                    </span></label>
                                                                                {!! Form::text('year_to_arabic', isset($arabic_to_date[0]) ? $arabic_to_date[0] : null , ['class' => 'col-md-3 col-sm-3', 'id'=>'year_to_arabic' , 'style' => 'direction: rtl', 'placeholder'=>'سنة']) !!}
                                                                                {!! Form::text('month_to_arabic', isset($arabic_to_date[1]) ? $arabic_to_date[1] : null , ['class' => 'col-md-3 col-sm-3', 'id'=>'month_to_arabic' , 'style' => 'direction: rtl;margin-left: 15px', 'placeholder'=>'شهر']) !!}
                                                                                {!! Form::text('day_to_arabic', isset($arabic_to_date[2]) ? $arabic_to_date[2] : null , ['class' => 'col-md-3 col-sm-3', 'id'=>'day_to_arabic' , 'style' => 'direction: rtl;margin-left: 15px', 'placeholder'=>'يوم']) !!}

                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- sub section ends here -->
                                                        </div>
                                                        <div class="col-md-6 col-sm-12">
                                                            <!-- sub section -->
                                                            <div class="grid simple">
                                                                <div class="grid-title"></div>
                                                                <div class="grid-body sub-section">
                                                                    <div class="row">
                                                                        <!-- dates handling -->
                                                                        <div class="col-md-12 col-sm-12">

                                                                            <div class="form-group row">
                                                                                <label for="time_from_arabic"><span
                                                                                            class="semi-bold">From Time<span
                                                                                                class="heading_arabic">من</span>
                                                                                        {{--<span style="color:red">*</span>--}}
                                                                                    </span></label>

                                                                                <style type="text/css">
                                                                                    #time_from_arabic_minutes, #time_from_arabic_hours, #time_from_arabic_type, #time_to_arabic_minutes, #time_to_arabic_hours, #time_to_arabic_type {
                                                                                        font-size: 15px;
                                                                                        width: auto;
                                                                                        margin-left: 10px;
                                                                                    }

                                                                                    #time_from_arabic_type,#time_to_arabic_type {
                                                                                        margin-right: 15px;
                                                                                    }
                                                                                </style>

                                                                                <div class="event_arabic_dt_div">
                                                                                    {!! Form::select('from_time_arabic_type',[''=>'','م'=>'م', 'ص'=>'ص'], isset($existing_arabic_to_time) ? $existing_arabic_from_time['type'] : null, ['class' => 'form-control','id'=>'time_from_arabic_type', 'style'=>'direction:rtl']) !!}

                                                                                    {!! Form::select('from_time_arabic_hours',$arabic_hours,isset($existing_arabic_to_time) ? $existing_arabic_from_time['hours'] : null, ['class' => 'form-control','id'=>'time_from_arabic_hours', 'style'=>'direction:rtl']) !!}

                                                                                    <span style="margin-left: 4px;margin-right: -6px;font-weight: bold;font-size: 21px; padding: 0">:</span>

                                                                                    {!! Form::select('from_time_arabic_minutes',$arabic_minutes, isset($existing_arabic_to_time) ? $existing_arabic_from_time['minutes'] : null, ['class' => 'form-control','id'=>'time_from_arabic_minutes', 'style'=>'direction:rtl']) !!}

                                                                                </div>
                                                                                <!-- <div class="row">

                                                                                    <div class="col-sm-3 col-md-3">
                                                                                        {!! Form::select('from_time_arabic_type',[''=>'','م'=>'م', 'ص'=>'ص'], isset($existing_arabic_to_time) ? $existing_arabic_from_time['type'] : null, ['class' => 'form-control','id'=>'time_from_arabic_type', 'style'=>'direction:rtl']) !!}
                                                                                    </div>

                                                                                    <div class="col-sm-3 col-md-3" style="margin-left: 7px;margin-right: 3px">
                                                                                        {!! Form::select('from_time_arabic_hours',$arabic_hours,isset($existing_arabic_to_time) ? $existing_arabic_from_time['hours'] : null, ['class' => 'form-control','id'=>'time_from_arabic_hours', 'style'=>'direction:rtl']) !!}
                                                                                    </div>
                                                                                    <div class="col-sm-1 col-md-1">
                                                                                        <span style="margin-left: -5px;font-weight: bold;font-size: 21px; padding: 0">:</span>
                                                                                    </div>


                                                                                    <div class="col-sm-3 col-md-3" style="margin-left: -24px;">
                                                                                        {!! Form::select('from_time_arabic_minutes',$arabic_minutes, isset($existing_arabic_to_time) ? $existing_arabic_from_time['minutes'] : null, ['class' => 'form-control','id'=>'time_from_arabic_minutes', 'style'=>'direction:rtl']) !!}
                                                                                    </div>
                                                                                </div> -->


                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="time_to_arabic"><span
                                                                                            class="semi-bold">To Time<span
                                                                                                class="heading_arabic">الى</span>
                                                                                        {{--<span style="color:red">*</span>--}}
                                                                                    </span></label>

                                                                                <div class="event_arabic_dt_div">
                                                                                    {!! Form::select('to_time_arabic_type',[''=>'','م'=>'م', 'ص'=>'ص'],isset($existing_arabic_to_time) ? $existing_arabic_to_time['type'] : null, ['class' => 'form-control','id'=>'time_to_arabic_type', 'style'=>'direction:rtl']) !!}

                                                                                    {!! Form::select('to_time_arabic_hours',$arabic_hours, isset($existing_arabic_to_time) ? $existing_arabic_to_time['hours'] : null, ['class' => 'form-control','id'=>'time_to_arabic_hours', 'style'=>'direction:rtl']) !!}

                                                                                    <span style="margin-left: 4px;margin-right: -6px;font-weight: bold;font-size: 21px; padding: 0">:</span>

                                                                                    {!! Form::select('to_time_arabic_minutes',$arabic_minutes, isset($existing_arabic_to_time) ? $existing_arabic_to_time['minutes'] : null, ['class' => 'form-control','id'=>'time_to_arabic_minutes', 'style'=>'direction:rtl']) !!}
                                                                                </div>
                                                                                <!-- <div class="row">
                                                                                    <div class="col-sm-3 col-md-3">
                                                                                        {!! Form::select('to_time_arabic_type',[''=>'','م'=>'م', 'ص'=>'ص'],isset($existing_arabic_to_time) ? $existing_arabic_to_time['type'] : null, ['class' => 'form-control','id'=>'time_to_arabic_type', 'style'=>'direction:rtl']) !!}
                                                                                    </div>
                                                                                    <div class="col-sm-3 col-md-3" style="margin-left: 7px;margin-right: 3px">
                                                                                        {!! Form::select('to_time_arabic_hours',$arabic_hours, isset($existing_arabic_to_time) ? $existing_arabic_to_time['hours'] : null, ['class' => 'form-control','id'=>'time_to_arabic_hours', 'style'=>'direction:rtl']) !!}
                                                                                    </div>
                                                                                    <div class="col-sm-1 col-md-1">
                                                                                        <span style="margin-left: -5px;font-weight: bold;font-size: 21px; padding: 0">:</span>
                                                                                    </div>

                                                                                    <div class="col-sm-3 col-md-3" style="margin-left: -24px">
                                                                                        {!! Form::select('to_time_arabic_minutes',$arabic_minutes, isset($existing_arabic_to_time) ? $existing_arabic_to_time['minutes'] : null, ['class' => 'form-control','id'=>'time_to_arabic_minutes', 'style'=>'direction:rtl']) !!}
                                                                                    </div>

                                                                                </div> -->
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- sub section ends here -->
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid simple">
                                        <div class="grid-title">
                                            <div><h4><span class="semi-bold">Event Description<span class="heading_arabic">نبذة عن المناسبة</span></span>
                                                </h4></div>
                                        </div>
                                        <div class="grid-body sub-section">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="description_english"><span
                                                                class="semi-bold">English<span class="heading_arabic">انجليزي</span> <span
                                                                    style="color:red">*</span></span></label>
                                                    {!! Form::textarea('description_english', null, ['class' => 'form-control','id'=>'description_english','rows'=>5,'required'=>true]) !!}
                                                    <span class="help-block"><span
                                                                id="char_description_english">500</span> <?php echo __('general_characters_left'); ?></span>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="description_arabic"><span class="semi-bold">Arabic<span
                                                                    class="heading_arabic">عربي</span> <span
                                                                    style="color:red">*</span></span></label>
                                                    {!! Form::textarea('description_arabic', null, ['style' => 'direction: rtl', 'class' => 'form-control','id'=>'description_arabic','rows'=>5,'required'=>true]) !!}
                                                    <span class="help-block"><span
                                                                id="char_description_arabic">500</span> <?php echo __('general_characters_left'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid simple">
                                        <div class="grid-title">
                                            <div><h4><span class="semi-bold">Event Terms & Conditions<span
                                                                class="heading_arabic">شروط و احكام المناسبة</span></span>
                                                </h4></div>
                                        </div>
                                        <div class="grid-body sub-section">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="tc_arabic_english"><span class="semi-bold">English<span
                                                                    class="heading_arabic">انجليزي</span></span></label>
                                                    {!! Form::textarea('tc_english', null, ['class' => 'form-control','id'=>'tc_arabic_english','rows'=>10]) !!}
                                                    <span class="help-block"><span
                                                                id="char_tc_arabic_english">5000</span> <?php echo __('general_characters_left'); ?></span>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="tc_arabic_arabic"><span class="semi-bold">Arabic<span
                                                                    class="heading_arabic">عربي</span></span></label>
                                                    {!! Form::textarea('tc_arabic', null, ['style' => 'direction: rtl', 'class' => 'form-control','id'=>'tc_arabic_arabic','rows'=>10]) !!}
                                                    <span class="help-block"><span
                                                                id="char_tc_arabic_arabic">5000</span> <?php echo __('general_characters_left'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><span
                                                class="bold">Event Contact</span><span
                                                class="heading_arabic bold">تواصل المناسبة</span></a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <div class="grid simple">
                                        <div class="grid-title">
                                            <div><h4><span class="semi-bold">Social Contacts<span
                                                                class="heading_arabic">الشبكات الاجتماعية</span></span></h4></div>
                                        </div>
                                        <div class="grid-body sub-section">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Twitter <br>تويتر</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::url('twitter_link', null , ['class' => 'form-control', 'onkeypress'=>'return (event.keyCode!=64)']) !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Instagram <br>انستقرام</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::url('instagram_link', null , ['class' => 'form-control', 'onkeypress'=>'return (event.keyCode!=64)']) !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">URL <br>رابط الموقع</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::url('url_link', null , ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6 col-sm-12">

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Snapchat <br>سناب تشات</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::url('snapchat_link', null , ['class' => 'form-control', 'onkeypress'=>'return (event.keyCode!=64)']) !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Facebook <br>فيسبوك</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::url('facebook_link', null , ['class' => 'form-control', 'onkeypress'=>'return (event.keyCode!=64)']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid simple">
                                        <div class="grid-title">
                                            <div><h4><span class="semi-bold">Event Contact<span
                                                                class="heading_arabic">تواصل المناسبة</span></span></h4></div>
                                        </div>
                                        <div class="grid-body sub-section">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Mobile <br>الهاتف المتحرك <span
                                                                        style="color:red"></span></label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-6 col-sm-12">
                                                            {!! Form::text('mobile', null , ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">Email <br>البريد الالكتروني <span
                                                                        style="color:red"></span></label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-6 col-sm-12">
                                                            {!! Form::text('email', null , ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6 col-sm-12">

                                                    <div class="form-group row">
                                                        <div class="col-md-3 col-sm-12 text-right">
                                                            <label class="form-label">URL <br>رابط الموقع</label>
                                                        </div>
                                                        <div class="input-with-icon  right col-md-9 col-sm-12">
                                                            {!! Form::url('url_link2', null , ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><span
                                                class="bold">Type</span><span class="heading_arabic bold">النوع</span></a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <div class="form-group row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="controls">
                                                @if(Auth::user()->can('tiers-create'))
                                                    <a href="javascript:void(0)" id="addEditTierModalBtn"
                                                       class="btn btn-success">Add Type اضافة نوع
                                                    </a>
                                                @endif
                                                {{--<div class="pull-right markaction">--}}
                                                    {{--@if(Auth::user()->can('tiers-destroy'))--}}
                                                        {{--<a id="del2" href="javascript:void(0)" class="btn btn-danger"--}}
                                                           {{--data-url="{{ URL::to('admin/tiers/bulkdelete') }}">Delete--}}
                                                            {{--حذف</a>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid form-group row">
                                        <div class="grid-body col-md-10 col-md-offset-1">
                                            <table class="table sortable table-hover table-bordered"
                                                   id="datatable-example2"
                                                   data-reorderUrl="{{route('admin.tiers.reorder')}}">
                                                <thead>
                                                <tr>
                                                    <th><input type="checkbox" class="group-checkable"
                                                             data-set="#datatable-example2 .checkboxes"/></th>
                                                    <th>Id</th>
                                                    <th>
                                                        <center>اسم النوع<br>Title English</center>
                                                    </th>
                                                    <th>
                                                        <center>اسم النوع<br>Title Arabic</center>
                                                    </th>
                                                    <th>
                                                        <center>السعر<br>Price</center>
                                                    </th>
                                                    <th>
                                                        <center>اجمالي المتاح<br>Quantity Offered</center>
                                                    </th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><span
                                                class="bold">Additional banner images</span><span class="heading_arabic bold">الصور الاضافية</span></a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <div class="form-group row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="controls">
                                                @if(Auth::user()->can("additionalimages-create"))

                                                    <a href="javascript:void(0)" data-toggle="modal"
                                                       data-target="#additionaImageModal" class="btn btn-success">Add
                                                        Image اضافة صورة
                                                    </a>
                                                @endif
                                                <div class="pull-right markaction">
                                                    @if(Auth::user()->can('additionalimages-destroy'))
                                                        <a id="del" href="javascript:void(0)" class="btn btn-danger"
                                                           data-url="{{ URL::to('admin/additionalimages/bulkimagedelete') }}">Delete
                                                            حذف</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <table class="table sortable table-hover table-bordered"
                                                   id="datatable-example"
                                                   data-reorderUrl="{{route('admin.additionalimages.reorder')}}">
                                                <thead>
                                                <tr>
                                                    <th><input type="checkbox" class="group-checkable"
                                                               data-set="#datatable-example .checkboxes"/></th>
                                                    <th>Id</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <input type="hidden" name="q" value="{{$request_type}}">
                    <div class="form-actions">
                        <a href="{{ URL::to('admin/events') }}" class="btn btn-default">Cancel إلغاء</a>
                    <!-- <a href="javascript:void(0)" class="btn btn-primary form-submit" style="float: right;">{{ $request_type == 'Add' ? 'Save إلغاء' : 'Update إلغاء' }}</a> -->
                        {!! Form::submit($request_type == 'Add' ? 'Save تحديث' : 'Update تحديث', ['class' => 'btn btn-primary btn-cons form-submit','style' => 'float:right']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <!-- additional images modal -->
    <div class="modal fade" id="additionaImageModal" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Image إضافة صورة
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="form-group row">
                            <div class="col-md-4 col-sm-12 text-right">
                                <label class="form-label">Image<br>الصورة</label>
                            </div>
                            <div class="input-with-icon  right col-md-6 col-sm-12">
                                <div id="image_additional_div">
                                    <div class="fileupload <?php  echo ($return != '') ? 'fileupload-exists' : 'fileupload-new'; ?>"
                                         data-provides="fileupload">

                                        <div class="fileupload-new thumbnail image_thumbnail_size">
                                            <img alt="" src="http://via.placeholder.com/200x150?text=no+image">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail image_parent_div"
                                             id="image3">
                                            <a data-gallery="" class="gallery-item"><img
                                                        src="http://via.placeholder.com/200x150?text=no+image"
                                                        class="existed_image"/></a>
                                        </div>

                                        <div>
                      <span class="btn btn-file">
                        <span class="fileupload-new">Select اختر الصورة</span>
                        <span class="fileupload-exists">Change تغيير الصورة</span>
                          {!! Form::file('additional_image', ['class' => 'selectimage','id'=>'additional_image','data-size_in_bytes'=>'512000', 'multiple'=>true]) !!}
                      </span>
                                            <a class="btn hide" style="display: inline-block" id="startCrop3">Crop قص الصورة</a>
                                            <a href="javascript:void(0)"
                                               class="btn remove_picture fileupload-exists hide"
                                               data-dismiss="fileupload">Remove حذف الصورة</a>
                                            <p id="image3-dim" class="label label-success" style="display:none;">
                                            </p>
                                            <p id="image3-size" class="label label-success" style="display:none;">
                                            </p>
                                        </div>
                                    </div>
                                    <code style="margin-left: 10px;display: -webkit-inline-box;">Allowed Image size: الحجم الاقصى</code>  500KB <br>
                                    <code style="margin-left: 10px">Allowed Dimension</code> 2:1<br/>

                                    <div class="img-container" class="input">
                                        <input type="hidden" class="x" name="logo_x3" id="x3"/>
                                        <input type="hidden" class="y" name="logo_y3" id="y3"/>
                                        <input type="hidden" class="w" name="logo_w3" id="w3"/>
                                        <input type="hidden" class="h" name="logo_h3" id="h3"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--<div class="form-group row">--}}
                        {{--<div class="col-md-4 col-sm-12 text-right">--}}
                        {{--<label class="form-label">Caption <br>العنوان أرابيك</label>--}}

                        {{--</div>--}}
                        {{--<div class="input-with-icon  right col-md-6 col-sm-12">--}}
                        {{--{!! Form::text('caption', null , ['class' => 'form-control','id'=>'caption']) !!}--}}
                        {{--<span class="help-block"><span--}}


                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>

                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close_picture_btn">
                            Cancel إلغاء
                        </button>

                        <button type="button" class="btn btn-green" id="add_picture_btn">Save حفظ
                        </button>
                    </center>
                </div>
                </form>
            </div>

        </div>
    </div>
    <!-- additional images modal end -->

    <!-- maps modal -->
    <div class="modal fade" id="googleMapModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                    <center>
                        <h4 class="modal-title"><span>Drop a pin</span><br><span style="text-align: center">اختر موقع</span></h4>
                    </center>

                </div>
                <div class="modal-body">
                    <input id="pac-input" class="controls pac-input" type="text" placeholder="Search Box"
                           style="width: 200px;">
                    <div class="form-group" id="location_map" style="width: 100%;height: 300px;"></div>

                    <div class="row">
                        <div class="text-right col-md-12 col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-danger clear_marker">Clear مسح</a>
                            <a href="javascript:void(0)" class="btn btn-success" onclick="copyMapLink()">Copy Map
                                Link نسخ رابط الموقع</a>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close_map">Close اغلاق</button>

                        <button type="button" class="btn btn-green" data-dismiss="modal" id="add_map">Add اضافة</button>

                    </center>
                </div>
            </div>

        </div>
    </div>
    <!-- maps modal end -->

    <!-- edit caption modal -->
    <div class="modal fade" id="editCaptionModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="editDescriptionModalLabel">Edit Caption</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="caption_image_id" id="edit_caption_id" value="">

                    <div class="form-group row">
                        <div class="col-md-4 col-sm-12 text-right">
                            <label class="form-label">Caption <br>العنوان أرابيك</label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                            {!! Form::text('edit_caption', null , ['class' => 'form-control','id'=>'edit_caption_input']) !!}
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="update_caption_btn">Update تحديث</button>
                    <button type="button" class="btn  btn-default" onclick='closeModal("#editCaptionModal")'
                            aria-label="Close">Cancel إلغاء
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- add tiers modal -->
    <div class="modal fade" id="addEditTierModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <form id="addEditTierForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="tier_modal_title">Type النوع</h4>
                        <input type="hidden" name="edit_tier_id" id="edit_tier_id" value="">
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-4 col-sm-12 " style="margin-right: -15px">
                                                <label class="form-label">Title English <br>اسم النوع بالانجليزي <span
                                                            style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8 col-sm-12">
                                                {!! Form::text('title_english_tier', null , ['class' => 'form-control', 'id'=>'title_english_tier','required'=>true]) !!}
                                                <span class="help-block"><span
                                                            id="char_title_english_tier">15</span> <?php echo 'characters left  الحروف المتبقية'; ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4 col-sm-12 " style="margin-right: -15px">
                                                <label class="form-label">Type color <br>لون النوع <span
                                                            style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8 col-sm-12">
                                                <div id="header_color_div">
                                                    <input type="text" class="form-control" id="header_color_tier"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-4 col-sm-12 " style="margin-right: -15px">
                                                <label class="form-label">Title Arabic <br>اسم النوع بالعربي <span
                                                            style="color:red">*</span></label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8 col-sm-12">
                                                {!! Form::text('title_arabic_tier', null , ['class' => 'form-control', 'id'=>'title_arabic_tier','required'=>true,'style'=>'direction: rtl']) !!}
                                                <span class="help-block"><span
                                                            id="char_title_arabic_tier">15</span> <?php echo 'characters left  الحروف المتبقية'; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-12 ">
                                                <label class="form-label">Ticket Price <br>سعر التذكرة</label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8 col-sm-12"
                                                 style="margin-top: 9px;margin-left: 3px">
                                                <label class="radio-inline">
                                                    <input type="radio" name="priceRadioBtn" value="0" checked
                                                           id="free_price_radio" style="margin-top: 4px">FREE مجانية
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="priceRadioBtn" value="1"
                                                           id="value_price_radio"
                                                           style="margin-top: 4px;margin-left: 1px"><span
                                                            style="margin-left: 16px;">Value القيمة</span>
                                                </label>
                                                <label class="radio-inline hidden" id="value_price_div">
                                                    <input type="number" min="1" max="10000" name="value_price"
                                                           id="value_price" required
                                                           style="padding: 0px 10px 0px 10px !important;" value="0"
                                                           class="form-control">
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-12 ">
                                                <label class="form-label">Quantity Offered <br>الكمية المتاحة</label>
                                            </div>
                                            <div class="input-with-icon  right col-md-8 col-sm-12"
                                                 style="margin-top: 9px;margin-left: 3px">
                                                <label class="radio-inline" style="margin-right: 9px;">
                                                    <input type="radio" name="unitRadioBtn" value="0" checked
                                                           id="open_unit_radio" style="margin-top: 4px">Open مفتوح
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="unitRadioBtn" value="1"
                                                           id="value_unit_radio"
                                                           style="margin-top: 4px;margin-left: 1px"><span
                                                            style="margin-left: 16px;">Limited محدد</span>
                                                </label>
                                                <label class="radio-inline hidden" id="value_unit_div">
                                                    <input type="number" min="1" max="1000" name="value_unit"
                                                           id="value_unit" required
                                                           style="padding: 0px 10px 0px 10px !important;" value="0"
                                                           class="form-control">
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="input-with-icon col-md-12 col-sm-12">
                                    <label for="description_arabic_tier"><span class="semi-bold">Description Arabic<span
                                                    class="heading_arabic">نبذة عن النوع بالعربي</span></span></label>
                                    {!! Form::textarea('description_arabic_tier', null, ['style' => 'direction: rtl' , 'class' => 'form-control ','id'=>'description_arabic_tier','rows'=>5]) !!}
                                    <span class="help-block"><span
                                                id="char_description_arabic_tier">250</span> <?php echo 'characters count الحروف المتبقية'; ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="input-with-icon col-md-12 col-sm-12">
                                    <label for="description_english_tier"><span
                                                class="semi-bold">Description English<span class="heading_arabic">نبذة عن النوع بالانجليزي</span></span></label>
                                    {!! Form::textarea('description_english_tier', null, ['class' => 'form-control','id'=>'description_english_tier','rows'=>5]) !!}
                                    <span class="help-block"><span
                                                id="char_description_english_tier">250</span> <?php echo 'characters count الحروف المتبقية'; ?></span>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="modal-footer">
                        <center>
                            <!-- <button type="button" class="btn btn-default" id="add_tier_btn">Submit</button> -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel إلغاء</button>

                            {!! Form::submit('Save حفظ', ['class' => 'btn btn-green tier-form-submit','id' => 'add_tier_btn']) !!}
                        </center>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div class="modal fade" id="eventStatusDeleted" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>--}}
                    {{--<h4 class="modal-title">Tier</h4>--}}
                {{--</div>--}}
                <div class="modal-body">
<center>
    <p>
        لا يمكنك حذف نوع يحتوي على طلبات او تذاكر سارية او غير سارية
    </p>
    <p>
        You can't delete a type that has any previous or current transactions
    </p>
</center>


                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK موافق</button>
                    </center>
                </div>
            </div>

        </div>
    </div>

    <div id="cropInfoModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body info-modal">
                <p class="message-content"></p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default" data-dismiss="modal">موافق</button>
            </div>
        </div>
    </div>
    </div>

    {!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
    <input type="hidden" value="" name="ids" id="ids"/>
    {!! Form::close() !!}

@endsection
@section('page_script')
    @parent
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.1/jquery-ui.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAe4ISmDhVvG-BXiqwXI2EkL3GdDFDxgM&libraries=places"
            async defer></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>--}}
    <script src="{{ asset('js/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    {{--<script type="text/javascript">--}}
    {{--$(function () {--}}
    {{--$('.event_date_picker').datepicker({--}}
    {{--autoclose: true,--}}
    {{--format: 'dd-mm-yyyy'--}}
    {{--});--}}
    {{----}}
    {{--});--}}
    {{--</script>--}}
    <script type="text/javascript">
        <?php if(!$event){ ?>
        $('.date_picker').val('<?php echo date('d-m-Y'); ?>');
        $('.time_picker').val('<?php echo date('H:i'); ?>');
        <?php }?>
        $(function () {
            var date = new Date();
            var existing_event_start_date = $("#date_from_english").val();
            $("#date_to_english").datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                startDate: existing_event_start_date != "" ? existing_event_start_date : date
            });

            $("#date_from_english").datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                startDate: date
            }).on('changeDate', function (selected) {
                $('#date_to_english').datepicker('remove');
                $('#date_to_english').val('');
                var minDate = new Date(selected.date.valueOf());
                $('#date_to_english').datepicker({
                    autoclose: true,
                    format: 'dd-mm-yyyy',
                    startDate: minDate
                });
            })
            $("#another_date").datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                startDate: date
            });
        });
        <?php $tier = count($event->tiers) > 0 ? true : false ?>

    </script>

    <script type="text/javascript" language="javascript" class="init">
        var divClone = $("#image_additional_div").clone();
        $(window).load(function () {
            initAutocomplete();
        });

        $(document).ready(function () {

            checksOnArabicTime();
            checksOnArabicDate();

            

            $(document).on('click','#startCrop3',function(){
                $('#image3-dim').removeClass('hide');
                $('#image3-dim').css("display","inline");
            });

            $('#add_map').on('click', function(){
                $('#map_address, #map_address_original').val($('#map_address_temp').val());

                $('#lat_input, #lat_original').val($('#lat_temp').val());
                $('#lng_input, #long_original').val($('#long_temp').val());
            });


            {{--console.log('{!! $tier !!}')--}}
            $('body').append($('#additionaImageModal'));
            $('body').append($('#fileSizeMessage'));
            $('#additionaImageModal').append($('#imageMessage'));

            initialise_colorpicker();
            priceRadioProcessing();
            unitRadioProcessing();


            $('#close_picture_btn').click(function () {
                $('.image3-imageareaselect').addClass('hidden');

                contentHandeling(divClone);
            });

            $('input[name=priceRadioBtn]').on('change', function () {
                priceRadioProcessing();
            });

            $('input[name=unitRadioBtn]').on('change', function () {
                unitRadioProcessing();
            });

            // $('#additional_image').on ('change',function () {
            //     changeImage();
            // });

            $('#tag_dropdown').select2({placeholder: 'Select Tag'});

            // $('#tag_dropdown').select2({placeholder: 'Select Tag'});


            $('.time_picker').clockpicker({
                autoclose: true,
                placement: 'top',
                align: 'left',
            });

            $("#description_english").keyup(function (event) {
                updateFeatureCharLimitNum('description_english', 'char_description_english', 500);
            });

            $("#description_arabic").keyup(function (event) {
                updateFeatureCharLimitNum('description_arabic', 'char_description_arabic', 500);
            });

            $("#tc_arabic_english").keyup(function (event) {
                updateFeatureCharLimitNum('tc_arabic_english', 'char_tc_arabic_english', 5000);
            });

            $("#tc_arabic_arabic").keyup(function (event) {
                updateFeatureCharLimitNum('tc_arabic_arabic', 'char_tc_arabic_arabic', 5000);
            });

            $("#title_english").keyup(function (event) {
                updateFeatureCharLimitNum('title_english', 'char_title_english', 60);
            });

            $("#title_arabic").keyup(function (event) {
                updateFeatureCharLimitNum('title_arabic', 'char_title_arabic', 60);
            });

            $("#description_english_tier").keyup(function (event) {
                updateFeatureCharLimitNum('description_english_tier', 'char_description_english_tier', 250);
            });

            $("#description_arabic_tier").keyup(function (event) {
                updateFeatureCharLimitNum('description_arabic_tier', 'char_description_arabic_tier', 250);
            });

            $("#title_arabic_tier").keyup(function (event) {
                updateFeatureCharLimitNum('title_arabic_tier', 'char_title_arabic_tier', 15);
            });

            $("#title_english_tier").keyup(function (event) {
                updateFeatureCharLimitNum('title_english_tier', 'char_title_english_tier', 15);
            });

            // $("#caption").keyup(function (event) {
            //     updateFeatureCharLimitNum('caption', 'char_caption_image', 40);
            // });

            $("#venue_text_english").keyup(function (event) {
                updateFeatureCharLimitNum('venue_text_english', 'char_venue_english', 150);
            });

            $("#venue_text_arabic").keyup(function (event) {
                updateFeatureCharLimitNum('venue_text_arabic', 'char_venue_arabic', 150);
            });

            updateFeatureCharLimitNum('title_english', 'char_title_english', 60);
            updateFeatureCharLimitNum('title_arabic', 'char_title_arabic', 60);
            updateFeatureCharLimitNum('venue_text_english', 'char_venue_english', 150);
            updateFeatureCharLimitNum('venue_text_arabic', 'char_venue_arabic', 150);
            updateFeatureCharLimitNum('description_arabic', 'char_description_arabic', 500);
            updateFeatureCharLimitNum('description_english', 'char_description_english', 500);
            updateFeatureCharLimitNum('tc_arabic_english', 'char_tc_arabic_english', 5000);
            updateFeatureCharLimitNum('tc_arabic_arabic', 'char_tc_arabic_arabic', 5000);

            updateFeatureCharLimitNum('title_arabic_tier', 'char_title_arabic_tier', 15);
            updateFeatureCharLimitNum('title_english_tier', 'char_title_english_tier', 15);
            updateFeatureCharLimitNum('description_english_tier', 'char_description_english_tier', 250);
            updateFeatureCharLimitNum('description_arabic_tier', 'char_description_arabic_tier', 250);

            // updateFeatureCharLimitNum('caption', 'char_caption_image', 40);

            // another dt toggle
            $('#is_another_dt').change(function () {
                this.checked ? $('#another_dt_div').removeClass('hidden') : $('#another_dt_div').addClass('hidden');
            });

            // submitting events format
            // jQuery('.form-submit').click(function (event) {
            //     form = $('#form_events');
            //     event.preventDefault();
            //     setTimeout(function () {
            //         form.submit();
            //     }, 500);
            //     jQuery('#progressbar').show();
            //     return true;
            // });


            //additional images datatable
            $('#datatable-example').DataTable({
                stateSave: true,
                // processing: true,
                serverSide: true,
                'pageLength': 25,
                "order": [],
                autoWidth: false,
                processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
                ajax: {
                    "url": "{{ URL::to('admin/additionalimages/datatable?imageable_id='.($event ? $event->id : 0)) }}",
                    "type": "POST"
                },
                columns: [
                    {data: 'check', name: 'check'},
                    {data: 'id', name: 'id'},
                    {data: 'image', name: 'image'},
                    {data: 'actions', name: 'actions'}
                ],
                "columnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0, 1, 2, 3],
                        "searchable": false
                    },
                    {
                        "targets": [0, 1, 2, 3],
                        "orderable": false
                    },
                    {
                        "width": "4%",
                        "targets": 0
                    },

                ]
            });


            $(document).on("click", "#add_picture_btn", function () {
                var cropPass = checkCropOk("2.13:1","w3,h3",$('#image3 .cropbox'));

                if(cropPass == true) {
                    console.log('hhhhyayyy')

                }else{
                    // console.log('yayyy')
                    // // alert(cropPass);
                    // $('.info-modal .message-content').text('مقاس صورة الغلاف يجب أن تتكون من أضعاف 2.13:1');
                    // $('#imageMessage').modal('show');
                    imageMessage(cropPass);
                    return false;
                }
                var image = $('#additional_image').get(0).files[0];
                var formData = new FormData();

                formData.append('image', image);
                formData.append('event_id', "{{ $event ? $event->id : 0 }}");
                formData.append('logo_h3', $('#h3').val());
                formData.append('logo_w3', $('#w3').val());
                formData.append('logo_x3', $('#x3').val());
                formData.append('logo_y3', $('#y3').val());
                formData.append('caption', $('#caption').val());

                if (image != undefined) {
                    $('#progressbar').show();
                    $.ajax({
                        url: "{{ URL::route('admin.additionalimages.uploadimage')}}",
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data, textStatus, jqXHR) {
                            if ($.isEmptyObject(data.error)) {
                                $('#datatable-example').DataTable().ajax.reload();
                                contentHandeling(divClone);
                                $('#progressbar').hide();
                            }
                        }
                    });
                    $('#additionaImageModal').modal('hide');
                    $('.image3-imageareaselect').addClass('hidden');
                }
                else {
                    imageMessage('Please upload images first');

                }
            });

            //add tier
            $(document).on("click", "#add_tier_btn", function () {

                $("#addEditTierForm").submit(function (e) {
                    e.preventDefault();
                });

                var title_english_tier = $('#title_english_tier').val();
                var title_arabic_tier = $('#title_arabic_tier').val();
                var header_color_tier = $('#header_color_tier').val();

                if (title_english_tier != '' && title_arabic_tier != '' && header_color_tier != '') {
                    var priceRadioVal = $('input[name=priceRadioBtn]:checked').val();
                    var unitRadioVal = $('input[name=unitRadioBtn]:checked').val();
                    var price = $('input[name=value_price]').val();
                    var unit = $('input[name=value_unit]').val();
                    var pass = false;
                    if (priceRadioVal == 0 && unitRadioVal == 0) {
                        pass = true
                    }

                    if (priceRadioVal == 1 && unitRadioVal == 0) {
                        if (price != 0) {
                            pass = true
                        }
                    }
                    if (priceRadioVal == 0 && unitRadioVal == 1) {
                        if (unit != 0) {
                            pass = true
                        }
                    }
                    if (priceRadioVal == 1 && unitRadioVal == 1) {
                        if (price != 0 && unit != 0) {
                            pass = true
                        }
                    }
                    if (pass == true) {

                        var formData = new FormData();
                        formData.append('event_id', "{{ $event ? $event->id : 0 }}");
                        formData.append('title_english', title_english_tier);
                        formData.append('title_arabic', title_arabic_tier);
                        formData.append('description_english', $('#description_english_tier').val());
                        formData.append('description_arabic', $('#description_arabic_tier').val());
                        formData.append('header_color', header_color_tier);
                        formData.append('price_type', priceRadioVal);
                        formData.append('total_quantity_type', unitRadioVal);
                        formData.append('price', $('#value_price').val());
                        formData.append('total_quantity', $('#value_unit').val());

                        console.log(formData)
                        var edit_tier_id = $('#edit_tier_id').val();
                        formData.append('edit_tier_id', edit_tier_id ? edit_tier_id : 0);

                        $.ajax({
                            url: "{{ URL::route('admin.tiers.addtier')}}",
                            type: "POST",
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (data, textStatus, jqXHR) {
                                if ($.isEmptyObject(data.error)) {
                                    $('#datatable-example2').DataTable().ajax.reload();
                                }
                            }
                        });
                        $('#addEditTierModal').modal('hide');
                    }
                }
            });

            $('#additionaImageModal').on('hidden.bs.modal', function () {
                // $(".imgareaselect-outer, .imgareaselect-handle").addClass('hidden');
                // $(".imgareaselect-selection").parents('div').addClass('hidden');
                // document.getElementById("additional_image").value = "";
                // document.getElementById("additional_image").value = "";
                // document.getElementById("additional_image").value = null;
                $('#caption').val('');
            });

            $('#addEditTierModalBtn').on('click', function () {
                $('#tier_modal_title').text('Add Tier إضافة الطبقة');
                $('#value_price_div,#value_unit_div').addClass('hidden');
                $('.color-preview.current-color').css("background-color", "");

                $('#char_title_arabic_tier,#char_title_english_tier').text(15);
                $('#char_description_english_tier,#char_description_arabic_tier').text(250);

                $('#addEditTierForm')[0].reset();
                $('#edit_tier_id').val('');
                $('#addEditTierModal').modal('show');
            });
        });

        function initialise_colorpicker() {
            $("#header_color_tier").pickAColor({
                showSpectrum: true,
                showSavedColors: true,
                saveColorsPerElement: false,
                fadeMenuToggle: true,
                showHexInput: true,
                showBasicColors: true,
                allowBlank: true,
                inlineDropdown: false
            });
        }

        function priceRadioProcessing() {
            var priceRadioVal = $('input[name=priceRadioBtn]:checked').val();
            priceRadioVal == 1 ? $('#value_price_div').removeClass('hidden') : $('#value_price_div').addClass('hidden');
        }

        function unitRadioProcessing() {
            var unitRadioVal = $('input[name=unitRadioBtn]:checked').val();
            unitRadioVal == 1 ? $('#value_unit_div').removeClass('hidden') : $('#value_unit_div').addClass('hidden');
        }

        function closeModal(id) {
            $(id).modal('hide');
        }

        function delAdditionalImage() {
            var id = $('#delButton').attr("data-id");
            console.log(id);
            if (id) {
                var formData = new FormData();
                formData.append('id', id);
                $.ajax({
                    url: "{{ URL::route('admin.additionalimages.delimage')}}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                        if ($.isEmptyObject(data.error)) {
                            $('#datatable-example').DataTable().ajax.reload();
                        }
                    }
                });

            }
        }

        {{--function delTier() {--}}
            {{--var id = $('#delButton').attr("data-id");--}}
            {{--console.log(id);--}}
            {{--if (id) {--}}
                {{--var formData = new FormData();--}}
                {{--formData.append('id', id);--}}
                {{--$.ajax({--}}
                    {{--url: "{{ URL::route('admin.tiers.deltier')}}",--}}
                    {{--type: "POST",--}}
                    {{--data: formData,--}}
                    {{--processData: false,--}}
                    {{--contentType: false,--}}
                    {{--success: function (data, textStatus, jqXHR) {--}}
                        {{--if ($.isEmptyObject(data.error)) {--}}
                            {{--$('#datatable-example2').DataTable().ajax.reload();--}}
                        {{--}--}}
                    {{--}--}}
                {{--});--}}
                {{--// $('#delMessage').modal('hide');--}}
            {{--}--}}
        {{--}--}}


        function delTier() {
            var id = $('#delButton').attr("data-id");
            console.log(id);
            if (id) {
                var formData = new FormData();
                formData.append('id', id);
                $.ajax({
                    url: "{{ URL::route('admin.tiers.deltier')}}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                        if ($.isEmptyObject(data.error)) {
                            $('#datatable-example2').DataTable().ajax.reload();
                        }
                    }
                });
                // $('#delMessage').modal('hide');
            }
        }

        $("#update_caption_btn").click(function () {
            // console.log("sadas");
            var formData = new FormData();
            formData.append('caption', $('#edit_caption_input').val());
            formData.append('image_id', $('#edit_caption_id').val());
            $.ajax({
                url: "{{ URL::route('admin.additionalimages.editcaption')}}",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data, textStatus, jqXHR) {
                    if ($.isEmptyObject(data.error)) {
                        $('#editCaptionModal').modal('hide');
                        $('#datatable-example').DataTable().ajax.reload();
                    }
                }
            });
        });

        function editCaption(id) {
            var caption = $('#' + id + '_additional_edit_caption').data('caption');
            $('#edit_caption_input').val(caption);
            $('#edit_caption_id').val(id);
            $('#editCaptionModal').modal('show');
        }

        function editTier(id) {
            var data = $('#' + id + '_edit_tier').data();

            $('#tier_modal_title').text('Edit Tier');
            $('#edit_tier_id').val(id);
            $('#title_english_tier').val(data.title_english);
            $('#title_arabic_tier').val(data.title_arabic);
            $('#description_english_tier').val(data.description_english);
            $('#description_arabic_tier').val(data.description_arabic);
            $('#value_price').val(data.price);
            $('#value_unit').val(data.total_quantity);

            $('#header_color_div').empty();
            $('#header_color_div').append('<input type="text" class="form-control" id="header_color_tier" value="">');
            $('#header_color_tier').val(data.header_color);
            initialise_colorpicker();

            var price_type = data.price_type;
            var total_quantity_type = data.total_quantity_type;

            price_type == 1 ? $("#value_price_radio").prop("checked", true) : $("#free_price_radio").prop("checked", true);
            total_quantity_type == 1 ? $("#value_unit_radio").prop("checked", true) : $("#open_unit_radio").prop("checked", true);

            priceRadioProcessing();
            unitRadioProcessing();

            updateFeatureCharLimitNum('title_arabic_tier', 'char_title_arabic_tier', 15);
            updateFeatureCharLimitNum('title_english_tier', 'char_title_english_tier', 15);
            updateFeatureCharLimitNum('description_english_tier', 'char_description_english_tier', 250);
            updateFeatureCharLimitNum('description_arabic_tier', 'char_description_arabic_tier', 250);

            $('#addEditTierModal').modal('show');
        }

        $(document).on('click', '#startCrop', function (e) {

            var image = $('#image1 .cropbox');
            AspectRatio = '1:1';
            maxWidth = MaxHeight = 4000;
            var value = $("#beacon_target").val();
            var originalWidth = image[0].naturalWidth;
            var originalHeight = image[0].naturalHeight;
            $('#image1 .cropbox').imgAreaSelect({
                handles: true,
                fadeSpeed: 200,
                onSelectChange: updateCoords,
                imageHeight: originalHeight,
                imageWidth: originalWidth,
                aspectRatio: AspectRatio,
                maxHeight: MaxHeight,
                maxWidth: maxWidth,
                parent: '#crop_parent_1'
            });
            $("#image1-dim").html('width: 0, height: 0');
            $("#image1-dim").show();

        });

        function updateCoords(img, selection) {
            if (!selection.width || !selection.height)
                return;

            $('#x').val(selection.x1);
            $('#y').val(selection.y1);
            $('#w').val(selection.width);
            $('#h').val(selection.height);
            $("#image1-dim").html('width: ' + selection.width + ', height: ' + selection.height);
        }

        $(document).on('click', '#startCrop2', function (e) {

            var image = $('#image2 .cropbox');
            AspectRatio = '2.13:1';
            maxWidth = MaxHeight = 4000;
            var value = $("#beacon_target").val();
            // console.log(image[0]);
            var originalWidth = image[0].naturalWidth;
            var originalHeight = image[0].naturalHeight;
            $('#image2 .cropbox').imgAreaSelect({
                handles: true,
                fadeSpeed: 200,
                onSelectChange: updateCoords2,
                imageHeight: originalHeight,
                imageWidth: originalWidth,
                aspectRatio: AspectRatio,
                maxHeight: MaxHeight,
                maxWidth: maxWidth,
                parent: '#crop_parent_2'
            });
            $("#image2-dim").html('width: 0, height: 0');
            $("#image2-dim").show();

        });


        function updateCoords2(img, selection) {
            if (!selection.width || !selection.height)
                return;

            $('#x2').val(selection.x1);
            $('#y2').val(selection.y1);
            $('#w2').val(selection.width);
            $('#h2').val(selection.height);
            $("#image2-dim").html('width: ' + selection.width + ', height: ' + selection.height);
        }

        $(document).on('click', '#startCrop3', function (e) {

            var image = $('#image3 .cropbox');
            AspectRatio = '2.13:1';
            maxWidth = MaxHeight = 4000;
            var value = $("#beacon_target").val();
            // console.log(image[0]);
            var originalWidth = image[0].naturalWidth;
            var originalHeight = image[0].naturalHeight;
            $('#image3 .cropbox').imgAreaSelect({
                handles: true,
                fadeSpeed: 200,
                onSelectChange: updateCoords3,
                imageHeight: originalHeight,
                imageWidth: originalWidth,
                aspectRatio: AspectRatio,
                maxHeight: MaxHeight,
                maxWidth: maxWidth,
                classPrefix: 'image3-imageareaselect imgareaselect'
                // parent:'#crop_parent_3'
            });
            $("#image3-dim").html('width: 0, height: 0');
            $("#image3-dim").show();

        });

        $(document).on('change', '#additional_image', function (e) {

            $('.image3-imageareaselect').addClass('hidden');

        });

        function imageMessage(message) {
            $('#imageMessage .message-content').html(message);
            $('#imageMessage').modal('show');
        }

        function updateCoords3(img, selection) {
            if (!selection.width || !selection.height)
                return;

            $('#x3').val(selection.x1);
            $('#y3').val(selection.y1);
            $('#w3').val(selection.width);
            $('#h3').val(selection.height);
            $("#image3-dim").html('width: ' + selection.width + ', height: ' + selection.height);
        }

        function contentHandeling(divClone) {
            $(".additional_image.imgareaselect-outer, .additional_image.imgareaselect-handle").addClass('hidden');

            $(".image3-imageareaselect.imgareaselect-selection").parent().addClass('hidden');
            $("#image_additional_div").replaceWith(divClone.clone());
        }

        $(document).ready(function () {

            var form = $('#form_events');
            var errorHandler1 = $('.errorHandler', form);
            var successHandler1 = $('.successHandler', form);

            $(".sortable").sortable({
                connectWith: '.sortable',
                iframeFix: false,
                items: 'div.grid',
                opacity: 0.8,
                helper: 'original',
                revert: true,
                forceHelperSize: true,
                placeholder: 'sortable-box-placeholder round-all',
                forcePlaceholderSize: true,
                tolerance: 'pointer'
            });

            $("#datatable-example.sortable tbody").sortable({
                helper: fixHelperModified,
                stop: updateIndex
            }).disableSelection();

            $("#datatable-example2.sortable tbody").sortable({
                helper: fixHelperModifiedTiers,
                stop: updateIndexTiers
            }).disableSelection();

            // $("#form_events").validate({
            //
            //     ignore: [],
            //     debug: true,
            //     rules: {
            //         mobile: {
            //             // required: true,
            //             minlength: 9,
            //             number: true,
            //             maxlength: 13
            //         },
            //         email: {
            //             // required: true,
            //             email: true
            //         },
            //         url_link2: {
            //             url: true
            //         },
            //         description_english: {
            //             // required: true,
            //         },
            //         description_arabic: {
            //             // required: true,
            //         },
            //         // logo_image: {
            //         //     required: true,
            //         // },
            //     },
            //     messages: {},
            //     invalidHandler: function (event, validator) { //display error alert on form submit
            //         successHandler1.hide();
            //         errorHandler1.show();
            //     },
            //
            //     errorElement: "span",
            //     errorClass: "help-block",
            //     highlight: function (element, errorClass, validClass) {
            //         // Only validation controls
            //         if (!$(element).hasClass('novalidation')) {
            //             $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //
            //         }
            //     },
            //     unhighlight: function (element, errorClass, validClass) {
            //         // Only validation controls
            //         if (!$(element).hasClass('novalidation')) {
            //             $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //         }
            //     },
            //     errorPlacement: function (error, element) {
            //         if (element.hasClass('select2-hidden-accessible')) {
            //             error.insertAfter(element.closest('.has-error').find('span.select2'));
            //         }
            //         else if (element.parent('.input-group').length) {
            //             error.insertAfter(element.parent());
            //         }
            //         else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
            //             error.insertAfter(element.parent().parent());
            //         }
            //         else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            //             error.appendTo(element.parent().parent());
            //         }
            //         else {
            //             error.insertAfter(element);
            //         }
            //     },
            //     submitHandler: function (form) {
            //         successHandler1.show();
            //         errorHandler1.hide();
            //         // submit form
            //         //$('#form2').submit();
            //         // setTimeout(function () {
            //         //     form.submit();
            //         // }, 500);
            //         jQuery('#progressbar').show();
            //     },
            // });

        });

        //tiers datatable
        var tiers_table = $('#datatable-example2').DataTable({
            stateSave: true,
            // processing: true,
            serverSide: true,
            'pageLength': 25,
            // "order": [[1, "desc"]],
            "order": [],
            autoWidth: false,
            processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
            ajax: {
                "url": "{{ URL::to('admin/tiers/datatable?event_id='.($event ? $event->id : 0)) }}",
                "type": "POST"
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'title_english', name: 'title_english'},
                {data: 'title_arabic', name: 'title_arabic'},
                {data: 'price', name: 'price'},
                {data: 'total_quantity', name: 'total_quantity'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0 , 6],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4, 5,6],
                    "orderable": false
                },
                {
                    "className": "dt-center",
                    "targets": [3, 4, 5],
                },
                {
                    "width": "4%",
                    "targets": 0,
                },
                { className: "hide_column", "targets": [ 0 ] }

            ]
        });

        // jQuery('#form_events').submit(function (event) {
        $(document).on('submit', '#form_events', function (event) {
            var pass = true;
            var pass2 = true;
            var pass3 = true;
            var pass4 = true;
            //some validations

            // var tiers_count = tiers_table.data().count();
            //
            // if (tiers_count > 0) {
            //
            //     if (pass == false) {
            //         return false;
            //     }
            //     form = this;
            //     jQuery('.btn-submit').attr('disabled', 'disabled');
            //     event.preventDefault();
            //     // setTimeout(function () {
            //     //     form.submit();
            //     // }, 500);
            //     jQuery('#progressbar').show();
            //
            //
            // } else {
            //     fileSizeMessage('At least one tier must add');
            //     return false;
            //
            // }
            if(arabicDateRequired() == false)
            {
                pass3 =  false;
                fileSizeMessage('Arabic date must be entered along with Arabic time');
            }

            var map_address = $('#map_address').val();
            if(map_address != '')
            {
                var venue_text_english = $('#venue_text_english').val();
                var venue_text_arabic = $('#venue_text_arabic').val();

                console.log(venue_text_english,venue_text_arabic, map_address);
                if(venue_text_english == '' || venue_text_arabic == '')
                {
                    pass3 =  false;
                    fileSizeMessage('Venues information must be entered along with map address');
                }
            }


            var logo_image = $("#images_div #image1 img").attr('src');

            if (!logo_image) {
                pass2 = false;
                fileSizeMessage('Logo Image is required');

            } else {
                var LogoImage = checkCropOk("1:1", "w,h", $('#image1 .cropbox'));
                var CoverImage = checkCropOk("2.13:1", "w2,h2", $('#image2 .cropbox'));
                var AdditionalImage = checkCropOk("2.12:1", "w3,h3", $('#image3 .cropbox'));
                // console.log(LogoImage, CoverImage);
                if (LogoImage == true && CoverImage == true && AdditionalImage == true) {
                    pass2 = true;
                    //some validations

                    if (pass2 == false) {
                        return false;
                    }

                    if(pass3 ==  false)
                    {
                        return false;
                    }

                    if(pass4 ==  false)
                    {
                        return false;
                    }
                    
                    form = this;
                    // jQuery('#btn-submit').attr('disabled', 'disabled');
                    event.preventDefault();
                    setTimeout(function () {
                        form.submit();
                    }, 500);
                    jQuery('#progressbar').show();

                    return true;
                } else {

                    fileSizeMessage('Picture(s) proportions are incorrect. Please upload correct picture');

                    return false;
                }
            }

            // if (pass == false || pass2 == false) {
            //     return false;
            // }
            // form = this;
            // jQuery('.btn-submit').attr('disabled', 'disabled');
            // event.preventDefault();
            // setTimeout(function () {
            //     form.submit();
            // }, 500);
            // jQuery('#progressbar').show();


            return false;
        });


        var fixHelperModified = function (e, tr) {
                console.log(e, tr)
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
            updateIndex = function (e, ui) {

                $('#datatable-example.table tbody tr:odd').removeClass('alt-row');
                $('#datatable-example.table tbody tr:even').removeClass().addClass('alt-row');
                var ids = [($('#datatable-example.table tbody tr').size() - 1)];
                var idsIndex = 0;
                $('#datatable-example.table tbody tr').each(function (i) {
                    rcheckbox = $(this).find(':checkbox');
                    if (rcheckbox.length > 0) {
                        ids[idsIndex] = rcheckbox.val();
                        idsIndex++;
                    }
                });
                rids = ids.join(',');
                var reorderUrl = "{{ URL::route('admin.additionalimages.reorder')}}";
                console.log(reorderUrl)
                $.post(
                    reorderUrl,
                    {
                        ids: rids
                    },
                    function (response) {

                    }, 'json');

            };


        var fixHelperModifiedTiers = function (e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
            updateIndexTiers = function (e, ui) {

                $('#datatable-example2.table tbody tr:odd').removeClass('alt-row');
                $('#datatable-example2.table tbody tr:even').removeClass().addClass('alt-row');
                var ids = [($('#datatable-example2.table tbody tr').size() - 1)];
                var idsIndex = 0;
                $('#datatable-example2.table tbody tr').each(function (i) {
                    rcheckbox = $(this).find(':checkbox');
                    if (rcheckbox.length > 0) {
                        ids[idsIndex] = rcheckbox.val();
                        idsIndex++;
                    }
                });
                rids = ids.join(',');
                var reorderUrl = "{{ URL::route('admin.tiers.reorder')}}";
                console.log(reorderUrl)

                $.post(
                    reorderUrl,
                    {
                        ids: rids
                    },
                    function (response) {

                    }, 'json');

            };

        $(document).on('submit', '.deleteTier', function () {
            // var ret = confirm("Are you sure you want to delete?");
            // if (ret) {
            // $("#progressbar").show();
            // return true;
            // }
            // return false;
            $("#progressbar").show();
            return false;
        });
        var Deleteform = '';
        $(document).on('click', '.deleteTier', function () {
            // var form = $('#form_events');
            Deleteform = this;
            tierid = $(this).attr('data-tierid');
            // var urlrequest = $(this).attr('data-url');
            $.ajax({
                beforeSend: function () {
                    // overlay_ajax();
                },
                dataType: 'JSON',
                url: base_path + "/admin/events/checkIfTierHasRequestOrTicket",
                type: 'POST',
                data: {
                    // _token: $('input[name=_token]').val(),
                    tierid: tierid,
                }
            })
                .done(function( result ) {
                    if(result == true){
                        delErrorModal("Tiers has running campaigns and can’t be deleted");
                    }else {

                            var formData = new FormData();
                            formData.append('id', tierid);
                            $.ajax({
                                url: "{{ URL::route('admin.tiers.deltier')}}",
                                type: "POST",
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (data, textStatus, jqXHR) {
                                    if ($.isEmptyObject(data.error)) {

                                        $('#datatable-example2').DataTable().ajax.reload();
                                    }
                                }
                            });

                    }

                });

            return false;

            // return false;
        });
        function delErrorModal(message, action) {
            $('#eventStatusDeleted .message-content').text(message);
            $('#eventStatusDeleted').modal('show');
        }

        function approveModal(message, action) {
            $('#deleteMessage .message-content').text(message);
            $('#deleteMessage').modal('show');
        }

        $(document).on('click', '.submitDeleteModal', function() {
            $("#progressbar").show();
            Deleteform.submit();

        });

        function checksOnArabicTime()
        {
            if($("#time_from_arabic_type").val().length > 0 || $("#time_from_arabic_hours").val().length > 0 || $("#time_from_arabic_minutes").val().length > 0 || $("#time_to_arabic_type").val().length > 0 || $("#time_to_arabic_hours").val().length > 0 || $("#time_to_arabic_minutes").val().length > 0)
            {
                $("#time_from_arabic_type,#time_from_arabic_hours,#time_from_arabic_minutes,#time_to_arabic_type,#time_to_arabic_hours,#time_to_arabic_minutes").attr("required", true);
            }else{
                $("#time_from_arabic_type,#time_from_arabic_hours,#time_from_arabic_minutes,#time_to_arabic_type,#time_to_arabic_hours,#time_to_arabic_minutes").attr("required", false);
            }

            $("#time_from_arabic_type,#time_from_arabic_hours,#time_from_arabic_minutes,#time_to_arabic_type,#time_to_arabic_hours,#time_to_arabic_minutes").on('change',function(e){
                if($("#time_from_arabic_type").val().length > 0 || $("#time_from_arabic_hours").val().length > 0 || $("#time_from_arabic_minutes").val().length > 0 || $("#time_to_arabic_type").val().length > 0 || $("#time_to_arabic_hours").val().length > 0 || $("#time_to_arabic_minutes").val().length > 0)
                {
                    $("#time_from_arabic_type,#time_from_arabic_hours,#time_from_arabic_minutes,#time_to_arabic_type,#time_to_arabic_hours,#time_to_arabic_minutes").attr("required", true);
                }else{
                    $("#time_from_arabic_type,#time_from_arabic_hours,#time_from_arabic_minutes,#time_to_arabic_type,#time_to_arabic_hours,#time_to_arabic_minutes").attr("required", false);
                }
            });
        }

        function checksOnArabicDate()
        {
            if($("#year_from_arabic").val().length > 0 || $("#month_from_arabic").val().length > 0 || $("#day_from_arabic").val().length > 0 || $("#year_to_arabic").val().length > 0 || $("#month_to_arabic").val().length > 0 || $("#day_to_arabic").val().length > 0)
            {
                $("#year_from_arabic,#month_from_arabic,#day_from_arabic,#year_to_arabic,#month_to_arabic,#day_to_arabic").attr("required", true);
            }else{
                $("#year_from_arabic,#month_from_arabic,#day_from_arabic,#year_to_arabic,#month_to_arabic,#day_to_arabic").attr("required", false);
            }

            $("#year_from_arabic,#month_from_arabic,#day_from_arabic,#year_to_arabic,#month_to_arabic,#day_to_arabic").on('change',function(e){
                if($("#year_from_arabic").val().length > 0 || $("#month_from_arabic").val().length > 0 || $("#day_from_arabic").val().length > 0 || $("#year_to_arabic").val().length > 0 || $("#month_to_arabic").val().length > 0 || $("#day_to_arabic").val().length > 0)
                {
                    $("#year_from_arabic,#month_from_arabic,#day_from_arabic,#year_to_arabic,#month_to_arabic,#day_to_arabic").attr("required", true);
                }else{
                    $("#year_from_arabic,#month_from_arabic,#day_from_arabic,#year_to_arabic,#month_to_arabic,#day_to_arabic").attr("required", false);
                }
            });
        }

        function arabicDateRequired()
        {
            var pass = true;
            var arabic_date_exist = ($("#year_from_arabic").val().length > 0 && $("#month_from_arabic").val().length > 0 && $("#day_from_arabic").val().length > 0 && $("#year_to_arabic").val().length > 0 && $("#month_to_arabic").val().length > 0 && $("#day_to_arabic").val().length > 0 ) ? true : false;
            var arabic_time_exist = ($("#time_from_arabic_type").val().length > 0 && $("#time_from_arabic_hours").val().length > 0 && $("#time_from_arabic_minutes").val().length > 0 && $("#time_to_arabic_type").val().length > 0 && $("#time_to_arabic_hours").val().length > 0 && $("#time_to_arabic_minutes").val().length > 0 ) ? true : false;

            if(arabic_time_exist)
            {
                if(arabic_date_exist)
                {
                    pass = true;
                }else{
                    pass = false;
                }
            }
            return pass;
        }
    </script>

@endsection
