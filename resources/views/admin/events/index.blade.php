@extends('layouts.app')
@section('content')
    <style type="text/css">
        table#datatable-example2, table#datatable-example {
            width: 100% !important;
        }
    </style>
    <div class="page-title">
        <center>
            <h3>Events أحداث</h3>
            <br>
            <span class="legends"><strong style="color: black">A:</strong> الحضور</span>
            <span class="legends"><strong style="color: black">T:</strong> التذاكر المتاحة</span>
            <span class="legends"><strong style="color: black">S:</strong> التذاكر المباعة</span>
            <span class="legends"><strong style="color: black">R:</strong> التذاكر المطلوبة</span>

            <span class="legends legends_circle"><i class="fa fa-circle selling_on_icon legend_icon"></i><span
                        class="legend_text">متاح للبيع</span></span>
            <span class="legends"><i class="fa fa-circle-o selling_of_icon legend_icon"></i><span class="legend_text">مغلق للبيع</span></span>

        </center>
    </div>
    <br>

    <div class="tab-content">
        <div style="margin-top: 15px">
        </div>
        <div class='control pull-left'  style="margin-left: 20px;margin-bottom: 0">
            <div class="nav-navbar-center">
                <ul class="nav nav-pills">
                    <li id="published_tab" style="margin-right: 7px"><a href="#publish_events" data-toggle="tab">Published<span class="heading_arabic" style="margin-left: 10px">نشرت</span><span class="heading_arabic" style="display: inline-block;margin-left: 10px">({{$publish_count}})</span></a></li>
                    <li id="unpublished_tab" style="margin-right: 7px"><a href="#unpublish_events " data-toggle="tab">Unpublished<span class="heading_arabic" style="margin-left: 10px">غير منشورة</span><span class="heading_arabic"style="display: inline-block;margin-left: 10px">({{$unpublish_count}})</span></a>
                    </li>
                </ul>
            </div>
        </div>

        <div id="publish_events" class="tab-pane fade">
            <div class="grid simple vertical green mobile-scroll">
                <div class="grid-title no-border"></div>
                <div class="grid-body no-border">
                    @if(Auth::user()->can("events-create"))

                        <div class="pull-left markaction">
                            <a href="{{ URL::to('admin/events/create') }}" class="btn btn-primary">Add إضافة مناسبة +</a>

                        </div>
                    @endif
                        <br><br><br>
                    {{--@if(Auth::user()->can("events-destroy"))--}}

                        {{--<div class="pull-right markaction">--}}
                            {{--<a id="del" href="javascript:void(0)" class="btn btn-danger"--}}
                               {{--data-url="{{ URL::to('admin/events/bulkdelete') }}">Delete حذف</a>--}}
                        {{--</div><br><br><br>--}}
                    {{--@endif--}}

                    <table class="table table-hover table-condensed " id="datatable-example">
                        <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable"
                                       data-set="#datatable-example .checkboxes"/></th>
                            <th>Id</th>
                            <th>
                                {{--<center> <br> </center>--}}
                            </th>
                            <th width="62%">
                                <center>المناسبة <br> Information</center>
                            </th>
                            <th>
                                <center>تاريخ النشر <br> Published</center>
                            </th>
                            <th>
                                <center>اخر تعديل <br> Last Edit</center>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div id="unpublish_events" class="tab-pane fade">
            <div class="grid simple vertical green mobile-scroll">
                <div class="grid-title no-border"></div>
                <div class="grid-body no-border">
                    {{--<div class="pull-right markaction">--}}
                        {{--@if(Auth::user()->can("events-destroy"))--}}
                            {{--<a id="del2" href="javascript:void(0)" class="btn btn-danger"--}}
                               {{--data-url="{{ URL::to('admin/events/bulkdelete') }}">Delete</a>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    <br><br><br>

                    <table class="table table-hover table-condensed" id="datatable-example2">
                        <thead>
                        <tr>
                            <th><input type="checkbox" class="group-checkable"
                                       data-set="#datatable-example2 .checkboxes"/></th>
                            <th>Id</th>
                            <th>
                                <center>شعار <br> Logo</center>
                            </th>
                            <th>
                                <center>شعار عنوان <br> Information</center>
                            </th>
                            <th>
                                <center>شعار نشرت <br> Published</center>
                            </th>
                            <th>
                                <center>شعار آخر تحديث <br> Last Edit</center>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


    </div>



    <!-- maps modal -->
    <div class="modal fade" id="eventStatusUnpublished" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
               <!--  <div class="modal-header">
                    <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Event</h4>
                </div> -->
                <div class="modal-body">

                    <center>
                        <p>
                            هذه المناسبة تحتوي تذاكر مباعة او طلبات سارية. يجب الغاء التذاكر والطلبات قبل ايقاف النشر
                        </p>
                        <p>
                            This event has valid tickets and/or pending requests. Cancel all valid tickets and/or pending requests before unpublishing.
                        </p>
                    </center>

                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK موافق</button>
                    </center>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="eventStatusDeleted" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Event</h4>
                </div> -->
                <div class="modal-body">
                    <center>
                        <p>
                            <!-- الحدث المحدد لديه تذاكر قيد التشغيل وطلبات مفتوحة. يجب أن يتم إلغاؤها أولاً ومن ثم يمكن أن تكون
                            الحملة غير مستغلة -->
                            لا يمكنك حذف مناسبة تحتوي على معلومات بيع او طلبات
                        </p>
                        <p>
                            <!-- Selected Event has running tickets and open requests. They has to be cancelled first and then
                            campaign can be deleted -->
                            You can't delete an event that has past transactions
                        </p>
                        
                    </center>

                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK موافق</button>
                    </center>
                </div>
            </div>

        </div>
    </div>

    {!! Form::open(['method' => 'POST','id'=> 'unpublishedForm', 'class'=>'deleteaction',"data-msg"=>"ايقاف نشر هذه المناسبة؟ <br> Unpublish this event now?"]) !!}
    {!! Form::close() !!}

    {{--{!! Form::open(array('route' =>["admin.event_requests.reject_status", 5], 'id'=>'unpublishedForm', 'role'=>'form')) !!}--}}

    {{--{!! Form::close() !!}--}}




    {!! Form::open(['method' => 'POST','class' => 'inline','id'=> 'bulkaction']) !!}
    <input type="hidden" value="" name="ids" id="ids"/>
    {!! Form::close() !!}



    <!-- maps modal -->
    <div class="modal fade" id="googleMapModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close clear_marker" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Map</h4>
                </div>
                <div class="modal-body">
                    {{--<input id="pac-input" class="controls pac-input" type="text" placeholder="Search Box"--}}
                    {{--style="width: 200px;">--}}
                    <div class="form-group" id="map_location" style="width: 100%;height: 400px;"></div>

                </div>

            </div>

        </div>
    </div>
    <!-- maps modal end -->



@endsection

@section('page_script')
    @parent

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAe4ISmDhVvG-BXiqwXI2EkL3GdDFDxgM&sensor=false&libraries=places"></script>



    <script type="text/javascript" language="javascript" class="init">

        function showMapLocation(lat, long) {

            var latlng = new google.maps.LatLng(lat, long);
            var map = new google.maps.Map(document.getElementById('map_location'), {
                center: latlng,
                zoom: 13
            });
            var marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: false,
                anchorPoint: new google.maps.Point(0, -29)
            });
            var infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', function () {
                var iwContent = '<div id="iw_container">' +
                    '<div class="iw_title"><b>Location</b> : Noida</div></div>';
                // including content to the infowindow
                infowindow.setContent(iwContent);
                // opening the infowindow in the current map and at the current marker location
                infowindow.open(map, marker);
            });

            // $('#googleMapModal').modal('show');

        }

        // google.maps.event.addDomListener(window, 'load', showMapLocation);

    </script>

    <script type="text/javascript" language="javascript" class="init">

        // $(window).load(function () {
        //     mapLocation();
        // });


        $(document).ready(function ($) {


            var event_type = '{{Request::get("source")}}';
            var event_id = '{{Request::get("id")}}';
            if (event_type == 'published') {
                $("#published_tab").addClass("active");
                $("#publish_events").addClass("active in");
                $("#unpublished_tab").removeClass("active");
            } else if (event_type == 'unpublished') {
                $("#unpublished_tab").addClass("active");
                $("#unpublish_events").addClass("active in");
                $("#published_tab").removeClass("active");
            } else {
                $("#published_tab").addClass("active");
                $("#publish_events").addClass("active in");
                $("#unpublished_tab").removeClass("active");
            }

            $('#published_tab').click(function (event) {
                history.pushState({}, null, "{{URL::route('admin.events.index')}}?source=published");
            });
            $('#unpublished_tab').click(function (event) {
                history.pushState({}, null, "{{URL::route('admin.events.index')}}?source=unpublished");
            });

            $('#datatable-example').DataTable({
                serverSide: false,
                'pageLength': 25,
                "order": [[1, "desc"]],
                processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
                ajax: {
                    "url": "{{ URL::to('admin/events/datatable') }}",
                    "type": "POST",
                    "data": {'type': 1}
                },
                columns: [
                    {data: 'check', name: 'check'},
                    {data: 'id', name: 'id'},
                    {data: 'logo_image', name: 'logo_image'},
                    {data: 'info', name: 'info'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_by', name: 'updated_by'},
                    {data: 'actions', name: 'actions'}
                ],
                "columnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0, 2, 6],
                        "searchable": false
                    },
                    {
                        "targets": [0, 1, 2, 3, 4, 5, 6],
                        "orderable": false
                    },
                    {
                        "className": "dt-center",
                        "targets": [4, 5]
                    }
                ]
            });

            $('#datatable-example2').DataTable({
                serverSide: false,
                'pageLength': 25,
                "order": [[1, "desc"]],
                processing: true,
                "language": {
                     "processing": "<img src='{{url('/')}}/assets/images/Webp.net-resizeimage.gif' />"
                },
                ajax: {
                    "url": "{{ URL::to('admin/events/datatable') }}",
                    "type": "POST",
                    "data": {'type': 0}
                },
                columns: [
                    {data: 'check', name: 'check'},
                    {data: 'id', name: 'id'},
                    {data: 'logo_image', name: 'logo_image'},
                    {data: 'info', name: 'info'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_by', name: 'updated_by'},
                    {data: 'actions', name: 'actions'}
                ],
                "columnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [0, 2, 6],
                        "searchable": false
                    },
                    {
                        "targets": [0, 1, 2, 3, 4, 5, 6],
                        "orderable": false
                    },
                    {
                        "className": "dt-center",
                        "targets": [4, 5]
                    }
                ]
            });
        });


        $(document).on('submit', '.unpublished', function () {
            // var ret = confirm("Are you sure you want to delete?");
            // if (ret) {
            // $("#progressbar").show();
            // return true;
            // }
            // return false;
            // $("#progressbar").show();
            return false;
        });
        // var form = '';
        $(document).on('click', '.unpublished', function () {
            var form = $('#form_events');
            eventid = $(this).attr('data-eventid');
            var urlrequest = $(this).attr('data-url');
            console.log(eventid);
            $.ajax({
                dataType: 'JSON',
                url: base_path + "/admin/events/checkIfEventHasRunningTicket",
                type: 'POST',
                data: {
                    // _token: $('input[name=_token]').val(),
                    eventid: eventid,
                },
                beforeSend: function () {
                    // overlay_ajax();
                },
                success: function (data) {
                    if (data.flag) {
                        $('#eventStatusUnpublished').modal('show');
                    } else {
                        // $("#progressbar").show();
                        $('#unpublishedForm').attr('action', urlrequest);

                        $('#unpublishedForm').submit();
                    }
                },
                error: function () {
                    // $('#modal-default').modal('show');
                },
                complete: function () {
                    // $('.box-body').unblock();
                }
            });

            // return false;
        });


        // $(document).on('submit', '.deleteEvent', function () {
        //     // var ret = confirm("Are you sure you want to delete?");
        //     // if (ret) {
        //     // $("#progressbar").show();
        //     // return true;
        //     // }
        //     // return false;
        //     $("#progressbar").show();
        //     return false;
        // });
        var Deleteform = '';
        $(document).on('click', '.deleteEvent', function () {
            $("#progressbar").show();
            // var form = $('#form_events');
            Deleteform = this;
            eventid = $(this).attr('data-eventid');
            // var urlrequest = $(this).attr('data-url');
            $.ajax({
                beforeSend: function () {
                    // overlay_ajax();
                },
                dataType: 'JSON',
                url: base_path + "/admin/events/checkIfEventHasRequestOrTicket",
                type: 'POST',
                data: {
                    // _token: $('input[name=_token]').val(),
                    eventid: eventid,
                }
            })
                .done(function( result ) {
                    if(result == true){
                        delErrorModal("Offer has running campaigns and can’t be deleted");
                    }else{
                        approveModal('هل انت متأكد من الحذف؟<br>Are you sure you want to delete ?');
                    }
                    $("#progressbar").hide();
                });

            return false;

            // return false;
        });
        function delErrorModal(message, action) {
            $('#eventStatusDeleted .message-content').html(message);
            $('#eventStatusDeleted').modal('show');
        }

        function approveModal(message, action) {
            $('#deleteEventMessage .message-content').html(message);
            $('#deleteEventMessage').modal('show');
        }

        $(document).on('click', '.submitEventDeleteModal', function() {
            $("#progressbar").show();
            Deleteform.submit();
        });
        //
        //
        //
        // $(document).on('click', '.markaction #delete', function (e) {
        //     e.preventDefault();
        //     bulkAction(this,'',true);
        // });
        //
        // var errorMessageStr = 'Nothing Selected';
        // var msg = "Are you sure you want to reject ?";
        // function bulkAction(element,count,confirm_reject){
        //     url = $(element).attr('data-url');
        //     var selectedCheckboxItems = $('#datatable-example'+count+' tbody :checkbox:checked').map(function () {
        //         return this.value;
        //     }).get();
        //
        //     if (selectedCheckboxItems.length > 0) {
        //         selectedItem = selectedCheckboxItems.join(',');
        //         selectedItems = selectedItem.replace('on,', '');
        //         delMsg = '';
        //
        //         if (confirm_reject) {
        //             delMsg = $(element).attr('data-msg');
        //
        //             if (delMsg) {
        //                 rejectBulkModal(delMsg);
        //             } else {
        //                 rejectBulkModal(msg);
        //             }
        //         }else{
        //
        //             $("#progressbar").show();
        //             $('#ids').attr('value', selectedItems);
        //             $('#bulkaction').attr('action', url);
        //             $('#bulkaction').submit();
        //         }
        //
        //     } else {
        //         showErrorMessage(errorMessageStr);
        //     }
        // }
        //
        //
        //
        // function showErrorMessage(msg) {
        //     var style = 'flat';
        //
        //     Messenger.options = {extraClasses: 'messenger-fixed messenger-on-top', theme: style};
        //     Messenger().post({
        //         message: msg,
        //         type: 'error',
        //         showCloseButton: true
        //     });
        // }

    </script>

@endsection