@extends('layouts.app')
@section('content')

<div class="page-title">
    <center>
        <h3>الأدوار - <span class="arabic_font">{{ $role ? __('edit.arabic') : __('add.arabic')}}</span></h3>
        <h3>Roles - <span class="semi-bold">{{ $role ? 'Edit' : 'Add'}}</span></h3>
    </center>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
{{--                <h4>{{ $role ? 'Edit' : 'Add'}} <span class="semi-bold">Role</span></h4>--}}
            </div>
            <div class="grid-body no-border"> <br>
                @if($errors->any())
                <div class="row">
                    <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                        <button data-dismiss="alert" class="close"></button>
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif
                {!! Form::model($role,['route' => $role ? ['admin.roles.update','id'=>$role->id] : 'admin.roles.store','method'=> $role ? 'PUT' : 'POST', 'id'=>'form_roles', 'novalidate'=>"novalidate"]) !!}
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('roles.form.field.name') }} <br> <span class="arabic_font">{{ __('roles.form.field.name_trans') }}</span> <span style="color:red"></span></label>

                    </div>
                    <!-- <span class="help">e.g. "Jonh Smith"</span> -->
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::text('name', null, ['class' => 'form-control','id'=>'name']) !!}
                        <span class="help-block"><span id="char_name_english">40</span> <?php echo __('general_characters_left'); ?></span>

                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('roles.form.field.display_name') }} <br> <span class="arabic_font">{{ __('roles.form.field.display_name_trans') }}</span> <span style="color:red"></span></label>

                    </div>
                    <!-- <span class="help">e.g. "Jonh Smith"</span> -->
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::text('display_name', null, ['class' => 'form-control','id'=>'display_name']) !!}
                        <span class="help-block"><span id="char_display_name_english">40</span> <?php echo __('general_characters_left'); ?></span>

                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3 col-sm-12 text-right">
                        <label class="form-label">{{ __('roles.form.field.description') }} <br> <span class="arabic_font">{{ __('roles.form.field.description_trans') }}</span> <span style="color:red"></span></label>
                    </div>
                    <!-- <span class="help">e.g. "Jonh Smith"</span> -->
                    <div class="input-with-icon  right col-md-6 col-sm-12">                                       

                        {!! Form::textarea('description', null, ['class' => 'form-control','rows'=>3,'id'=>'description']) !!}
                        <span class="help-block"><span id="char_description_english">1000</span> <?php echo __('general_characters_left'); ?></span>

                    </div>
                </div>
                
                <div class="form-group row">
                  <div class="col-md-3 col-sm-12 text-right">
                      <label class="control-label">{{ __('roles.form.field.assign_permissions') }} <br> <span class="arabic_font">{{ __('roles.form.field.assign_permissions_trans') }}</span> <span style="color:red"></span></label>

                  </div>
                  <div class="col-md-6 col-sm-12 text">
                    <label for='check_all_permitions' class="check">
                        <input name='check_all' type="checkbox" id="check_all_permitions"/> All Permissions جميع الصلاحيات
                    </label>
                  </div>
                  <div class="col-md-2 col-sm-12">
                    <a href="{{ URL::to('admin/permissions')}}" class='btn btn-primary'>Reload Permission إعادة الصلاحيات</a>
                  </div>
                </div>
                <?php foreach ($permissionsArray as $resource => $actions ) { ?>
                            <div class="form-group row">
                                <?php // \Zend\Debug\Debug::dump( $resource);die();?>
                                <label class="col-md-3 col-xs-12 text-right control-label"><?php echo (array_key_exists($resource,$readable))?$readable[$resource]:$resource; ?> </label>
                                <div class="col-md-8 col-xs-12">
                                    <?php foreach ($actions as $key => $value) { 
                                        $arr = array();
                                        ?>
                                        <div class="col-md-2">                                    
                                            <label class="check">
                                                <input type="checkbox" <?php echo array_key_exists($key,$role_permissions)? 'checked': '' ?> name="permissions[<?php echo  $key; ?>]" class="role_check"/> <?php echo $value; ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                <?php } ?>
                <div class="form-actions">  
                    
                    <div>
                        <a href="{{URL::to('admin/roles')}}" class="btn btn-default">Cancel إلغاء</a>
                        {!! Form::submit($role ? 'Update تحديث' : 'Save حفظ', ['class' => 'btn btn-primary pull-right','id' => 'btn-submit']) !!}

                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection
@section('page_script')
@parent
<script>
    jQuery('#form_roles').submit(function (event) {
        var pass = true;
        //some validations

        if (pass == false) {
            return false;
        }
        form = this;
        jQuery('#btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();

        return true;
    });
    $(document).ready(function(){
        $('#check_all_permitions').bind('change',function(){
            if(this.checked){
                $('.role_check').prop('checked',true);
            } else {
                $('.role_check').prop('checked',false);
            }
        });
        $("#name").keyup(function (event) {
            updateFeatureCharLimitNum('name', 'char_name_english', 40);
        });
        $("#display_name").keyup(function (event) {
            updateFeatureCharLimitNum('display_name', 'char_display_name_english', 40);
        });
        $("#description").keyup(function (event) {
            updateFeatureCharLimitNum('description', 'char_description_english', 1000);
        });
        updateFeatureCharLimitNum('name', 'char_name_english', 40);
        updateFeatureCharLimitNum('display_name', 'char_display_name_english', 40);

        updateFeatureCharLimitNum('description', 'char_description_english', 1000);


    });
</script>

@endsection
