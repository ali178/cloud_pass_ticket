@extends('layouts.app')
@section('content')


<div class="page-title">
    <center>
        <h3>Roles <span class="arabic_font">الأدوار</span></h3>
    </center>
</div>


<div class="tab-content">
    <div class="tab-pane fade active in">
        <div class='control' style="margin-left: 20px;margin-top: 0;margin-bottom: 0;">
    
    <a href="{{ URL::to('admin/roles/create') }}" class="btn btn-primary">Add إضافة +</a>
    
    <div class="pull-right markaction">
        @if(Auth::user()->can("roles-destroy"))
            <a id="del" href="javascript:void(0)" class="btn btn-danger" data-url="{{ URL::to('admin/roles/bulkdelete') }}">Delete حذف</a>
        @endif
    </div>
</div>
<br>
<div class="grid simple vertical green mobile-scroll">
    <div class="grid-title no-border">
        {{--<h4>All <span class="semi-bold">Roles</span></h4>--}}
    </div>
    <div class="grid-body no-border">
        <table class="table table-hover table-condensed" id="datatable-example" >
            <thead>
                <tr>
                    <th><input type="checkbox" class="group-checkable" data-set="#datatable-example .checkboxes" /></th>
                    <th>Id</th>
                    <th>{!! __('roles.table.name') !!}</th>
                    <th>{!! __('roles.table.display_name') !!}</th>
                    <th></th>

                </tr>
            </thead>
        </table>
    </div>
</div>
    </div>
</div>
{!! Form::open([
'method' => 'POST',
'class' => 'inline',
'id'=> 'bulkaction'
]) !!}
<input type="hidden" value="" name="ids" id="ids" />
{!! Form::close() !!} 
@endsection

@section('page_script')
@parent
<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function ($) {

        $('#datatable-example').DataTable({
            serverSide: false,
            'pageLength': 25,
            "order": [[1, "desc"]],
            ajax: {
                "url": "{{ URL::to('admin/roles/datatable') }}",
                "type": "POST"
            },
            columns: [
                {data: 'check', name: 'check'},
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'display_name', name: 'display_name'},
                {data: 'actions', name: 'actions'}
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [0,4],
                    "searchable": false
                },
                {
                    "targets": [0, 1, 2, 3, 4],
                    "orderable": false
                }
            ]
        });
    });
</script>

@endsection