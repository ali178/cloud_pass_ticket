@extends('layouts.app')
@section('content')

<style type="text/css">
    .page-title .title_arabic{
        margin-bottom: 0px;
    }
    .page-title .title_english{
        margin-top: 0px;
    }
</style>

<div class="page-title">
    <h3 class="title_arabic"><span class="arabic_font">{{ __('settings.form.title.arabic') }}</span> - <span class="semi-bold"><span class="arabic_font">{{ $setting ? __('edit.arabic') : __('add.arabic')}}</span></span></h3>
    <h3 class="title_english">{{ __('settings.form.title.english') }} - <span class="semi-bold">{{ $setting ? __('edit.english') : __('add.english')}}</span></h3>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border"></div>
            <div class="grid-body no-border"> <br>
                
                @if($errors->any())
                <div class="row">
                    <div class="alert alert-error col-md-6 col-sm-12 col-md-offset-3">
                        <button data-dismiss="alert" class="close"></button>
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif

                {!! Form::model($setting,['route' => ['admin.settings.update','id'=>$setting->id],'method'=> 'PUT', 'id'=>'form_settings']) !!}

                
                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.phone') }} <br> <span class="arabic_font">{{ __('settings.form.field.phone_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('phone', null, ['class' => 'form-control','onkeypress'=>'return isNumberKey(event)', 'maxlength'=>'15']) !!}

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.email') }} <br> <span class="arabic_font">{{ __('settings.form.field.email_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::email('email', null, ['class' => 'form-control']) !!}

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.copyright_english') }} <br> <span class="arabic_font">{{ __('settings.form.field.copyright_english_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('copyright_english', null, ['class' => 'form-control']) !!}

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.copyright_arabic') }} <br> <span class="arabic_font">{{ __('settings.form.field.copyright_arabic_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">                                       
                            {!! Form::text('copyright_arabic', null, ['style' => 'direction: rtl','class' => 'form-control']) !!}

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3 col-sm-12 text-right">
                            <label class="form-label">{{ __('settings.form.field.vat') }} <br> <span class="arabic_font">{{ __('settings.form.field.vat_trans') }}</span> <span style="color:red"></span></label>
                        </div>
                        <div class="input-with-icon  right col-md-6 col-sm-12">
                             {!! Form::input('number','vat_value', null, ['class' => 'form-control','min'=>'0','max'=>'99999']) !!}

                        </div>
                    </div>

                    <div class="form-actions" style="padding-top: 20px; padding-bottom: 15px; text-align: right;">
                        <button type="submit" class="btn btn-primary" id="btn-submit">{!! $setting ? __('reasons.form.update_english').' / <span class="arabic_font">'.__('reasons.form.update_arabic').'</span>' : __('reasons.form.save_english').' / <span class="arabic_font">'.__('reasons.form.save_arabic').'</span>' !!} </button>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection
@section('page_script')
@parent
<script>
    jQuery('#form_settings').submit(function (event) {
        return pass == false ? false : true ;
        form = this;
        jQuery('#btn-submit').attr('disabled', 'disabled');
        event.preventDefault();
        setTimeout(function () {
            form.submit();
        }, 500);
        jQuery('#progressbar').show();
        return true;
    });
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }


</script>

@endsection
