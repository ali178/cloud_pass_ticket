@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{--{{ config('app.name') }}--}}
            <span style="font-style: italic">Ticket Pass</span>
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <footer style="color: white">
                <div>
                    <center>
                        <p style="font-size: 12px;">Developed By <span class="arabic_font">ريتز كارلتون</span></p>
                        <img class="img-responsive" src="http://acclivousbyte.com/ticketing/public/assets/images/5W2H.png" style="width: auto;height: 20px; display: inline;">
                    </center>
                </div>

            </footer>
        @endcomponent
    @endslot
@endcomponent
