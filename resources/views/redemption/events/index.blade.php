@extends('redemption.layout')
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container">
            <div class="elevation-3dp">
            @include('redemption.sections.header')
                <div class="listing-div">
                    <div class="card-div">
                        @foreach($events as $event)

                            <?php
                                $remaining = 0;
                                foreach ($event->tiers as $tier) {
                                    $price_type=$tier->checkFreeAndOpenOrFreeOny();
                                    $adminTicketCount = \Ticketing\Models\Issued_ticket::getTotalAdminIssuedSumNew(3,$tier->event_id,$tier->id);
                                    if($price_type=='free_and_unliminted_qty' || $price_type=='not_free_unlimited_qty'){
                                       $remainingQty=1;
                                    }else{
                                       $remainingQty=$tier->total_quantity - $tier->totaAvailable();
                                       $remainingQty=$tier->total_quantity - ($tier->totaAvailable() + $adminTicketCount);
                                   }

                                   $remaining = $remaining + $remainingQty;
                                }
                            ?>
                            @if($event->cover_image)
                                <?php echo View::make('site.events._image_card',['event'=>$event, 'source'=>'redemption','remaining'=>$remaining]); ?>
                            @else
                                <?php echo View::make('site.events._info_card',['event'=>$event, 'source'=>'redemption','remaining'=>$remaining]); ?>
                            @endif
                        @endforeach
                    </div>
                    <style type="text/css">
                        a, a:hover { color: inherit; }
                    </style>
                </div>
            </div>
        </div>
    </div>
@endsection