@extends('redemption.layout')
@section('content')
    <style type="text/css">
        .english_date_from{
            margin-right: -7px !important;
        }
        .english_date_to{
            margin-left: -15px !important;
        }

        .english_time_to{
            margin-left: -16px !important;
        }
        .english_time_from{
            margin-right: -11px !important;
            margin-left: 4px;
        }

        .dt_class{
            font-size: 0.9em;
        }

        .arabic_time_from{
            margin-left: -14px !important;
        }

        .margin_top_fix1,.margin_top_fix2{
            margin-right: -10px !important;
        }
        .main-col-container,.elevation-3dp{
            height: 100% !important;
        }

        .vip-tag{
            display: block;
            margin-bottom: -25px;
            text-align: right;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container">
            <div class="elevation-3dp">
            @include('redemption.sections.header')
                <div class="listing-div">
                    <div class="card-div">
                        <div class="row">
                            <div class="imgdd">
                                <center>
                                    <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px" style="margin-top: -15px;">
                                    <p style="font-weight: bold"> {{$event->title_arabic}}</p>
                                    <p style="font-weight: bold">{{$event->title_english}}</p>
                                </center>
                                <br>
                            </div>
                        </div>
                        <form  role="form" method="POST" action="{{ url('/redemption/events/'.$event->event_id_str) }}">
                            {{ csrf_field() }}
                            <div class="row brinfo">
                            <center>
                                <p style="margin-bottom: 0px;color: black;font-weight: bold">باركود</p>
                                <p style="color: black;font-weight: bold">Barcode</p>
                                <p class="clearfix"><span class="pull-left" style="font-size: 12px;">Write/Enter Barcode</span>
                                    <span class="pull-right"  style="font-size: 12px;">ادخل/اكتب الباركود</span></p>
                                    <input type="text" placeholder="E559849-T394893-34" class="form-control form-group br-input" name="ticket_no" maxlength="22">
                                    <div class="form-group has-feedback no-margin">
                                        @if ($errors->has('ticket_no'))
                                            <span class="help-block">
                                         {{ $errors->first('ticket_no') }}
                                         </span>
                                        @endif
                                    </div>

                                <button type="submit" class="b-search">Search ابحث</button>
                            </center>
                        </div>
                        </form>
                        <br>


                       @if(isset($flag))
                        @if($redemption_ticket && $redemption_ticket->redeem_status !=0)
                           <style type="text/css">
                               .main-col-container,.elevation-3dp{
                                    height: auto !important;
                                }

                           </style>
                            <div class="row" id="infodiv">
                                {{--<div class="alert-msg">--}}
                                    {{--<a href="javascript:void(0)" class="close-bx" onclick="hide_div()">&times;</a>--}}
                                    {{--<center>--}}
                                        {{--<p>Ticket Found Successfully.</p>--}}
                                        {{--<p>تم استرداد هذه التذكرة بالفعل</p>--}}
                                    {{--</center>--}}
                                {{--</div>--}}
                                <div class="alert-spnel">

                                    <div class="ticket-info">

                                        @if($issued_ticket->tag_as_vip)
                                            <span class="vip-tag">
                                                <img src="{{asset('assets/images/vip_tag.png')}}" style="width: auto; height: 25px;">
                                            </span>
                                        @endif
                                        <b>{{$redemption_ticket->ticket_no}}</b>
                                        <p style="font-size: 12px;color: #989797">{{date('d/m/Y H:m:A',strtotime($event->created_at))}}</p>
                                        <p style="font-size: 12px;color: #989797">Ordered by &nbsp;&nbsp;&nbsp;أمر بها</p>
                                        <p>{{$issued_ticket->customer->full_name}}</p>
                                        <p style="font-size: 12px;color: #989797">Mobile no.&nbsp;&nbsp;&nbsp; الهاتف المتحرك</p>
                                        <p>{{$issued_ticket->customer->mobile}}</p>
                                        <p style="font-size: 12px;color: #989797">Email&nbsp;&nbsp;&nbsp;البريد الإلكتروني</p>
                                        <p>{{$issued_ticket->customer->email}}</p>
                                    </div>
                                    <br>
                                    <div class="panel panel-default">
                                        <!-- <div class="panel-heading text-center" style="background:#{{$redemption_ticket->tier->header_color}} !important;">{{$redemption_ticket->tier->title_english}} &emsp; {{$redemption_ticket->tier->title_arabic}}</div> -->

                                        <header class="tier_header" style="background-color: #{{$redemption_ticket->tier->header_color}} !important;margin: -1px;">
                                                <span>{{$redemption_ticket->tier->title_english}}</span>
                                                <span class="arabic_font right">{{$redemption_ticket->tier->title_arabic}}</span>
                                            </header>
                                        <div class="panel-body">
                                            <div class="media">
                                               <!--  <div class="media-left">
                                                    <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px">
                                                </div> -->
                                                <div class="media-body" style="font-weight: bold;">
                                                    <center>
                                                        <img class="img-responsive" src="{{asset($event->logo_image)}}" width="50px" height="50px" >
                                                    </center>
                                                    <br>
                                                    <p class="media-heading text-center">{{$event->title_arabic}}</p>
                                                    <p class="text-center">{{$event->title_english}}</p>

                                                </div>
                                            </div>
                                            <center>
                                                <p class="clearfix">
                                                    <span>{{$event->description_english}}</span>
                                                    <br><br>
                                                    <span>{{$event->description_arabic}}</span>
                                                </p>
                                            </center>
                                            {{--<div class="dtbx">--}}

                                                 {{--<span class="pull-left">--}}
                                                     {{--{{date('d/m/Y',strtotime($event->from_date_english))}} - {{date('d/m/Y',strtotime($event->to_date_english))}}--}}
                                                 {{--</span>--}}
                                                      {{--<span class="pull-right">--}}
                                                          {{--{{$event->from_date_arabic}}  -  {{$event->to_date_arabic}}--}}

                                                 {{--</span>--}}



                                                 {{--<span class="pull-left">--}}

                                                    {{--{{date('h:m:A',strtotime($event->from_time_english))}} - {{date('h:m:A',strtotime($event->to_time_english))}}--}}
                                                 {{--</span>--}}

                                                 {{--<span class="pull-right">--}}
                                                     {{--{{$event->from_time_arabic}} -  {{$event->to_time_arabic }}--}}
                                                 {{--</span>--}}


                                            {{--</div>--}}
                                            <br>
                                            <?php echo View::make('site.events._event_date_times',['event'=>$event]); ?>
                                        </div>
                                    </div>

                                    @if($redemption_ticket && $redemption_ticket->redeem_status !=0)
                                        <center>
                                            {!! Form::open(["method" => "POST","route" => ["redemption.ticket.redeem", $event->event_id_str]]) !!}
                                                {!! Form::hidden('ticket_no', $redemption_ticket ? $redemption_ticket->ticket_no : null , ['class' => '']) !!}
                                                <button type="submit" class="b-search">Redeem استبدال</button>
                                            {!! Form::close() !!}
                                        </center>
                                    @endif
                                    
                                </div>
                            </div>

                        @else
                            <div class="row">
                                <div class="alert"  style="text-align: center; background-color: #FFCCFF">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <center>
                                      <span>Enter ticket id has already been redemeed</span>
                                      <br>
                                      <span class="arabic_font">أدخل معرف البطاقة قد تم استرداده بالفعل</span>
                                    </center>
                                </div>
                             </div>
                        @endif

                        @endif



                        @if(!isset($flag))
                        <style type="text/css">
                            .main-col-container,.elevation-3dp{
                                height: 100% !important;
                            }
                        </style>
                         <div class="row">
                            @if(Session::has('flash_message'))
                                <div class="alert"  style="text-align: center; background-color: #FFCCFF">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <center>
                                      <br>  {!! Session::get('flash_message') !!}
                                    </center>
                                </div>
                                @endif
                         </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function hide_div() {
           $('#infodiv').remove();
        }
    </script>
@endsection