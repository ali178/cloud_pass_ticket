<div class="card-div">
    <a href="{{ URL::route('events',['event_id'=>$event->id]) }}">
	<img class="img-responsive right-corner-image" src="{{asset('assets/images/event_status_'.$event->tag.'.png')}}" style="width: auto;height: 30px;">
    <div class="row card" style="height: auto;">
        <img class="img-responsive" src="{{asset($event->cover_image)}}" style="width: 100%;height: 100%; max-height: 402px;">

        <div class="middle">
        	<div class="middle-info-div">
        		<p class="middle_text">{{ date('d.m.y', strtotime($event->from_date_english)) }}<br>THE PARTY<br>To End All Parties</p>
        	</div>    
		</div>
      
    </div>

    {{--<div class="row" style="margin: 2% -15px 0px -15px">--}}
        {{--<hr class="card_bottom_line">--}}
        {{--<img class="img-responsive" src="{{asset('assets/images/card_bottom.png')}}" style="width: auto;height: auto;">--}}
    {{--</div>--}}
    </a>
</div>