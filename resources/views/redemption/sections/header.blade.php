<style>
	.open > .dropdown-menu {
		display: block;
		height: 40px;
	}

	.redemption-main-container {
	    position: relative;
	    text-align: center;
	    
	}

	.left-user-icon {
    position: absolute;
    top: 13px;
    left: 13px;
}
</style>

<div class="navbar">
	<div class="site_logo redemption-main-container">

		@if(Auth::guard('redemptions')->user())
			<div class="left-user-icon">
				<div class="profile-pic dropdown-toggle" id="profile-pic-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<img class="pull-left" src="{{asset('assets/images/user.png')}}" style="width: 39px;">
				</div>
				<ul class="dropdown-menu profile-pic-dropdown" aria-labelledby="profile-pic-dropdown">
					<li>
						<a class="dropdown-item" href="{{ url('/redemption/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }} خروج </a>
						<form id="logout-form" action="{{ url('/redemption/logout') }}" method="GET" style="display: none;">
							@csrf
						</form>
					</li>
				</ul>
			</div>
		@endif
		<center>
			<a href="{{url('/redemption')}}">
				<img src="{{asset('assets/images/samplebanner.png')}}">
			</a>
		</center>
	</div>
</div>

<!-- <style>
	.open > .dropdown-menu {
		display: block;
		height: 40px;
	}
</style>

<div class="ehead-top">
	<div class="text-center user">
		@if(Auth::guard('redemptions')->user())
		<div class="profile-pic dropdown-toggle" id="profile-pic-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<img class="pull-left" src="{{asset('assets/images/user.png')}}">
		</div>
		<ul class="dropdown-menu profile-pic-dropdown" aria-labelledby="profile-pic-dropdown">
			<li>
				<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</li>
		</ul>
		@endif
		<span>ا ب ت</span>
	</div>
</div> -->