@extends('redemption.layout')
@section('content')
<style type="text/css">
    .main-col-container,.elevation-3dp{
        height: 100% !important;
    }
</style>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container">
            <div class="elevation-3dp">
            @include('redemption.sections.header')
                <div class="listing-div" style="padding-top: 0px !important;">
                    <div class="card-div">

                        @if(Session::has('success'))
                            <div class="col-md-12">
                                <div class="alert callout callout-info alert-dismissable" style="margin-top: 10px; margin-bottom:10px"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success !</strong>&emsp;
                                    {{ Session::get('success') }}
                                </div>
                            </div>
                            <br>
                        @endif
                        <form  role="form" method="POST" action="{{ url('/redemption/login') }}">
                            {{ csrf_field() }}
                            <div class="row brinfo" style="margin-top: 22px;border: none">
                            <center>
                                <strong style="color: black">لوحة التحكم الفداء</strong><br>
                                <strong style="margin-bottom: 10px;display: inline-block;color: black">Redemption Control Panel</strong>

                            <!-- <p class="clearfix"><span class="pull-left">Email*</span>
                                    <span class="pull-right">الباركود</span></p>
                                <input type="text" placeholder="Email" class="form-control form-group br-input" name="email" value="{{ old('email') }}" required>
                                <div class="form-group has-feedback no-margin">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                     {{ $errors->first('email') }}
                                     </span>
                                   @endif
                                 </div> -->

                                 <p class="clearfix" style="font-size: 12px;margin-top: 22px"><span class="pull-left">Username*</span>
                                    <span class="pull-right">اسم المستخدم</span></p>
                                <input type="text"  class="form-control form-group br-input" name="username" value="{{ old('username') }}" required>
                                <div class="form-group has-feedback no-margin">
                                @if ($errors->has('username'))
                                    <span class="help-block" style="color: red">

                                        @if($errors->first('username') == 'These credentials do not match our records')
                                        <span>لا تتطابق بيانات الاعتماد هذه مع سجلاتنا</span><br>
                                        <span>These credentials do not match our records</span>
                                        @endif

                                     </span>
                                   @endif
                                 </div>


                                <p class="clearfix" style="font-size: 12px;"><span class="pull-left">Password*</span>
                                    <span class="pull-right">الكلمة مرور</span></p>
                                <input type="password" class="form-control form-group br-input" name="password" required>
                                <div class="form-group has-feedback no-margin">
                                    @if ($errors->has('password'))
                                        <span class="help-block" style="color: red">
                                       {{ $errors->first('password') }}
                                     </span>
                                    @endif
                                </div>



                                <button type="submit" class="b-search">Login دخول </button>
                            </center>



                        </div>
                        </form>
                    </div>

                </div>
            </div>


        </div>

    </div>
@endsection