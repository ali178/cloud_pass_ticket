<div class="header navbar navbar-inverse ">
  <div class="navbar-inner">
    <div class="header-quick-nav">

      <div class="pull-left">
        <div class="chat-toggler sm">
          <div class="profile-pic dropdown-toggle" id="profile-pic-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <img  src="http://www.fastrackerzkennel.com/wp-content/uploads/2014/03/male-placeholder-image.jpeg" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="35" height="35" />
            <div class="availability-bubble online"></div>
          </div>
          
          <ul class="dropdown-menu profile-pic-dropdown" aria-labelledby="profile-pic-dropdown">
            <li>
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}&nbsp;&nbsp;خروج</a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
          </ul>

        </div>

        <ul class="nav quick-section">
          <li class="quicklinks">
            <a href="javascript:void(0)">
              {{ ucwords(Auth::user()->name) }}
            </a>
          </li>
        </ul>

      </div>

      <div class="pull-right">
        <div class="form-group" style="text-align: right;margin: 11px">
          <img src="{{asset('assets/images/bottom_logo.png')}}" class="bb-logo" style="width: 30%;height: auto">
          <br/>
        </div>

      </div>

    </div>
  </div>
</div>