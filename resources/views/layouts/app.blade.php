<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ticket Pass</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}"/>

    <!-- Styles -->

    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-1.8.3.min.js')}}" type="text/javascript"></script>
    <link href="{{asset('css/jquery.fancybox.min.css')}}" rel="stylesheet" type="text/css">

    <style type="text/css">
       .loading {
            position: absolute;
            width: 100%;
            height: 2500px;
            z-index: 1000000;
            background-color: rgba(255, 255, 255, 0.5);
            top: 0;
            /*  opacity: 0.6;*/
            display: none;
            text-align: center;
        }
        .loadingimage {

            color: #0aa699;

            position: fixed;
            top: 300px;
            left: 50%;
            font-size: 20px!important;
        }
    </style>

</head> <script type="text/javascript">
    var base_path = '{{URL::to('/')}}';
</script>
<body class="horizontal-menu" >
    <div class="page-container row-fluid">
        @if(Auth::check())
            @include('layouts.header')
        @endif
        <div class="page-content">
            @if(Auth::check())
                @include('layouts.top_navigation')
                <div id="crop_parent_1"></div>
                <div id="crop_parent_2"></div>
                <div id="crop_parent_3"></div>
                <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>

                <!-- delete modal Bulk popup -->
                <div id="deleteBulkMessage" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <p class="message-content"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary submitDeleteBulkModal" data-dismiss="modal">Yes نعم</button>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">No لا</button>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- end daleel popup Bulk modal -->

                <!-- delete modal popup -->
                <div id="deleteMessage" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <center>
                                    <p class="message-content"></p>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary submitDeleteModal" data-dismiss="modal">Yes نعم</button>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">No لا</button>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- end daleel popup modal -->

                <!-- delete modal popup -->
                <div id="deleteEventMessage" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <center>
                                <p class="message-content"></p>
                                    
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary submitEventDeleteModal" data-dismiss="modal">Yes نعم</button>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">No لا</button>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- end daleel popup modal -->

                <div id="progressbar" class="loading">
                    <i class="fa fa-refresh fa-spin loadingimage"></i>
                </div>

                <!-- delete modal popup -->
                <div id="deleteMessage1" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <center>
                                    <p class="message-content"></p>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary submitDeleteModal1" data-dismiss="modal">Yes نعم</button>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">No لا</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end daleel popup modal -->
                <div id="cropInfoModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px ">
                            <div class="modal-body info-modal">
                                <p class="message-content" style="display: -webkit-inline-box;"></p>
                                <!-- <p>Please Upload Both (Device/Web) images in proper proportion</p> -->
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-default" data-dismiss="modal">Ok موافق</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end crop info modal -->


                <!-- image size error modal popup -->
                <div id="fileSizeMessage" class="modal fade fileSizeModel" style="background: rgba(0, 0, 0,0.2);">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px ">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <center>
                                    <p class="message-content"></p>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" data-dismiss="modal" >Ok موافق</button>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- end daleel popup modal -->

                <div id="imageMessage" class="modal fade" style="background: rgba(0, 0, 0,0.2);">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px ">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <center>
                                    <p class="message-content"></p>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" onclick="$('#imageMessage').modal('toggle');">Ok موافق</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="delTierMessage" class="modal fade" style="background: rgba(0, 0, 0,0.2);">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px ">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <p class="message-content">Are you sure you want to delete?</p>
                            </div>
                            <div class="modal-footer">
                                <button id="delButton"  type="submit" class="btn btn-primary" data-dismiss="modal" onclick="delTier()">Yes نعم</button>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">No لا</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="delImageMessage" class="modal fade" style="background: rgba(0, 0, 0,0.2);">
                    <div class="modal-dialog">
                        <div class="modal-content" style="margin-top: 70px ">
                            <div class="modal-body info-modal" style="padding: 20px;">
                                <p class="message-content">Are you sure you want to delete?</p>
                            </div>
                            <div class="modal-footer">
                                <button id="delButton"  type="submit" class="btn btn-primary" data-dismiss="modal" onclick="delAdditionalImage()">Yes نعم</button>
                                <button type="submit" class="btn btn-default" data-dismiss="modal">No لا</button>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $.ajaxSetup({
                        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
                    });
                    $('body').append($('.modal'));

                </script>
            @endif
            <div class="content">
                @if(Session::has('flash_message'))
                <div class="alert {{ (Session::has('flash_message_status'))?'alert-error':'alert-success'}}" style="text-align: center;">
                    <button class="close" data-dismiss="alert"></button>
                    <center style="margin-left: 30px">
                        {!! Session::get('flash_message') !!}
                    </center>
                </div>
                @endif
                @yield('content')
            </div>
        </div>
    </div>
    <!-- Scripts -->


    <script src="{{ asset('js/admin.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
        });
    </script>
    {{--<script src="{{ asset('js/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('js/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('js/boostrap-clockpicker/bootstrap-clockpicker.min.js')}}" type="text/javascript"></script>--}}

    <script src="{{asset('js/jquery.fancybox.min.js')}}"></script>

    <script>
        $('[data-fancybox]').fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
            autoCenter  : true
        });
    </script>
    @yield('page_script')
</body>
</html>
