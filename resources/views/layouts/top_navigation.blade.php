<?php
$url = Request::url();
$fullUrl = Request::fullUrl();
$route = Request::route();
$route_name = $route->getName();
$arr = explode('.', $route_name);
$controller = count($arr) > 1 ? $arr[1] : '';
$action = count($arr) > 2 ? $arr[2] : '';

?>
<div class="bar">
    <div class="bar-inner header-section" id="bar-inner">
        <ul class="menus text-center">

            @if(Auth::user()->can('dashboard'))
                <li class="{{ $controller == 'admin' ? 'active' : ''}}">
                    <a href="{{url('/admin')}}">
                        <span class="title">الرئيسية</span>
                        <br>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->can('events-index'))
                <li class="{{ $controller == 'events' ? 'active' : ''}}">
                    <a href="{{url('/admin/events')}}">
                        <span class="title">المناسبات</span>
                        <br>
                        <span class="title">Events</span>
                    </a>
                </li>
            @endif


            @if(Auth::user()->can('reasons-index') || Auth::user()->can('documents-index') || Auth::user()->can('settings-index'))
                <li class="classic {{ $controller == 'roles' || $controller == 'users' ? 'active' : ''}}"><a
                            href="#"><span class="title">الاعدادات<br>Settings</span> <span class="caret"></span></a>
                    <ul class="classic  myClassic">
                        @if(Auth::user()->can('reasons-index'))
                            <li class="{{ $controller == 'reasons' ? 'active' : ''}}">
                                <a href="{{url('/admin/reasons')}}">
                                    <span class="title">{!! __('nav.menu.reasons') !!}</span>
                                </a>
                            </li>
                        @endif
                        @if(Auth::user()->can('documents-index'))
                            <li class="{{ $controller == 'documents' ? 'active' : ''}}">
                                <a href="{{url('/admin/documents')}}">
                                    <span class="title">مستندات</span>
                                    <br>
                                    <span class="title">Documents</span>
                                </a>
                            </li>
                        @endif
                        @if(Auth::user()->can('settings-index'))
                            <li class="{{ $controller == 'settings' ? 'active' : ''}}">
                                <a href="{{url('/admin/settings')}}">
                                    <span class="title">الآخرين</span>
                                    <br>
                                    <span class="title">Others</span>
                                </a>
                            </li>
                        @endif

                    </ul>
                </li>
            @endif



        <!-- Users -->
            @if(Auth::user()->can('users-index') || Auth::user()->can('roles-index'))
                <li class="classic {{ $controller == 'roles' || $controller == 'users' ? 'active' : ''}}"><a
                            href="#"><span class="title">المستخدمين<br>Users</span> <span class="caret"></span></a>
                    <ul class="classic  myClassic">
                        @if(Auth::user()->can('users-index'))
                            <li class="{{ $controller == 'users' ? 'active' : ''}}">
                                <a href="{{url('/admin/users')}}">
                                    <div class="row">
                                        <div class="col-xs-3 menu-image">
                                            <img src="{{asset('http://www.fastrackerzkennel.com/wp-content/uploads/2014/03/male-placeholder-image.jpeg')}}">
                                        </div>
                                        <div class="col-xs-9">
                                            <span class="title">المدراء</span>
                                            <br>
                                            <span class="title">Users</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endif

                        
                        @if(Auth::user()->can('roles-index'))
                            <li class="{{ $controller == 'roles' ? 'active' : ''}}">
                                <a href="{{url('/admin/roles')}}">
                                    <div class="row">
                                        <div class="col-xs-3 menu-image">
                                            <img src="{{asset('http://www.fastrackerzkennel.com/wp-content/uploads/2014/03/male-placeholder-image.jpeg')}}">
                                        </div>
                                        <div class="col-xs-9">
                                            <span class="title">المهام</span>
                                            <br>
                                            <span class="title">Roles</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endif
                        
                    </ul>
                </li>
            @endif

        </ul>
    </div>
</div>