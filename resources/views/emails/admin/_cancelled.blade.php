<head>
	<style type="text/css">
	
			.site_logo img{

		    width: 100%;height: auto;

		}
		.main-col-container{
		    position: absolute !important;
		    width: 375px !important;
		    left: 0 !important;
		    right: 0 !important;
		    margin-left: auto !important;
		    margin-right: auto !important;
		    padding: 0px !important;
		}
		.detail-container .navbar{
	    	margin-top: 0px;
	    	margin-bottom: 0px !important;
		}
		.listing-div{
		    padding: 30px !important;
		}

		.detail-container .elevation-3dp {
		    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12);
		    padding-top: 0px;
		}
		.detail_main_div{
		  	padding-top: 18px; margin-top: 0px;
		}
		.arabic_font{
		    font-family: Geeza;
		}
		.event_detail_title{
		  	font-size: 12px;
		}

		.arabic_font.event_detail_title{margin-bottom: 5px;}

		footer {
		    /* position: fixed; */
		    left: 0;
		    bottom: 0;
		    /*width: 100%;*/
		    background-color: rgb(74, 74, 74);
		    color: rgb(255, 255, 255);
		    padding: 10px;
		}



	.navbar {
	    position: relative;
	    min-height: 50px;
	    margin-bottom: 20px;
	    /*border: 1px solid rgba(0, 0, 0, 0);*/
	    border: unset;
	}
	.navbar {
	    border-radius: unset;
	    /*background-color: rgb(247,231,0);*/
	    /*background-color: rgb(235,216,90);*/
	    color: rgb(255, 255, 255);
	    min-height: 70px;
	    /* margin-top: -20px; */
	    margin-bottom: 20px !important;
	    /* box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12); */
	}
	.detail-container .navbar {
	    margin-top: 0px;
	    margin-bottom: 0px !important;
}

.contact_us_text{

    font-size: 0.9em;

}
.contact_us_header{
	font-size: 10px;
	color: #4A4A4A;
}

.tnd_copyright{

    color: #9B9B9B;

    font-size: 8px;
    color: #B1B1B1;

}

.tnd_copyright > a{

    color: #9B9B9B;

}

.underline

{

    text-decoration: underline;

}

.event_detail_title.event_detail_title_english{
	margin-top: 0px;
}
	</style>

</head>
<body>
<div class="row" style="background-color: rgb(240,240,240);">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container" style="background-color: white;">
        <div class="elevation-3dp">
        @include('site.sections.header')
      	<div class="listing-div detail_main_div">
              <div class="row">
                  <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                      <center>
                      <img class="img-responsive" src="{{asset($content['logo_image'])}}" width="50px" height="50px">
                      </center>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
                  </div>
              </div>

              <center>
                  <h3 class="arabic_font event_detail_title">
                      {{$content['title_arabic']}}
                  </h3>
                  <h3 class="event_detail_title event_detail_title_english">
                      {{$content['title_english']}}
                  </h3>
              </center>
              <!-- <br> -->

              <!-- content here -->
              	<p style="font-size: 18px; direction: rtl;" class="arabic_font">مرحبا {{$content['full_name']}}،</p>
				<p style="font-size: 18px; direction: rtl;" class="arabic_font">
					<!-- شكرا لكم لشراء تذاكر لهذا الحدث <strong>{{ $content['title_arabic'] }}</strong>. تجدون لك تذكرة المرفقة مع البريد الإلكتروني.<br><br> -->
					{{ $content['reason_description_arabic'] }}<br><br>{{ $content['id_type_arabic'] }}: <strong>{{ $content['complete_id'] }}</strong><br><br>تحياتنا،<br><br>اصدقائك في تيكيت باس
				</p>
				<!-- <br> -->
              	<p style="font-size: 18px;">Dear {{$content['full_name']}},</p>
				<p style="font-size: 18px;">
					<!-- Thank you for purchase tickets for the event <strong>{{ $content['title_english'] }}</strong>. Please find yours ticket attached with the email.<br><br> -->
					{{ $content['reason_description_english'] }}<br><br>{{ $content['id_type'] }} <strong>{{ $content['complete_id'] }}</strong><br><br>Regards,<br><br>Your friends at TicketPass
				</p>
      	</div>

        <br>
          @include('site.events._contact_us_bottom')
          <br>
      </div>
      @include('site.sections.footer')
    </div>
</div>
</body>