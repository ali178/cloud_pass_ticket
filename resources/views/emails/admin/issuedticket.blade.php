<head>
	<style type="text/css">
	
			.site_logo img{

		    width: 100%;height: auto;

		}
		.main-col-container{
		    position: absolute !important;
		    width: 375px !important;
		    left: 0 !important;
		    right: 0 !important;
		    margin-left: auto !important;
		    margin-right: auto !important;
		    padding: 0px !important;
		}
		.detail-container .navbar{
	    	margin-top: 0px;
	    	margin-bottom: 0px !important;
		}
		.listing-div{
		    padding: 30px !important;
		}

		.detail-container .elevation-3dp {
		    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12);
		    padding-top: 0px;
		}
		.detail_main_div{
		  	padding-top: 18px; margin-top: 0px;
		}
		.arabic_font{
		    font-family: Geeza;
		}
		.event_detail_title{
		  	font-size: 12px;
		}

		.arabic_font.event_detail_title{margin-bottom: 5px;}

		footer {
		    /* position: fixed; */
		    left: 0;
		    bottom: 0;
		    /*width: 100%;*/
		    background-color: rgb(74, 74, 74);
		    color: rgb(255, 255, 255);
		    padding: 10px;
		}



	.navbar {
	    position: relative;
	    min-height: 50px;
	    margin-bottom: 20px;
	    /*border: 1px solid rgba(0, 0, 0, 0);*/
	    border: unset;
	}
	.navbar {
	    border-radius: unset;
	    /*background-color: rgb(247,231,0);*/
	    /*background-color: rgb(235,216,90);*/
	    color: rgb(255, 255, 255);
	    min-height: 70px;
	    /* margin-top: -20px; */
	    margin-bottom: 20px !important;
	    /* box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12); */
	}
	.detail-container .navbar {
	    margin-top: 0px;
	    margin-bottom: 0px !important;
}

.contact_us_text{

    font-size: 0.9em;

}
.contact_us_header{
	font-size: 10px;
	color: #4A4A4A;
}

.tnd_copyright{

    color: #9B9B9B;

    font-size: 8px;
    color: #B1B1B1;

}

.tnd_copyright > a{

    color: #9B9B9B;

}

.underline

{

    text-decoration: underline;

}

.event_detail_title.event_detail_title_english{
	margin-top: 0px;
}

.grid {
    clear: both;
    margin-top: 0px;
    margin-bottom: 25px;
    padding: 0px;
}
.grid.simple {
    padding: 0px !important;
}

.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid rgba(0, 0, 0, 0);
    /*border-top-left-radius: 3px;*/
    /*border-top-right-radius: 3px;*/
}

.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: inherit;
}

.grid.simple .grid-body {
    background-color: rgb(250, 250, 250);
    padding: 10px;
    border: 1px solid rgb(211, 211, 211);
    color: rgb(111, 123, 138);
}

.sub-section {
    border-top: 0 !important;
}
	</style>

</head>
<body>
<div class="row" style="background-color: rgb(240,240,240);">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container" style="background-color: white;">
        <div class="elevation-3dp">
        @include('site.sections.header')
      	<div class="listing-div detail_main_div">
              <div class="row">
                  <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                      <center>
                      <img class="img-responsive" src="{{asset($content['logo_image'])}}" width="50px" height="50px">
                      </center>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
                  </div>
              </div>

              <center>
                  <h3 class="arabic_font event_detail_title">
                      {{$content['title_arabic']}}
                  </h3>
                  <h3 class="event_detail_title event_detail_title_english">
                      {{$content['title_english']}}
                  </h3>
              </center>
              <!-- <br> -->

              <!-- content here -->
              	<p style="font-size: 18px; direction: rtl;" class="arabic_font">مرحبا {{$content['full_name']}}،</p>
				<p style="font-size: 18px; direction: rtl;" class="arabic_font">
					تم إصدار طلبك لتذاكر <strong>{{ $content['title_arabic'] }}</strong> بنجاح. ستجد روابط التذاكر في اسفل الرسالة. تواصل معنا على {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['email'] : '' }} لأي استفسار. الرجاء ذكر رقم التذاكر في كل مراسلاتك معنا.
					<br><br>
					شكراً لزيارتك واختيارك تيكيت باس.<br><br> رقم التذاكر:   <strong>{{ $content['complete_ticket_no'] }}</strong><br><br>تحياتنا،<br><br>اصدقائك في تيكيت باس
				</p>
				<!-- <br> -->
              	<p style="font-size: 18px;">Dear {{$content['full_name']}},</p>
				<p style="font-size: 18px;">
					Your tickets request for <strong>{{ $content['title_english'] }}</strong> has been successfully processed. Please find your purchase links below. Contact us at {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['email'] : '' }} for any questions you may have. Please include your ticket ID in all communications.
					<br><br>Thank you for visiting TicketPass.<br><br>Ticket ID: <strong>{{ $content['complete_ticket_no'] }}</strong><br><br>Regards,<br><br>Your friends at TicketPass
				</p>

				<br>

				<center>
		      		<span style="font-size: 18px;" class="arabic_font">معلومات التذاكر</span>
		      		<br>
		      		<span style="font-size: 18px;">Tickets Information</span>
		      	</center>

		      	<br>

		      	@if(count($content['tiers'])>0)
					@foreach (array_chunk($content['tiers'],3) as $chunked_tiers)
						<?php $num=1; ?>
						@foreach ($chunked_tiers as $tier)
							<?php
								$tl=$content['issued_ticket']->getTierColorByTierId($tier['tier_id']);
							?>

							<div class="grid simple">
                                    <div class="panel-heading" style="background-color:#{{$tl->header_color}};color:white;">
                                        <h4 class="panel-title" style="color: white; font-size: 14px;font-weight: normal;">
                                            <strong><span style="color: white">{{$tl->title_english }}</span></strong> <div style="float: right"> <span style="color: white" class="arabic_font">{{$tl->title_arabic }}</span> </div>
                                        </h4>
                                    </div>
                                <div class="grid-body sub-section">
                                    <div class="row">
                                        <div>
                                        	<center>
                                            <?php $redemption_tickets =$content['issued_ticket']->getIndividualTicketOfTier($content['issued_ticket']->id,$tier['tier_id']) ?>

                                                  	<ul style="list-style-type: none;padding: 0px !important;">
                                                    	@foreach($redemption_tickets as $redemption_ticket)
                                                          	<li style="padding: 0px !important;"><h4><a target="_blank" href='{{ route("admin.issued_tickets.genbarcodeofticket",[$redemption_ticket->random_id_string]) }}' style="text-decoration: none !important;color: #4A90E2;font-size: 18px; font-weight: normal;"> {{$redemption_ticket->ticket_no}}</a></h4></li>
                                                    	@endforeach
                                                  	</ul>

                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>

						@endforeach
					@endforeach
				@endif
		      	
      	</div>

        <br>
          @include('site.events._contact_us_bottom')
          <br>
      </div>
      @include('site.sections.footer')
    </div>
</div>
</body>
<!-- 

<center>
<img src="{{$content['logo_image']}}" data-auto-embed="base64">
{{--[![IMAGE ALT TEXT HERE]({{$content['logo_image']}})](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)--}}
</center>
<center>
{{ $content['title_english'] }}
<br/>
{{ $content['title_arabic'] }}
</center>
<p style="text-align:right">
أصدقائي لزيارة  الأهرامات مرة في الشهر,  <strong>{{ $content['title_arabic'] }}</strong>أحب  أيضا المشي بجانب نهر النيل حيث النسي
المنعش و الأحب السفر و زيارة الدول الأخرى, زرت مرة المغرب و أعجبني كثيرا, الناس هناك كرماء و لطفاء, إستمتعت بالأكلات المغربية مثل الكسكس و غيرها. أيضا زرت الأردن, بلد رائع حقا! أمضيت يوما كاملا أستمتع بمناظر البتراء, منازل منحوثة على الصخر ... زيارتي المقبلة سوف تكون للإسبانيا, حيث أرغب بزيارة ساحة الحمراء بالأندلس, بنقوشه الفنية الجميلة, أنا مشتاقة support@abctickeing.com حقا لتلك الرحلة.
</p>
<p style="text-align:right">{{ $content['post_ticket_no'] }}أنا إسمي فاطمة, أعيش في مصرا </p>
<p style="text-align:right">
أحب بلدي كث
</p>
<p style="text-align:right">
أعيش في مصرا
</p>

<p>Dear {{$content['full_name']}}</p>
<p>I always encourage learners to learn how to read Arabic in its original script, because the
transliteration doesn’t provide accurate phonetics  <strong>{{ $content['title_english'] }}</strong> Arabic letters then try to first read the first part and see how well you will do, if not then help yourself with the transliteration and finally if you didnt know the meaning of some parts then check out the translation. Enjoyable reading<br/>
Request ID: {{ $content['post_ticket_no'] }}
</p>

Thanks,<br>
{{ config('app.name') }}

<p style="text-align: center">
أنا إسمي فاطمة, أعيش في مصرا<br/>Tickets Information
</p>

@if(count($content['tiers'])>0)
	@foreach (array_chunk($content['tiers'],3) as $chunked_tiers)
		<?php $num=1; ?>
		@foreach ($chunked_tiers as $tier)
			<?php
				$tl=$content['issued_ticket']->getTierColorByTierId($tier['tier_id']);
			?>
			<table style="width: 100%;max-width: 100%;margin-bottom: 20px; border: 1px solid grey">
				<tr style="background-color:#{{$tl->header_color}};color:white;">
				<th style="text-align: left;padding: 15px">{{$tl->title_english }}</th>
				<th style="text-align: right;padding: 15px">{{$tl->title_arabic }}</th>
				</tr>
				<?php $redemption_tickets =$content['issued_ticket']->getIndividualTicketOfTier($content['issued_ticket']->id,$tier['tier_id']) ?>

				@foreach($redemption_tickets as $redemption_ticket)
					<tr>
					<td style="text-align: center; font-size: 18px" colspan="2"><a target="_blank" href='{{ route("admin.issued_tickets.genbarcodeofticket",[$redemption_ticket->id]) }}'> {{$redemption_ticket->ticket_no}}</a></td>
					</tr>
				@endforeach
			</table>
		@endforeach
	@endforeach
@endif
<br/>
<center>
<span><strong>Contact Us <span>اتصل بنا</span></strong></span>
<br>
<span>966-548209873</span>
<br>
<span >abccorp@contactus.com</span>
<br>
<br>
<span class="tnd_copyright">
<a href="javascript:void(0)" class="underline" data-toggle="modal" data-target="#general_tnc_modal"><span class="underline">Terms &amp; Condtions <span>الأحكام والشروط</span></span></a>
<br><br>
<span>Copyright ABC Corp. 2018 All rights resverd.</span>
<br>
<span class="arabic_font">ريتز كارلتون، الرياض ٢٠١٨</span>
</span>
</center>

 -->