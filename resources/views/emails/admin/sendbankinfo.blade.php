<head>

	<style type="text/css">
			.site_logo img{

		    width: 100%;height: auto;

		}
		.main-col-container{
		    position: absolute !important;
		    width: 375px !important;
		    left: 0 !important;
		    right: 0 !important;
		    margin-left: auto !important;
		    margin-right: auto !important;
		    padding: 0px !important;
		}
		.detail-container .navbar{
	    	margin-top: 0px;
	    	margin-bottom: 0px !important;
		}
		.listing-div{
		    padding: 30px !important;
		}

		.detail-container .elevation-3dp {
		    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12);
		    padding-top: 0px;
		}
		.detail_main_div{
		  	padding-top: 18px; margin-top: 0px;
		}
		.arabic_font{
		    font-family: Geeza;
		}
		.event_detail_title{
		  	font-size: 12px;
		}

		.arabic_font.event_detail_title{margin-bottom: 5px;}

		footer {
		    /* position: fixed; */
		    left: 0;
		    bottom: 0;
		    /*width: 100%;*/
		    background-color: rgb(74, 74, 74);
		    color: rgb(255, 255, 255);
		    padding: 10px;
		}



	.navbar {
	    position: relative;
	    min-height: 50px;
	    margin-bottom: 20px;
	    /*border: 1px solid rgba(0, 0, 0, 0);*/
	    border: unset;
	}
	.navbar {
	    border-radius: unset;
	    /*background-color: rgb(247,231,0);*/
	    /*background-color: rgb(235,216,90);*/
	    color: rgb(255, 255, 255);
	    min-height: 70px;
	    /* margin-top: -20px; */
	    margin-bottom: 20px !important;
	    /* box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12); */
	}
	.detail-container .navbar {
	    margin-top: 0px;
	    margin-bottom: 0px !important;
}

.contact_us_text{

    font-size: 0.9em;

}
.contact_us_header{
	font-size: 10px;
	color: #4A4A4A;
}

.tnd_copyright{

    color: #9B9B9B;

    font-size: 8px;
    color: #B1B1B1;

}

.tnd_copyright > a{

    color: #9B9B9B;

}

.underline

{

    text-decoration: underline;

}

.event_detail_title.event_detail_title_english{
	margin-top: 0px;
}

/*inpage styling*/
.card-div:not(:last-child){

    margin-bottom: 50px !important;

}

.card-div {

    width: 100%;

    position: relative;

}

.card-simple {
    padding: 5px;
}
.card {
    border: 1px solid rgb(181,181,181);
}

.tickets_review_table_div {
    height: auto !important;
}

.content_text,.tier_description_text,#review_tickets_table thead,.checkbox_info_txt,.tnc_popup_text,.confirmation_info_description{
	font-size: 12px;
	color: #4A4A4A;
}

/*tickets review*/

#review_tickets_table{

    margin-bottom: 0px;

  }

  

  tfoot{

    font-weight: bold;

    text-align: right;

  }



  #review_tickets_table .price {

    text-align: right;

  }



  #review_tickets_table .qty {

    text-align: right;

  }



  #review_tickets_table>tbody>tr>td, 

  #review_tickets_table>thead>tr>td, 

  #review_tickets_table>thead>tr>th

  {

    border-top: 0px;

  }



  #review_tickets_table>thead>tr>th{

    border-bottom: 1px solid rgb(181,181,181);

  }

  #review_tickets_table>tfoot>tr>td

  {

    border-top: 0px;

  }



  .border_top{

    border-top: 1px solid rgb(181,181,181);

  }

  

  .tickets_review_table_div{

    /*background: rgb(247,244,241);*/

    height: auto !important;

  }

	  table>tbody>tr{
	  	padding: 4px !important;	
	  }

	  table>tbody>tr:last-child>td{
	  	border-bottom: 1px solid rgb(181,181,181);
	  }

  /*table>tfoot>tr>td{
  	border-top: 1px solid rgb(181,181,181) !important;

  }*/

	</style>

</head>
<body>
<div class="row" style="background-color: rgb(240,240,240);">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container" style="background-color: white;">
        <div class="elevation-3dp">
        @include('site.sections.header')
      	<div class="listing-div detail_main_div">
              <div class="row">
                  <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                      <center>
                      <img class="img-responsive" src="{{asset($content['logo_image'])}}" width="50px" height="50px">
                      </center>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
                  </div>
              </div>

              <center>
                  <h3 class="arabic_font event_detail_title">
                      {{$content['title_arabic']}}
                  </h3>
                  <h3 class="event_detail_title event_detail_title_english">
                      {{$content['title_english']}}
                  </h3>
              </center>
              <!-- <br> -->

              <!-- content here -->
              	<p style="font-size: 18px; direction: rtl;" class="arabic_font">مرحبا {{$content['full_name']}}،</p>
				<p style="font-size: 18px; direction: rtl;" class="arabic_font">
					شكراً لطلبك تذاكر <strong>{{ $content['title_arabic'] }}</strong> مرفق مع هذه الرسالة معلومات التحويل البنكي. الرجاء تحويل مبلغ وقدر <strong>SR. {{ $content['total_price'] }}</strong> ريال سعودي ممثلاً قيمة طلبكم خلال يوم عمل واحد. الرجاء إرسال صورة من التحويل الى البريد {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['email'] : '' }} مع ضرورة ذكر رقم الطلب في عنوان الرسالة.

					<br>
					<br>
					رقم الطلب: <strong>{{$content['complete_request_no']}}</strong>
					<br>
					<br>
					معلومات المستفيد البنكية:
					<br>
					<br>
					{{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['beneficiary_name_arabic'] : '' }}
					<br>
					{{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['bank_name_arabic'] : '' }}
					<br>
					حساب {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['account'] : '' }}
					<br>
					ايبان {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['iban'] : '' }}
					<br>
					<br>
					تحياتنا،
					<br>
					<br>
					اصدقائك في تكيت باس
				</p>
				<!-- <br> -->
              	<p style="font-size: 18px;">Dear {{$content['full_name']}},</p>
				<p style="font-size: 18px;">

					Thank your for requesting tickets for <strong>{{ $content['title_english'] }}</strong>. Please bank transfer <strong>SR. {{ $content['total_price'] }}</strong> Saudi Riyals to the bank details below within one business day. Please send a copy of your transfer to {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['email'] : '' }} and mention your request ID in the subject line.
					<br>
					<br>
					Request ID: <strong>{{$content['complete_request_no']}}</strong>
					<br>
					<br>
					Beneficiary Bank Details:
					<br>
					<br>
					{{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['beneficiary_name_english'] : '' }}
					<br>
					{{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['bank_name_english'] : '' }}
					<br>
					Account {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['account'] : '' }}
					<br>
					IBAN {{ isset($shared_site_vars['settings']) ? $shared_site_vars['settings']['iban'] : '' }}
					<br>
					<br>
					Regards,
					<br>
					<br>
					TicketPass ticketing team
				</p>
      		<br>
      		
      		<!-- table -->
      		<center>
	      		<span style="font-size: 18px;" class="arabic_font">ملخص الطلب</span>
	      		<br>
	      		<span style="font-size: 18px;">Request Summary</span>
	      	</center>
	      	<br>
      		<div class="card-div">
			  <div class="row card card-simple tickets_review_table_div">
			    <div class="table-responsive">          
			      <table class="table" id="review_tickets_table" style="width: 100%;">
			        <thead>
			          <tr>
			            <th style="text-align: left;">
			              <span class="arabic_font">قسم</span>
			              <br>
			              <span>Type</span>
			            </th>
			            <th></th>
			            <th class="qty">
			              <span class="arabic_font">كمية</span>
			              <br>
			              <span>Qty</span>
			            </th>
			            <th class="price">
			              <span class="arabic_font">السعر</span>
			              <br>
			              <span>Price</span>
			            </th>
			          </tr>
			        </thead>
			        <tbody style="font-size: 12px;">
			        <?php 
			        	$subTotalAmount=0.00;
			        	$temp_vat = 0;
			        ?>
			        @foreach($content['event_request_details'] as $event_request_detail)
			          <tr>
			            <td>{{$event_request_detail->tier->title_english}}</td>
			            <td>{{$event_request_detail->tier->title_arabic}}</td>
			            <td class="qty">{{$event_request_detail->total_qty  }}</td>
			            <td class="price">
			            	<?php
				            	$item_price = $event_request_detail->price * $event_request_detail->total_qty;
				            	$temp_vat = $temp_vat + (($content['event_request']->vat/100)*$item_price);
	                			$item_price = $item_price - ($content['event_request']->vat/100)*$item_price;
				            	$subTotalAmount=$subTotalAmount + $item_price;
			            	?>
			             @if($event_request_detail->price>0) 
			             	<!-- {{$item_price }} -->
			             	{{number_format($item_price,2)}}
			             @else {{'Free'}} @endif
			            </td>
			          </tr>

			          @endforeach
			        </tbody>

			        <tfoot style="font-size: 12px;">
			        	<tr>
			        		<td><hr></td>
			        		<td><hr></td>
			        		<td><hr></td>
			        		<td><hr></td>
			        	</tr>
			          <tr>
			            <td colspan="2" class="arabic_font">
			              المجموع الفرعي
			            </td>
			            <td>Sub Total</td>
			            <!-- <td>{{$subTotalAmount}}</td> -->
			            {{number_format($subTotalAmount,2)}}
			          </tr>
			          <tr>
			            <td colspan="2" class="arabic_font">
			            	<?php
			            		$arabic_vat = \Ticketing\Models\Event::e2a($content['event_request']->vat);
			            	?>
			              ٪وات {{$arabic_vat}}
			            </td>
			            <td>Vat {{$content['event_request']->vat}}%</td>
			            <td>
			                <?php  
			                	$vat_value=($content['event_request']->vat/100) * $subTotalAmount;
			                	// $vat_value = ceil($vat_value);
			                	$vat_value = $temp_vat;
			                ?>
			              {{number_format($vat_value,2)}}
			            </td>
			          </tr>
			          <tr>
			            <td colspan="2" class="arabic_font">
			              الإجمالي
			            </td>
			            <td>Total</td>
			            <td>

			            	{{number_format($subTotalAmount + $vat_value,2)}}
			              <!-- {{$subTotalAmount + $vat_value}} -->
			            </td>
			          </tr>
			        </tfoot>

			      </table>
			    </div>
			  </div>
			</div>
			<!-- table end -->
      	</div>
      	<br>
      			
          @include('site.events._contact_us_bottom')
          <br>
      </div>
      @include('site.sections.footer')
    </div>
</div>
</body>

<!-- <center>
 <img src="{{$content['logo_image']}}" data-auto-embed="base64">
 {{--[![IMAGE ALT TEXT HERE]({{$content['logo_image']}})](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)--}}
</center>
<center>
{{ $content['title_english'] }}
<br/>
{{ $content['title_arabic'] }}
</center>
<p style="text-align:right">
أصدقائي لزيارة  الأهرامات مرة في الشهر,  <strong>{{ $content['title_arabic'] }}</strong>أحب  أيضا المشي بجانب نهر النيل حيث النسي
 SR.المنعش و <strong>{{ $content['total_price'] }}</strong>الأحب السفر و زيارة الدول الأخرى, زرت مرة المغرب و أعجبني كثيرا, الناس هناك كرماء و لطفاء, إستمتعت بالأكلات المغربية مثل الكسكس و غيرها. أيضا زرت الأردن, بلد رائع حقا! أمضيت يوما كاملا أستمتع بمناظر البتراء, منازل منحوثة على الصخر ... زيارتي المقبلة سوف تكون للإسبانيا, حيث أرغب بزيارة ساحة الحمراء بالأندلس, بنقوشه <strong>IBAN</strong>الفنية الجميلة, أنا مشتاقة support@abctickeing.com حقا لتلك الرحلة.
</p>
<p style="text-align:right">{{ $content['post_request_no'] }}أنا إسمي فاطمة, أعيش في مصرا </p>
<p style="text-align:right">
34534534554634556454
أحب بلدي كث
</p>
<p style="text-align:right">
أحب بلدي كث
</p>
<p style="text-align:right">
أعيش في مصرا
</p>

<p>Dear {{$content['full_name']}}</p>
<p>I always encourage learners to learn how to read Arabic in its original script, because the
transliteration doesn’t provide accurate phonetics  <strong>{{ $content['title_english'] }}</strong> Arabic letters then try to
<strong>IBAN</strong> first read the first part and see how well you will do, if not then help yourself with the transliteration <strong>{{ $content['total_price'] }}</strong> and finally if you didnt know the meaning of some parts then check out the translation. Enjoyable reading<br/>
Request ID: {{ $content['post_request_no'] }}<br/>IBAN: 34534534554634556454
</p>

Thanks,<br>
{{ config('app.name') }}

<p style="text-align: center">
أنا إسمي فاطمة, أعيش في مصرا<br/>Review tickets and cost
</p>

<table width="100%">
<tr>
<th>الفرعي<br/>Type</th><th>الفرعي <br/>Qty</th><th>الفرعي <br/>Price</th>
</tr>
<?php $subTotalAmount=0.00 ?>
 @foreach($content['event_request_details'] as $event_request_detail)
<tr>
<?php $subTotalAmount=$subTotalAmount + $event_request_detail->price * $event_request_detail->total_qty;?>
 <td style="text-align: center">{{$event_request_detail['tier']->title_english}} {{$event_request_detail['tier']->title_arabic}}</td>
 <td style="text-align: center">{{$event_request_detail->total_qty  }} </td>
 <td style="text-align: center"> @if($event_request_detail->price>0) {{$event_request_detail->price  }}@else {{'Free'}} @endif </td>
</tr>
 @endforeach
 <tr>
 <td></td><td>المجموع الفرعي Sub Total</td><td>{{$subTotalAmount}}</td>
 </tr>
        <?php  $vat_value=5/100 * $subTotalAmount  ?>
  <tr>
   <td></td><td>Vat 5% وات ٥٪</td><td>{{$vat_value}}</td>
  </tr>
  <tr>
   <td></td><td>Net المبلغ الإجمالي</td><td>{{$subTotalAmount + $vat_value}}</td>
  </tr>

</table>
<br/>


<center>
<span><strong>Contact Us <span>اتصل بنا</span></strong></span>
<br>
<span>966-548209873</span>
<br>
<span >abccorp@contactus.com</span>
<br>
<br>
<span class="tnd_copyright">
<a href="javascript:void(0)" class="underline" data-toggle="modal" data-target="#general_tnc_modal"><span class="underline">Terms &amp; Condtions <span>الأحكام والشروط</span></span></a>
<br><br>
<span>Copyright ABC Corp. 2018 All rights resverd.</span>
<br>
<span class="arabic_font">ريتز كارلتون، الرياض ٢٠١٨</span>
</span>
</center>


 -->