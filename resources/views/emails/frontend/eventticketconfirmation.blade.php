<head>
	<style type="text/css">
	
			.site_logo img{

		    width: 100%;height: auto;

		}
		.main-col-container{
		    position: absolute !important;
		    width: 375px !important;
		    left: 0 !important;
		    right: 0 !important;
		    margin-left: auto !important;
		    margin-right: auto !important;
		    padding: 0px !important;
		}
		.detail-container .navbar{
	    	margin-top: 0px;
	    	margin-bottom: 0px !important;
		}
		.listing-div{
		    padding: 30px !important;
		}

		.detail-container .elevation-3dp {
		    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12);
		    padding-top: 0px;
		}
		.detail_main_div{
		  	padding-top: 18px; margin-top: 0px;
		}
		.arabic_font{
		    font-family: Geeza;
		}
		.event_detail_title{
		  	font-size: 12px;
		}

		.arabic_font.event_detail_title{margin-bottom: 5px;}

		footer {
		    /* position: fixed; */
		    left: 0;
		    bottom: 0;
		    /*width: 100%;*/
		    background-color: rgb(74, 74, 74);
		    color: rgb(255, 255, 255);
		    padding: 10px;
		}



	.navbar {
	    position: relative;
	    min-height: 50px;
	    margin-bottom: 20px;
	    /*border: 1px solid rgba(0, 0, 0, 0);*/
	    border: unset;
	}
	.navbar {
	    border-radius: unset;
	    /*background-color: rgb(247,231,0);*/
	    /*background-color: rgb(235,216,90);*/
	    color: rgb(255, 255, 255);
	    min-height: 70px;
	    /* margin-top: -20px; */
	    margin-bottom: 20px !important;
	    /* box-shadow: 0 3px 4px 0 rgba(0, 0, 0, .14), 0 3px 3px -2px rgba(0, 0, 0, .2), 0 1px 8px 0 rgba(0, 0, 0, .12); */
	}
	.detail-container .navbar {
	    margin-top: 0px;
	    margin-bottom: 0px !important;
}

.contact_us_text{

    font-size: 0.9em;

}
.contact_us_header{
	font-size: 10px;
	color: #4A4A4A;
}

.tnd_copyright{

    color: #9B9B9B;

    font-size: 8px;
    color: #B1B1B1;

}

.tnd_copyright > a{

    color: #9B9B9B;

}

.underline

{

    text-decoration: underline;

}

.event_detail_title.event_detail_title_english{
	margin-top: 0px;
}
	</style>

</head>
<body>
<div class="row" style="background-color: rgb(240,240,240);">
      <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 col-xl-4 col-xl-offset-4 main-col-container detail-container" style="background-color: white;">
        <div class="elevation-3dp">
        @include('site.sections.header')
      	<div class="listing-div detail_main_div">
              <div class="row">
                  <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                      <center>
                      <img class="img-responsive" src="{{asset($content['logo_image'])}}" width="50px" height="50px">
                      </center>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 detail_date_time">
                  </div>
              </div>

              <center>
                  <h3 class="arabic_font event_detail_title">
                      {{$content['title_arabic']}}
                  </h3>
                  <h3 class="event_detail_title event_detail_title_english">
                      {{$content['title_english']}}
                  </h3>
              </center>
              <!-- <br> -->

              <!-- content here -->
              	<p style="font-size: 18px; direction: rtl;" class="arabic_font">مرحبا {{$content['full_name']}}،</p>
				<p style="font-size: 18px; direction: rtl;" class="arabic_font">
                    شكراً على زيارتك لتيكت باس وطلبك لتذاكر <strong>{{ $content['title_arabic'] }}</strong>. @if(isset($content['complete_request_no']))رقم طلبك هو <strong>{{ $content['complete_request_no'] }}</strong>@endif
					<br><br>
					هذا البريد ليس اثبات شراء. سيتم التواصل معك قريباً.
					<br><br>
					تحياتنا،<br><br>اصدقائك في تيكيت باس
				</p>
				<!-- <br> -->
              	<p style="font-size: 18px;">Dear {{$content['full_name']}},</p>
				<p style="font-size: 18px;">Thank your for visiting TicketPass and requesting tickets for <strong>{{ $content['title_english'] }}</strong>. 
				@if(isset($content['complete_request_no']))Your request ID is <strong>{{ $content['complete_request_no'] }}</strong>@endif<br><br>This is just a request confirmation. It is not a proof of purchase. You will receive a follow up email soon.<br><br>Regards,<br><br>Your friends at TicketPass
				</p>
      	</div>

        <br>
          @include('site.events._contact_us_bottom')
          <br>
      </div>
      @include('site.sections.footer')
    </div>
</div>
</body>
<!-- <center>
<img src="{{$content['logo_image']}}" data-auto-embed="base64">
</center>
<center>
 {{ $content['title_english'] }}
 <br/>
 {{ $content['title_arabic'] }}
</center>

<p style="text-align: right">أنا إسمي فاط<</p>
<p style="text-align: right">
 أنا إسمي فاطمة, أعيش في مصر, أبلغ من العمر 20 سنة, أدرس بجامعة القاهرة,  <strong>{{ $content['title_arabic'] }}</strong> أحب بلدي كثيرا, أذهب مع أصدقائي لزيارة الأهرامات مرة في الشهر, أحب أيضا المشي بجانب
</p>

<p style="text-align: right">
 أحب بلدي كثيرا,
</p>

<p>Dear {{$content['full_name']}},</p>
<p>
 I always encourage learners to learn how to read Arabic in its original script, because the transliteration doesn’t provide accurate phonetics  <strong>{{ $content['title_english'] }}</strong> Arabic letters then try to first read the first part and see how well you will do, if not then help yourself with the transliteration, and finally if you didn’t know the meaning of some parts, then check out the translation. Enjoyable reading!
</p>

Thanks,<br>
{{ config('app.name') }}



<center>
 <span><strong>Contact Us <span>اتصل بنا</span></strong></span>
 <br>
 <span>966-548209873</span>
 <br>
 <span >abccorp@contactus.com</span>
 <br>
 <br>
 <span class="tnd_copyright">
        <a href="javascript:void(0)" class="underline" data-toggle="modal" data-target="#general_tnc_modal"><span class="underline">Terms &amp; Condtions <span>الأحكام والشروط</span></span></a>

        <br><br>

        <span>Copyright ABC Corp. 2018 All rights resverd.</span>
        <br>
        <span class="arabic_font">ريتز كارلتون، الرياض ٢٠١٨</span>
        </span>
</center>
 -->