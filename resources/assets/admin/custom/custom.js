$(document).ready(function(){
	$(document).on('change','#datatable-example .group-checkable',function () {
        dataTableCheckAll(this,'');
    });

    $(document).on('change','#datatable-example2 .group-checkable',function () {
        dataTableCheckAll(this,'2');
    });

    $(document).on('change','#datatable-example3 .group-checkable',function () {
        dataTableCheckAll(this,'3');
    });

    $(document).on('change','#datatable-example4 .group-checkable',function () {
        dataTableCheckAll(this,'4');
    });

    $(document).on('change','#datatable-example5 .group-checkable',function () {
        dataTableCheckAll(this,'5');
    });

    $(document).on('click', '.markaction #del', function (e) {
        e.preventDefault();

        bulkAction(this,'',true);
    });

    $(document).on('click', '.markaction #del2', function (e) {
        e.preventDefault();
        bulkAction(this,'2',true);
    });

    $(document).on('click', '.markaction #del3', function (e) {
        e.preventDefault();
        bulkAction(this,'3',true);
    });

    $(document).on('click', '.markaction #del4', function (e) {
        e.preventDefault();
        bulkAction(this,'4',true);
    });

    $(document).on('click', '.markaction #del5', function (e) {
      e.preventDefault();
      bulkAction(this,'5',true);
    });

    $(document).on('click', '.markaction #show', function (e) {
        e.preventDefault();
        bulkAction(this,'');
    });

    $(document).on('click', '.markaction #show2', function (e) {
        e.preventDefault();
        bulkAction(this,'2');
    });

    $(document).on('click', '.markaction #show3', function (e) {
        e.preventDefault();
        bulkAction(this,'3');
    });

    $(document).on('click', '.markaction #show4', function (e) {
        e.preventDefault();
        bulkAction(this,'4');
    });

    $(document).on('click', '.markaction #hide', function (e) {
        e.preventDefault();
        bulkAction(this,'');
    });

    $(document).on('click', '.markaction #hide2', function (e) {
        e.preventDefault();
        bulkAction(this,'2');
    });

    $(document).on('click', '.markaction #hide3', function (e) {
        e.preventDefault();
        bulkAction(this,'3');
    });

    $(document).on('click', '.markaction #hide4', function (e) {
        e.preventDefault();
        bulkAction(this,'4');
    });

    $(document).on('submit', '.deleteaction', function() {
        var ret = confirm("Are you sure you want to delete?");

        if (ret) {
            $("#progressbar").show();
            return true;
        }
        return false;
    });

    $('#startCrop, #startCrop1, #startCrop2, #startCrop3, #startCrop4, #startCrop5, #startCrop6, #startCrop7').on('click',function(e){
        var crop_id = this.id.replace(/[^0-9]/g, '');
        var id = crop_id == '' ? 1 : crop_id;
        $('#image'+id+'-dim').removeClass('hide');

    });

    //native theme delete popups
    var form = '';
    $(document).on('click', '.deleteaction', function() {
        var msg = "Are you sure you want to delete?";
        form = this;

        delMsg = '';

        delMsg = $(this).attr('data-msg');

        if (delMsg) {
             deleteModal(delMsg);
        } else {
             deleteModal(msg);
        }

        return false;
    });


    //native theme delete popups



    function deleteModal(message, action) {
        $('#deleteMessage .message-content').text(message);
        $('#deleteMessage').modal('show');
    }

    $(document).on('click', '.submitDeleteModal', function() {
        $("#progressbar").show();
       form.submit();
    });


  /////////////// by iqbal /////////////
    //native theme delete popups
    ///////////////// by iqbal end ////////////////




});

function updateFeatureCharLimitNum(textarea,limitElement,limitNum){
    var field = document.getElementById(textarea);
    
    if (field.value.length > limitNum) {
        field.value = field.value.substring(0, limitNum);
    } else {
        $('#'+limitElement).html(limitNum - field.value.length);
    }
}

function dataTableCheckAll(element,count) {
    var table_id = '#datatable-example'+count;
    if ($(element).prop('checked')) {
        $(table_id+' tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', true);
        });
    } else {
        $(table_id+' tbody tr td input[type="checkbox"]').each(function () {
            $(this).prop('checked', false);
        });
    }
}

function fileSizeMessage(message){
  $('#fileSizeMessage .message-content').html(message);
  $('#fileSizeMessage').modal('show');
}
// function bulkAction(element,count,confirm_delete){
//   url = $(element).attr('data-url');
//   var selectedCheckboxItems = $('#datatable-example'+count+' tbody :checkbox:checked').map(function () {
//       return this.value;
//   }).get();
//   //console.log(selectedCheckboxItems);
//   if (selectedCheckboxItems.length > 0) {
//       selectedItem = selectedCheckboxItems.join(',');
//       selectedItems = selectedItem.replace('on,', '');
//       delMsg = '';
//       var verify = true;
//       if (confirm_delete) {
//         delMsg = $(this).attr('data-msg');
//         if (delMsg) {
//           verify = confirm(delMsg + 'Are you sure you want to delete ?');
//         } else {
//           verify = confirm('Are you sure you want to delete ?');
//         }
//       }
//       if (verify) {
//           $("#progressbar").show();
//           $('#ids').attr('value', selectedItems);
//           $('#bulkaction').attr('action', url);
//           $('#bulkaction').submit();
//       }
//   } else {
//       showErrorMessage('Nothing Selected');

//   }
// }

var errorMessageStr = 'Nothing Selected';
    var msg = "Are you sure you want to delete ?";
    function bulkAction(element,count,confirm_delete){
      url = $(element).attr('data-url');
      var selectedCheckboxItems = $('#datatable-example'+count+' tbody :checkbox:checked').map(function () {
          return this.value;
      }).get();
      
      if (selectedCheckboxItems.length > 0) {
        selectedItem = selectedCheckboxItems.join(',');
        selectedItems = selectedItem.replace('on,', '');
        delMsg = '';

        if (confirm_delete) {
            delMsg = $(element).attr('data-msg');

            if (delMsg) {
                 deleteBulkModal(delMsg);
            } else {
                 deleteBulkModal(msg);
            }
        }else{
            $("#progressbar").show();
            $('#ids').attr('value', selectedItems);
            $('#bulkaction').attr('action', url);
            $('#bulkaction').submit();
        }
          
      } else {
          showErrorMessage(errorMessageStr);
      }
    }

    function deleteBulkModal(message, action) {
        $('#deleteBulkMessage .message-content').text(message);
        $('#deleteBulkMessage').modal('show');
    }

    $(document).on('click', '.submitDeleteBulkModal', function() { 
        $("#progressbar").show();
        $('#ids').attr('value', selectedItems);
        $('#bulkaction').attr('action', url);
        $('#bulkaction').submit();
    });

function showErrorMessage(msg) {
    var style = 'flat';

    Messenger.options = {extraClasses: 'messenger-fixed messenger-on-top', theme: style};
    Messenger().post({
        message: msg,
        type: 'error',
        showCloseButton: true
    });
}

//maps config
function initAutocomplete() {

    var map = new google.maps.Map(document.getElementById('location_map'), {
      center: {lat: 23.885942, lng: 45.079162},
      zoom: 12,
      mapTypeId: 'roadmap'
    });
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
    var markers = [];

    // Add markers on page load/reload if there is already a place selected
    var lat = $('#lat_input').val();
    var lng = $('#lng_input').val();

    if (lat!='' && lng!='') {
        var lat = $('#lat_input').val();
        var lng = $('#lng_input').val();
        // Create a marker for each place.
        var latLang = new google.maps.LatLng(lat,lng);
        var bounds = new google.maps.LatLngBounds();
        markers.push(new google.maps.Marker({
          map: map,
          // icon: icon,
          title: input.value,
          position: latLang
        }));
        map.panTo(latLang);
        // bounds.extend(latLang);
        // map.fitBounds(bounds);

    }
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(map, 'click', function(event) {
        // placeMarker(event.latLng, map);
        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];
        // add a new marker
        markers.push(new google.maps.Marker({
          map: map,
          position: event.latLng
        }));
        console.log(event.latLng.lat());
        $('#lat_input').val(event.latLng.lat());
        $('#lng_input').val(event.latLng.lng());

        //get location from lat,long
        geocoder.geocode({
          'latLng': event.latLng
        }, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $('#map_address').val(results[0].formatted_address);
            }
          }
        });

    });
    $('.clear_marker').click(function(e){
      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];
      $('#lat_input').val('');
      $('#lng_input').val('');
      $('#map_address').val('');
    });
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        // if (!place.geometry) {
        //   console.log("Returned place contains no geometry");
        //   return;
        // }
        
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
}
function copyMapLink(){
  var url = 'https://www.google.com/maps/?q=loc:' + $('#lat_input').val() + '+' + $('#lng_input').val();
  window.prompt("Copy to clipboard: Ctrl+C, Enter", url);
};
function placeMarker(position, map) {
  var marker = new google.maps.Marker({
    position: position,
    map: map
  });  
  map.panTo(position);
}

//maps end