<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/admin', 'HomeController@index')->name('home');
//Route::get('/admin', function () {
////    return view('welcome');
//    return redirect('admin/');
//});
//Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm');
//Route::post('admin/login', 'Admin\Auth\LoginController@login');
//Route::get('admin/logout', 'Admin\Auth\LoginController@logout');



Route::get('/dashboard', 'Admin\DashboardController@index')->name('home');

Route::get('issued_tickets/genbarcodeofticket/{id}', 'Admin\IssuedTicketController@genbarcodeofticket')->name('admin.issued_tickets.genbarcodeofticket');

// Route::group(['prefix' => 'admin','as' => 'admin.','namespace' => 'Admin', 'middleware' => []], function () {
//     Route::get('issued_tickets/genbarcodeofticket/{id}', 'IssuedTicketController@genbarcodeofticket')->name('issued_tickets.genbarcodeofticket');

// });

Route::group(['prefix' => 'admin','as' => 'admin.','namespace' => 'Admin', 'middleware' => ['auth','check_permissions']], function () {

    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/', 'DashboardController@index');
    // Additional routes for users
	Route::post('users/bulkshow', ['as'=>'users.bulkshow','uses'=>'UsersController@bulkshow']);
    Route::post('users/bulkhide', ['as'=>'users.bulkhide','uses'=>'UsersController@bulkhide']);
    Route::post('users/bulkdelete', ['as'=>'users.bulkdelete','uses'=>'UsersController@bulkdelete']);
    Route::post('users/datatable', ['as'=>'users.datatable','uses'=>'UsersController@datatable']);

	// Additional routes for roles
    Route::post('roles/bulkshow',  ['as'=>'roles.bulkshow','uses'=>'RoleController@bulkshow']);
    Route::post('roles/bulkhide',  ['as'=>'roles.bulkhide','uses'=>'RoleController@bulkhide']);
    Route::post('roles/bulkdelete', ['as'=>'roles.bulkdelete','uses'=>'RoleController@bulkdelete']);
    Route::post('roles/datatable',  ['as'=>'roles.datatable','uses'=>'RoleController@datatable']);

    // Additional routes for events
    Route::match(['GET', 'PUT'], "events/{event_id}/addeditevent", ['as' => 'events.addeditevent', 'uses' => 'EventController@addeditevent']);
    Route::post('events/bulkshow',  ['as'=>'events.bulkshow','uses'=>'EventController@bulkshow']);
    Route::post('events/bulkhide',  ['as'=>'events.bulkhide','uses'=>'EventController@bulkhide']);
    Route::post('events/bulkdelete', ['as'=>'events.bulkdelete','uses'=>'EventController@bulkdelete']);
    Route::post('events/datatable',  ['as'=>'events.datatable','uses'=>'EventController@datatable']);
    Route::post('events/{event_id}/status',  ['as'=>'events.status','uses'=>'EventController@status']);
    Route::post('events/{event_id}/selling',  ['as'=>'events.selling','uses'=>'EventController@selling']);
    Route::post('events/checkIfEventHasRunningTicket', 'EventController@checkIfEventHasRunningTicket');
    Route::post('events/checkIfEventHasRequestOrTicket', 'EventController@checkIfEventHasRequestOrTicket');
    Route::post('events/checkIfTierHasRequestOrTicket', 'EventController@checkIfTierHasRequestOrTicket');

    // Additional routes for Documents
    Route::post('documents/bulkshow', 'DocumentController@bulkshow');
    Route::post('documents/bulkhide', 'DocumentController@bulkhide');
    Route::post('documents/bulkdelete', 'DocumentController@bulkdelete');
    Route::post('documents/datatable', 'DocumentController@datatable');


    ////////// by iqbal ////////////////////

    // Additional routes for events
   // Route::match(['GET', 'PUT'], "event_requests/{event_request_id}/addediteventrequest", ['as' => 'event_requests.addediteventrequest', 'uses' => 'EventRequestController@addediteventrequest']);
   // Route::post('event_requests/bulkshow',  ['as'=>'events.bulkshow','uses'=>'EventRequestController@bulkshow']);
   // Route::post('event_requests/bulkhide',  ['as'=>'events.bulkhide','uses'=>'EventRequestController@bulkhide']);

    Route::post('event_requests/bulkreject', ['as'=>'event_requests.bulkreject','uses'=>'EventRequestController@bulkreject']);
    Route::post('event_requests/bulkdelete', ['as'=>'event_requests.bulkdelete','uses'=>'EventRequestController@bulkdelete']);

    Route::post('event_requests/datatable',  ['as'=>'events.datatable','uses'=>'EventRequestController@datatable']);

    Route::post('event_requests/getreasonbytype', 'EventRequestController@getreasonbytype');
    Route::post('event_requests/getdescriptionbyreason', 'EventRequestController@getdescriptionbyreason');




    Route::post('event_requests/{event_request_id}/pending_status',  ['as'=>'event_requests.pending_status','uses'=>'EventRequestController@pending_status']);




    Route::post('event_requests/{event_request_id}/issue_status',  ['as'=>'event_requests.issue_status','uses'=>'EventRequestController@issue_status']);
    Route::post('event_requests/{event_request_id}/reject_status',  ['as'=>'event_requests.reject_status','uses'=>'EventRequestController@reject_status']);
    Route::post('event_requests/{event_request_id}/selling',  ['as'=>'event_requests.selling','uses'=>'EventRequestController@selling']);


    Route::post('issued_tickets/bulkreject', ['as'=>'issued_tickets.bulkreject','uses'=>'IssuedTicketController@bulkreject']);
    Route::post('issued_tickets/getreasonbytype', 'IssuedTicketController@getreasonbytype');
    Route::post('issued_tickets/getdescriptionbyreason', 'IssuedTicketController@getdescriptionbyreason');
    Route::post('issued_tickets/datatable',  ['as'=>'events.datatable','uses'=>'IssuedTicketController@datatable']);

    Route::post('issued_tickets/bulkissuedticketsdelete',  ['as'=>'events.bulkissuedticketsdelete','uses'=>'IssuedTicketController@bulkdelete']);
    //////////// end by iqbal //////////////






    //Additional routes for additional images
    Route::post('additionalimages/datatable', ['as'=>'additionalimages.datatable','uses'=>'AdditionalImageController@datatable']);
    Route::post('additionalimages/uploadimage', ['as'=>'additionalimages.uploadimage','uses'=>'AdditionalImageController@uploadimage']);
    Route::post('additionalimages/delimage', ['as'=>'additionalimages.delimage','uses'=>'AdditionalImageController@delimage']);
    Route::post('additionalimages/editcaption', ['as'=>'additionalimages.editcaption','uses'=>'AdditionalImageController@editcaption']);
    Route::post('additionalimages/bulkimagedelete',['as'=>'additionalimages.bulkimagedelete','uses'=>'AdditionalImageController@bulkimagedelete']);
    Route::post('additionalimages/reorder', ['as'=>'additionalimages.reorder','uses'=>'AdditionalImageController@reorder']);

    //Additional routes for tiers

    Route::post('tiers/datatable', ['as'=>'tiers.datatable','uses'=>'TierController@datatable']);
    Route::post('tiers/addtier', ['as'=>'tiers.addtier','uses'=>'TierController@addtier']);
    Route::post('tiers/deltier', ['as'=>'tiers.deltier','uses'=>'TierController@deltier']);
    Route::post('tiers/edittier', ['as'=>'tiers.edittier','uses'=>'TierController@edittier']);
    Route::post('tiers/bulkdelete',['as'=>'tiers.bulkdelete','uses'=>'TierController@bulkdelete']);
    Route::post('tiers/reorder', ['as'=>'tiers.reorder','uses'=>'TierController@reorder']);


    // Additional routes for reasons
    Route::post('reasons/bulkshow',  ['as'=>'reasons.bulkshow','uses'=>'ReasonController@bulkshow']);
    Route::post('reasons/bulkhide',  ['as'=>'reasons.bulkhide','uses'=>'ReasonController@bulkhide']);
    Route::post('reasons/bulkdelete', ['as'=>'reasons.bulkdelete','uses'=>'ReasonController@bulkdelete']);
    Route::post('reasons/datatable',  ['as'=>'reasons.datatable','uses'=>'ReasonController@datatable']);

    // resource routes
    Route::resource('tiers', 'TierController');
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UsersController');
    Route::resource('events', 'EventController');
    Route::resource('settings', 'SettingController');
    Route::resource('documents', 'DocumentController');

    //by iqbal
   // Route::get('event_requests/viewtickets/{event_id}', 'EventRequestController@viewtickets')->name('event_requests.viewtickets');
    Route::get('event_requests/{event_id}', 'EventRequestController@index')->name('event_requests');
    Route::resource('event_requests', 'EventRequestController');

    Route::post('issued_tickets/datatableCancelledTicket',  ['as'=>'events.datatable','uses'=>'IssuedTicketController@datatableCancelledTicket']);

    Route::post('issued_tickets/{issued_ticket_id}/cancelled_status',  ['as'=>'issued_tickets.cancelled_status','uses'=>'IssuedTicketController@cancelled_status']);
    Route::post('issued_tickets/{issued_ticket_id}/refunded_status',  ['as'=>'issued_tickets.refunded_status','uses'=>'IssuedTicketController@refunded_status']);
    Route::post('issued_tickets/{issued_ticket_id}/reject_status',  ['as'=>'issued_tickets.reject_status','uses'=>'IssuedTicketController@reject_status']);
//   Route::get('issued_tickets/genbarcodeofticket/{id}', 'IssuedTicketController@genbarcodeofticket')->name('issued_tickets.genbarcodeofticket');
    Route::get('issued_tickets/viewissuedticket/{id}', 'IssuedTicketController@viewissuedticket')->name('issued_tickets.viewissuedticket');
    Route::get('issued_tickets/resend_ticket/{id}', 'IssuedTicketController@resend_ticket')->name('issued_tickets.resend_ticket');
    Route::get('issued_tickets/cancelledticket/{id}', 'IssuedTicketController@cancelledticket')->name('issued_tickets.cancelledticket');
    Route::post('issued_tickets/store/{id}', 'IssuedTicketController@store')->name('issued_tickets.store');
    Route::get('issued_tickets/create/{id}', 'IssuedTicketController@create')->name('issued_tickets.create');
    Route::get('issued_tickets/{id}', 'IssuedTicketController@index')->name('issued_tickets');
    Route::resource('issued_tickets', 'IssuedTicketController', ['except' => ['create', 'store', 'update']]);

    // end by iqblal
    Route::resource('reasons', 'ReasonController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('additionalimages', 'AdditionalImageController');
   // Auth::routes();
});

Route::get('redemption/login', ['uses' => 'Redemption\Auth\LoginController@showLoginForm']);
Route::post('redemption/login', ['uses' => 'Redemption\Auth\LoginController@login']);
Route::get('redemption/logout', ['uses' => 'Redemption\Auth\LoginController@logout']);


// public app routes
//Redemption
Route::get('redemption', 'Redemption\HomeController@index')->name('home');
Route::group(['namespace' => 'Redemption', 'prefix' => 'redemption'], function () {
    Route::get('events/{event_id}','EventController@index')->name('events');
    Route::post('events/{event_id}','EventController@index')->name('events');
    Route::get('/', 'HomeController@index');
    Route::post('events/{event_id}/redeem',  ['as'=>'redemption.ticket.redeem','uses'=>'EventController@redeem']);
   // Auth::routes();
});




Route::group(['as' => 'site.','namespace' => 'Site'], function () {
    Route::get('/',['as'=>'events.index','uses'=>'EventController@index']);
    Route::get('events', ['as'=>'events.index','uses'=>'EventController@index']);
    Route::get('events/{event_id}/detail', ['as'=>'events.detail','uses'=>'EventController@detail']);
    Route::get('events/{event_id}/tickets', ['as'=>'events.tickets','uses'=>'EventController@tickets']);
    Route::get('events/{event_id}/tickets/review', ['as'=>'events.tickets.review','uses'=>'EventController@tickets_review']);

    Route::get('events/{event_id}/tickets/review/confirmation', ['as'=>'events.tickets.review.confirmation','uses'=>'EventController@confirmation']);

    //Route for tnc
    Route::get('tnc',  ['as'=>'events.tnc','uses'=>'EventController@tnc']);
});
Route::post('events/store/{event_id}', 'Site\EventController@store')->name('events.store');
Route::post('events/eventSearchOnFrontend', 'Site\EventController@eventSearchOnFrontend');
Route::post('events/loadEventDetail', 'Site\EventController@loadEventDetail');